﻿using System.Collections.Generic;
using System.Drawing;

namespace PrsAnalyzer.Const {
    public static class Colors {

        /// <summary>
        /// 画面コントロールの背景色
        /// </summary>
        public static Color _BACK_COLOR_FROM = Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(220)))), ((int)(((byte)(241)))));
        public static Color _BACK_COLOR_GROUP = Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(237)))), ((int)(((byte)(248)))));
        public static Color _BACK_COLOR_BTN_OFF_ENTER = Color.RoyalBlue;
        public static Color _BACK_COLOR_BTN_ON_ENTER = Color.Cyan;
        public static Color _BACK_COLOR_BTN_READ_ONLY = Color.LightGray;
        public static Color _BACK_COLOR_BTN_UNABLED = Color.Gray;

        /// <summary>
        /// グリッド編集時のセル背景色
        /// </summary>
        public static Color _BACK_COLOR_CELL_EDITED = Color.Yellow;
        public static Color _BACK_COLOR_ERROR = Color.Red;

        /// <summary>
        /// TimeTracker背景色
        /// </summary>
        public static Color _BACK_COLOR_TIME_TRACKER_A = Color.LightPink;
        public static Color _BACK_COLOR_TIME_TRACKER_B = Color.MistyRose;
        public static Color _BACK_COLOR_TIME_TRACKER_C = Color.BurlyWood;
        public static Color _BACK_COLOR_TIME_TRACKER_E = Color.Khaki;
        public static Color _BACK_COLOR_TIME_TRACKER_G = Color.LemonChiffon;
        public static Color _BACK_COLOR_TIME_TRACKER_H = Color.YellowGreen;
        public static Color _BACK_COLOR_TIME_TRACKER_L = Color.LightCyan;
        public static Color _BACK_COLOR_TIME_TRACKER_M = Color.Aqua;
        public static Color _BACK_COLOR_TIME_TRACKER_N = Color.LightSkyBlue;
        public static Color _BACK_COLOR_TIME_TRACKER_T = Color.LightSteelBlue;
        public static Color _BACK_COLOR_TIME_TRACKER_X = Color.Thistle;
        public static Color _BACK_COLOR_TIME_TRACKER_Y = Color.Yellow;

        /// <summary>
        /// 文字色
        /// </summary>
        public static Color _FORE_COLOR_CLICK = SystemColors.ControlText;
        public static Color _FORE_COLOR_Y = Color.Red;

        /// <summary>
        /// TimeTracker背景色リスト
        /// </summary>
        public static readonly Dictionary<string, Color> _DIC_TIME_TRACKER_COLOR = new Dictionary<string, Color>() {
            { ProjectConst.GyomuKbn.A.ToString(), _BACK_COLOR_TIME_TRACKER_A },
            { ProjectConst.GyomuKbn.B.ToString(), _BACK_COLOR_TIME_TRACKER_B },
            { ProjectConst.GyomuKbn.C.ToString(), _BACK_COLOR_TIME_TRACKER_C },
            { ProjectConst.GyomuKbn.E.ToString(), _BACK_COLOR_TIME_TRACKER_E },
            { ProjectConst.GyomuKbn.G.ToString(), _BACK_COLOR_TIME_TRACKER_G },
            { ProjectConst.GyomuKbn.H.ToString(), _BACK_COLOR_TIME_TRACKER_H },
            { ProjectConst.GyomuKbn.L.ToString(), _BACK_COLOR_TIME_TRACKER_L },
            { ProjectConst.GyomuKbn.M.ToString(), _BACK_COLOR_TIME_TRACKER_M },
            { ProjectConst.GyomuKbn.N.ToString(), _BACK_COLOR_TIME_TRACKER_N },
            { ProjectConst.GyomuKbn.T.ToString(), _BACK_COLOR_TIME_TRACKER_T },
            { ProjectConst.GyomuKbn.X.ToString(), _BACK_COLOR_TIME_TRACKER_X },
            { ProjectConst.GyomuKbn.Y.ToString(), _BACK_COLOR_TIME_TRACKER_Y },
            { "ERR", _BACK_COLOR_ERROR },
        };
    }
}
