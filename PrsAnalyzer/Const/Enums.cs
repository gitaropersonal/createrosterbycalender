﻿using System.Collections.Generic;

namespace PrsAnalyzer.Const {
    public static class Enums {
        /// <summary>
        /// ログイン権限区分
        /// </summary>
        public enum AuthorityKbn {
            None = -1,
            Normal = 1,
            Admin = 10,
        }
        /// <summary>
        /// 担当区分
        /// </summary>
        public enum TantoKbn {
            None = -1,
            Naiko = 0,
            Inin = 1,
            Haken = 2,
        }
        /// <summary>
        /// 役職区分
        /// </summary>
        public enum YakushokuKbn {
            None = -1,
            Tanto = 0,
            Kacho = 1,
            Bucho = 2,
            CEO = 10,
        }
        /// <summary>
        /// グリッドのセル編集区分
        /// </summary>
        public enum EditStatus {
            NONE = 0,
            SHOW = 1,
            INSERT = 10,
            UPDATE = 20,
        }
        /// <summary>
        /// 作成勤務表のタイプ
        /// </summary>
        public enum CreateKinmuhyoType {
            PRS = 0,
            KINMUHYO = 2,
        }
        /// <summary>
        /// 初期化モード
        /// </summary>
        public enum InitMode {
            All = 0,
            EnableCon = 10,
        }
        /// <summary>
        /// システムメニュ
        /// </summary>
        public enum SysType {
            PRS_JISSEKI = 1,
            PROJECT_CAPTURE = 2,
            MASTER = 3,
        }
        /// <summary>
        /// ログイン権限マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<AuthorityKbn, string>> _DIC_KENGEN_KBN = new Dictionary<AuthorityKbn, string>() {
            { AuthorityKbn.Normal, "一般"},
            { AuthorityKbn.Admin, "管理者"},
        };
        /// <summary>
        /// 担当区分マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<TantoKbn, string>> _DIC_TANTO_KBN = new Dictionary<TantoKbn, string>() {
            { TantoKbn.Naiko, "内工"},
            { TantoKbn.Inin , "委任"},
            { TantoKbn.Haken, "派遣"},
        };
        /// <summary>
        /// 役職区分マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<YakushokuKbn, string>> _DIC_YAKUSHOKU_KBN = new Dictionary<YakushokuKbn, string>() {
            { YakushokuKbn.Tanto, "一般"},
            { YakushokuKbn.Kacho, "課長"},
            { YakushokuKbn.Bucho, "部長"},
            { YakushokuKbn.CEO  , "CEO"},
        };

        /// <summary>
        /// 勤務表の名前辞書
        /// </summary>
        public static readonly Dictionary<CreateKinmuhyoType, string> DicKinmuhyoName = new Dictionary<CreateKinmuhyoType, string>() {
            { CreateKinmuhyoType.PRS,   "PRS" },
            { CreateKinmuhyoType.KINMUHYO, "勤務表" },
        };
    }
}
