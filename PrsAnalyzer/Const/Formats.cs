﻿namespace PrsAnalyzer.Const {
    public static class Formats {
        public const string _FORMAT_DATE_YYYYMMDD_SLASH          = "yyyy/MM/dd";
        public const string _FORMAT_DATE_YYYYMM                  = "yyyyMM";
        public const string _FORMAT_DATE_YYMM                    = "yyMM";
        public const string _FORMAT_DATE_YYYYMMDD                = "yyyyMMdd";
        public const string _FORMAT_DATE_YYYYMM_SLASH            = "yyyy/MM";
        public const string _FORMAT_DATE_YYYYM_NENTSUKI          = "yyyy年M月";
        public const string _FORMAT_DATE_YYYYMM_NENTSUKI         = "yyyy年MM月";
        public const string _FORMAT_DATE_YYYYMD_NENTSUKIHI       = "yyyy年M月d日";
        public const string _FORMAT_DATE_YYYY_NEN                = "yyyy年";
        public const string _FORMAT_DATE_YYMM_NENTSUKI           = "yy年MM月";
        public const string _FORMAT_DATE_MD_SLASH                = "M/d";
        public const string _FORMAT_DATE_M_TSUKI                 = "M月";
        public const string _FORMAT_PRE_ZERO_2                   = "0#";
        public const string _FORMAT_ZERO_2                       = "00";
        public const string _FORMAT_FLOAT_1                      = "#0.0";
        public const string _FORMAT_FLOAT_2                      = "#0.00";
        public const string _FORMAT_FLOAT_3                      = "#0.000";
        public const string _FORMAT_FLOAT_1_PCT                  = "#0.0%";
        public const string _FORMAT_FLOAT_2_HUMANMOON            = "#0.00人月";
        public const string _FORMAT_FLOAT_MONEY                  = "¥ ###,##0";
        public const string _FORMAT_FLOAT_MONEY_1000             = "###,##0.0";
        public const string _FORMAT_DATE_YYYYMMDDHHMMSS          = "yyyyMMddhhmmss";
        public const string _FORMAT_DATE_YYYYMMDDHHMM_SLASH      = "yyyy/MM/dd HH:mm";
        public const string _FORMAT_DATE_YYYYMMDDHHMMSS_SLASH    = "yyyy/MM/dd HH:mm:ss";
        public const string _FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH = "yyyy/MM/dd HH:mm:ss.FFF";
        public const string _FORMAT_TIME_HMM                     = "H:mm";
        public const string _FORMAT_TIME_HHMM                    = "HH:mm";
        public const string _FORMAT_CMB_BU_FORMAT                = "{0}----{1}";
        public const string _FORMAT_CMB_GROUP_FORMAT             = "{0}-{1}";
        public const string _ENCODING_SJIS                       = "Shift_JIS";
        public const string _ENCODING_UTF8                       = "UTF-8";
    }
}
