﻿namespace PrsAnalyzer.Const.Kinmuhyo {
    /// <summary>
    /// 勤務表Excelセル
    /// </summary>
    public static class CellsKinmuhyo {
        public const string _COL_WEEK_DAY             = "C";
        public const string _COL_HOLI_DAY             = "D";
        public const string _COL_WORK_STATUS          = "E";
        public const string _COL_AM_REST              = "F";
        public const string _COL_PM_REST              = "G";
        public const string _COL_SHUKKIN_HH           = "H";
        public const string _COL_SHUKKIN_MM           = "I";
        public const string _COL_TAIKIN_HH            = "J";
        public const string _COL_TAIKIN_MM            = "K";
        public const string _COL_JITSUDOU_HH          = "L";
        public const string _COL_JITSUDOU_MM          = "M";
        public const string _COL_KYUKEI_HH            = "N";
        public const string _COL_KYUKEI_MM            = "O";
        public const string _COL_ZANGYO_HH            = "P";
        public const string _COL_ZANGYO_MM            = "Q";
        public const string _COL_SHINYA_HH            = "R";
        public const string _COL_SHINYA_MM            = "S";
        public const string _COL_KYUJITSU_HH          = "T";
        public const string _COL_KYUJITSU_MM          = "U";
        public const string _COL_KADO_HH              = "X";
        public const string _COL_KADO_MM              = "Y";
        public const string _COL_MST_CHARGE_NO        = "AO";
        public const string _COL_MST_SAGYO_NAME_DISP  = "AR";
        public const string _COL_MST_SAGYO_NAME       = "BE";

        public const int _SHEET_HEADER_ROW_NUM               = 7;
        public const int _SHEET_HEADER_COL_NUM               = 1;
        public const int _WORK_DETAILS_MAX_WORK_NUM          = 10;
        public const int _WORK_DETAILS_WORK_COL_INTERVAL     = 3;
        public const int _WORK_DETAILS_DEFAULT_COL_INDEX     = 26;
        public const int _WORK_MASTER_BODY_START_ROW_INDEX   = 64;
        public const int _WORK_MASTER_CHARGE_NO_COL_INDEX    = 1;
        public const int _WORK_MASTER_WORK_NAME_COL_INDEX    = 4;
        public const int _WORK_MASTER_MAX_ROW_COUNT          = 18;
        public const int _CSV_EMPLOEE_COL_COUNT              = 22;
        public const int _CSV_BU_COL_COUNT                   = 3;
        public const int _CSV_GROUP_COL_COUNT                = 4;
        public const int _CSV_MHSC_COL_COUNT                 = 3;
        public const string _CELL_NAME_NAME                  = "I5";
        public const string _CELL_NAME_YYYY                  = "B5";
        public const string _CELL_NAME_MM                    = "E5";
        public const string _SHEET_KINMUHYO_NAME             = "作業報告書";
        public const string _MARK_STAR                       = "★";
        public const string _DEFAULT_KINMUHYO_NAME           = "作業時間外報告書.xlsx";
        public const string _FROMAR_TIME                     = "{0}:{1}";

        public const int _NIGHT_SHIFT_START_HH               = 21;
        public const int _NIGHT_SHIFT_END_HH                 = 9;
        public const int _KINMUHYO_HEADER_ROW_COUNT          = 7;
        public const int _NIGHT_SHIFT_KYUKEI_TIME            = 2;
        public const int _NIGHT_SHIFT_WARIMASHI_TIME         = 4;
        public const int _NIGHT_SHIFT_SHINYA_TIME            = 6;
        public const int _COL_FIRST_WORK_NAME_INDEX          = 26;
        public const int _COL_MAX_WORK_NAME_INDEX            = 55;
        public const int _ROW_IDX_MST_START                  = 64;
        public const int _ROW_IDX_MST_END                    = 81;
        public const int _ROW_IDX_HIDDEN_WORK_BUF            = 21;

        public const string _MARK_REST          = "休";
        public const string _MARK_PAID          = "有";
        public const string _MARK_FURIKAE       = "振";
        public const string _WORK_NAME_REST     = "休暇";
        public static string[] _WEEK_END_DAYS   = { "土", "日" };
        public static string _HOLIDAY_MARK      = "祝";
    }
}
