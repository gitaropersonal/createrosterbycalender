﻿namespace PrsAnalyzer.Const.Kinmuhyo {
    public static class CellsPRS {

        #region 勤務表シート
        // 行番号
        public const int _ROW_SYUTTAIKIN_START = 5;
        public const int _ROW_MEISAI_START     = 10;
        public const int _MAX_ROW_COUNT        = 400;

        // 列番号
        public const int _COL_NUM_HEADER_COL_START = 1;
        public const int _COL_NUM_HEADER_COL_END = 6;
        public const int _COL_KOSU_START = 8;
        public const int _COL_NUM_AL = 38;

        // セル
        public const string _CELL_NAME  = "B3";
        public const string _CELL_YYYY  = "G1";
        public const string _CELL_MM    = "I1";
        public const string _CELL_TOTAL = "G9";

        // 列
        public const string _COL_PROJECT_CODE = "A";
        public const string _COL_KOKYAKU_NAME = "B";
        public const string _COL_PROJECT_NAME = "C";
        public const string _COL_KOUTEI_NAME = "D";
        public const string _COL_DETAIL_NAME = "E";
        public const string _COL_SUM_KOSU = "F";
        public const string _COL_MEI_STRT = "G";
        public const string _COL_KOU_STRT = "H";
        public const string _COL_MEI__END = "AL";

        // フォント
        public const string _FONT_NAME_KOSU = "メイリオ";
        public const int _FONT_SIZE_KOSU = 8;

        // Excel関数
        public const string _FORMULA_SUM_TOTAL       = "=SUM(F10:F{0})";
        public const string _FORMULA_SUM_TOTAL_A_DAY = "=SUM({0}10:{0}{1})";
        public const string _FORMULA_SUM_KINTAI      = "=SUM(H{0}:AL{0})";

        // ファイル名
        public const string _FILE_PRS_NAME = "PRS.xlsx";
        #endregion

        #region 作業実績報告書（委任用）
        /// <summary>会社コード</summary>
        public const string _CELL_ININ_KAISHA_CODE = "C10";
        /// <summary>会社名</summary>
        public const string _CELL_ININ_KAISHA_NAME = "C11";
        /// <summary>派遣元責任者名</summary>
        public const string _CELL_ININ_SEKININ_NAME = "F11";
        /// <summary>連絡先TEL</summary>
        public const string _CELL_ININ_TEL = "G12";
        /// <summary>SB作業担当部署名</summary>
        public const string _CELL_ININ_TANTO_BUSHO = "A59";
        /// <summary>指揮命令者名</summary>
        public const string _CELL_ININ_SHIKI_MEIREI = "G59";
        #endregion

        #region 管理台帳（要員派遣）
        /// <summary>会社コード</summary>
        public const string _CELL_HAKEN_KAISHA_CODE = "C10";
        /// <summary>会社名</summary>
        public const string _CELL_HAKEN_KAISHA_NAME = "C11";
        /// <summary>派遣元責任者部署名/役職名</summary>
        public const string _CELL_HAKEN_BUSHO_YAKUSHOKU = "C13";
        /// <summary>派遣元責任者名</summary>
        public const string _CELL_HAKEN_SEKININ_NAME = "F11";
        /// <summary>連絡先TEL</summary>
        public const string _CELL_HAKEN_TEL = "G12";
        /// <summary>指揮命令者名</summary>
        public const string _CELL_HAKEN_SHIKI_MEIREI = "H59";
        #endregion

        // シート名
        public const string _SHEET_KINMUHYO = "勤務表";
        public const string _SHEET_PRS = "PRS";
        public const string _SHEET_KYUGYODATE = "祝日";
        public const string _SHEET_JISSEKI_HOKOKUSHO = "作業実績報告書（委任用）";
        public const string _SHEET_KANRIDAICHO = "管理台帳（要員派遣）";
    }
}
