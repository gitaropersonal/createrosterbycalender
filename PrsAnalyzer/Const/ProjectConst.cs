﻿using PrsAnalyzer.Entity;
using System.Collections.Generic;

namespace PrsAnalyzer.Const {
    public class ProjectConst {

        /// <summary>依頼元</summary>
        public const string _KOKYAKU_NAME_SYANAI = "社内";
        public const string _KOKYAKU_NAME_FUTOKUTEI = "不特定客先";

        /// <summary>特殊なプロジェクトコード</summary>
        public const string _PROJECT_CODE_AP_HOSYU    = "E2100001";
        public const string _PROJECT_CODE_MANAGEMENT  = "A3201001";
        public const string _PROJECT_CODE_SHANAI_MC   = "B3201001";
        public const string _PROJECT_CODE_TEACHING    = "C3201001";
        public const string _PROJECT_CODE_REST        = "X9999999";
        public const string _PROJECT_CODE_UNREDISTRED = "Y9999999";
        public const string _PROJECT_CODE_SUMMARY_ROW = "Z9999999";

        /// <summary>プロジェクト期間</summary>
        public const string _MIN_PROJECT_SPAN = "1900/01";
        public const string _MAX_PROJECT_SPAN = "9999/12";

        // <summary>プロジェクトコードの桁数</summary>
        public const int _PROJECT_CODE_LENGTH = 8;

        #region enum
        /// <summary>
        /// プロジェクト部署区分
        /// </summary>
        public enum PBushoKbn {
            NONE = -1,
            COMMON = 0,
            DEV = 1,
            FIELD_SUPPROT = 2,
        }
        /// <summary>
        /// 検収条件区分
        /// </summary>
        public enum KenshuJokenKbn {
            JISSEKI = 1,
            KOTEI = 2,
        }
        /// <summary>
        /// 業務区分
        /// </summary>
        public enum GyomuKbn{
            A = 1,
            B = 2,
            C = 3,
            E = 5,
            G = 7,
            H = 8,
            L = 12,
            M = 13,
            N = 14,
            T = 20,
            X = 24,
            Y = 25,
        }
        /// <summary>
        /// 顧客名区分
        /// </summary>
        public enum KokyakumeiKbn {
            NONE = 0,
            IRAIMOTO = 1,
            SYANAI = 9001,
            FUTOKUTEI = 9002,
        }
        #endregion

        #region Dictionary
        /// <summary>
        /// 検収条件リスト
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<KenshuJokenKbn, string>> _DIC_KENSHU_JOKEN = new Dictionary<KenshuJokenKbn, string>() {
            {KenshuJokenKbn.JISSEKI, "実績"},
            {KenshuJokenKbn.KOTEI, "固定"}
        };
        /// <summary>
        /// プロジェクト区分
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<PBushoKbn, string>> _DIC_PROJECT_KBN = new Dictionary<PBushoKbn, string>() {
            { PBushoKbn.COMMON, "共通"},
            { PBushoKbn.DEV, "開発部"},
            { PBushoKbn.FIELD_SUPPROT, "フィールドサポート部"},
        };
        /// <summary>
        /// 業務区分リスト
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<string, PBushoKbn>> _DIC_GYOMU_KBN = new Dictionary<string, PBushoKbn>() {
            {GyomuKbn.A.ToString(), PBushoKbn.COMMON},
            {GyomuKbn.B.ToString(), PBushoKbn.COMMON},
            {GyomuKbn.C.ToString(), PBushoKbn.COMMON},
            {GyomuKbn.E.ToString(), PBushoKbn.DEV},
            {GyomuKbn.G.ToString(), PBushoKbn.DEV},
            {GyomuKbn.H.ToString(), PBushoKbn.FIELD_SUPPROT},
            {GyomuKbn.L.ToString(), PBushoKbn.DEV},
            {GyomuKbn.M.ToString(), PBushoKbn.FIELD_SUPPROT},
            {GyomuKbn.N.ToString(), PBushoKbn.DEV},
            {GyomuKbn.T.ToString(), PBushoKbn.DEV},
            {GyomuKbn.X.ToString(), PBushoKbn.COMMON},
            {GyomuKbn.Y.ToString(), PBushoKbn.COMMON},
        };
        /// <summary>
        /// 特殊なプロジェクト・工程・詳細リスト
        /// </summary>
        public static readonly IEnumerable<MX05Detail> _LST_SPECIAL_DETAIL = new List<MX05Detail> {
            { new MX05Detail(_PROJECT_CODE_MANAGEMENT , 1, 1, "部／課の全体ミーティング") },
            { new MX05Detail(_PROJECT_CODE_MANAGEMENT , 1, 2, "PRS・週報の作成") },
            { new MX05Detail(_PROJECT_CODE_MANAGEMENT , 4, 1, "台風等による会社からの帰宅措置") },
            { new MX05Detail(_PROJECT_CODE_SHANAI_MC  , 1, 1, "SBのM/C関連の作業") },
            { new MX05Detail(_PROJECT_CODE_REST       , 1, 1, "有休") },
            { new MX05Detail(_PROJECT_CODE_REST       , 2, 1, "振休") },
            { new MX05Detail(_PROJECT_CODE_UNREDISTRED, 1, 1, string.Empty) },
        };
        #endregion
    }
}
