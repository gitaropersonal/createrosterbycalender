﻿using System.Collections.Generic;

namespace PrsAnalyzer.Const
{
    /// <summary>
    /// 時間
    /// </summary>
    public static class TimeConst {
        public enum RestType {
            NONE = 0,
            AM = 3,
            PM = 5,
            PAID = 10,
            FURIKAE = 11,
            OTHER = 12,
        }
        public static readonly IEnumerable<KeyValuePair<RestType, string>> DicRestType = new Dictionary<RestType, string> {
            { RestType.NONE         , "なし" },
            { RestType.AM           , "午前休" },
            { RestType.PM           , "午後休" },
            { RestType.PAID         , "有給休暇" },
            { RestType.FURIKAE      , "振替休暇" },
            { RestType.OTHER        , "その他" },
        };
        public const double _DEFAULT_WORK_TIME_PER_DAY = 7.5;
        public const int _PRS_WORKSTART_TIME_HH        = 7;
        public const int _PRS_WORKEND_HH               = 17;
        public const int _PRS_WORKEND_MM               = 30;

        public const int _DEFAULT_NOON_REST_START_HH   = 12;
        public const int _NIGHT_RESTSTART_HH           = 22;
        public const int _MAX_DATE_COUNT_AN_MONTH      = 31;
        public const int _MINUTE_AN_HOUR               = 60;
        public const int _HOURS_A_DAY                  = 24;
        public const int _DAY_COUNT_WEEK               = 7;
        public const int _MONTH_COUNT_PER_YEAR         = 12;
    }
}
