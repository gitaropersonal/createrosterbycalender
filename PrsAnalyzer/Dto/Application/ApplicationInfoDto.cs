﻿using PrsAnalyzer.Const;
using System.Windows.Forms;

namespace PrsAnalyzer.Dto {
    public class ApplicationInfoDto {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="edaNum"></param>
        /// <param name="appButton"></param>
        /// <param name="sysType"></param>
        /// <param name="defaultEnabled"></param>
        public ApplicationInfoDto(int edaNum, Button appButton, Enums.SysType sysType, bool defaultEnabled = true) {
            SystemType = sysType;
            EdaNum = edaNum;
            AppButton = appButton;
            DefaultEnabled = defaultEnabled;
        }
        public Enums.SysType SystemType {get;}
        public int EdaNum { get;}
        public Button AppButton { get;}
        public bool DefaultEnabled { get; }
    }
}
