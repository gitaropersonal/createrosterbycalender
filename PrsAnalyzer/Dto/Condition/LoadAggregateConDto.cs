﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class LoadAggregateConDto {
        public LoadAggregateConDto() {
            StrStartDt = string.Empty;
            StrEndDt = string.Empty;
            PBushoKbn = ProjectConst.PBushoKbn.NONE;
        }
        public string StrStartDt { get; set; }
        public string StrEndDt { get; set; }
        public float KijunTimeMonth { get; set; }
        public float WorkTimeDay { get; set; }
        public ProjectConst.PBushoKbn PBushoKbn {get; set;}
        public bool IsContainX { get; set; }
    }
}
