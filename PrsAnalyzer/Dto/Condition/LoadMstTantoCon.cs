﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Entity {
    public class LoadMstTantoCon {
        public LoadMstTantoCon() {
            TantoID = string.Empty;
            BushoCD = string.Empty;
            KaishaCD = string.Empty;
            AuthorityKbn = Enums.AuthorityKbn.None;
            TantoKbn = Enums.TantoKbn.None;
            YakushokuKbn = Enums.YakushokuKbn.None;
            IsContaiDeleted = false;
        }
        public string TantoID { get; set; }
        public string BushoCD { get; set; }
        public string KaishaCD { get; set; }
        public Enums.AuthorityKbn AuthorityKbn { get; set; }
        public Enums.TantoKbn TantoKbn { get; set; }
        public Enums.YakushokuKbn YakushokuKbn { get; set; }
        public bool IsContaiDeleted { get; set; }
    }
}
