﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class LoadProjectLooksConDto {
        public LoadProjectLooksConDto() {
            TantoID = string.Empty;
            TaishoYM = string.Empty;
            ProjectCD = string.Empty;
            ProjectName = string.Empty;
            GyomuKbn = string.Empty;
            KokyakuName = string.Empty;
            PBushoKbn = ProjectConst.PBushoKbn.NONE;
            KokyakuKbn = ProjectConst.KokyakumeiKbn.NONE;
        }
        public string TantoID { get; set; }
        public string TaishoYM { get; set; }
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string GyomuKbn { get; set; }
        public ProjectConst.PBushoKbn PBushoKbn { get; set; }
        public ProjectConst.KokyakumeiKbn KokyakuKbn { get; set; }
        public string KokyakuName { get; set; }
        public bool IsExistZengetsuJisseki { get; set; }
        public bool IsExistKongetsuJisseki { get; set; }
        public bool IsContainDeleted { get; set; }
        public bool IsContainX { get; set; }
        public bool IsDetailBrankOnly { get; set; }
    }
}
