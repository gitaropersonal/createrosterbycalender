﻿namespace PrsAnalyzer.Dto {
    public class AnkenExcelDto {
        public string Iraimoto { get; set; }
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string KokyakuName { get; set; }
        public string KouteiSeq { get; set; }
        public string KouteiName { get; set; }
        public string Kousu { get; set; }
        public string KenshuJoken { get; set; }
        public string Biko { get; set; }
    }
}
