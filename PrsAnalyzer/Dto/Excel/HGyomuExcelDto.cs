﻿namespace PrsAnalyzer.Dto {
    public class HGyomuExcelDto {
        public string ProjectCD { get; set; }
        public string HBushoName { get; set; }
        public string KokyakuName { get; set; }
        public string SagyoNaiyo { get; set; }
        public string STantoName { get; set; }
        public string ETantoName { get; set; }
    }
}
