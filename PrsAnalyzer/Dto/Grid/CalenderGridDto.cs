﻿namespace PrsAnalyzer.Dto {
    public class CalenderGridDto {
        public CalenderGridDto(){
            MstRowNum_SunDay = -1;
            MstRowNum_Monday = -1;
            MstRowNum_TuesDay = -1;
            MstRowNum_WednesDay = -1;
            MstRowNum_TursDay = -1;
            MstRowNum_FriDay = -1;
            MstRowNum_SaturDay = -1;
        }
        public string SunDay { get; set; }
        public string Monday { get; set; }
        public string TuesDay { get; set; }
        public string WednesDay { get; set; }
        public string TursDay { get; set; }
        public string FriDay { get; set; }
        public string SaturDay { get; set; }
        public int MstRowNum_SunDay { get; set; }
        public int MstRowNum_Monday { get; set; }
        public int MstRowNum_TuesDay { get; set; }
        public int MstRowNum_WednesDay { get; set; }
        public int MstRowNum_TursDay { get; set; }
        public int MstRowNum_FriDay { get; set; }
        public int MstRowNum_SaturDay { get; set; }
        public string StartTime { get; set; }
    }
}
