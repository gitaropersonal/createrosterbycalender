﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class InputAnkenExcelGridDto : AnkenExcelDto {
        public InputAnkenExcelGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public Enums.EditStatus Status { get; set; }
    }
}
