﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class MstBushoGridDto {
        public MstBushoGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public string BushoCD { get; set; }
        public string BushoName { get; set; }
        public string PBushoKbn { get; set; }
        public string PBushoKbnName { get; set; }
        public bool DelFlg { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
