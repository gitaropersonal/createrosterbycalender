﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class MstEmploeeGridDto {
        public MstEmploeeGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public int AuthorityKbn { get; set; }
        public int TantoKbn { get; set; }
        public int YakushokuKbn { get; set; }
        public string EmploeeId { get; set; }
        public string EmploeeName { get; set; }
        public string BushoCD { get; set; }
        public string BushoName { get; set; }
        public string KaishaCD { get; set; }
        public string KaishaName { get; set; }
        public string AuthorityKbnName { get; set; }
        public string TantoKbnName { get; set; }
        public string YakushokuKbnName { get; set; }
        public string MailAddress { get; set; }
        public bool DelFlg { get; set; }
        public Enums.EditStatus Status { get; set; }
}
}
