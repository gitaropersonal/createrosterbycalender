﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class MstKouteiGridDto {
        public MstKouteiGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public string PBushoKbn { get; set; }
        public string KouteiSeq { get; set; }
        public string KokyakuName { get; set; }
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string PBushoKbnName { get; set; }
        public string KouteiName { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
