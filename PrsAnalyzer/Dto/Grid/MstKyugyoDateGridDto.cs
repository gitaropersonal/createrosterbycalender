﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class MstKyugyoDateGridDto {
        public MstKyugyoDateGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public string KyugyoYMD { get; set; }
        public string KyugyoName { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
