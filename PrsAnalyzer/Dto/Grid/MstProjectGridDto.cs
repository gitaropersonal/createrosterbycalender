﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Entity {
    public class MstProjectGridDto {
        public MstProjectGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string KokyakuName { get; set; }
        public string PBushoKbn { get; set; }
        public string PBushoKbnName { get; set; }
        public string StartYM_Disp { get; set; }
        public string EndYM_Disp { get; set; }
        public string StartYM { get; set; }
        public string EndYM { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
