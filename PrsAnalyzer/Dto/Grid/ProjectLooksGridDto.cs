﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class ProjectLooksGridDto {
        public ProjectLooksGridDto() {
            Status = Enums.EditStatus.NONE;
        }
        public string KouteiSeq { get; set; }
        public string DetailSeq { get; set; }
        public string Iraimoto { get; set; }
        public string KokyakuName { get; set; }
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string KouteiName { get; set; }
        public string Detail { get; set; }
        public string Biko { get; set; }
        public bool DelFlg { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
