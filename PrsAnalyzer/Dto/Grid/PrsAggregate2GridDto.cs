﻿namespace PrsAnalyzer.Dto {
    public class PrsAggregate2GridDto {
        public string ProjectCD { get; set; }
        public string KokyakuName { get; set; }
        public string ProjectCD_Disp { get; set; }
        public string ProjectName { get; set; }
        public string KouteiName { get; set; }
        public string TantoName { get; set; }
        public string WorkTimeH { get; set; }
        public string KousuNinNichi { get; set; }
        public string Kousu { get; set; }
    }
}
