﻿namespace PrsAnalyzer.Dto {
    public class SagyoNaiyoLooksGridDto {

        public int RowNum { get; set; }
        public string TaishoYM_Disp { get; set; }
        public string TaishoYM { get; set; }
        public string KokyakuName { get; set; }
        public string ProjectCD { get; set; }
        public string KouteiSeq { get; set; }
        public string DetailSeq { get; set; }
        public string ProjectName { get; set; }
        public string KouteiName { get; set; }
        public string Detail { get; set; }
        public string SagyoNaiyoFull { get; set; }
    }
}
