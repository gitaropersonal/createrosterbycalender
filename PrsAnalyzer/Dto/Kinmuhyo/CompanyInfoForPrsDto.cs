﻿namespace PrsAnalyzer.Dto {
    public class KaishaInfoForPrsDto {
        public string KaishaCD { get; set; }
        public string KaishaName { get; set; }
        public string BushoYakushokuName { get; set; }
        public string SekininName { get; set; }
        public string TelNo { get; set; }
        public string BushoName { get; set; }
        public string ShikiMeireiTantoName { get; set; }
    }
}
