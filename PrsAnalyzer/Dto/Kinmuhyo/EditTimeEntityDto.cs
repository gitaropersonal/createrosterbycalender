﻿using PrsAnalyzer.Const;
using System;

namespace PrsAnalyzer.Dto {
    public class EditTimeEntryDto {
        public EditTimeEntryDto() {
            RestType = TimeConst.RestType.NONE;
        }
        /// <summary>
        /// ユーザーID
        /// </summary>
        public string UserId{ get; set; }
        /// <summary>
        /// ユーザー名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// プロジェクトコード
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// プロジェクト名
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        /// 工程
        /// </summary>
        public string KouteiName { get; set; }
        /// <summary>
        /// 詳細
        /// </summary>
        public string Detail { get; set; }
        /// <summary>
        /// 日付
        /// </summary>
        public string WorkDate { get; set; }
        /// <summary>
        /// 実績工数（日単位合計）
        /// </summary>
        public decimal TotalManHourDay { get; set; }
        /// <summary>
        /// 実績工数（作業名単位合計）
        /// </summary>
        public decimal TotalManHourSagyo { get; set; }
        /// <summary>
        /// 作業時間（h）
        /// </summary>
        public int ManHourH { get; set; }
        /// <summary>
        /// 作業時間（m）
        /// </summary>
        public int ManHourM { get; set; }
        /// <summary>
        /// 作業開始時刻
        /// </summary>
        public DateTime WorkStartTime { get; set; }
        /// <summary>
        /// 作業終了時刻
        /// </summary>
        public DateTime WorkEndTime { get; set; }
        /// <summary>
        /// 休暇
        /// </summary>
        public string RestMark { get; set; }
        /// <summary>
        /// 休憩時間
        /// </summary>
        public decimal RestTime { get; set; }
        /// <summary>
        /// 残業時間
        /// </summary>
        public decimal ZangyoTime { get; set; }
        /// <summary>
        /// 深夜残業
        /// </summary>
        public decimal ShinyaTime { get; set; }
        /// <summary>
        /// 休日タイプ
        /// </summary>
        public TimeConst.RestType RestType { get; set; }
    }
}
