﻿using PrsAnalyzer.Const;

namespace PrsAnalyzer.Dto {
    public class WorkTimeDto {
        public WorkTimeDto(){
            RestType = TimeConst.RestType.NONE;
        }
        public int Day { get; set; }
        public string WeekDay { get; set; }
        public string StartHH { get; set; }
        public string StartMM { get; set; }
        public string EndHH { get; set; }
        public string EndMM { get; set; }
        public string RestHH { get; set; }
        public string RestMM { get; set; }
        public string ZangyoHH { get; set; }
        public string ZangyoMM { get; set; }
        public string ShinyaHH { get; set; }
        public string ShinyaMM { get; set; }
        public string RestMark { get; set; }
        public decimal TotalWorkTime { get; set; }
        public TimeConst.RestType RestType { get; set; }
    }
}
