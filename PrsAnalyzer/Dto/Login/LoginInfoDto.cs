﻿namespace PrsAnalyzer.Dto {
    public class LoginInfoDto {
        public string TantoID { get; set; }
        public string TantoName { get; set; }
        public string BushoCD { get; set; }
        public int AuthorityKbn { get; set; }
        public int PBushoKbn { get; set; }
    }
}
