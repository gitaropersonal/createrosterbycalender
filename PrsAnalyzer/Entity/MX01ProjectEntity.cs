﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX01ProjectEntity {
        public string ProjectCD { get; set; }
        public string ProjectName { get; set; }
        public string KokyakuName { get; set; }
        public int PBushoKbn { get; set; }
        public string StartYM { get; set; }
        public string EndYM { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
