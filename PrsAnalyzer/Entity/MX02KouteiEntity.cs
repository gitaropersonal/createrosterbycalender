﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX02KouteiEntity {
        public string ProjectCD { get; set; }
        public int KouteiSeq { get; set; }
        public string KouteiName { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
