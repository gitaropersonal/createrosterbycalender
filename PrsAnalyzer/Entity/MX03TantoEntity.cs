﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX03TantoEntity {
        public string TantoID { get; set; }
        public string TantoName { get; set; }
        public string BushoCD { get; set; }
        public string BushoName { get; set; }
        public string KaishaCD { get; set; }
        public string KaishaName { get; set; }
        public string MailAddress { get; set; }
        public int AuthorityKbn { get; set; }
        public int TantoKbn { get; set; }
        public int YakushokuKbn { get; set; }
        public string DelFlg { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
