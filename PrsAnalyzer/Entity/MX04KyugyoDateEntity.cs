﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX04KyugyoDateEntity {
        public DateTime KyugyoYMD { get; set; }
        public string KyugyoName { get; set; }
    }
}
