﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX05Detail {
        public string TantoID { get; set; }
        public string ProjectCD { get; set; }
        public int KouteiSeq { get; set; }
        public int DetailSeq { get; set; }
        public string Detail { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MX05Detail() {
        }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="projectCd"></param>
        /// <param name="kouteiSeq"></param>
        /// <param name="detailSeq"></param>
        /// <param name="detail"></param>
        public MX05Detail(string projectCd, int kouteiSeq, int detailSeq, string detail) {
            this.ProjectCD = projectCd;
            this.KouteiSeq = kouteiSeq;
            this.DetailSeq = detailSeq;
            this.Detail = detail;
        }
    }
}
