﻿using PrsAnalyzer.Const;
using System;

namespace PrsAnalyzer.Entity {
    public class MX06BushoEntity {
        public MX06BushoEntity() {
            PBushoKbn = (int)ProjectConst.PBushoKbn.NONE;
        }
        public string BushoCD { get; set; }
        public string BushoName { get; set; }
        public int PBushoKbn { get; set; }
        public string DelFlg { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
