﻿using System;

namespace PrsAnalyzer.Entity {
    public class MX07KaishaEntity {
        public string KaishaCD { get; set; }
        public string KaishaName { get; set; }
        public string KaishaNameR { get; set; }
        public string KaishaNameKN { get; set; }
        public string KaishaKBN { get; set; }
        public string TankaKBNJ { get; set; }
        public string TankaKBNU { get; set; }
        public string SekininName { get; set; }
        public string SekininBusho { get; set; }
        public string SekininYaku { get; set; }
        public string TelNo { get; set; }
        public string HakenKyokaNo { get; set; }
        public string BankName { get; set; }
        public string StartYM { get; set; }
        public string EndYM { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }

    }
}
