﻿using System;

namespace PrsAnalyzer.Entity {
    public class PX02SagyoMeisaiEntity {
        public string TaishoYMD { get; set; }
        public string TantoID { get; set; }
        public DateTime StartTime { get; set; }
        public string ProjectCD { get; set; }
        public string KouteiSeq { get; set; }
        public string DetailSeq { get; set; }
        public DateTime EndTime { get; set; }
        public int SagyoTimeH { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
