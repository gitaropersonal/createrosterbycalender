﻿using System;

namespace PrsAnalyzer.Entity {
    public class PX03AnkenExcelEntity {
        public string TaishoYM { get; set; }
        public string ProjectCD { get; set; }
        public string KouteiSeq { get; set; }
        public int PBushoKbn { get; set; }
        public string Iraimoto { get; set; }
        public string Kousu { get; set; }
        public string KenshuJoken { get; set; }
        public string Biko { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
