﻿using System;

namespace PrsAnalyzer.Entity {
    public class VPX01ProjectLooksEntity {
        public string TaishoYM { get; set; }
        public string TantoID { get; set; }
        public string ProjectCD { get; set; }
        public string KouteiSeq { get; set; }
        public string DetailSeq { get; set; }
        public string PBushoKbn { get; set; }
        public string KokyakuName { get; set; }
        public string ProjectName { get; set; }
        public string KouteiName { get; set; }
        public string Iraimoto { get; set; }
        public string Detail { get; set; }
        public string Biko { get; set; }
        public string DelFlg { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
