﻿namespace PrsAnalyzer.Entity {
    public class WorkItemEntity {
        public string No { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string KouteiName { get; set; }
        public string Detail { get; set; }
        public string DtStart { get; set; }
        public string DtEnd { get; set; }
    }
}
