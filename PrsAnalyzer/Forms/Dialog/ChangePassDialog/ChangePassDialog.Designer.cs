﻿namespace PrsAnalyzer.Forms {
    partial class ChangePassDialog {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassDialog));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.txtNewPassWordRe = new System.Windows.Forms.TextBox();
            this.lblNewPassWordRe = new System.Windows.Forms.Label();
            this.txtNewPassWord = new System.Windows.Forms.TextBox();
            this.lblNewPassWord = new System.Windows.Forms.Label();
            this.txtOldPassWord = new System.Windows.Forms.TextBox();
            this.txtTantoId = new System.Windows.Forms.TextBox();
            this.lblPassWord = new System.Windows.Forms.Label();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblTantoID = new System.Windows.Forms.Label();
            this.pnlExecute.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.txtNewPassWordRe);
            this.pnlExecute.Controls.Add(this.lblNewPassWordRe);
            this.pnlExecute.Controls.Add(this.txtNewPassWord);
            this.pnlExecute.Controls.Add(this.lblNewPassWord);
            this.pnlExecute.Controls.Add(this.txtOldPassWord);
            this.pnlExecute.Controls.Add(this.txtTantoId);
            this.pnlExecute.Controls.Add(this.lblPassWord);
            this.pnlExecute.Controls.Add(this.pnlFootor);
            this.pnlExecute.Controls.Add(this.lblTantoID);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(280, 307);
            this.pnlExecute.TabIndex = 0;
            // 
            // txtNewPassWordRe
            // 
            this.txtNewPassWordRe.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNewPassWordRe.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtNewPassWordRe.Location = new System.Drawing.Point(51, 191);
            this.txtNewPassWordRe.MaxLength = 16;
            this.txtNewPassWordRe.Name = "txtNewPassWordRe";
            this.txtNewPassWordRe.Size = new System.Drawing.Size(179, 24);
            this.txtNewPassWordRe.TabIndex = 3;
            // 
            // lblNewPassWordRe
            // 
            this.lblNewPassWordRe.AutoSize = true;
            this.lblNewPassWordRe.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNewPassWordRe.Location = new System.Drawing.Point(48, 175);
            this.lblNewPassWordRe.Name = "lblNewPassWordRe";
            this.lblNewPassWordRe.Size = new System.Drawing.Size(157, 17);
            this.lblNewPassWordRe.TabIndex = 238;
            this.lblNewPassWordRe.Text = "新しいパスワード（再入力）";
            // 
            // txtNewPassWord
            // 
            this.txtNewPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNewPassWord.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtNewPassWord.Location = new System.Drawing.Point(51, 140);
            this.txtNewPassWord.MaxLength = 16;
            this.txtNewPassWord.Name = "txtNewPassWord";
            this.txtNewPassWord.Size = new System.Drawing.Size(179, 24);
            this.txtNewPassWord.TabIndex = 2;
            // 
            // lblNewPassWord
            // 
            this.lblNewPassWord.AutoSize = true;
            this.lblNewPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNewPassWord.Location = new System.Drawing.Point(48, 124);
            this.lblNewPassWord.Name = "lblNewPassWord";
            this.lblNewPassWord.Size = new System.Drawing.Size(183, 17);
            this.lblNewPassWord.TabIndex = 236;
            this.lblNewPassWord.Text = "新しいパスワード（半角英数字）";
            // 
            // txtOldPassWord
            // 
            this.txtOldPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOldPassWord.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtOldPassWord.Location = new System.Drawing.Point(51, 89);
            this.txtOldPassWord.MaxLength = 16;
            this.txtOldPassWord.Name = "txtOldPassWord";
            this.txtOldPassWord.Size = new System.Drawing.Size(179, 24);
            this.txtOldPassWord.TabIndex = 1;
            // 
            // txtTantoId
            // 
            this.txtTantoId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoId.Location = new System.Drawing.Point(51, 40);
            this.txtTantoId.MaxLength = 6;
            this.txtTantoId.Name = "txtTantoId";
            this.txtTantoId.Size = new System.Drawing.Size(179, 24);
            this.txtTantoId.TabIndex = 0;
            // 
            // lblPassWord
            // 
            this.lblPassWord.AutoSize = true;
            this.lblPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPassWord.Location = new System.Drawing.Point(48, 73);
            this.lblPassWord.Name = "lblPassWord";
            this.lblPassWord.Size = new System.Drawing.Size(187, 17);
            this.lblPassWord.TabIndex = 234;
            this.lblPassWord.Text = "現在のパスワード（半角英数字）";
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.btnCancel);
            this.pnlFootor.Controls.Add(this.btnOK);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 250);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(280, 57);
            this.pnlFootor.TabIndex = 200;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCancel.Location = new System.Drawing.Point(146, 17);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 28);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "キャンセル（X）";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnOK.Location = new System.Drawing.Point(30, 17);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(110, 28);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK（O）";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblTantoID
            // 
            this.lblTantoID.AutoSize = true;
            this.lblTantoID.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoID.Location = new System.Drawing.Point(48, 24);
            this.lblTantoID.Name = "lblTantoID";
            this.lblTantoID.Size = new System.Drawing.Size(62, 17);
            this.lblTantoID.TabIndex = 233;
            this.lblTantoID.Text = "担当者ID";
            // 
            // ChangePassDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 307);
            this.Controls.Add(this.pnlExecute);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 500);
            this.MinimumSize = new System.Drawing.Size(300, 200);
            this.Name = "ChangePassDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "パスワード変更";
            this.pnlExecute.ResumeLayout(false);
            this.pnlExecute.PerformLayout();
            this.pnlFootor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Label lblTantoID;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.Label lblPassWord;
        public System.Windows.Forms.TextBox txtOldPassWord;
        public System.Windows.Forms.TextBox txtTantoId;
        public System.Windows.Forms.TextBox txtNewPassWordRe;
        public System.Windows.Forms.Label lblNewPassWordRe;
        public System.Windows.Forms.TextBox txtNewPassWord;
        public System.Windows.Forms.Label lblNewPassWord;
        public System.Windows.Forms.Button btnCancel;
    }
}

