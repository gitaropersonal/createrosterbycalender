﻿using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System.Drawing;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class ChangePassDialogFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private static ChangePassDialog _Body;
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private static string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public ChangePassDialogFormLogic(ChangePassDialog body) {

            _Body = body;
            _Body.txtOldPassWord.PasswordChar = '*';
            _Body.txtNewPassWord.PasswordChar = '*';
            _Body.txtNewPassWordRe.PasswordChar = '*';

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.O:
                        _Body.btnOK.Focus();
                        _Body.btnOK.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnCancel.Focus();
                        _Body.btnCancel.PerformClick();
                        break;
                }
            };

            // OKボタン押下イベント
            _Body.btnOK.Click += (s, e) => {

                // OKボタン押下時Validate
                if (ValidateBtnOK()) {

                    // 確認ダイアログ
                    if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                        return;
                    }
                    // パスワードの更新
                    new LoginService().UpdLoginInfo(_DB_CONN_STRING, _Body.txtTantoId.Text, _Body.txtNewPassWord.Text);

                    // 更新完了メッセージ
                    DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
                    _Body.Close();
                }
            };
            // キャンセルボタン押下イベント
            _Body.btnCancel.Click += (s, e) => {

                _Body.Close();

            };
            // テキストボックスValidated
            _Body.txtTantoId.Validated += (s, e) => {
                _Body.txtTantoId.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtTantoId.Text);
            };
            // テキストボックスValidated
            _Body.txtOldPassWord.Validated += (s, e) => {
                _Body.txtOldPassWord.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtOldPassWord.Text);
            };
            // テキストボックスValidated
            _Body.txtNewPassWord.Validated += (s, e) => {
                _Body.txtNewPassWord.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtNewPassWord.Text);
            };
            // テキストボックスValidated
            _Body.txtNewPassWordRe.Validated += (s, e) => {
                _Body.txtNewPassWordRe.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtNewPassWordRe.Text);
            };
        }
        /// <summary>
        /// OKボタン押下時Validate
        /// </summary>
        private bool ValidateBtnOK() {
            _Body.txtTantoId.BackColor = Color.Empty;
            _Body.txtOldPassWord.BackColor = Color.Empty;
            _Body.txtNewPassWord.BackColor = Color.Empty;
            _Body.txtNewPassWordRe.BackColor = Color.Empty;

            // 空文字チェック（Proxy）
            if (!ValidateEmptyProxy()) {
                return false;
            }
            // パスワードの新旧チェック
            if (_Body.txtOldPassWord.Text == _Body.txtNewPassWord.Text) {
                _Body.txtNewPassWord.Focus();
                _Body.txtNewPassWord.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_CHANGE_PASS);
                return false;
            }
            // 新しいパスワードのマッチングチェック
            if (_Body.txtNewPassWord.Text != _Body.txtNewPassWordRe.Text) {
                _Body.txtNewPassWord.Focus();
                _Body.txtNewPassWord.BackColor = Colors._BACK_COLOR_ERROR;
                _Body.txtNewPassWordRe.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_MATCH_PASS);
                return false;
            }
            // ログイン情報取得
            var loginInfo = new LoginService().LoadLoginInfo(_DB_CONN_STRING, _Body.txtTantoId.Text, _Body.txtOldPassWord.Text);

            // 照合処理
            if (string.IsNullOrEmpty(loginInfo.TantoID)) {
                _Body.txtTantoId.Focus();
                _Body.txtTantoId.BackColor = Colors._BACK_COLOR_ERROR;
                _Body.txtOldPassWord.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_LOGIN_MISS);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 空文字チェック（Proxy）
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmptyProxy() {
            if (!ValidateEmpty(_Body.txtTantoId, _Body.lblTantoID)) {
                return false;
            }
            if (!ValidateEmpty(_Body.txtOldPassWord, _Body.lblPassWord)) {
                return false;
            }
            if (!ValidateEmpty(_Body.txtNewPassWord, _Body.lblNewPassWord)) {
                return false;
            }
            if (!ValidateEmpty(_Body.txtNewPassWordRe, _Body.lblNewPassWordRe)) {
                return false;
            }
            if (!ValidateEmpty(_Body.txtTantoId, _Body.lblTantoID)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 空文字チェック
        /// </summary>
        /// <param name="txtBox"></param>
        /// <param name="lbl"></param>
        /// <returns></returns>
        private bool ValidateEmpty(TextBox txtBox, Label lbl) {
            if (string.IsNullOrEmpty(txtBox.Text)) {
                txtBox.Focus();
                txtBox.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_EMPTY, lbl.Text));
                return false;
            }
            return true;
        }
    }
}
