﻿namespace PrsAnalyzer.Forms {
    partial class LoginDialogBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginDialogBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.txtPassWord = new System.Windows.Forms.TextBox();
            this.txtTantoId = new System.Windows.Forms.TextBox();
            this.lblPassWord = new System.Windows.Forms.Label();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.lnkChangePass = new System.Windows.Forms.LinkLabel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblTantoID = new System.Windows.Forms.Label();
            this.pnlExecute.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.txtPassWord);
            this.pnlExecute.Controls.Add(this.txtTantoId);
            this.pnlExecute.Controls.Add(this.lblPassWord);
            this.pnlExecute.Controls.Add(this.pnlFootor);
            this.pnlExecute.Controls.Add(this.lblTantoID);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(380, 157);
            this.pnlExecute.TabIndex = 0;
            // 
            // txtPassWord
            // 
            this.txtPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPassWord.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtPassWord.Location = new System.Drawing.Point(112, 54);
            this.txtPassWord.MaxLength = 16;
            this.txtPassWord.Name = "txtPassWord";
            this.txtPassWord.Size = new System.Drawing.Size(179, 24);
            this.txtPassWord.TabIndex = 1;
            // 
            // txtTantoId
            // 
            this.txtTantoId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoId.Location = new System.Drawing.Point(112, 29);
            this.txtTantoId.MaxLength = 6;
            this.txtTantoId.Name = "txtTantoId";
            this.txtTantoId.Size = new System.Drawing.Size(179, 24);
            this.txtTantoId.TabIndex = 0;
            // 
            // lblPassWord
            // 
            this.lblPassWord.AutoSize = true;
            this.lblPassWord.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPassWord.Location = new System.Drawing.Point(48, 57);
            this.lblPassWord.Name = "lblPassWord";
            this.lblPassWord.Size = new System.Drawing.Size(59, 17);
            this.lblPassWord.TabIndex = 234;
            this.lblPassWord.Text = "パスワード";
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.lnkChangePass);
            this.pnlFootor.Controls.Add(this.btnLogin);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 100);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(380, 57);
            this.pnlFootor.TabIndex = 200;
            // 
            // lnkChangePass
            // 
            this.lnkChangePass.AutoSize = true;
            this.lnkChangePass.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lnkChangePass.Location = new System.Drawing.Point(46, 10);
            this.lnkChangePass.Name = "lnkChangePass";
            this.lnkChangePass.Size = new System.Drawing.Size(119, 17);
            this.lnkChangePass.TabIndex = 100;
            this.lnkChangePass.TabStop = true;
            this.lnkChangePass.Text = "パスワード変更（P）";
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogin.Location = new System.Drawing.Point(268, 17);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(100, 28);
            this.btnLogin.TabIndex = 10;
            this.btnLogin.Text = "ログイン（O）";
            this.btnLogin.UseVisualStyleBackColor = true;
            // 
            // lblTantoID
            // 
            this.lblTantoID.AutoSize = true;
            this.lblTantoID.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoID.Location = new System.Drawing.Point(48, 32);
            this.lblTantoID.Name = "lblTantoID";
            this.lblTantoID.Size = new System.Drawing.Size(62, 17);
            this.lblTantoID.TabIndex = 233;
            this.lblTantoID.Text = "担当者ID";
            // 
            // LoginDialogBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 157);
            this.Controls.Add(this.pnlExecute);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 500);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "LoginDialogBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ログイン";
            this.pnlExecute.ResumeLayout(false);
            this.pnlExecute.PerformLayout();
            this.pnlFootor.ResumeLayout(false);
            this.pnlFootor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Label lblTantoID;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnLogin;
        public System.Windows.Forms.Label lblPassWord;
        public System.Windows.Forms.TextBox txtPassWord;
        public System.Windows.Forms.TextBox txtTantoId;
        public System.Windows.Forms.LinkLabel lnkChangePass;
    }
}

