﻿using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class LoginDialogFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private static LoginDialogBody _Body;
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private static string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public LoginDialogFormLogic(LoginDialogBody body) {

            _Body = body;
            _Body.txtPassWord.PasswordChar = '*';

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.P:
                        _Body.lnkChangePass.Focus();
                        LinkLabelEnter();
                        break;
                    case Keys.O:
                        _Body.btnLogin.Focus();
                        _Body.btnLogin.PerformClick();
                        break;
                }
            };

            // ログインボタン押下イベント
            _Body.btnLogin.Click += (s, e) => {

                // ログインボタン押下イベント
                BtnLogin_ClickEvent();

            };
            // リンクラベル押下イベント
            _Body.lnkChangePass.Click += (s, e) => {

                // リンクラベル押下イベント
                LinkLabelEnter();

            };
            // テキストボックスValidated
            _Body.txtTantoId.Validated += (s, e) => {
                _Body.txtTantoId.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtTantoId.Text);
            };
            // テキストボックスValidated
            _Body.txtPassWord.Validated += (s, e) => {
                _Body.txtPassWord.Text = StringUtil.CutHalfMarkCharsEx(_Body.txtPassWord.Text);
            };
        }
        /// <summary>
        /// ログインボタン押下イベント
        /// </summary>
        private void BtnLogin_ClickEvent() {
            _Body.txtTantoId.BackColor = Color.Empty;
            _Body.txtPassWord.BackColor = Color.Empty;

            if (string.IsNullOrEmpty(_Body.txtTantoId.Text)) {
                _Body.txtTantoId.Focus();
                _Body.txtTantoId.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_EMPTY, _Body.lblTantoID.Text));
                return;
            }
            if (string.IsNullOrEmpty(_Body.txtPassWord.Text)) {
                _Body.txtPassWord.Focus();
                _Body.txtPassWord.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_EMPTY, _Body.lblPassWord.Text));
                return;
            }
            try {
                // ログイン情報取得
                var loginInfo = new LoginService().LoadLoginInfo(_DB_CONN_STRING, _Body.txtTantoId.Text, _Body.txtPassWord.Text);

                // 照合処理
                if (string.IsNullOrEmpty(loginInfo.TantoID)) {
                    _Body.txtTantoId.Focus();
                    _Body.txtTantoId.BackColor = Colors._BACK_COLOR_ERROR;
                    _Body.txtPassWord.BackColor = Colors._BACK_COLOR_ERROR;
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_LOGIN_MISS);
                    return;
                }
                // 照合処理がOKなら、メニュー画面を開く
                _Body.Visible = false;
                if (new PAF0000_MainMenuBody(loginInfo).ShowDialog() == DialogResult.Cancel) {
                    _Body.Close();
                    return;
                }
                _Body.txtPassWord.Text = string.Empty;
                _Body.Visible = true;
                _Body.txtTantoId.Focus();
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// リンクラベル押下イベント
        /// </summary>
        private void LinkLabelEnter() {
            var dialog = new ChangePassDialog();
            _Body.Visible = false;
            new ChangePassDialog().ShowDialog();
            _Body.Visible = true;
        }
    }
}
