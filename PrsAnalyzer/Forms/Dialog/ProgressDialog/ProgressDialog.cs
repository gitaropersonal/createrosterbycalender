﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public partial class ProgressDialog : Form {
        private Label label1;
        public ProgressBar progressBar;

        /// <summary>
        /// 実行関数
        /// </summary>
        public Delegate Method { get; set; }
        public string ScreenId { get; set; }
        public string ScreenName { get; set; }
        public bool Success { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ProgressDialog() {
            InitializeComponent();
            Success = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.ControlBox = false;
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
            BackgroundWorker bgw = new BackgroundWorker();

            this.Load += (s, e) => {
                progressBar.Style = ProgressBarStyle.Marquee;

                bgw.WorkerSupportsCancellation = true;
                bgw.DoWork += bw_DoWork;

                // 処理実行
                bgw.RunWorkerAsync();
            };

            bgw.RunWorkerCompleted += (s, e) => {
                this.Close();
            };

        }

        /// <summary>
        /// バックグラウンド処理
        /// </summary>
        private void bw_DoWork(object sender, DoWorkEventArgs e) {

            try {
                // Formを閉じる
                Action EndForm = () => this.Close();

                Method.DynamicInvoke();
                // 処理が終了すると画面が閉じられる
                this.Invoke(EndForm);

                Success = true;
            }
            catch {
                // 例外はMethod内で処理する
                Success = false;
            }
        }

        public void Init() { }

        public void ToDefaultCursor() { }

        public void ToWaitCursor() { }
    }
}
