﻿namespace PrsAnalyzer.Forms {
    partial class PAF0000_MainMenuBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF0000_MainMenuBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.pnlUserApplication = new System.Windows.Forms.Panel();
            this.grpUserApplication = new System.Windows.Forms.GroupBox();
            this.pnlUserAppInner = new System.Windows.Forms.Panel();
            this.btnPAF2030 = new System.Windows.Forms.Button();
            this.btnPAF3020 = new System.Windows.Forms.Button();
            this.btnPAF14000_MstIraimoto = new System.Windows.Forms.Button();
            this.btnPAF2020 = new System.Windows.Forms.Button();
            this.btnPAF3050 = new System.Windows.Forms.Button();
            this.btnPAF3040 = new System.Windows.Forms.Button();
            this.btnPAF3030 = new System.Windows.Forms.Button();
            this.btnPAF1030 = new System.Windows.Forms.Button();
            this.btnPAF2010 = new System.Windows.Forms.Button();
            this.btnPAF1020 = new System.Windows.Forms.Button();
            this.btnPAF3010 = new System.Windows.Forms.Button();
            this.btnPAF1010 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grpSystemMenu = new System.Windows.Forms.GroupBox();
            this.pnlSysMenuInner = new System.Windows.Forms.Panel();
            this.btnSysMenu3 = new System.Windows.Forms.Button();
            this.btnSysMenu2 = new System.Windows.Forms.Button();
            this.btnSysMenu1 = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnLogOff = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLoginInfo = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblRevision = new System.Windows.Forms.Label();
            this.pnlExecute.SuspendLayout();
            this.pnlUserApplication.SuspendLayout();
            this.grpUserApplication.SuspendLayout();
            this.pnlUserAppInner.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpSystemMenu.SuspendLayout();
            this.pnlSysMenuInner.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.pnlUserApplication);
            this.pnlExecute.Controls.Add(this.panel2);
            this.pnlExecute.Controls.Add(this.pnlFootor);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Padding = new System.Windows.Forms.Padding(0, 42, 0, 0);
            this.pnlExecute.Size = new System.Drawing.Size(887, 576);
            this.pnlExecute.TabIndex = 0;
            // 
            // pnlUserApplication
            // 
            this.pnlUserApplication.AutoScroll = true;
            this.pnlUserApplication.Controls.Add(this.grpUserApplication);
            this.pnlUserApplication.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlUserApplication.Location = new System.Drawing.Point(332, 42);
            this.pnlUserApplication.Name = "pnlUserApplication";
            this.pnlUserApplication.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.pnlUserApplication.Size = new System.Drawing.Size(848, 475);
            this.pnlUserApplication.TabIndex = 100;
            // 
            // grpUserApplication
            // 
            this.grpUserApplication.Controls.Add(this.pnlUserAppInner);
            this.grpUserApplication.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpUserApplication.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpUserApplication.Location = new System.Drawing.Point(0, 0);
            this.grpUserApplication.Name = "grpUserApplication";
            this.grpUserApplication.Size = new System.Drawing.Size(838, 475);
            this.grpUserApplication.TabIndex = 100;
            this.grpUserApplication.TabStop = false;
            this.grpUserApplication.Text = "ユーザアプリケーション";
            // 
            // pnlUserAppInner
            // 
            this.pnlUserAppInner.AutoScroll = true;
            this.pnlUserAppInner.Controls.Add(this.btnPAF2030);
            this.pnlUserAppInner.Controls.Add(this.btnPAF3020);
            this.pnlUserAppInner.Controls.Add(this.btnPAF14000_MstIraimoto);
            this.pnlUserAppInner.Controls.Add(this.btnPAF2020);
            this.pnlUserAppInner.Controls.Add(this.btnPAF3050);
            this.pnlUserAppInner.Controls.Add(this.btnPAF3040);
            this.pnlUserAppInner.Controls.Add(this.btnPAF3030);
            this.pnlUserAppInner.Controls.Add(this.btnPAF1030);
            this.pnlUserAppInner.Controls.Add(this.btnPAF2010);
            this.pnlUserAppInner.Controls.Add(this.btnPAF1020);
            this.pnlUserAppInner.Controls.Add(this.btnPAF3010);
            this.pnlUserAppInner.Controls.Add(this.btnPAF1010);
            this.pnlUserAppInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUserAppInner.Location = new System.Drawing.Point(3, 20);
            this.pnlUserAppInner.Name = "pnlUserAppInner";
            this.pnlUserAppInner.Size = new System.Drawing.Size(832, 452);
            this.pnlUserAppInner.TabIndex = 100;
            // 
            // btnPAF2030
            // 
            this.btnPAF2030.Enabled = false;
            this.btnPAF2030.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF2030.Location = new System.Drawing.Point(276, 46);
            this.btnPAF2030.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF2030.Name = "btnPAF2030";
            this.btnPAF2030.Size = new System.Drawing.Size(270, 44);
            this.btnPAF2030.TabIndex = 418;
            this.btnPAF2030.Text = "案件追加";
            this.btnPAF2030.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF2030.UseVisualStyleBackColor = true;
            this.btnPAF2030.Visible = false;
            // 
            // btnPAF3020
            // 
            this.btnPAF3020.Enabled = false;
            this.btnPAF3020.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF3020.Location = new System.Drawing.Point(276, 4);
            this.btnPAF3020.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF3020.Name = "btnPAF3020";
            this.btnPAF3020.Size = new System.Drawing.Size(270, 44);
            this.btnPAF3020.TabIndex = 417;
            this.btnPAF3020.Text = "部署";
            this.btnPAF3020.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF3020.UseVisualStyleBackColor = true;
            this.btnPAF3020.Visible = false;
            // 
            // btnPAF14000_MstIraimoto
            // 
            this.btnPAF14000_MstIraimoto.Enabled = false;
            this.btnPAF14000_MstIraimoto.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF14000_MstIraimoto.Location = new System.Drawing.Point(6, 400);
            this.btnPAF14000_MstIraimoto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF14000_MstIraimoto.Name = "btnPAF14000_MstIraimoto";
            this.btnPAF14000_MstIraimoto.Size = new System.Drawing.Size(270, 44);
            this.btnPAF14000_MstIraimoto.TabIndex = 416;
            this.btnPAF14000_MstIraimoto.Text = "依頼元";
            this.btnPAF14000_MstIraimoto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF14000_MstIraimoto.UseVisualStyleBackColor = true;
            this.btnPAF14000_MstIraimoto.Visible = false;
            // 
            // btnPAF2020
            // 
            this.btnPAF2020.Enabled = false;
            this.btnPAF2020.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF2020.Location = new System.Drawing.Point(6, 136);
            this.btnPAF2020.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF2020.Name = "btnPAF2020";
            this.btnPAF2020.Size = new System.Drawing.Size(270, 44);
            this.btnPAF2020.TabIndex = 205;
            this.btnPAF2020.Text = "H個別コードExcel取込";
            this.btnPAF2020.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF2020.UseVisualStyleBackColor = true;
            this.btnPAF2020.Visible = false;
            // 
            // btnPAF3050
            // 
            this.btnPAF3050.Enabled = false;
            this.btnPAF3050.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF3050.Location = new System.Drawing.Point(6, 356);
            this.btnPAF3050.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF3050.Name = "btnPAF3050";
            this.btnPAF3050.Size = new System.Drawing.Size(270, 44);
            this.btnPAF3050.TabIndex = 415;
            this.btnPAF3050.Text = "休業日";
            this.btnPAF3050.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF3050.UseVisualStyleBackColor = true;
            this.btnPAF3050.Visible = false;
            // 
            // btnPAF3040
            // 
            this.btnPAF3040.Enabled = false;
            this.btnPAF3040.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF3040.Location = new System.Drawing.Point(6, 312);
            this.btnPAF3040.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF3040.Name = "btnPAF3040";
            this.btnPAF3040.Size = new System.Drawing.Size(270, 44);
            this.btnPAF3040.TabIndex = 410;
            this.btnPAF3040.Text = "工程";
            this.btnPAF3040.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF3040.UseVisualStyleBackColor = true;
            this.btnPAF3040.Visible = false;
            // 
            // btnPAF3030
            // 
            this.btnPAF3030.Enabled = false;
            this.btnPAF3030.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF3030.Location = new System.Drawing.Point(6, 268);
            this.btnPAF3030.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF3030.Name = "btnPAF3030";
            this.btnPAF3030.Size = new System.Drawing.Size(270, 44);
            this.btnPAF3030.TabIndex = 405;
            this.btnPAF3030.Text = "プロジェクト";
            this.btnPAF3030.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF3030.UseVisualStyleBackColor = true;
            this.btnPAF3030.Visible = false;
            // 
            // btnPAF1030
            // 
            this.btnPAF1030.Enabled = false;
            this.btnPAF1030.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF1030.Location = new System.Drawing.Point(6, 180);
            this.btnPAF1030.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF1030.Name = "btnPAF1030";
            this.btnPAF1030.Size = new System.Drawing.Size(270, 44);
            this.btnPAF1030.TabIndex = 300;
            this.btnPAF1030.Text = "実績集計";
            this.btnPAF1030.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF1030.UseVisualStyleBackColor = true;
            this.btnPAF1030.Visible = false;
            // 
            // btnPAF2010
            // 
            this.btnPAF2010.Enabled = false;
            this.btnPAF2010.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF2010.Location = new System.Drawing.Point(6, 92);
            this.btnPAF2010.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF2010.Name = "btnPAF2010";
            this.btnPAF2010.Size = new System.Drawing.Size(270, 44);
            this.btnPAF2010.TabIndex = 200;
            this.btnPAF2010.Text = "プロジェクト一覧Excel取込";
            this.btnPAF2010.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF2010.UseVisualStyleBackColor = true;
            this.btnPAF2010.Visible = false;
            // 
            // btnPAF1020
            // 
            this.btnPAF1020.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF1020.Location = new System.Drawing.Point(6, 48);
            this.btnPAF1020.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF1020.Name = "btnPAF1020";
            this.btnPAF1020.Size = new System.Drawing.Size(270, 44);
            this.btnPAF1020.TabIndex = 105;
            this.btnPAF1020.Text = "作業一覧";
            this.btnPAF1020.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF1020.UseVisualStyleBackColor = true;
            this.btnPAF1020.Visible = false;
            // 
            // btnPAF3010
            // 
            this.btnPAF3010.Enabled = false;
            this.btnPAF3010.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF3010.Location = new System.Drawing.Point(6, 224);
            this.btnPAF3010.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF3010.Name = "btnPAF3010";
            this.btnPAF3010.Size = new System.Drawing.Size(270, 44);
            this.btnPAF3010.TabIndex = 400;
            this.btnPAF3010.Text = "担当者";
            this.btnPAF3010.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF3010.UseVisualStyleBackColor = true;
            this.btnPAF3010.Visible = false;
            // 
            // btnPAF1010
            // 
            this.btnPAF1010.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPAF1010.Location = new System.Drawing.Point(6, 4);
            this.btnPAF1010.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPAF1010.Name = "btnPAF1010";
            this.btnPAF1010.Size = new System.Drawing.Size(270, 44);
            this.btnPAF1010.TabIndex = 100;
            this.btnPAF1010.Text = "実績登録";
            this.btnPAF1010.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPAF1010.UseVisualStyleBackColor = true;
            this.btnPAF1010.Visible = false;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.grpSystemMenu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 42);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel2.Size = new System.Drawing.Size(332, 475);
            this.panel2.TabIndex = 0;
            // 
            // grpSystemMenu
            // 
            this.grpSystemMenu.Controls.Add(this.pnlSysMenuInner);
            this.grpSystemMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSystemMenu.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpSystemMenu.Location = new System.Drawing.Point(10, 0);
            this.grpSystemMenu.Name = "grpSystemMenu";
            this.grpSystemMenu.Size = new System.Drawing.Size(312, 475);
            this.grpSystemMenu.TabIndex = 1;
            this.grpSystemMenu.TabStop = false;
            this.grpSystemMenu.Text = "システムメニュ";
            // 
            // pnlSysMenuInner
            // 
            this.pnlSysMenuInner.AutoScroll = true;
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu3);
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu2);
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu1);
            this.pnlSysMenuInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSysMenuInner.Location = new System.Drawing.Point(3, 20);
            this.pnlSysMenuInner.Name = "pnlSysMenuInner";
            this.pnlSysMenuInner.Size = new System.Drawing.Size(306, 452);
            this.pnlSysMenuInner.TabIndex = 0;
            // 
            // btnSysMenu3
            // 
            this.btnSysMenu3.Enabled = false;
            this.btnSysMenu3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu3.Location = new System.Drawing.Point(5, 92);
            this.btnSysMenu3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSysMenu3.Name = "btnSysMenu3";
            this.btnSysMenu3.Size = new System.Drawing.Size(270, 44);
            this.btnSysMenu3.TabIndex = 15;
            this.btnSysMenu3.Text = "マスタメンテナンス";
            this.btnSysMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu3.UseVisualStyleBackColor = true;
            // 
            // btnSysMenu2
            // 
            this.btnSysMenu2.Enabled = false;
            this.btnSysMenu2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu2.Location = new System.Drawing.Point(5, 48);
            this.btnSysMenu2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSysMenu2.Name = "btnSysMenu2";
            this.btnSysMenu2.Size = new System.Drawing.Size(270, 44);
            this.btnSysMenu2.TabIndex = 5;
            this.btnSysMenu2.Text = "プロジェクト取込";
            this.btnSysMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu2.UseVisualStyleBackColor = true;
            // 
            // btnSysMenu1
            // 
            this.btnSysMenu1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu1.Location = new System.Drawing.Point(5, 4);
            this.btnSysMenu1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSysMenu1.Name = "btnSysMenu1";
            this.btnSysMenu1.Size = new System.Drawing.Size(270, 44);
            this.btnSysMenu1.TabIndex = 0;
            this.btnSysMenu1.Text = "PRS実績";
            this.btnSysMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu1.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel3);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 517);
            this.pnlFootor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(887, 59);
            this.pnlFootor.TabIndex = 200;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnLogOff);
            this.panel3.Controls.Add(this.btnClose);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(557, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(330, 59);
            this.panel3.TabIndex = 1000;
            // 
            // btnLogOff
            // 
            this.btnLogOff.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogOff.Location = new System.Drawing.Point(71, 11);
            this.btnLogOff.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(120, 35);
            this.btnLogOff.TabIndex = 1000;
            this.btnLogOff.Text = "ログオフ（F）";
            this.btnLogOff.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(197, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 2000;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.panel1);
            this.pnlHeader.Controls.Add(this.panel6);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(887, 35);
            this.pnlHeader.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblLoginInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.panel1.Size = new System.Drawing.Size(429, 35);
            this.panel1.TabIndex = 1;
            // 
            // lblLoginInfo
            // 
            this.lblLoginInfo.AutoSize = true;
            this.lblLoginInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblLoginInfo.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblLoginInfo.Location = new System.Drawing.Point(10, 10);
            this.lblLoginInfo.Name = "lblLoginInfo";
            this.lblLoginInfo.Size = new System.Drawing.Size(59, 17);
            this.lblLoginInfo.TabIndex = 235;
            this.lblLoginInfo.Text = "ログイン：";
            this.lblLoginInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblRevision);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(662, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 10, 10, 0);
            this.panel6.Size = new System.Drawing.Size(225, 35);
            this.panel6.TabIndex = 1;
            // 
            // lblRevision
            // 
            this.lblRevision.AutoSize = true;
            this.lblRevision.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblRevision.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRevision.Location = new System.Drawing.Point(139, 10);
            this.lblRevision.Name = "lblRevision";
            this.lblRevision.Size = new System.Drawing.Size(76, 17);
            this.lblRevision.TabIndex = 234;
            this.lblRevision.Text = "1.X.X.XXX";
            this.lblRevision.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PAF0000_MainMenuBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 576);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlExecute);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1200, 800);
            this.MinimumSize = new System.Drawing.Size(680, 400);
            this.Name = "PAF0000_MainMenuBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRSアナライザ";
            this.pnlExecute.ResumeLayout(false);
            this.pnlUserApplication.ResumeLayout(false);
            this.grpUserApplication.ResumeLayout(false);
            this.pnlUserAppInner.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.grpSystemMenu.ResumeLayout(false);
            this.pnlSysMenuInner.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlHeader.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Label lblRevision;
        public System.Windows.Forms.Panel pnlFootor;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblLoginInfo;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel pnlUserApplication;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnLogOff;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox grpSystemMenu;
        private System.Windows.Forms.GroupBox grpUserApplication;
        private System.Windows.Forms.Panel pnlSysMenuInner;
        public System.Windows.Forms.Button btnSysMenu3;
        public System.Windows.Forms.Button btnSysMenu2;
        public System.Windows.Forms.Button btnSysMenu1;
        private System.Windows.Forms.Panel pnlUserAppInner;
        public System.Windows.Forms.Button btnPAF2020;
        public System.Windows.Forms.Button btnPAF3050;
        public System.Windows.Forms.Button btnPAF3040;
        public System.Windows.Forms.Button btnPAF3030;
        public System.Windows.Forms.Button btnPAF1030;
        public System.Windows.Forms.Button btnPAF2010;
        public System.Windows.Forms.Button btnPAF1020;
        public System.Windows.Forms.Button btnPAF3010;
        public System.Windows.Forms.Button btnPAF1010;
        public System.Windows.Forms.Button btnPAF3020;
        public System.Windows.Forms.Button btnPAF14000_MstIraimoto;
        public System.Windows.Forms.Button btnPAF2030;
    }
}

