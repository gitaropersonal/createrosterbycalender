﻿using PrsAnalyzer.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public partial class PAF0000_MainMenuBody : Form {
        public PAF0000_MainMenuBody(LoginInfoDto loginDto) {
            InitializeComponent();
            var fl = new PAF0000_MainMenuFormLogic(this, loginDto);
        }
    }
}
