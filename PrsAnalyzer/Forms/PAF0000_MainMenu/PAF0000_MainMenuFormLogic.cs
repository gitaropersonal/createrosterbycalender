﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF0000_MainMenuFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private static PAF0000_MainMenuBody _Body;
        private const int _BODY_WIDTH = 680;
        private const int _BODY_HEIGHT = 500;
        private const int _PANEL_WIDTH = 325;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// システムメニュ一覧
        /// </summary>
        private List<ApplicationInfoDto> _LST_SYSTEM_MENU = new List<ApplicationInfoDto>();
        /// <summary>
        /// ユーザアプリケーション一覧
        /// </summary>
        private List<ApplicationInfoDto> _LST_APPLICATION = new List<ApplicationInfoDto>();
        /// <summary>
        /// ユーザアプリケーションボタン位置
        /// </summary>
        private const int _USER_APP_START_LOCATION_X = 6;
        private const int _USER_APP_SPAN = 44;
        /// <summary>
        /// ログインユーザ情報
        /// </summary>
        private const string _LBL_LOGIN_USER_NAME = "ログイン：{0}";
        private const string _LOGIN_USER_ADMIN = "（管理者）";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public PAF0000_MainMenuFormLogic(PAF0000_MainMenuBody body, LoginInfoDto loginInfo) {

            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.D1:
                        _Body.btnSysMenu1.Focus();
                        _Body.btnSysMenu1.PerformClick();
                        break;
                    case Keys.D2:
                        _Body.btnSysMenu2.Focus();
                        _Body.btnSysMenu2.PerformClick();
                        break;
                    case Keys.D3:
                        _Body.btnSysMenu3.Focus();
                        _Body.btnSysMenu3.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                    case Keys.F:
                        _Body.btnLogOff.Focus();
                        _Body.btnLogOff.PerformClick();
                        break;
                }
            };

            #region システムメニュ
            _Body.btnSysMenu1.Click += (s, e) => { BtnSysMenuClick(Enums.SysType.PRS_JISSEKI); };
            _Body.btnSysMenu2.Click += (s, e) => { BtnSysMenuClick(Enums.SysType.PROJECT_CAPTURE); };
            _Body.btnSysMenu3.Click += (s, e) => { BtnSysMenuClick(Enums.SysType.MASTER); };
            #endregion

            #region ユーザアプリケーション
            // １．PRS実績
            _Body.btnPAF1010.Click += (s, e) => { ShowChildForm(_Body.btnPAF1010, new PAF1010_PrsEntryBody(LoginInfo)); };
            _Body.btnPAF1020.Click += (s, e) => { ShowChildForm(_Body.btnPAF1020, new PAF1020_ProjectLooksBody(LoginInfo)); };
            _Body.btnPAF1030.Click += (s, e) => { ShowChildForm(_Body.btnPAF1030, new PAF1030_PrsAggregateBody(LoginInfo)); };
            // ２．プロジェクト取込
            _Body.btnPAF2010.Click += (s, e) => { ShowChildForm(_Body.btnPAF2010, new PAF2010_AnkenExcelCaptureBody(LoginInfo)); };
            _Body.btnPAF2020.Click += (s, e) => { ShowChildForm(_Body.btnPAF2020, new PAF2020_HGyomuExcelCaptureBody(LoginInfo)); };
            _Body.btnPAF2030.Click += (s, e) => { ShowChildForm(_Body.btnPAF2030, new PAF2030_AnkenExcelInputBody(LoginInfo)); };
            // ３．マスタメンテナンス
            _Body.btnPAF3010.Click += (s, e) => { ShowChildForm(_Body.btnPAF3010, new PAF3010_MstEmploeeBody(LoginInfo)); };
            _Body.btnPAF3020.Click += (s, e) => { ShowChildForm(_Body.btnPAF3020, new PAF3020_MstBushoBody(LoginInfo)); };
            _Body.btnPAF3030.Click += (s, e) => { ShowChildForm(_Body.btnPAF3030, new PAF3030_MstProjectBody(LoginInfo)); };
            _Body.btnPAF3040.Click += (s, e) => { ShowChildForm(_Body.btnPAF3040, new PAF3040_MstKouteiBody(LoginInfo)); };
            _Body.btnPAF3050.Click += (s, e) => { ShowChildForm(_Body.btnPAF3050, new PAF3050_MstKyugyoDateBody(LoginInfo)); };
            #endregion

            // ログオフボタン押下
            _Body.btnLogOff.Click += (s, e) => {
                Close(DialogResult.Retry);
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                Close();
            };
        }
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 画面サイズ
            _Body.MaximumSize = new Size(_BODY_WIDTH, _BODY_HEIGHT);
            _Body.MinimumSize = new Size(_BODY_WIDTH, _BODY_HEIGHT);

            // パネルサイズ
            _Body.pnlUserApplication.Width = _PANEL_WIDTH;

            // システムメニュを初期化
            InitLstSystemMenuInfo();

            // ユーザアプリケーションを初期化
            InitLstUserApplicationInfo();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

            // バージョン
            _Body.lblRevision.Text = ControlUtil.GetVersion();

            // ログイン権限
            _Body.lblLoginInfo.Text = string.Format(_LBL_LOGIN_USER_NAME, LoginInfo.TantoName);
            if (LoginInfo.AuthorityKbn == (int)Enums.AuthorityKbn.Admin) {
                _Body.lblLoginInfo.Text += _LOGIN_USER_ADMIN;
            }
            // フォーカス
            _Body.btnSysMenu1.Focus();
        }
        /// <summary>
        /// 初期化（システムメニュ情報）
        /// </summary>
        private void InitLstSystemMenuInfo() {
            _LST_SYSTEM_MENU.Clear();

            // 管理者のみ使用できるシステムメニュ
            bool enableAdmin = (LoginInfo.AuthorityKbn == (int)Enums.AuthorityKbn.Admin);

            // １．PRS実績
            _LST_SYSTEM_MENU.Add(new ApplicationInfoDto(0, _Body.btnSysMenu1, Enums.SysType.PRS_JISSEKI));
            // ２．プロジェクト取込
            _LST_SYSTEM_MENU.Add(new ApplicationInfoDto(0, _Body.btnSysMenu2, Enums.SysType.PROJECT_CAPTURE, enableAdmin));
            // ３．マスタメンテナンス
            _LST_SYSTEM_MENU.Add(new ApplicationInfoDto(0, _Body.btnSysMenu3, Enums.SysType.MASTER, enableAdmin));

            // ボタンテキストの整備
            foreach (var dto in _LST_SYSTEM_MENU) {
                dto.AppButton.Enabled = dto.DefaultEnabled;
                dto.AppButton.Text = $"({(int)dto.SystemType}) {dto.AppButton.Text}";
            }
        }
        /// <summary>
        /// 初期化（ユーザアプリケーション情報）
        /// </summary>
        private void InitLstUserApplicationInfo() {
            _LST_APPLICATION.Clear();

            // １．PRS実績
            _LST_APPLICATION.Add(new ApplicationInfoDto(1, _Body.btnPAF1010, Enums.SysType.PRS_JISSEKI));
            _LST_APPLICATION.Add(new ApplicationInfoDto(2, _Body.btnPAF1020, Enums.SysType.PRS_JISSEKI));
            _LST_APPLICATION.Add(new ApplicationInfoDto(3, _Body.btnPAF1030, Enums.SysType.PRS_JISSEKI));
            // ２．プロジェクト取込
            _LST_APPLICATION.Add(new ApplicationInfoDto(1, _Body.btnPAF2010, Enums.SysType.PROJECT_CAPTURE, LoginInfo.PBushoKbn == (int)ProjectConst.PBushoKbn.DEV));
            _LST_APPLICATION.Add(new ApplicationInfoDto(2, _Body.btnPAF2020, Enums.SysType.PROJECT_CAPTURE, LoginInfo.PBushoKbn == (int)ProjectConst.PBushoKbn.FIELD_SUPPROT));
            _LST_APPLICATION.Add(new ApplicationInfoDto(3, _Body.btnPAF2030, Enums.SysType.PROJECT_CAPTURE));
            // ３．マスタメンテナンス
            _LST_APPLICATION.Add(new ApplicationInfoDto(1, _Body.btnPAF3010, Enums.SysType.MASTER));
            _LST_APPLICATION.Add(new ApplicationInfoDto(2, _Body.btnPAF3020, Enums.SysType.MASTER));
            _LST_APPLICATION.Add(new ApplicationInfoDto(3, _Body.btnPAF3030, Enums.SysType.MASTER));
            _LST_APPLICATION.Add(new ApplicationInfoDto(4, _Body.btnPAF3040, Enums.SysType.MASTER));
            _LST_APPLICATION.Add(new ApplicationInfoDto(5, _Body.btnPAF3050, Enums.SysType.MASTER));

            // ボタンテキストの整備
            foreach (var dto in _LST_APPLICATION) {
                dto.AppButton.Text = $"({(int)dto.SystemType}) - {dto.EdaNum}.{dto.AppButton.Text}";
            }
        }
        /// <summary>
        /// システムメニュボタン押下処理
        /// </summary>
        /// <param name="sysType"></param>
        private void BtnSysMenuClick(Enums.SysType sysType) {

            _Body.pnlUserApplication.AutoScroll = false;
            _Body.pnlUserApplication.Height = _BODY_HEIGHT * 10;

            // ソート
            _LST_APPLICATION = _LST_APPLICATION.OrderBy(n => (int)n.SystemType).ThenBy(n => n.EdaNum).ToList();

            // 選択されたメニュに属するアプリケーション
            bool focued = false;
            var tgtAppList = _LST_APPLICATION.Where(n => n.SystemType == sysType).ToList();
            for (int i = 0; i < tgtAppList.Count; i++) {
                var tgtInfo = tgtAppList[i];
                tgtInfo.AppButton.Visible = true;
                tgtInfo.AppButton.Enabled = tgtInfo.DefaultEnabled;
                tgtInfo.AppButton.TabIndex = ((int)tgtInfo.SystemType * 100) + (i * 5);
                tgtInfo.AppButton.Location = new Point(_USER_APP_START_LOCATION_X, _Body.btnSysMenu1.Location.Y + (_USER_APP_SPAN * i));

                // 先頭の機能にフォーカス
                if (!focued && tgtInfo.AppButton.Enabled) {
                    tgtInfo.AppButton.Focus();
                    focued = true;
                }
            }
            // 選択されなかったメニュに属するアプリケーション
            var otherAppList = _LST_APPLICATION.Where(n => n.SystemType != sysType).ToList();
            foreach (var otherInfo in otherAppList) {
                otherInfo.AppButton.Visible = false;
            }
            _Body.pnlUserApplication.Height = _BODY_HEIGHT;
            _Body.pnlUserApplication.AutoScroll = true;
        }
        /// <summary>
        /// 子画面表示
        /// </summary>
        /// <param name="btn"></param>
        /// <param name="ChildForm"></param>
        private void ShowChildForm(Button btn, Form ChildForm) {
            ChildForm.Text = btn.Text;
            _Body.Visible = false;
            ChildForm.ShowDialog();
            _Body.Visible = true;
            btn.Focus();
        }
        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="dr"></param>
        private void Close(DialogResult dr = DialogResult.Cancel) {
            var drConfirm = DialogResult.None;
            switch (dr) {
                case DialogResult.Cancel:
                    drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                    break;
                case DialogResult.Retry:
                    drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_LOGOFF);
                    break;
            }
            if (drConfirm == DialogResult.OK) {
                _Body.DialogResult = dr;
                _Body.Close();
            }
        }
    }
}
