﻿namespace PrsAnalyzer.Forms {
    partial class PAF1010_PrsEntryBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF1010_PrsEntryBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCreateKinmuhyo = new System.Windows.Forms.Button();
            this.btnFolderOpen = new System.Windows.Forms.Button();
            this.btnCreatePRS = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.chkKongetsuExist = new System.Windows.Forms.CheckBox();
            this.chkZengetsuExist = new System.Windows.Forms.CheckBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnConClear = new System.Windows.Forms.Button();
            this.pnlKokyakuRdo = new System.Windows.Forms.Panel();
            this.rdoSyanai = new System.Windows.Forms.RadioButton();
            this.rdoIraimoto = new System.Windows.Forms.RadioButton();
            this.rdoNothing = new System.Windows.Forms.RadioButton();
            this.txtKokyakuName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbGyomuKbn = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSagyoName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvPlojectLooks = new System.Windows.Forms.DataGridView();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SagyoNaiyoFull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaishoYM_Disp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaishoYM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvPlojectLooksSel = new System.Windows.Forms.DataGridView();
            this.Detail_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SagyoNaiyoFull_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaishoYM_Disp_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailSeq_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiSeq_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaishoYM_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowNum_Sel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabSagyoMaster = new System.Windows.Forms.TabControl();
            this.rdoFutokutei = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.cmbBushoName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mcSelectedDate = new System.Windows.Forms.MonthCalendar();
            this.cmbEmploeeName = new System.Windows.Forms.ComboBox();
            this.lblEmploeeName = new System.Windows.Forms.Label();
            this.txtEmploeeId = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnInputSagyo = new System.Windows.Forms.Button();
            this.btnDelSagyo = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dgvPrsJisseki = new System.Windows.Forms.DataGridView();
            this.SunDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MonDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TuesDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WednesDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThurthDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FriDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaturDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_Sunday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_MonDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_TuesDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_WednesDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_TursDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_FriDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MstRowNum_SaturDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlExecute.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.pnlKokyakuRdo.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlojectLooks)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlojectLooksSel)).BeginInit();
            this.tabSagyoMaster.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsJisseki)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.pnlHeader);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(1534, 941);
            this.pnlExecute.TabIndex = 25;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.panel6);
            this.pnlHeader.Controls.Add(this.panel10);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHeader.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1534, 941);
            this.pnlHeader.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.groupBox2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel10.Size = new System.Drawing.Size(1534, 272);
            this.panel10.TabIndex = 0;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.textBox7);
            this.pnlFootor.Controls.Add(this.panel1);
            this.pnlFootor.Controls.Add(this.textBox6);
            this.pnlFootor.Controls.Add(this.panel2);
            this.pnlFootor.Controls.Add(this.textBox5);
            this.pnlFootor.Controls.Add(this.textBox3);
            this.pnlFootor.Controls.Add(this.textBox4);
            this.pnlFootor.Controls.Add(this.textBox1);
            this.pnlFootor.Controls.Add(this.textBox2);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 891);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1534, 50);
            this.pnlFootor.TabIndex = 5000;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCreateKinmuhyo);
            this.panel1.Controls.Add(this.btnFolderOpen);
            this.panel1.Controls.Add(this.btnCreatePRS);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(545, 50);
            this.panel1.TabIndex = 5100;
            // 
            // btnCreateKinmuhyo
            // 
            this.btnCreateKinmuhyo.Enabled = false;
            this.btnCreateKinmuhyo.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCreateKinmuhyo.Location = new System.Drawing.Point(389, 7);
            this.btnCreateKinmuhyo.Name = "btnCreateKinmuhyo";
            this.btnCreateKinmuhyo.Size = new System.Drawing.Size(120, 35);
            this.btnCreateKinmuhyo.TabIndex = 55;
            this.btnCreateKinmuhyo.Text = "勤務表作成（K）";
            this.btnCreateKinmuhyo.UseVisualStyleBackColor = true;
            // 
            // btnFolderOpen
            // 
            this.btnFolderOpen.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnFolderOpen.Location = new System.Drawing.Point(137, 7);
            this.btnFolderOpen.Name = "btnFolderOpen";
            this.btnFolderOpen.Size = new System.Drawing.Size(120, 35);
            this.btnFolderOpen.TabIndex = 53;
            this.btnFolderOpen.Text = "保存場所（F）";
            this.btnFolderOpen.UseVisualStyleBackColor = true;
            // 
            // btnCreatePRS
            // 
            this.btnCreatePRS.Enabled = false;
            this.btnCreatePRS.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCreatePRS.Location = new System.Drawing.Point(263, 7);
            this.btnCreatePRS.Name = "btnCreatePRS";
            this.btnCreatePRS.Size = new System.Drawing.Size(120, 35);
            this.btnCreatePRS.TabIndex = 54;
            this.btnCreatePRS.Text = "PRS作成（P）";
            this.btnCreatePRS.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(11, 7);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 52;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1367, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(167, 50);
            this.panel2.TabIndex = 5200;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(31, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 101;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 272);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(10, 10, 15, 50);
            this.panel6.Size = new System.Drawing.Size(1534, 669);
            this.panel6.TabIndex = 2000;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(886, 21);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(50, 19);
            this.textBox7.TabIndex = 31;
            this.textBox7.TabStop = false;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(830, 21);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(50, 19);
            this.textBox6.TabIndex = 30;
            this.textBox6.TabStop = false;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(774, 21);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(50, 19);
            this.textBox5.TabIndex = 29;
            this.textBox5.TabStop = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(718, 21);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(50, 19);
            this.textBox4.TabIndex = 28;
            this.textBox4.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(662, 21);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(50, 19);
            this.textBox3.TabIndex = 27;
            this.textBox3.TabStop = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(606, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(50, 19);
            this.textBox2.TabIndex = 26;
            this.textBox2.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(550, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(50, 19);
            this.textBox1.TabIndex = 25;
            this.textBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel14);
            this.groupBox2.Controls.Add(this.panel9);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(10, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1514, 272);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "登録作業";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.tabSagyoMaster);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(357, 20);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1154, 249);
            this.panel14.TabIndex = 17;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.groupBox3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(3, 20);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.panel9.Size = new System.Drawing.Size(354, 249);
            this.panel9.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.panel13);
            this.groupBox3.Controls.Add(this.panel11);
            this.groupBox3.Controls.Add(this.pnlKokyakuRdo);
            this.groupBox3.Controls.Add(this.txtKokyakuName);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cmbGyomuKbn);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtSagyoName);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtProjectCode);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(10, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(334, 249);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "検索条件";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(12, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 85;
            this.label6.Text = "検索オプション";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.chkKongetsuExist);
            this.panel13.Controls.Add(this.chkZengetsuExist);
            this.panel13.Location = new System.Drawing.Point(106, 18);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(196, 26);
            this.panel13.TabIndex = 74;
            // 
            // chkKongetsuExist
            // 
            this.chkKongetsuExist.AutoSize = true;
            this.chkKongetsuExist.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkKongetsuExist.Location = new System.Drawing.Point(98, 0);
            this.chkKongetsuExist.Name = "chkKongetsuExist";
            this.chkKongetsuExist.Size = new System.Drawing.Size(98, 26);
            this.chkKongetsuExist.TabIndex = 1;
            this.chkKongetsuExist.Text = "今月実績あり";
            this.chkKongetsuExist.UseVisualStyleBackColor = true;
            // 
            // chkZengetsuExist
            // 
            this.chkZengetsuExist.AutoSize = true;
            this.chkZengetsuExist.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkZengetsuExist.Location = new System.Drawing.Point(0, 0);
            this.chkZengetsuExist.Name = "chkZengetsuExist";
            this.chkZengetsuExist.Size = new System.Drawing.Size(98, 26);
            this.chkZengetsuExist.TabIndex = 0;
            this.chkZengetsuExist.Text = "前月実績あり";
            this.chkZengetsuExist.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(3, 201);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(328, 45);
            this.panel11.TabIndex = 80;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.btnConClear);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(183, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(145, 45);
            this.panel12.TabIndex = 10;
            // 
            // btnConClear
            // 
            this.btnConClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnConClear.Location = new System.Drawing.Point(19, 5);
            this.btnConClear.Name = "btnConClear";
            this.btnConClear.Size = new System.Drawing.Size(120, 35);
            this.btnConClear.TabIndex = 12;
            this.btnConClear.Text = "条件クリア（C）";
            this.btnConClear.UseVisualStyleBackColor = true;
            // 
            // pnlKokyakuRdo
            // 
            this.pnlKokyakuRdo.Controls.Add(this.rdoFutokutei);
            this.pnlKokyakuRdo.Controls.Add(this.rdoSyanai);
            this.pnlKokyakuRdo.Controls.Add(this.rdoIraimoto);
            this.pnlKokyakuRdo.Controls.Add(this.rdoNothing);
            this.pnlKokyakuRdo.Location = new System.Drawing.Point(106, 121);
            this.pnlKokyakuRdo.Name = "pnlKokyakuRdo";
            this.pnlKokyakuRdo.Size = new System.Drawing.Size(199, 43);
            this.pnlKokyakuRdo.TabIndex = 78;
            // 
            // rdoSyanai
            // 
            this.rdoSyanai.AutoSize = true;
            this.rdoSyanai.Location = new System.Drawing.Point(145, 1);
            this.rdoSyanai.Name = "rdoSyanai";
            this.rdoSyanai.Size = new System.Drawing.Size(52, 21);
            this.rdoSyanai.TabIndex = 2;
            this.rdoSyanai.TabStop = true;
            this.rdoSyanai.Text = "社内";
            this.rdoSyanai.UseVisualStyleBackColor = true;
            // 
            // rdoIraimoto
            // 
            this.rdoIraimoto.AutoSize = true;
            this.rdoIraimoto.Location = new System.Drawing.Point(72, 0);
            this.rdoIraimoto.Name = "rdoIraimoto";
            this.rdoIraimoto.Size = new System.Drawing.Size(65, 21);
            this.rdoIraimoto.TabIndex = 1;
            this.rdoIraimoto.TabStop = true;
            this.rdoIraimoto.Text = "依頼元";
            this.rdoIraimoto.UseVisualStyleBackColor = true;
            // 
            // rdoNothing
            // 
            this.rdoNothing.AutoSize = true;
            this.rdoNothing.Location = new System.Drawing.Point(0, 0);
            this.rdoNothing.Name = "rdoNothing";
            this.rdoNothing.Size = new System.Drawing.Size(72, 21);
            this.rdoNothing.TabIndex = 0;
            this.rdoNothing.TabStop = true;
            this.rdoNothing.Text = "選択なし";
            this.rdoNothing.UseVisualStyleBackColor = true;
            // 
            // txtKokyakuName
            // 
            this.txtKokyakuName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokyakuName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKokyakuName.Location = new System.Drawing.Point(106, 170);
            this.txtKokyakuName.MaxLength = 100;
            this.txtKokyakuName.Name = "txtKokyakuName";
            this.txtKokyakuName.Size = new System.Drawing.Size(199, 24);
            this.txtKokyakuName.TabIndex = 79;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(12, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 84;
            this.label5.Text = "業務区分";
            // 
            // cmbGyomuKbn
            // 
            this.cmbGyomuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGyomuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbGyomuKbn.FormattingEnabled = true;
            this.cmbGyomuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbGyomuKbn.ItemHeight = 17;
            this.cmbGyomuKbn.Location = new System.Drawing.Point(106, 95);
            this.cmbGyomuKbn.MaxLength = 35;
            this.cmbGyomuKbn.Name = "cmbGyomuKbn";
            this.cmbGyomuKbn.Size = new System.Drawing.Size(58, 25);
            this.cmbGyomuKbn.TabIndex = 77;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(12, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 83;
            this.label4.Text = "顧客名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 82;
            this.label3.Text = "作業内容";
            // 
            // txtSagyoName
            // 
            this.txtSagyoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSagyoName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSagyoName.Location = new System.Drawing.Point(106, 70);
            this.txtSagyoName.MaxLength = 50;
            this.txtSagyoName.Name = "txtSagyoName";
            this.txtSagyoName.Size = new System.Drawing.Size(199, 24);
            this.txtSagyoName.TabIndex = 76;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 81;
            this.label2.Text = "ﾌﾟﾛｼﾞｪｸﾄｺｰﾄﾞ";
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProjectCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProjectCode.Location = new System.Drawing.Point(106, 45);
            this.txtProjectCode.MaxLength = 8;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Size = new System.Drawing.Size(83, 24);
            this.txtProjectCode.TabIndex = 75;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvPlojectLooks);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1032, 243);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvPlojectLooks
            // 
            this.dgvPlojectLooks.AllowUserToAddRows = false;
            this.dgvPlojectLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPlojectLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNum,
            this.ProjectCD,
            this.KokyakuName,
            this.TaishoYM,
            this.KouteiSeq,
            this.DetailSeq,
            this.TaishoYM_Disp,
            this.SagyoNaiyoFull,
            this.ProjectName,
            this.KouteiName,
            this.Detail});
            this.dgvPlojectLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPlojectLooks.Location = new System.Drawing.Point(3, 3);
            this.dgvPlojectLooks.MultiSelect = false;
            this.dgvPlojectLooks.Name = "dgvPlojectLooks";
            this.dgvPlojectLooks.ReadOnly = true;
            this.dgvPlojectLooks.RowTemplate.Height = 21;
            this.dgvPlojectLooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPlojectLooks.Size = new System.Drawing.Size(1026, 237);
            this.dgvPlojectLooks.TabIndex = 12;
            // 
            // Detail
            // 
            this.Detail.DataPropertyName = "Detail";
            this.Detail.HeaderText = "詳細";
            this.Detail.Name = "Detail";
            this.Detail.ReadOnly = true;
            // 
            // KouteiName
            // 
            this.KouteiName.DataPropertyName = "KouteiName";
            this.KouteiName.HeaderText = "工程";
            this.KouteiName.Name = "KouteiName";
            this.KouteiName.ReadOnly = true;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "ProjectName";
            this.ProjectName.HeaderText = "作業内容";
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.ReadOnly = true;
            // 
            // SagyoNaiyoFull
            // 
            this.SagyoNaiyoFull.DataPropertyName = "SagyoNaiyoFull";
            this.SagyoNaiyoFull.HeaderText = "作業名";
            this.SagyoNaiyoFull.Name = "SagyoNaiyoFull";
            this.SagyoNaiyoFull.ReadOnly = true;
            this.SagyoNaiyoFull.Visible = false;
            this.SagyoNaiyoFull.Width = 480;
            // 
            // TaishoYM_Disp
            // 
            this.TaishoYM_Disp.DataPropertyName = "TaishoYM_Disp";
            this.TaishoYM_Disp.HeaderText = "対象年月";
            this.TaishoYM_Disp.Name = "TaishoYM_Disp";
            this.TaishoYM_Disp.ReadOnly = true;
            this.TaishoYM_Disp.Visible = false;
            // 
            // DetailSeq
            // 
            this.DetailSeq.DataPropertyName = "DetailSeq";
            this.DetailSeq.HeaderText = "詳細Seq";
            this.DetailSeq.Name = "DetailSeq";
            this.DetailSeq.ReadOnly = true;
            this.DetailSeq.Visible = false;
            // 
            // KouteiSeq
            // 
            this.KouteiSeq.DataPropertyName = "KouteiSeq";
            this.KouteiSeq.HeaderText = "工程Seq";
            this.KouteiSeq.Name = "KouteiSeq";
            this.KouteiSeq.ReadOnly = true;
            this.KouteiSeq.Visible = false;
            // 
            // TaishoYM
            // 
            this.TaishoYM.DataPropertyName = "TaishoYM";
            this.TaishoYM.HeaderText = "対象年月（内部）";
            this.TaishoYM.Name = "TaishoYM";
            this.TaishoYM.ReadOnly = true;
            this.TaishoYM.Visible = false;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            this.KokyakuName.HeaderText = "顧客目";
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.ReadOnly = true;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトID";
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.ReadOnly = true;
            this.ProjectCD.Visible = false;
            // 
            // RowNum
            // 
            this.RowNum.DataPropertyName = "RowNum";
            this.RowNum.HeaderText = "行番号";
            this.RowNum.Name = "RowNum";
            this.RowNum.ReadOnly = true;
            this.RowNum.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvPlojectLooksSel);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1146, 219);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "作業一覧（M）";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvPlojectLooksSel
            // 
            this.dgvPlojectLooksSel.AllowUserToAddRows = false;
            this.dgvPlojectLooksSel.AllowUserToResizeRows = false;
            this.dgvPlojectLooksSel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPlojectLooksSel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNum_Sel,
            this.KokyakuName_Sel,
            this.ProjectCD_Sel,
            this.TaishoYM_Sel,
            this.KouteiSeq_Sel,
            this.DetailSeq_Sel,
            this.TaishoYM_Disp_Sel,
            this.SagyoNaiyoFull_Sel,
            this.ProjectName_Sel,
            this.KouteiName_Sel,
            this.Detail_Sel});
            this.dgvPlojectLooksSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPlojectLooksSel.Location = new System.Drawing.Point(3, 3);
            this.dgvPlojectLooksSel.MultiSelect = false;
            this.dgvPlojectLooksSel.Name = "dgvPlojectLooksSel";
            this.dgvPlojectLooksSel.ReadOnly = true;
            this.dgvPlojectLooksSel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPlojectLooksSel.RowTemplate.Height = 21;
            this.dgvPlojectLooksSel.Size = new System.Drawing.Size(1140, 213);
            this.dgvPlojectLooksSel.TabIndex = 11;
            // 
            // Detail_Sel
            // 
            this.Detail_Sel.DataPropertyName = "Detail";
            this.Detail_Sel.HeaderText = "詳細";
            this.Detail_Sel.Name = "Detail_Sel";
            this.Detail_Sel.ReadOnly = true;
            this.Detail_Sel.Width = 250;
            // 
            // KouteiName_Sel
            // 
            this.KouteiName_Sel.DataPropertyName = "KouteiName";
            this.KouteiName_Sel.HeaderText = "工程";
            this.KouteiName_Sel.Name = "KouteiName_Sel";
            this.KouteiName_Sel.ReadOnly = true;
            this.KouteiName_Sel.Width = 80;
            // 
            // ProjectName_Sel
            // 
            this.ProjectName_Sel.DataPropertyName = "ProjectName";
            this.ProjectName_Sel.HeaderText = "作業内容";
            this.ProjectName_Sel.Name = "ProjectName_Sel";
            this.ProjectName_Sel.ReadOnly = true;
            this.ProjectName_Sel.Width = 175;
            // 
            // SagyoNaiyoFull_Sel
            // 
            this.SagyoNaiyoFull_Sel.DataPropertyName = "SagyoNaiyoFull";
            this.SagyoNaiyoFull_Sel.HeaderText = "作業名";
            this.SagyoNaiyoFull_Sel.Name = "SagyoNaiyoFull_Sel";
            this.SagyoNaiyoFull_Sel.ReadOnly = true;
            this.SagyoNaiyoFull_Sel.Visible = false;
            this.SagyoNaiyoFull_Sel.Width = 460;
            // 
            // TaishoYM_Disp_Sel
            // 
            this.TaishoYM_Disp_Sel.DataPropertyName = "TaishoYM_Disp";
            this.TaishoYM_Disp_Sel.HeaderText = "対象年月";
            this.TaishoYM_Disp_Sel.Name = "TaishoYM_Disp_Sel";
            this.TaishoYM_Disp_Sel.ReadOnly = true;
            this.TaishoYM_Disp_Sel.Visible = false;
            // 
            // DetailSeq_Sel
            // 
            this.DetailSeq_Sel.DataPropertyName = "DetailSeq";
            this.DetailSeq_Sel.HeaderText = "詳細Seq";
            this.DetailSeq_Sel.Name = "DetailSeq_Sel";
            this.DetailSeq_Sel.ReadOnly = true;
            this.DetailSeq_Sel.Visible = false;
            // 
            // KouteiSeq_Sel
            // 
            this.KouteiSeq_Sel.DataPropertyName = "KouteiSeq";
            this.KouteiSeq_Sel.HeaderText = "工程Seq";
            this.KouteiSeq_Sel.Name = "KouteiSeq_Sel";
            this.KouteiSeq_Sel.ReadOnly = true;
            this.KouteiSeq_Sel.Visible = false;
            // 
            // TaishoYM_Sel
            // 
            this.TaishoYM_Sel.DataPropertyName = "TaishoYM";
            this.TaishoYM_Sel.HeaderText = "対象年月（内部）";
            this.TaishoYM_Sel.Name = "TaishoYM_Sel";
            this.TaishoYM_Sel.ReadOnly = true;
            this.TaishoYM_Sel.Visible = false;
            // 
            // ProjectCD_Sel
            // 
            this.ProjectCD_Sel.DataPropertyName = "ProjectCD";
            this.ProjectCD_Sel.HeaderText = "ﾌﾟﾛｼﾞｪｸﾄｺｰﾄﾞ";
            this.ProjectCD_Sel.Name = "ProjectCD_Sel";
            this.ProjectCD_Sel.ReadOnly = true;
            this.ProjectCD_Sel.Width = 95;
            // 
            // KokyakuName_Sel
            // 
            this.KokyakuName_Sel.DataPropertyName = "KokyakuName";
            this.KokyakuName_Sel.HeaderText = "顧客名";
            this.KokyakuName_Sel.Name = "KokyakuName_Sel";
            this.KokyakuName_Sel.ReadOnly = true;
            // 
            // RowNum_Sel
            // 
            this.RowNum_Sel.DataPropertyName = "RowNum";
            this.RowNum_Sel.HeaderText = "行番号";
            this.RowNum_Sel.Name = "RowNum_Sel";
            this.RowNum_Sel.ReadOnly = true;
            this.RowNum_Sel.Visible = false;
            // 
            // tabSagyoMaster
            // 
            this.tabSagyoMaster.Controls.Add(this.tabPage1);
            this.tabSagyoMaster.Controls.Add(this.tabPage2);
            this.tabSagyoMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSagyoMaster.Location = new System.Drawing.Point(0, 0);
            this.tabSagyoMaster.Name = "tabSagyoMaster";
            this.tabSagyoMaster.SelectedIndex = 0;
            this.tabSagyoMaster.Size = new System.Drawing.Size(1154, 249);
            this.tabSagyoMaster.TabIndex = 200;
            // 
            // rdoFutokutei
            // 
            this.rdoFutokutei.AutoSize = true;
            this.rdoFutokutei.Location = new System.Drawing.Point(0, 22);
            this.rdoFutokutei.Name = "rdoFutokutei";
            this.rdoFutokutei.Size = new System.Drawing.Size(91, 21);
            this.rdoFutokutei.TabIndex = 4;
            this.rdoFutokutei.TabStop = true;
            this.rdoFutokutei.Text = "不特定客先";
            this.rdoFutokutei.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1509, 609);
            this.groupBox1.TabIndex = 2100;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "実績登録";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox4);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 20);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1503, 586);
            this.panel5.TabIndex = 59;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.grpInput);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(10, 0, 10, 300);
            this.panel4.Size = new System.Drawing.Size(354, 586);
            this.panel4.TabIndex = 2200;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.cmbBushoName);
            this.grpInput.Controls.Add(this.label7);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.mcSelectedDate);
            this.grpInput.Controls.Add(this.cmbEmploeeName);
            this.grpInput.Controls.Add(this.lblEmploeeName);
            this.grpInput.Controls.Add(this.txtEmploeeId);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(10, 0);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(334, 286);
            this.grpInput.TabIndex = 38;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "登録条件";
            // 
            // cmbBushoName
            // 
            this.cmbBushoName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBushoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbBushoName.FormattingEnabled = true;
            this.cmbBushoName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbBushoName.Location = new System.Drawing.Point(105, 202);
            this.cmbBushoName.MaxLength = 35;
            this.cmbBushoName.Name = "cmbBushoName";
            this.cmbBushoName.Size = new System.Drawing.Size(199, 25);
            this.cmbBushoName.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(11, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 17);
            this.label7.TabIndex = 56;
            this.label7.Text = "部署名";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 54;
            this.label1.Text = "対象日（T）";
            // 
            // mcSelectedDate
            // 
            this.mcSelectedDate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.mcSelectedDate.Location = new System.Drawing.Point(105, 24);
            this.mcSelectedDate.MaxSelectionCount = 1;
            this.mcSelectedDate.Name = "mcSelectedDate";
            this.mcSelectedDate.ShowToday = false;
            this.mcSelectedDate.TabIndex = 0;
            // 
            // cmbEmploeeName
            // 
            this.cmbEmploeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmploeeName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbEmploeeName.FormattingEnabled = true;
            this.cmbEmploeeName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbEmploeeName.Location = new System.Drawing.Point(105, 228);
            this.cmbEmploeeName.MaxLength = 35;
            this.cmbEmploeeName.Name = "cmbEmploeeName";
            this.cmbEmploeeName.Size = new System.Drawing.Size(103, 25);
            this.cmbEmploeeName.TabIndex = 2;
            // 
            // lblEmploeeName
            // 
            this.lblEmploeeName.AutoSize = true;
            this.lblEmploeeName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeName.Location = new System.Drawing.Point(11, 231);
            this.lblEmploeeName.Name = "lblEmploeeName";
            this.lblEmploeeName.Size = new System.Drawing.Size(60, 17);
            this.lblEmploeeName.TabIndex = 53;
            this.lblEmploeeName.Text = "担当者名";
            // 
            // txtEmploeeId
            // 
            this.txtEmploeeId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeId.Location = new System.Drawing.Point(209, 228);
            this.txtEmploeeId.MaxLength = 6;
            this.txtEmploeeId.Name = "txtEmploeeId";
            this.txtEmploeeId.Size = new System.Drawing.Size(95, 24);
            this.txtEmploeeId.TabIndex = 3;
            this.txtEmploeeId.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel7);
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(354, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1149, 586);
            this.groupBox4.TabIndex = 2500;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PRS実績(J)";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvPrsJisseki);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 20);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 55);
            this.panel3.Size = new System.Drawing.Size(1143, 563);
            this.panel3.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnDelSagyo);
            this.panel8.Controls.Add(this.btnInputSagyo);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(854, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(289, 50);
            this.panel8.TabIndex = 2650;
            // 
            // btnInputSagyo
            // 
            this.btnInputSagyo.Enabled = false;
            this.btnInputSagyo.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnInputSagyo.Location = new System.Drawing.Point(41, 7);
            this.btnInputSagyo.Name = "btnInputSagyo";
            this.btnInputSagyo.Size = new System.Drawing.Size(120, 35);
            this.btnInputSagyo.TabIndex = 24;
            this.btnInputSagyo.Text = "登録（I）";
            this.btnInputSagyo.UseVisualStyleBackColor = true;
            // 
            // btnDelSagyo
            // 
            this.btnDelSagyo.Enabled = false;
            this.btnDelSagyo.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDelSagyo.Location = new System.Drawing.Point(163, 7);
            this.btnDelSagyo.Name = "btnDelSagyo";
            this.btnDelSagyo.Size = new System.Drawing.Size(120, 35);
            this.btnDelSagyo.TabIndex = 25;
            this.btnDelSagyo.Text = "削除（D）";
            this.btnDelSagyo.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(3, 533);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1143, 50);
            this.panel7.TabIndex = 2602;
            // 
            // dgvPrsJisseki
            // 
            this.dgvPrsJisseki.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPrsJisseki.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPrsJisseki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrsJisseki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SunDay,
            this.MonDay,
            this.TuesDay,
            this.WednesDay,
            this.ThurthDay,
            this.FriDay,
            this.SaturDay,
            this.MstRowNum_Sunday,
            this.MstRowNum_MonDay,
            this.MstRowNum_TuesDay,
            this.MstRowNum_WednesDay,
            this.MstRowNum_TursDay,
            this.MstRowNum_FriDay,
            this.MstRowNum_SaturDay,
            this.StartTime});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPrsJisseki.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPrsJisseki.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrsJisseki.Location = new System.Drawing.Point(0, 0);
            this.dgvPrsJisseki.Name = "dgvPrsJisseki";
            this.dgvPrsJisseki.ReadOnly = true;
            this.dgvPrsJisseki.RowHeadersWidth = 55;
            this.dgvPrsJisseki.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPrsJisseki.RowTemplate.Height = 21;
            this.dgvPrsJisseki.Size = new System.Drawing.Size(1143, 508);
            this.dgvPrsJisseki.TabIndex = 2500;
            // 
            // SunDay
            // 
            this.SunDay.DataPropertyName = "SunDay";
            this.SunDay.HeaderText = "（日）";
            this.SunDay.MaxInputLength = 20;
            this.SunDay.Name = "SunDay";
            this.SunDay.ReadOnly = true;
            this.SunDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SunDay.Width = 135;
            // 
            // MonDay
            // 
            this.MonDay.DataPropertyName = "MonDay";
            this.MonDay.HeaderText = "（月）";
            this.MonDay.Name = "MonDay";
            this.MonDay.ReadOnly = true;
            this.MonDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MonDay.Width = 135;
            // 
            // TuesDay
            // 
            this.TuesDay.DataPropertyName = "TuesDay";
            this.TuesDay.HeaderText = "（火）";
            this.TuesDay.Name = "TuesDay";
            this.TuesDay.ReadOnly = true;
            this.TuesDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TuesDay.Width = 135;
            // 
            // WednesDay
            // 
            this.WednesDay.DataPropertyName = "WednesDay";
            this.WednesDay.HeaderText = "（水）";
            this.WednesDay.Name = "WednesDay";
            this.WednesDay.ReadOnly = true;
            this.WednesDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WednesDay.Width = 135;
            // 
            // ThurthDay
            // 
            this.ThurthDay.DataPropertyName = "ThurthDay";
            this.ThurthDay.HeaderText = "（木）";
            this.ThurthDay.Name = "ThurthDay";
            this.ThurthDay.ReadOnly = true;
            this.ThurthDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ThurthDay.Width = 135;
            // 
            // FriDay
            // 
            this.FriDay.DataPropertyName = "FriDay";
            this.FriDay.HeaderText = "（金）";
            this.FriDay.Name = "FriDay";
            this.FriDay.ReadOnly = true;
            this.FriDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FriDay.Width = 135;
            // 
            // SaturDay
            // 
            this.SaturDay.DataPropertyName = "SaturDay";
            this.SaturDay.HeaderText = "（土）";
            this.SaturDay.Name = "SaturDay";
            this.SaturDay.ReadOnly = true;
            this.SaturDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SaturDay.Width = 135;
            // 
            // MstRowNum_Sunday
            // 
            this.MstRowNum_Sunday.DataPropertyName = "MstRowNum_Sunday";
            this.MstRowNum_Sunday.HeaderText = "マスタ行番号（日）";
            this.MstRowNum_Sunday.Name = "MstRowNum_Sunday";
            this.MstRowNum_Sunday.ReadOnly = true;
            this.MstRowNum_Sunday.Visible = false;
            // 
            // MstRowNum_MonDay
            // 
            this.MstRowNum_MonDay.DataPropertyName = "MstRowNum_MonDay";
            this.MstRowNum_MonDay.HeaderText = "マスタ行番号（月）";
            this.MstRowNum_MonDay.Name = "MstRowNum_MonDay";
            this.MstRowNum_MonDay.ReadOnly = true;
            this.MstRowNum_MonDay.Visible = false;
            // 
            // MstRowNum_TuesDay
            // 
            this.MstRowNum_TuesDay.DataPropertyName = "MstRowNum_TuesDay";
            this.MstRowNum_TuesDay.HeaderText = "マスタ行番号（火）";
            this.MstRowNum_TuesDay.Name = "MstRowNum_TuesDay";
            this.MstRowNum_TuesDay.ReadOnly = true;
            this.MstRowNum_TuesDay.Visible = false;
            // 
            // MstRowNum_WednesDay
            // 
            this.MstRowNum_WednesDay.DataPropertyName = "MstRowNum_WednesDay";
            this.MstRowNum_WednesDay.HeaderText = "マスタ行番号（水）";
            this.MstRowNum_WednesDay.Name = "MstRowNum_WednesDay";
            this.MstRowNum_WednesDay.ReadOnly = true;
            this.MstRowNum_WednesDay.Visible = false;
            // 
            // MstRowNum_TursDay
            // 
            this.MstRowNum_TursDay.DataPropertyName = "MstRowNum_TursDay";
            this.MstRowNum_TursDay.HeaderText = "マスタ行番号（木）";
            this.MstRowNum_TursDay.Name = "MstRowNum_TursDay";
            this.MstRowNum_TursDay.ReadOnly = true;
            this.MstRowNum_TursDay.Visible = false;
            // 
            // MstRowNum_FriDay
            // 
            this.MstRowNum_FriDay.DataPropertyName = "MstRowNum_FriDay";
            this.MstRowNum_FriDay.HeaderText = "マスタ行番号（金）";
            this.MstRowNum_FriDay.Name = "MstRowNum_FriDay";
            this.MstRowNum_FriDay.ReadOnly = true;
            this.MstRowNum_FriDay.Visible = false;
            // 
            // MstRowNum_SaturDay
            // 
            this.MstRowNum_SaturDay.DataPropertyName = "MstRowNum_SaturDay";
            this.MstRowNum_SaturDay.HeaderText = "マスタ行番号（土）";
            this.MstRowNum_SaturDay.Name = "MstRowNum_SaturDay";
            this.MstRowNum_SaturDay.ReadOnly = true;
            this.MstRowNum_SaturDay.Visible = false;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "StartTime";
            this.StartTime.HeaderText = "開始時刻";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            this.StartTime.Visible = false;
            // 
            // PAF1010_PrsEntryBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1534, 941);
            this.Controls.Add(this.pnlFootor);
            this.Controls.Add(this.pnlExecute);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(950, 972);
            this.Name = "PAF1010_PrsEntryBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRS実績登録";
            this.pnlExecute.ResumeLayout(false);
            this.pnlHeader.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.pnlFootor.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.pnlKokyakuRdo.ResumeLayout(false);
            this.pnlKokyakuRdo.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlojectLooks)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlojectLooksSel)).EndInit();
            this.tabSagyoMaster.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsJisseki)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel pnlHeader;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button btnCreateKinmuhyo;
        public System.Windows.Forms.Button btnFolderOpen;
        public System.Windows.Forms.Button btnCreatePRS;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.TextBox textBox7;
        public System.Windows.Forms.TextBox textBox6;
        public System.Windows.Forms.TextBox textBox5;
        public System.Windows.Forms.TextBox textBox4;
        public System.Windows.Forms.TextBox textBox3;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.TabControl tabSagyoMaster;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.DataGridView dgvPlojectLooksSel;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNum_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaishoYM_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailSeq_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaishoYM_Disp_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn SagyoNaiyoFull_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName_Sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail_Sel;
        public System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.DataGridView dgvPlojectLooks;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaishoYM;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaishoYM_Disp;
        private System.Windows.Forms.DataGridViewTextBoxColumn SagyoNaiyoFull;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Panel panel13;
        public System.Windows.Forms.CheckBox chkKongetsuExist;
        public System.Windows.Forms.CheckBox chkZengetsuExist;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        public System.Windows.Forms.Button btnConClear;
        public System.Windows.Forms.Panel pnlKokyakuRdo;
        public System.Windows.Forms.RadioButton rdoSyanai;
        public System.Windows.Forms.RadioButton rdoIraimoto;
        public System.Windows.Forms.RadioButton rdoNothing;
        public System.Windows.Forms.TextBox txtKokyakuName;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cmbGyomuKbn;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSagyoName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtProjectCode;
        public System.Windows.Forms.RadioButton rdoFutokutei;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.GroupBox grpInput;
        public System.Windows.Forms.ComboBox cmbBushoName;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.MonthCalendar mcSelectedDate;
        public System.Windows.Forms.ComboBox cmbEmploeeName;
        public System.Windows.Forms.Label lblEmploeeName;
        public System.Windows.Forms.TextBox txtEmploeeId;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.Button btnDelSagyo;
        public System.Windows.Forms.Button btnInputSagyo;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvPrsJisseki;
        private System.Windows.Forms.DataGridViewTextBoxColumn SunDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MonDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn TuesDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn WednesDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThurthDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn FriDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaturDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_Sunday;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_MonDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_TuesDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_WednesDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_TursDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_FriDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn MstRowNum_SaturDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
    }
}

