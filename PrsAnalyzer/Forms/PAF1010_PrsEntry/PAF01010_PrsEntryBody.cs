﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF1010_PrsEntryBody : Form {
        public PAF1010_PrsEntryBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF1010_PrsEntryFormLogic(this, loginInfo);
        }
    }
}
