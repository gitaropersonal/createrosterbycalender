﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Logic.Kinmuhyo;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Const.Kinmuhyo;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF1010_PrsEntryFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF1010_PrsEntryBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// デフォルト勤務表パス
        /// </summary>
        private readonly string _DEFAULT_KINMUHYO_PATH = Path.Combine(Environment.CurrentDirectory, "Template");
        /// <summary>
        /// 勤務表作成パス
        /// </summary>
        private readonly string _OUTPUT_KINMUHYO_PATH = ConfigUtil.LoadOutputFolder();
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private static string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// エクスプローラのexe名
        /// </summary>
        private const string _FILE_NAME_EXPLORE_EXE = "EXPLORER.EXE";
        /// <summary>
        /// Service（社員マスタ）
        /// </summary>
        private PAF3010_MstEmploeeService _MST_EMPLOEE_SERVICE = new PAF3010_MstEmploeeService();
        /// <summary>
        /// 祝日リスト
        /// </summary>
        private Dictionary<DateTime, string> _HOLIDAYS;

        /// <summary>
        /// PRS実績グリッドの行・列サイズ
        /// </summary>
        private const int _MIN_ROW_HEIGTH = 18;
        private const int _DEFAULT_ROW_HEIGTH = 33;
        private const int _MAX_ROW_HEIGTH = 82;
        private const int _MIN_COL_WIDTH = 60;
        private const int _MAX_COL_WIDTH = 270;
        private const int _DGV_CALENDER_WIDTH = 72;
        private static int _CURR_ROW_HEIGHT = 40;
        private const float _MIN_FONT_SIZE = 8;

        /// <summary>
        /// PRS実績グリッドの行・列Index
        /// </summary>
        private const int _COL_IDX_SATURDAY = 6;
        private const int _ROW_IDX_REST_START_TOP = 11;
        private const int _ROW_IDX_REST_START_BOTTOM = 12;
        private const int _ROW_IDX_WORK_START_TOP = 5;
        private const int _ROW_IDX_WORK_END_BOTTOM = 22;

        /// <summary>
        /// PRS実績グリッドの特定の値
        /// </summary>
        private const int _GRID_START_TIME_H = 6;
        private const int _SGRID_TART_TIME_M = 30;
        private const int _WORK_START_TIME_H = 9;
        private const int _CORE_TIME_BORDER_WIDTH = 3;

        /// <summary>
        /// 日付選択関連の変数
        /// </summary>
        private DateTime _SELECTED_DATE = DateTime.Now;
        private const string _COL_NAME_ADD_SEL = "_Sel";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF1010_PrsEntryFormLogic(PAF1010_PrsEntryBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 画面サイズ変更時
            _Body.SizeChanged += (s, e) => {
                // 列幅調整
                EditGridColWidth();
            };
            // 開くボタン押下
            _Body.btnFolderOpen.Click += (s, e) => {
                string fileName = _Body.mcSelectedDate.SelectionStart.ToString(Formats._FORMAT_DATE_YYYYMM_NENTSUKI);
                string folderPath = Path.Combine(_OUTPUT_KINMUHYO_PATH, fileName);
                CommonUtil.CreateFolder(folderPath);
                Process.Start(_FILE_NAME_EXPLORE_EXE, folderPath);
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // PRS作成ボタン押下
            _Body.btnCreatePRS.Click += (s, e) => {
                CreateEvent(Enums.CreateKinmuhyoType.PRS);
            };
            // 勤務表作成ボタン押下
            _Body.btnCreateKinmuhyo.Click += (s, e) => {
                CreateEvent(Enums.CreateKinmuhyoType.KINMUHYO);
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // 登録ボタン押下
            _Body.btnInputSagyo.Click += (s, e) => {
                BtnInputClickEvent();
            };
            // 削除ボタン押下
            _Body.btnDelSagyo.Click += (s, e) => {
                BtnDelClickEvent();
            };
            // 条件クリアボタン押下
            _Body.btnConClear.Click += (s, e) => {

                // プロジェクト検索条件初期化
                InitSearchProjectLooksCondition();
                
                // グリッドに作業内容をセット
                SetGridValProjectLooksProxy(false);
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // PRS実績グリッド行サイズ変更
            _Body.dgvPrsJisseki.RowHeightChanged += DgvRowHeightChangedEvent;
            // PRS実績グリッド列サイズ変更
            _Body.dgvPrsJisseki.ColumnWidthChanged += DgvColumnWidthChangedEvent;
            // PRS実績グリッドCellPainting
            _Body.dgvPrsJisseki.CellPainting += (s, e) => {
                DgvCalenderCellPainting(s, e);
            };
            //TabControlのSelectingイベントハンドラ
            _Body.tabSagyoMaster.Selecting += (s, e) => {
                if (e.TabPageIndex == 1) {
                    e.Cancel = true;
                }
            };
            // 作業一覧グリッドCellPainting
            _Body.dgvPlojectLooksSel.CellPainting += (s, e) => {
                // 作業一覧の背景色をセット
                PaintCellProjectLooks();
            };
            // 社員名SelectionChangeCommitted
            _Body.cmbEmploeeName.SelectedValueChanged += (s, e) => {
                SelectionChangeCommited_EmploeeName();
            };
            // テキストボックスValidated
            _Body.txtEmploeeId.Validated += (s, e) => {
                LostFocus_EmploeeId();
            };
            // グリッド行番号描画
            _Body.dgvPlojectLooksSel.RowPostPaint += (s, e) => {
                GridRowUtil<ProjectLooksGridDto>.SetRowNum(s, e);
            };
            // グリッド行番号描画（日付）
            _Body.dgvPrsJisseki.RowPostPaint += (s, e) => {
                WriteGridRowHeaderTime(s, e);
            };
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.T:
                        _Body.mcSelectedDate.Focus();
                        break;
                    case Keys.I:
                        _Body.btnInputSagyo.PerformClick();
                        _Body.dgvPrsJisseki.Focus();
                        break;
                    case Keys.D:
                        _Body.btnDelSagyo.PerformClick();
                        _Body.dgvPrsJisseki.Focus();
                        break;
                    case Keys.M:
                        _Body.dgvPlojectLooksSel.Focus();
                        break;
                    case Keys.C:
                        _Body.btnConClear.PerformClick();
                        _Body.btnConClear.Focus();
                        break;
                    case Keys.J:
                        _Body.dgvPrsJisseki.Focus();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.F:
                        _Body.btnFolderOpen.Focus();
                        _Body.btnFolderOpen.PerformClick();
                        break;
                    case Keys.P:
                        _Body.btnCreatePRS.Focus();
                        _Body.btnCreatePRS.PerformClick();
                        break;
                    case Keys.K:
                        _Body.btnCreateKinmuhyo.Focus();
                        _Body.btnCreateKinmuhyo.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// 対象日ValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void DtpDateSelected(object s, EventArgs e) {

            // イベント一旦退避
            _Body.mcSelectedDate.DateChanged -= DtpDateSelected;

            var tgtDate = _Body.mcSelectedDate.SelectionStart;
            if(KinmuhyoUtil.JudgeIsSameYMD(tgtDate, DateTime.Now)) {
                _SELECTED_DATE = new DateTime(tgtDate.Year, tgtDate.Month, tgtDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
            }
            else {
                _SELECTED_DATE = new DateTime(tgtDate.Year, tgtDate.Month, tgtDate.Day).AddHours(_WORK_START_TIME_H);
            }
            if (_SELECTED_DATE.Hour < TimeConst._PRS_WORKSTART_TIME_HH) {
                _SELECTED_DATE = _SELECTED_DATE.AddDays(-1);
            }
            // 隠しコントロールに対象日をセット
            SetDateInvisibleTxt();

            // 日付範囲設定
            SetDateRange();

            // グリッドに作業一覧セット
            SetGridValProjectLooksProxy();

            // イベント回復
            _Body.mcSelectedDate.DateChanged += DtpDateSelected;
        }
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 社員IDロストフォーカス
        /// </summary>
        private void LostFocus_EmploeeId() {
            if (!ExecuteLostFocus()) {
                return;
            }
            // 背景色初期化
            InitColor();

            // Validate
            if (!ValidateTantoIdSelected()) {

                // テキストクリア
                _Body.txtEmploeeId.Text = string.Empty;
                _Body.cmbEmploeeName.Text = string.Empty;

                // 作成ボタン使用可否切り替え
                SwitchEnable_BtnCrete(false);

                // グリッドに作業一覧セット
                SetGridValProjectLooksProxy();
                return;
            }
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
                IsContaiDeleted = false,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);
            if (mstData.Any()) {
                _Body.cmbEmploeeName.Text = mstData.First(n => n.TantoID == _Body.txtEmploeeId.Text).TantoName;

                // 作成ボタン使用可否切り替え
                SwitchEnable_BtnCrete(true);
            }
            // グリッドに作業一覧セット
            SetGridValProjectLooksProxy();
        }
        /// <summary>
        /// 社員名SelectionChangeCommitted
        /// </summary>
        private void SelectionChangeCommited_EmploeeName() {
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, new LoadMstTantoCon());
            if (!mstData.Exists(n => n.TantoName == _Body.cmbEmploeeName.Text)) {

                // テキストクリア
                _Body.txtEmploeeId.Text = string.Empty;

            } else {

                _Body.txtEmploeeId.Text = mstData.First(n => n.TantoName == _Body.cmbEmploeeName.Text).TantoID;

            }
            // グリッドに作業一覧セット
            SetGridValProjectLooksProxy();
        }
        /// <summary>
        /// 登録ボタン押下イベント
        /// </summary>
        private void BtnInputClickEvent() {
            if (_Body.dgvPlojectLooksSel.SelectedRows == null || _Body.dgvPlojectLooksSel.SelectedRows.Count == 0) {
                DialogUtil.ShowErroMsg($"作業一覧の{Messages._MSG_ERROR_SELECTED_ROW_NOTHING}");
                return;
            }
            var entities = new List<PX02SagyoMeisaiEntity>();
            var dtoLooks = new SagyoNaiyoLooksGridDto();
            var dtoCalender = new CalenderGridDto();
            var sysDate = DateTime.Now;
            foreach (DataGridViewCell c in _Body.dgvPrsJisseki.SelectedCells) {
                if (c.RowIndex < 1 || c.RowIndex == _ROW_IDX_REST_START_TOP || c.RowIndex == _ROW_IDX_REST_START_BOTTOM) {
                    continue;
                }
                // 作業明細dto
                DataGridViewRow rCalender = _Body.dgvPrsJisseki.Rows[c.RowIndex];
                int currMasterRowIdx = _Body.dgvPlojectLooksSel.SelectedRows[0].Index;
                DataGridViewRow rLooks = _Body.dgvPlojectLooksSel.Rows[currMasterRowIdx];
                DateTime startTime = TypeConversionUtil.ToDateTime(GetStartTimeFromCellIdx(c), Formats._FORMAT_DATE_YYYYMMDDHHMM_SLASH);
                var entity = new PX02SagyoMeisaiEntity() {
                    TantoID = _Body.txtEmploeeId.Text,
                    ProjectCD = StringUtil.TostringNullForbid(rLooks.Cells[$"{nameof(dtoLooks.ProjectCD)}{_COL_NAME_ADD_SEL}"].Value),
                    KouteiSeq = StringUtil.TostringNullForbid(rLooks.Cells[$"{nameof(dtoLooks.KouteiSeq)}{_COL_NAME_ADD_SEL}"].Value),
                    DetailSeq = StringUtil.TostringNullForbid(rLooks.Cells[$"{nameof(dtoLooks.DetailSeq)}{_COL_NAME_ADD_SEL}"].Value),
                    TaishoYMD = GetTaishoDateFromInvisibleTxt(c.ColumnIndex),
                    StartTime = startTime,
                    EndTime = startTime.AddMinutes(TimeConst._MINUTE_AN_HOUR / 2),
                    SagyoTimeH = TimeConst._MINUTE_AN_HOUR / 2,
                    AddDate = sysDate,
                    UpdDate = sysDate,
                };
                // 対象日が異なる場合は追加しない
                string taishoYM = TypeConversionUtil.ToDateTime(entity.TaishoYMD).ToString(Formats._FORMAT_DATE_YYYYMM);
                if (_SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM) != taishoYM) {
                    continue;
                }
                // グリッドセルに値セット
                c.Value = _Body.dgvPlojectLooksSel.SelectedRows[0].Cells[nameof(dtoLooks.SagyoNaiyoFull) + _COL_NAME_ADD_SEL].Value;
                rCalender.Cells[nameof(CalenderGridDto.StartTime)].Value = GetStartTimeFromCellIdx(c);

                // グリッドにマスタ行の行インデックスをセット
                int tgtRowNum = TypeConversionUtil.ToInteger(rLooks.Cells[nameof(dtoLooks.RowNum) + _COL_NAME_ADD_SEL].Value);
                _Body.dgvPrsJisseki.Rows[c.RowIndex].Cells[c.ColumnIndex + TimeConst._DAY_COUNT_WEEK].Value = tgtRowNum;

                // 登録対象に追加
                entities.Add(entity);
            }
            // 行追加・削除後のセル編集
            EditCalenderCell_AfterAddDel();

            // 作業明細登録・更新
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => InsOrUpdSagyoMeisai(entities)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
        }
        /// <summary>
        /// 作業明細登録・更新
        /// </summary>
        /// <param name="Entities"></param>
        private void InsOrUpdSagyoMeisai(List<PX02SagyoMeisaiEntity> Entities) {
            try {
                var service = new PAF1010_PrsEntryService();
                foreach (var entity in Entities) {

                    // 登録済か？
                    if (service.JudgeIsExistRec(_DB_CONN_STRING, entity)) {

                        // 更新
                        service.UpdSagyoMeisai(_DB_CONN_STRING, entity);
                        continue;
                    }
                    // 登録
                    service.InsertSagyoMeisai(_DB_CONN_STRING, entity);
                }
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// 削除ボタン押下イベント
        /// </summary>
        private void BtnDelClickEvent() {
            if (_Body.dgvPrsJisseki.SelectedCells == null || _Body.dgvPrsJisseki.SelectedCells.Count == 0) {
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => DelSagyoMeisai()));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
        }
        /// <summary>
        /// 作業明細削除イベント
        /// </summary>
        private void DelSagyoMeisai() {
            var service = new PAF1010_PrsEntryService();
            try {
                foreach (DataGridViewCell c in _Body.dgvPrsJisseki.SelectedCells) {
                    if (c.RowIndex < 1) {
                        continue;
                    }
                    // Entity取得
                    var delEntity = new PX02SagyoMeisaiEntity() {
                        TaishoYMD = GetTaishoDateFromInvisibleTxt(c.ColumnIndex),
                        TantoID = _Body.txtEmploeeId.Text,
                        StartTime = TypeConversionUtil.ToDateTime(GetStartTimeFromCellIdx(c)),
                    };

                    // 対象日が異なる場合は追加しない
                    string taishoYM = TypeConversionUtil.ToDateTime(delEntity.TaishoYMD).ToString(Formats._FORMAT_DATE_YYYYMM);
                    if (_SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM) != taishoYM) {
                        continue;
                    }
                    // PRS実績に値セット
                    _Body.dgvPrsJisseki.Rows[c.RowIndex].Cells[c.ColumnIndex + TimeConst._DAY_COUNT_WEEK].Value = -1;

                    // 削除
                    service.DelSagyoMeisai(_DB_CONN_STRING, delEntity);
                }
                // 行追加・削除後のセル編集
                EditCalenderCell_AfterAddDel();
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// 行追加・削除後のセル編集
        /// </summary>
        private void EditCalenderCell_AfterAddDel() {
            if (_Body.dgvPrsJisseki.SelectedCells == null || _Body.dgvPrsJisseki.SelectedCells.Count == 0) {
                return;
            }
            foreach (DataGridViewCell c in _Body.dgvPrsJisseki.SelectedCells) {
                foreach (DataGridViewRow r in _Body.dgvPrsJisseki.Rows) {
                    if (r.Index == 0) {
                        continue;
                    }
                    int tgtDataColIdx = c.ColumnIndex + TimeConst._DAY_COUNT_WEEK;
                    int currMasterRowIdx = TypeConversionUtil.ToInteger(r.Cells[tgtDataColIdx].Value, -1);

                    // 行追加・削除後のセル表示値編集
                    EditCalenderCellDisp_AfterAddDel(currMasterRowIdx, r.Cells[c.ColumnIndex]);
                }
            }
            // PRS実績グリッド背景色セット
            PaintCellPrsJisseki();
        }
        /// <summary>
        /// 行追加・削除後のセル表示値編集
        /// </summary>
        /// <param name="currMasterRowIdx"></param>
        /// <param name="Cell"></param>
        private void EditCalenderCellDisp_AfterAddDel(int currMasterRowIdx, DataGridViewCell Cell) {

            int tgtDataColIdx = Cell.ColumnIndex + TimeConst._DAY_COUNT_WEEK;
            int beforeMasterRowIdx = TypeConversionUtil.ToInteger(_Body.dgvPrsJisseki.Rows[Cell.RowIndex - 1].Cells[tgtDataColIdx].Value, -1);
            if (currMasterRowIdx < 0) {

                // 作業一覧に表示されている作業が登録されていない場合はクリア
                Cell.Value = string.Empty;
                return;
            }
            if (-1 < beforeMasterRowIdx && currMasterRowIdx == beforeMasterRowIdx) {

                // １つ前のセルと同じ作業の場合は表示値をクリア
                Cell.Value = string.Empty;
                return;
            }
            var dto = new SagyoNaiyoLooksGridDto();
            string cellName = nameof(dto.SagyoNaiyoFull);
            Cell.Value = StringUtil.TostringNullForbid(_Body.dgvPlojectLooks.Rows[currMasterRowIdx].Cells[cellName].Value);
        }
        /// <summary>
        /// 作成ボタン押下イベント
        /// </summary>
        /// <param name="kinmuhyoType"></param>
        private void CreateEvent(Enums.CreateKinmuhyoType kinmuhyoType) {

            // 社員情報取得
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
            };
            var tgtEmp = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con).First();

            // 対象年
            var dtStart = new DateTime(_SELECTED_DATE.Year, _SELECTED_DATE.Month, 1);

            // 勤務表の名称
            string kinmuhyoFileNameR = $"{_SELECTED_DATE.ToString("M月の")}{Enums.DicKinmuhyoName.First(n => n.Key == kinmuhyoType).Value}";

            // 勤務表作成
            switch (kinmuhyoType) {
                case Enums.CreateKinmuhyoType.PRS:
                    CreateEventPRS(kinmuhyoFileNameR, tgtEmp, dtStart);
                    break;
                case Enums.CreateKinmuhyoType.KINMUHYO:
                    CreateEvent(kinmuhyoFileNameR, tgtEmp, dtStart);
                    break;
            }
        }
        #endregion

        #region グリッド
        /// <summary>
        /// CellPainting（PRS実績）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void DgvCalenderCellPainting(object s, DataGridViewCellPaintingEventArgs e) {

            if (_Body.dgvPrsJisseki.Rows == null || _Body.dgvPrsJisseki.Rows.Count == 0) {
                return;
            }
            // セルの下側の境界線を「境界線なし」に設定
            e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;

            // 1行目や列ヘッダ、行ヘッダの場合は何もしない
            if (e.RowIndex < 1 || e.ColumnIndex < 0) {
                return;
            }
            if (e.RowIndex % 2 == 0) {
                // セルの上側の境界線を「境界線なし」に設定
                e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            }
            else {
                // セルの上側の境界線を既定の境界線に設定
                e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.Single;
            }
            // コアタイムの時刻を太線で強調
            _Body.dgvPrsJisseki.Rows[_ROW_IDX_WORK_START_TOP - 1].DividerHeight = _CORE_TIME_BORDER_WIDTH;
            _Body.dgvPrsJisseki.Rows[_ROW_IDX_WORK_END_BOTTOM - 1].DividerHeight = _CORE_TIME_BORDER_WIDTH;
        }
        /// <summary>
        /// グリッドの行すべての高さを調整
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void DgvRowHeightChangedEvent(object s, DataGridViewRowEventArgs e) {

            // イベントを一旦解除
            _Body.dgvPrsJisseki.RowHeightChanged -= DgvRowHeightChangedEvent;

            
            if (_Body.dgvPrsJisseki.Rows != null && _Body.dgvPrsJisseki.Rows.Count == 0) {
                return;
            }
            int currRowIdx = e.Row.Index;
            int currRowHeight = e.Row.Height;
            if (_MAX_ROW_HEIGTH < currRowHeight) {
                currRowHeight = _MAX_ROW_HEIGTH;
            }
            if (currRowHeight < _MIN_ROW_HEIGTH) {
                currRowHeight = _MIN_ROW_HEIGTH;
            }
            foreach (DataGridViewRow r in _Body.dgvPrsJisseki.Rows) {
                r.Height = currRowHeight;
                float fontSize = GetRowHFontSize(r);
                r.DefaultCellStyle.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, fontSize, FontStyle.Regular);
            }
            _CURR_ROW_HEIGHT = currRowHeight;

            // イベントを回復
            _Body.dgvPrsJisseki.RowHeightChanged += DgvRowHeightChangedEvent;

        }
        /// <summary>
        /// 行ヘッダーのフォントサイズ取得
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        private float GetRowHeaderFontSize(DataGridViewRow r) {
            float fontSize = ControlUtil._DEFAULT_FONT_SIZE;
            if (r.Height < _DEFAULT_ROW_HEIGTH) {
                fontSize = _MIN_FONT_SIZE;
            }
            return fontSize;
        }
        /// <summary>
        /// 行のフォントサイズ取得
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        private float GetRowHFontSize(DataGridViewRow r) {
            float fontSize = ControlUtil._DEFAULT_FONT_SIZE;
            int rowHeight = r.Height;
            if (rowHeight <= _MAX_ROW_HEIGTH - 25) {
                fontSize = _MIN_FONT_SIZE;
            }
            return fontSize;
        }
        /// <summary>
        /// グリッドの行すべての高さを調整
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void DgvColumnWidthChangedEvent(object s, DataGridViewColumnEventArgs e) {

            // イベントを一旦解除
            _Body.dgvPrsJisseki.ColumnWidthChanged -= DgvColumnWidthChangedEvent;

            int currColIdx = e.Column.Index;
            int currColHeight = e.Column.Width;
            if (_MAX_COL_WIDTH < currColHeight) {
                currColHeight = _MAX_COL_WIDTH;
            }
            if (currColHeight < _MIN_COL_WIDTH) {
                currColHeight = _MIN_COL_WIDTH;
            }
            foreach (DataGridViewColumn c in _Body.dgvPrsJisseki.Columns) {
                c.Width = currColHeight;
            }
            // イベントを回復
            _Body.dgvPrsJisseki.ColumnWidthChanged += DgvColumnWidthChangedEvent;

        }
        /// <summary>
        /// 作業一覧検索条件編集
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SearchConChange(object s, EventArgs e) {
            if (!ExecuteLostFocus()) {
                return;
            }
            SetGridValProjectLooksProxy(false);
        }
        /// <summary>
        /// グリッドに作業一覧セット（Proxy）
        /// </summary>
        /// <param name="isRefleshPrsJisseki"></param>
        private void SetGridValProjectLooksProxy(bool isRefleshPrsJisseki = true) {

            // カーソルを待機カーソルに変更
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            try {
                // グリッドに値セット（プロジェクト一覧）
                SetGridValProjectLooks();

                if (isRefleshPrsJisseki) {

                    // 作業一覧クリア
                    _Body.dgvPrsJisseki.Rows.Clear();

                    // グリッド初期化（PRS実績）
                    InitGridPrsJisseki();

                    // グリッドに値セット（PRS実績）
                    SetMeisaiOnGridProxy(_Body.txtEmploeeId.Text, _Body.textBox1.Text, _Body.textBox7.Text);

                    // グリッド背景色セット（PRS実績）
                    PaintCellPrsJisseki();
                }
                // ボタン使用可否切替
                SwitchEnable_BtnCrete(true);

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex.Message);
            }
            finally {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// グリッドに作業一覧セット
        /// </summary>
        private void SetGridValProjectLooks() {

            // 検索条件作成
            var conHidden = new LoadProjectLooksConDto() {
                TantoID = _Body.txtEmploeeId.Text,
                TaishoYM = _SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM),
                PBushoKbn = ProjectConst.PBushoKbn.NONE,
            };
            // 作業一覧取得（非表示）
            var sagyoNaiyoList = new PAF1010_PrsEntryService().LoadProjectLooks(_DB_CONN_STRING, conHidden);
            if (!sagyoNaiyoList.Any()) {
                _Body.dgvPlojectLooks.Rows.Clear();
                _Body.dgvPlojectLooksSel.Rows.Clear();
                return;
            }
            // 取得した作業をグリッドにセット
            var gridDataHidden = new BindingList<SagyoNaiyoLooksGridDto>();
            
            string tgtTaishoYM = string.Empty;
            int rowNum = 0;
            foreach (var data in sagyoNaiyoList) {
                data.RowNum = rowNum;
                gridDataHidden.Add(data);
                rowNum++;
            }
            // 作業一覧取得（表示）
            var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked);
            string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked, _Body.txtKokyakuName.Text);

            // 検索条件作成
            var conVisible = new LoadProjectLooksConDto() {
                TantoID = _Body.txtEmploeeId.Text,
                TaishoYM = _SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM),
                ProjectCD = _Body.txtProjectCode.Text,
                ProjectName = _Body.txtSagyoName.Text,
                GyomuKbn = _Body.cmbGyomuKbn.Text,
                KokyakuKbn = kokyakuKbn,
                KokyakuName = kokyakuName,
                IsExistZengetsuJisseki = _Body.chkZengetsuExist.Checked,
                IsExistKongetsuJisseki = _Body.chkKongetsuExist.Checked,
                IsContainDeleted = true,
                PBushoKbn = (ProjectConst.PBushoKbn)LoginInfo.PBushoKbn
            };
            // 作業一覧取得
            var sagyoNaiyoListDisp = new PAF1010_PrsEntryService().LoadProjectLooks(_DB_CONN_STRING, conVisible);
            var gridDataShow = new BindingList<SagyoNaiyoLooksGridDto>();
            foreach (var data in sagyoNaiyoListDisp) {
                data.RowNum = gridDataHidden.Where(n => n.ProjectCD == data.ProjectCD && n.KouteiSeq == data.KouteiSeq && n.DetailSeq == data.DetailSeq).First().RowNum;
                gridDataShow.Add(data);
            }
            _Body.dgvPlojectLooks.DataSource = gridDataHidden;
            _Body.dgvPlojectLooksSel.DataSource = gridDataShow;

            // 作業一覧の背景色をセット
            PaintCellProjectLooks();
        }
        /// <summary>
        /// グリッドに作業明細セット（Proxy）
        /// </summary>
        /// <param name="emploeeId"></param>
        /// <param name="strStartDate"></param>
        /// <param name="strEndDate"></param>
        private void SetMeisaiOnGridProxy(string emploeeId, string strStartDate, string strEndDate) {

            // 登録済み作業明細検索
            var meisaiList = new PAF1010_PrsEntryService().SearchSagyoMeisai(_DB_CONN_STRING, emploeeId, strStartDate, strEndDate);

            // 作業一覧を取得
            var sagyoMaster = new List<SagyoNaiyoLooksGridDto>();
            foreach (DataGridViewRow r in _Body.dgvPlojectLooks.Rows) {
                var dto = GridRowUtil<SagyoNaiyoLooksGridDto>.GetRowModel(r);
                sagyoMaster.Add(dto);
            }
            // 作業明細をグリッドにセット
            var strSelectedDate = _SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM);
            SetMeisaiOnGrid(meisaiList, sagyoMaster, strSelectedDate);
        }
        /// <summary>
        /// グリッドに作業明細セット
        /// </summary>
        /// <param name="meisaiList"></param>
        /// <param name="sagyoMaster"></param>
        /// <param name="strSelectedDate"></param>
        private void SetMeisaiOnGrid(List<PX02SagyoMeisaiEntity> meisaiList, List<SagyoNaiyoLooksGridDto> sagyoMaster, string strSelectedDate) {

            var editTgtCellPairList = new List<KeyValuePair<DataGridViewCell, int>>();
            foreach (var meisai in meisaiList) {
                int tgtColIdx = GetColIdxFromInvisiTxt(TypeConversionUtil.ToDateTime(meisai.TaishoYMD, Formats._FORMAT_DATE_YYYYMMDD_SLASH));
                int tgtRowIdx = GetRowIdxFromStartTime(meisai.TaishoYMD, meisai.StartTime);
                string shogoTaishoYM = TypeConversionUtil.ToDateTime(meisai.TaishoYMD).ToString(Formats._FORMAT_DATE_YYYYMM);
                var tgtmaster = sagyoMaster.FirstOrDefault(n => n.TaishoYM == shogoTaishoYM
                                                             && n.ProjectCD == meisai.ProjectCD
                                                             && n.KouteiSeq == meisai.KouteiSeq
                                                             && n.DetailSeq == meisai.DetailSeq);
                if (tgtmaster == null) {

                    // 作業一覧に存在しない場合はスルー
                    continue;
                }
                var strTaishoYMD = TypeConversionUtil.ToDateTime(meisai.TaishoYMD).ToString(Formats._FORMAT_DATE_YYYYMM);
                if (strTaishoYMD != strSelectedDate) {

                    // 対象日と異なる年月の場合はスルー
                    continue;
                }
                _Body.dgvPrsJisseki.Rows[tgtRowIdx].Cells[tgtColIdx].Value = tgtmaster.SagyoNaiyoFull;
                _Body.dgvPrsJisseki.Rows[tgtRowIdx].Cells[tgtColIdx + TimeConst._DAY_COUNT_WEEK].Value = sagyoMaster.IndexOf(tgtmaster);
                int idx = sagyoMaster.IndexOf(tgtmaster);
                editTgtCellPairList.Add(new KeyValuePair<DataGridViewCell, int>(_Body.dgvPrsJisseki.Rows[tgtRowIdx].Cells[tgtColIdx], idx));
            }
            foreach (var pair in editTgtCellPairList) {

                // 行追加・削除後のセル表示値編集
                EditCalenderCellDisp_AfterAddDel(pair.Value, pair.Key);

            }
        }
        /// <summary>
        /// グリッド行番号描画（日付）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void WriteGridRowHeaderTime(object s, DataGridViewRowPostPaintEventArgs e) {
            if (_Body.dgvPrsJisseki.Rows == null || _Body.dgvPrsJisseki.Rows.Count == 0) {
                return;
            }
            DataGridViewRow r = _Body.dgvPrsJisseki.Rows[0];
            var startTime = new DateTime(_Body.mcSelectedDate.SelectionStart.Year
                                       , _Body.mcSelectedDate.SelectionStart.Month
                                       , _Body.mcSelectedDate.SelectionStart.Day).AddHours(7);

            GridRowUtil<CalenderGridDto>.SetRowTime(s, e, startTime, TimeConst._MINUTE_AN_HOUR / 2, GetRowHeaderFontSize(r));
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate（社員ID選択）
        /// </summary>
        private bool ValidateTantoIdSelected() {

            // 背景色初期化
            InitColor();

            // 社員IDチェック
            if (string.IsNullOrEmpty(_Body.txtEmploeeId.Text)) {
                return false;
            }
            // 社員マスタロード
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
                IsContaiDeleted = false,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);

            // 存在チェック
            if (!mstData.Any()) {
                _Body.txtEmploeeId.BackColor = Color.Red;
                _Body.txtEmploeeId.Focus();
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_MST);
                return false;
            }
            return true;
        }
        /// <summary>
        /// <summary>
        /// Validate（作成処理・PRS）
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="tgtEmp"></param>
        /// <returns></returns>
        private bool ValidateCreateRoster_PRS(MX03TantoEntity tgtEmp, DateTime dtStart) {

            // デフォルトPRSの存在チェック
            string defaultPrsPath = Path.Combine(_DEFAULT_KINMUHYO_PATH, CellsPRS._FILE_PRS_NAME);
            if (!File.Exists(defaultPrsPath)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_TEMPLETE);
                return false;
            }
            // 勤務表を開きっぱなしにしていないかチェック
            string prsFullPath = KinmuhyoUtil.GetFullPathKinmuhyo(_OUTPUT_KINMUHYO_PATH, tgtEmp, dtStart, Enums.CreateKinmuhyoType.PRS);
            if (!CommonUtil.ValidateFileOpened(prsFullPath)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_FILE_ALREADY_OPENDE);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Validate（作成処理・TYPE2）
        /// </summary>
        /// <param name="tgtEmp"></param>
        /// <param name="dtStart"></param>
        /// <returns></returns>
        private bool ValidateCreateRoster_TYPE2(MX03TantoEntity tgtEmp, DateTime dtStart) {

            // デフォルト勤務表の存在チェック
            string defaultKinmuhyoPath = Path.Combine(_DEFAULT_KINMUHYO_PATH, CellsKinmuhyo._DEFAULT_KINMUHYO_NAME);
            if (!File.Exists(defaultKinmuhyoPath)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_TEMPLETE);
                return false;
            }
            // 勤務表を開きっぱなしにしていないかチェック
            string kinmuhyoFullPath = KinmuhyoUtil.GetFullPathKinmuhyo(_OUTPUT_KINMUHYO_PATH, tgtEmp, dtStart, Enums.CreateKinmuhyoType.KINMUHYO);
            if (!CommonUtil.ValidateFileOpened(kinmuhyoFullPath)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_FILE_ALREADY_OPENDE);
                return false;
            }
            return true;
        }
        #endregion

        #region BussinessLogic
        /// <summary>
        /// 勤務表作成処理（PRS）
        /// </summary>
        /// <param name="kinmuhyoFileNameR"></param>
        /// <param name="tgtEmp"></param>
        /// <param name="dtStart"></param>
        private void CreateEventPRS(string kinmuhyoFileNameR, MX03TantoEntity tgtEmp, DateTime dtStart) {

            // Validate
            if (!ValidateCreateRoster_PRS(tgtEmp, dtStart)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(string.Format(Messages._MSG_CONFIRM_CREATE_SOMETHING, kinmuhyoFileNameR)) != DialogResult.OK) {
                return;
            }
            // カーソルを待機カーソルに変更
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // PRS作成
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => new CreatePrsLogic().Main(tgtEmp, dtStart)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 確認ダイアログ
            string msg = string.Format(Messages._MSG_FINISH_CREATE_SOMETHING, kinmuhyoFileNameR);
            if (DialogUtil.ShowInfoMsgOKCancel(msg) != DialogResult.OK) {
                return;
            }
            // PRSを開く
            string prsFullPath = KinmuhyoUtil.GetFullPathKinmuhyo(_OUTPUT_KINMUHYO_PATH, tgtEmp, dtStart, Enums.CreateKinmuhyoType.PRS);
            Process.Start(prsFullPath);

            // カーソルの回復
            Cursor.Current = Cursor.Current;
            
        }
        /// <summary>
        /// 勤務表作成処理
        /// </summary>
        /// <param name="kinmuhyoFileNameR"></param>
        /// <param name="tgtEmp"></param>
        /// <param name="dtStart"></param>
        private void CreateEvent(string kinmuhyoFileNameR, MX03TantoEntity tgtEmp, DateTime dtStart) {

            // Validate
            if (!ValidateCreateRoster_TYPE2(tgtEmp, dtStart)) {
                return;
            }
            // 確認ダイアログ
            string msg = string.Format(Messages._MSG_CONFIRM_CREATE_SOMETHING, kinmuhyoFileNameR);
            if (DialogUtil.ShowInfoMsgOKCancel(msg) != DialogResult.OK) {
                return;
            }
            // カーソルを待機カーソルに変更
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // 勤務表作成
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => new CreateKinmuhyoLogic().Main(tgtEmp, dtStart)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 確認ダイアログ
            msg = string.Format(Messages._MSG_FINISH_CREATE_SOMETHING, kinmuhyoFileNameR);
            if (DialogUtil.ShowInfoMsgOKCancel(msg) != DialogResult.OK) {
                return;
            }
            // 勤務表を開く
            string kinmuhyoFullPath = KinmuhyoUtil.GetFullPathKinmuhyo(_OUTPUT_KINMUHYO_PATH, tgtEmp, dtStart, Enums.CreateKinmuhyoType.KINMUHYO);
            Process.Start(kinmuhyoFullPath);

            // カーソルの回復
            Cursor.Current = Cursor.Current;
        }

        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 作業一覧検索イベントの追加・退避
            AddOrDelSearchConChange(false);

            // 隠しコントロールを非表示
            _Body.textBox1.Visible = false;
            _Body.textBox2.Visible = false;
            _Body.textBox3.Visible = false;
            _Body.textBox4.Visible = false;
            _Body.textBox5.Visible = false;
            _Body.textBox6.Visible = false;
            _Body.textBox7.Visible = false;
            
            // 背景色
            InitColor();

            // 対象日
            _SELECTED_DATE = DateTime.Now;
            if (_SELECTED_DATE.Hour < TimeConst._PRS_WORKSTART_TIME_HH) {
                _SELECTED_DATE = _SELECTED_DATE.AddDays(-1);
            }
            // イベント一旦退避
            _Body.mcSelectedDate.DateChanged -= DtpDateSelected;

            // 日付範囲設定
            SetDateRange();

            // イベント回復
            _Body.mcSelectedDate.DateChanged += DtpDateSelected;

            // 祝日リスト
            _HOLIDAYS = new MstCommonService().GetHolidays(_DB_CONN_STRING, DateTime.Now.Year);

            // 隠しコントロールに対象日をセット
            SetDateInvisibleTxt();

            // 部署名コンボボックス初期化
            InitBushoNameCombo();

            // 社員ID
            InitEmploeeNameCombo();

            // 社員名
            _Body.txtEmploeeId.Text = string.Empty;

            // プロジェクト検索条件初期化
            InitSearchProjectLooksCondition();

            // グリッド
            _Body.dgvPlojectLooks.Rows.Clear();
            _Body.dgvPlojectLooksSel.Rows.Clear();
            _Body.dgvPlojectLooksSel.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // フォーカスと列ヘッダテキストをセット
            InitFocusAndHeaderText();

            // 列の幅調整（PRS実績）
            EditGridRowColumnStyle();

            // フォーカス
            _Body.chkZengetsuExist.Focus();

            // 作成ボタン使用可否切り替え
            SwitchEnable_BtnCrete(false);

            // ログイン権限に応じた担当者ID・担当者名をセット
            _Body.txtEmploeeId.Text = LoginInfo.TantoID;
            _Body.cmbEmploeeName.Text = LoginInfo.TantoName;

            // グリッドに作業一覧セット
            if (LoginInfo.AuthorityKbn == (int)Enums.AuthorityKbn.Normal) {
                _Body.txtEmploeeId.Enabled = false;
                _Body.cmbEmploeeName.Enabled = false;
                _Body.cmbBushoName.Enabled = false;
            }
            // 行の高さ調整
            _CURR_ROW_HEIGHT = _DEFAULT_ROW_HEIGTH;

            // グリッド表示
            SetGridValProjectLooksProxy();
        }
        /// <summary>
        /// 社員名コンボボックス初期化
        /// </summary>
        private void InitEmploeeNameCombo() {
            _Body.cmbEmploeeName.Items.Clear();
            _Body.cmbEmploeeName.Items.Add(string.Empty);
            var con = new LoadMstTantoCon() {
                BushoCD = LoginInfo.BushoCD,
                IsContaiDeleted = false,
            };
            _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con).ForEach(data => _Body.cmbEmploeeName.Items.Add(data.TantoName));
            _Body.cmbEmploeeName.SelectedIndex = 0;
        }
        /// <summary>
        /// 部署名コンボボックス初期化
        /// </summary>
        private void InitBushoNameCombo() {

            // 初期化
            _Body.cmbBushoName.Items.Clear();

            // 条件作成
            var con = new MX06BushoEntity() {
                BushoCD = LoginInfo.BushoCD,
            };
            // マスタデータ取得
            var mstDatas = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, con, true);
            mstDatas.ForEach(data => _Body.cmbBushoName.Items.Add(data.BushoName));
            _Body.cmbBushoName.SelectedIndex = 0;
        }
        /// <summary>
        /// 作業一覧検索イベントの追加・退避
        /// </summary>
        /// <param name="isAddMode"></param>
        private void AddOrDelSearchConChange(bool isAddMode) {
            if (isAddMode) {
                // 前月実績ありチェックボックス編集
                _Body.chkZengetsuExist.CheckedChanged += SearchConChange;
                // 今月実績ありチェックボックス編集
                _Body.chkKongetsuExist.CheckedChanged += SearchConChange;
                // プロジェクトコードテキストボックス編集
                _Body.txtProjectCode.Validated += SearchConChange;
                // 作業内容テキストボックス編集
                _Body.txtSagyoName.Validated += SearchConChange;
                // 業務区分コンボボックス編集
                _Body.cmbGyomuKbn.SelectedIndexChanged += SearchConChange;
                // 選択なしラジオボタン選択編集
                _Body.rdoNothing.CheckedChanged += SearchConChange;
                // 依頼元ラジオボタン選択編集
                _Body.rdoIraimoto.CheckedChanged += SearchConChange;
                // 社内ラジオボタン選択編集
                _Body.rdoSyanai.CheckedChanged += SearchConChange;
                // 不特定客先ラジオボタン選択編集
                _Body.rdoFutokutei.CheckedChanged += SearchConChange;
                // 顧客名テキストボックス編集
                _Body.txtKokyakuName.Validated += SearchConChange;
                return;
            }
            // 前月実績ありチェックボックス編集
            _Body.chkZengetsuExist.CheckedChanged -= SearchConChange;
            // 今月実績ありチェックボックス編集
            _Body.chkKongetsuExist.CheckedChanged -= SearchConChange;
            // プロジェクトコードテキストボックス編集
            _Body.txtProjectCode.Validated -= SearchConChange;
            // 作業内容テキストボックス編集
            _Body.txtSagyoName.Validated -= SearchConChange;
            // 業務区分コンボボックス編集
            _Body.cmbGyomuKbn.SelectedIndexChanged -= SearchConChange;
            // 選択なしラジオボタン選択編集
            _Body.rdoNothing.CheckedChanged -= SearchConChange;
            // 依頼元ラジオボタン選択編集
            _Body.rdoIraimoto.CheckedChanged -= SearchConChange;
            // 社内ラジオボタン選択編集
            _Body.rdoSyanai.CheckedChanged -= SearchConChange;
            // 不特定客先ラジオボタン選択編集
            _Body.rdoFutokutei.CheckedChanged -= SearchConChange;
            // 顧客名テキストボックス編集
            _Body.txtKokyakuName.Validated -= SearchConChange;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {

            // 作業一覧検索イベントの追加・退避
            AddOrDelSearchConChange(false);

            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // プロジェクト名称
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // 前月実績チェックボックス
            _Body.chkZengetsuExist.Checked = false;

            // 今月実績チェックボックス
            _Body.chkKongetsuExist.Checked = false;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;

            // 作業一覧検索イベントの追加・退避
            AddOrDelSearchConChange(true);
        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {

            // 初期化
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);

            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length == 1) {
                    _Body.cmbGyomuKbn.Items.Add(pair.Key);
                }
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// 作成ボタン使用可否切り替え
        /// </summary>
        /// <param name="enablded"></param>
        private void SwitchEnable_BtnCrete(bool enablded) {

            // 登録・削除ボタン
            bool isAllowAddDel = (_Body.dgvPlojectLooks.Rows != null && 0 < _Body.dgvPlojectLooks.Rows.Count);
            if (enablded && isAllowAddDel) {

                // 使用可能
                _Body.btnCreatePRS.Enabled = true;
                _Body.btnCreateKinmuhyo.Enabled = true;
                _Body.btnInputSagyo.Enabled = true;
                _Body.btnDelSagyo.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnCreatePRS.Enabled = false;
            _Body.btnCreateKinmuhyo.Enabled = false;
            _Body.btnInputSagyo.Enabled = false;
            _Body.btnDelSagyo.Enabled = false;
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {
            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);
        }
        /// <summary>
        /// フォーカスと列ヘッダテキストを初期化（PRS実績）
        /// </summary>
        private void InitFocusAndHeaderText() {

            // 列ヘッダーセルの背景色を変更可能にする
            _Body.dgvPrsJisseki.EnableHeadersVisualStyles = false;

            // グリッド先頭セルのフォーカスを削除
            bool isExistRow = (_Body.dgvPrsJisseki.Rows != null && _Body.dgvPrsJisseki.Rows.Count != 0);
            if (isExistRow) {
                _Body.dgvPrsJisseki.Rows[0].Cells[0].Selected = false;
            }
            DateTime startDayOfWeek = _SELECTED_DATE.AddDays(-(int)_SELECTED_DATE.DayOfWeek);
            int addDayCount = 0;
            foreach (DataGridViewColumn c in _Body.dgvPrsJisseki.Columns) {
                
                if (c.HeaderText.Contains("（") && c.HeaderText.Contains("）")) {

                    // ヘッダーテキスト初期化（PRS実績）
                    EditHeaderTextPrsJisseki(startDayOfWeek, addDayCount, c);

                    // フォーカス位置初期化（PRS実績）
                    EditFocusPrsJisseki(startDayOfWeek, addDayCount, isExistRow, c);

                }
                addDayCount++;
            }
        }
        /// <summary>
        /// ヘッダーテキスト初期化（PRS実績）
        /// </summary>
        /// <param name="startDayOfWeek"></param>
        /// <param name="addDayCount"></param>
        private void EditHeaderTextPrsJisseki(DateTime startDayOfWeek, int addDayCount, DataGridViewColumn c) {
            DateTime currDate = startDayOfWeek.AddDays(addDayCount);
            string strDayOfWeek = KinmuhyoUtil.ConvertWBWeekDay(currDate, currDate.Day);
            c.HeaderText = currDate.ToString(Formats._FORMAT_DATE_MD_SLASH) + string.Format("\r\n（{0}）", strDayOfWeek);

            DateTime compareDate = _SELECTED_DATE;
            if (KinmuhyoUtil.JudgeIsSameYMD(currDate, compareDate)) {

                // PRS実績にシステム日付がある場合はヘッダーセル背景色を変更
                c.HeaderCell.Style.BackColor = SystemColors.MenuHighlight;
                c.HeaderCell.Style.ForeColor = Color.White;
            }
            else {
                c.HeaderCell.Style.BackColor = Color.Empty;
                c.HeaderCell.Style.ForeColor = Colors._FORE_COLOR_CLICK;
            }
        }
        /// <summary>
        /// フォーカス位置初期化（PRS実績）
        /// </summary>
        /// <param name="startDayOfWeek"></param>
        /// <param name="addDayCount"></param>
        /// <param name="isExistRow"></param>
        /// <param name="c"></param>
        private void EditFocusPrsJisseki(DateTime startDayOfWeek, int addDayCount, bool isExistRow, DataGridViewColumn c) {
            DateTime currDate = startDayOfWeek.AddDays(addDayCount);
            if (isExistRow && JudgeIsSameCol(currDate) && KinmuhyoUtil.JudgeIsSameYMD(currDate, _SELECTED_DATE)) {

                // 現在日時に応じてフォーカス移動
                string strTaishoDate = GetTaishoDateFromInvisibleTxt(c.Index);
                var dtCompareDate = currDate;
                if (currDate.Hour < TimeConst._PRS_WORKSTART_TIME_HH) {
                    dtCompareDate = currDate.AddDays(1);
                }
                int cIdx = c.Index;
                int tgtRowIdx = GetRowIdxFromStartTime(strTaishoDate, dtCompareDate);
                _Body.dgvPrsJisseki.Rows[tgtRowIdx].Cells[cIdx].Selected = true;
                _Body.dgvPrsJisseki.CurrentCell = _Body.dgvPrsJisseki.Rows[tgtRowIdx].Cells[cIdx];
            }
        }
        /// <summary>
        /// グリッドスタイル初期化（PRS実績）
        /// </summary>
        private void InitGridPrsJisseki() {

            if (string.IsNullOrEmpty(_Body.txtEmploeeId.Text)) {

                // 列の幅調整
                EditGridRowColumnStyle();
                return;   
            }
            int maxRowCount = (TimeConst._HOURS_A_DAY * 2) + 1;
            for (int rowIdx = 0; rowIdx < maxRowCount; rowIdx++) {

                // 7:00～翌日7:00の行を追加
                var row = new DataGridViewRow();
                _Body.dgvPrsJisseki.Rows.Add(row);
                DataGridViewRow r = _Body.dgvPrsJisseki.Rows[rowIdx];

                // 行の高さ調整
                r.Height = _CURR_ROW_HEIGHT;

                foreach (DataGridViewCell c in r.Cells) {
                    if (rowIdx == 0 || rowIdx == _ROW_IDX_REST_START_TOP || rowIdx == _ROW_IDX_REST_START_BOTTOM) {
                        // 編集不可能列は濃いグレーに設定
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_UNABLED;
                        continue;
                    }
                    // 休憩時間帯・土日祝はグレーに設定
                    if (PaintCellLightGray(c)) {
                        continue;
                    }
                }
            }
            // フォーカスと列ヘッダテキストをセット
            InitFocusAndHeaderText();
        }
        /// <summary>
        /// 列の幅調整（PRS実績）
        /// </summary>
        private void EditGridRowColumnStyle() {
            // イベントを一旦解除
            _Body.dgvPrsJisseki.RowHeightChanged -= DgvRowHeightChangedEvent;

            // グリッド初期化
            _Body.dgvPrsJisseki.Rows.Clear();

            // 列幅調整
            EditGridColWidth();

            // イベントを回復
            _Body.dgvPrsJisseki.RowHeightChanged += DgvRowHeightChangedEvent;
        }
        #endregion

        #region 汎用処理
        /// <summary>
        /// プロジェクト一覧グリッド背景色セット
        /// </summary>
        private void PaintCellProjectLooks() {
            var dto = new SagyoNaiyoLooksGridDto();
            foreach (DataGridViewRow r in _Body.dgvPlojectLooksSel.Rows) {
                string gyoumKbn = StringUtil.TostringNullForbid(r.Cells[nameof(dto.ProjectCD) + _COL_NAME_ADD_SEL].Value).Substring(0, 1);
                foreach (DataGridViewCell c in r.Cells) {
                    ControlUtil.SetTimeTrackerCellStyle(gyoumKbn, c);
                }
            }
        }
        /// <summary>
        /// PRS実績グリッド背景色セット
        /// </summary>
        private void PaintCellPrsJisseki() {

            var strSelectedDate = _SELECTED_DATE.ToString(Formats._FORMAT_DATE_YYYYMM);
            int rowIdx = 0;
            foreach (DataGridViewRow r in _Body.dgvPrsJisseki.Rows) {

                // 行のフォントサイズセット
                r.DefaultCellStyle.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, _MIN_FONT_SIZE, FontStyle.Regular);
                foreach (DataGridViewCell c in r.Cells) {
                    if (TimeConst._DAY_COUNT_WEEK <= c.ColumnIndex) {
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_UNABLED;
                        continue;
                    }
                    if (rowIdx == 0 || rowIdx == _ROW_IDX_REST_START_TOP || rowIdx == _ROW_IDX_REST_START_BOTTOM) {
                        // 編集不可能列は濃いグレーに設定
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_UNABLED;
                        continue;
                    }
                    // 対象月以外の列は濃いグレーに設定
                    string taishoDate = TypeConversionUtil.ToDateTime(GetTaishoDateFromInvisibleTxt(c.ColumnIndex)).ToString(Formats._FORMAT_DATE_YYYYMM);
                    if (strSelectedDate != taishoDate) {
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_UNABLED;
                        continue;
                    }
                    int tgtDataColIdx = c.ColumnIndex + TimeConst._DAY_COUNT_WEEK;
                    int currMasterRowIdx = TypeConversionUtil.ToInteger(r.Cells[tgtDataColIdx].Value, -1);
                    if (currMasterRowIdx < 0) {

                        // 休憩時間帯・土日祝はグレーに設定
                        if (PaintCellLightGray(c)) {
                            continue;
                        }
                        // 作業登録されていないセルは無色をセット
                        c.Style.BackColor = Color.Empty;
                        continue;
                    }
                    // プロジェクトコードに応じて背景色セット
                    var dto = new SagyoNaiyoLooksGridDto();
                    string selName = nameof(dto.SagyoNaiyoFull);
                    string tgtLooksValDisp = StringUtil.TostringNullForbid(_Body.dgvPlojectLooks.Rows[currMasterRowIdx].Cells[selName].Value);
                    ControlUtil.SetTimeTrackerCellStyle(tgtLooksValDisp.Substring(1, 1), c);
                }
                rowIdx++;
            }
        }
        /// <summary>
        /// 休憩時間帯・土日祝はグレーに設定
        /// </summary>
        /// <param name="c"></param>
        private bool PaintCellLightGray(DataGridViewCell c) {
            string strTaishoDate = GetTaishoDateFromInvisibleTxt(c.ColumnIndex);
            bool isHoliday = _HOLIDAYS.ContainsKey(TypeConversionUtil.ToDateTime(strTaishoDate));
            if ((c.RowIndex == _ROW_IDX_REST_START_TOP || c.RowIndex == _ROW_IDX_REST_START_BOTTOM) || c.ColumnIndex == 0 || c.ColumnIndex == _COL_IDX_SATURDAY || isHoliday) {
                c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 選択日時と同じ列の日付か判定
        /// </summary>
        /// <param name="currDate"></param>
        /// <returns></returns>
        private bool JudgeIsSameCol(DateTime currDate) {
            if (KinmuhyoUtil.JudgeIsSameYMD(currDate, _SELECTED_DATE)) {
                return true;
            }
            var nextDate = _SELECTED_DATE.AddDays(1);
            if (KinmuhyoUtil.JudgeIsSameYMD(currDate, nextDate) && 0 <= currDate.Hour && currDate.Hour <= _GRID_START_TIME_H) {
                return true;
            }
            return false;
        }
        /// <summary>
        /// グリッド列の幅調整
        /// </summary>
        private void EditGridColWidth() {
            int dgvWidth = _Body.dgvPrsJisseki.Size.Width;
            foreach (DataGridViewColumn c in _Body.dgvPrsJisseki.Columns) {
                c.Width = ((dgvWidth - _DGV_CALENDER_WIDTH) / TimeConst._DAY_COUNT_WEEK);
            }
        }
        /// <summary>
        /// 隠しコントロールに対象日をセット
        /// </summary>
        private void SetDateInvisibleTxt() {
            DateTime startDayOfWeek = _SELECTED_DATE.AddDays(-(int)_SELECTED_DATE.DayOfWeek);
            _Body.textBox1.Text = startDayOfWeek.AddDays(0).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox2.Text = startDayOfWeek.AddDays(1).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox3.Text = startDayOfWeek.AddDays(2).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox4.Text = startDayOfWeek.AddDays(3).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox5.Text = startDayOfWeek.AddDays(4).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox6.Text = startDayOfWeek.AddDays(5).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            _Body.textBox7.Text = startDayOfWeek.AddDays(6).ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
        }
        /// <summary>
        /// 列インデックスから対象日を取得
        /// </summary>
        /// <param name="colIdx"></param>
        /// <param name="date"></param>
        private string GetTaishoDateFromInvisibleTxt(int colIdx) {
            switch (colIdx) {
                case 0:
                    return _Body.textBox1.Text;
                case 1:
                    return _Body.textBox2.Text;
                case 2:
                    return _Body.textBox3.Text;
                case 3:
                    return _Body.textBox4.Text;
                case 4:
                    return _Body.textBox5.Text;
                case 5:
                    return _Body.textBox6.Text;
                case 6:
                    return _Body.textBox7.Text;
            }
            return string.Empty;
        }
        /// <summary>
        /// 隠しコントロールから列インデックスを取得
        /// </summary>
        /// <param name="taishoYMD"></param>
        /// <returns></returns>
        private int GetColIdxFromInvisiTxt(DateTime taishoYMD) {
            string strTaishoYMD = taishoYMD.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            if (_Body.textBox1.Text == strTaishoYMD) {
                return 0;
            }
            if (_Body.textBox2.Text == strTaishoYMD) {
                return 1;
            }
            if (_Body.textBox3.Text == strTaishoYMD) {
                return 2;
            }
            if (_Body.textBox4.Text == strTaishoYMD) {
                return 3;
            }
            if (_Body.textBox5.Text == strTaishoYMD) {
                return 4;
            }
            if (_Body.textBox6.Text == strTaishoYMD) {
                return 5;
            }
            if (_Body.textBox7.Text == strTaishoYMD) {
                return 6;
            }
            return -1;
        }
        /// <summary>
        /// セルインデックスから作業開始時刻取得
        /// </summary>
        /// <param name="currCell"></param>
        /// <returns></returns>
        private string GetStartTimeFromCellIdx(DataGridViewCell currCell) {
            DateTime CurrDate = TypeConversionUtil.ToDateTime(GetTaishoDateFromInvisibleTxt(currCell.ColumnIndex));
            CurrDate = CurrDate.AddHours(_GRID_START_TIME_H).AddMinutes(_SGRID_TART_TIME_M);
            var ret =  CurrDate.AddMinutes(currCell.RowIndex * TimeConst._MINUTE_AN_HOUR / 2).ToString(Formats._FORMAT_DATE_YYYYMMDDHHMM_SLASH);
            return ret;
        }
        /// <summary>
        /// 作業開始時刻から行インデックスを取得
        /// </summary>
        /// <param name="strTaishoDate"></param>
        /// <param name="startTime"></param>
        /// <returns></returns>
        private int GetRowIdxFromStartTime(string strTaishoDate,  DateTime startTime) {
            DateTime dtTaishoDate = TypeConversionUtil.ToDateTime(strTaishoDate, Formats._FORMAT_DATE_YYYYMMDD_SLASH);
            dtTaishoDate = dtTaishoDate.AddHours(_GRID_START_TIME_H).AddMinutes(_SGRID_TART_TIME_M);
            var span = startTime - dtTaishoDate;
            int ret = (int)span.TotalMinutes / (TimeConst._MINUTE_AN_HOUR / 2);
            if (ret < 0) {
                ret = _ROW_IDX_WORK_START_TOP;
            }
            return ret;
        }
        /// <summary>
        /// 日付範囲設定
        /// </summary>
        private void SetDateRange() {
            DateTime tgtDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            _Body.mcSelectedDate.SelectionStart = _SELECTED_DATE;
            _Body.mcSelectedDate.MaxDate = tgtDate.AddMonths(2).AddMilliseconds(-1);
            _Body.mcSelectedDate.MinDate = tgtDate.AddMonths(-6);
        }
        #endregion

    }
}
