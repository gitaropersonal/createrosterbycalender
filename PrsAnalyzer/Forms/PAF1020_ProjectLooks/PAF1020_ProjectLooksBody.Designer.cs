﻿namespace PrsAnalyzer.Forms {
    partial class PAF1020_ProjectLooksBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF1020_ProjectLooksBody));
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvProjectLooks = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chkContainDel = new System.Windows.Forms.CheckBox();
            this.chkBrankDetail = new System.Windows.Forms.CheckBox();
            this.chkKongetsuExist = new System.Windows.Forms.CheckBox();
            this.chkZengetsuExist = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSagyoName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.pnlKokyakuRdo = new System.Windows.Forms.Panel();
            this.rdoFutokutei = new System.Windows.Forms.RadioButton();
            this.rdoSyanai = new System.Windows.Forms.RadioButton();
            this.rdoIraimoto = new System.Windows.Forms.RadioButton();
            this.rdoNothing = new System.Windows.Forms.RadioButton();
            this.txtKokyakuName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbGyomuKbn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTgtMonth = new System.Windows.Forms.ComboBox();
            this.lblTgtMonth = new System.Windows.Forms.Label();
            this.lblEmploeeId = new System.Windows.Forms.Label();
            this.cmbEmploeeName = new System.Windows.Forms.ComboBox();
            this.lblEmploeeName = new System.Windows.Forms.Label();
            this.txtEmploeeId = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRowCopy = new System.Windows.Forms.Button();
            this.btnAllCheckOn = new System.Windows.Forms.Button();
            this.btnAllCheckOff = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.KouteiSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PBushoKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Iraimoto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Biko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectLooks)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pnlKokyakuRdo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvProjectLooks);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel3.Location = new System.Drawing.Point(0, 203);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.panel3.Size = new System.Drawing.Size(1384, 543);
            this.panel3.TabIndex = 20;
            // 
            // dgvProjectLooks
            // 
            this.dgvProjectLooks.AllowUserToAddRows = false;
            this.dgvProjectLooks.AllowUserToResizeRows = false;
            this.dgvProjectLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProjectLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KouteiSeq,
            this.PBushoKbn,
            this.DetailSeq,
            this.Iraimoto,
            this.KokyakuName,
            this.ProjectCD,
            this.ProjectName,
            this.KouteiName,
            this.Detail,
            this.Biko,
            this.DelFlg,
            this.Status});
            this.dgvProjectLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProjectLooks.Location = new System.Drawing.Point(15, 15);
            this.dgvProjectLooks.Name = "dgvProjectLooks";
            this.dgvProjectLooks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvProjectLooks.RowTemplate.Height = 21;
            this.dgvProjectLooks.Size = new System.Drawing.Size(1354, 528);
            this.dgvProjectLooks.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 203);
            this.panel1.TabIndex = 0;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.label5);
            this.grpInput.Controls.Add(this.panel5);
            this.grpInput.Controls.Add(this.label3);
            this.grpInput.Controls.Add(this.txtSagyoName);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.txtProjectCode);
            this.grpInput.Controls.Add(this.pnlKokyakuRdo);
            this.grpInput.Controls.Add(this.txtKokyakuName);
            this.grpInput.Controls.Add(this.label4);
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.cmbGyomuKbn);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.cmbTgtMonth);
            this.grpInput.Controls.Add(this.lblTgtMonth);
            this.grpInput.Controls.Add(this.lblEmploeeId);
            this.grpInput.Controls.Add(this.cmbEmploeeName);
            this.grpInput.Controls.Add(this.lblEmploeeName);
            this.grpInput.Controls.Add(this.txtEmploeeId);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 193);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(277, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 17);
            this.label5.TabIndex = 66;
            this.label5.Text = "検索オプション";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.chkContainDel);
            this.panel5.Controls.Add(this.chkBrankDetail);
            this.panel5.Controls.Add(this.chkKongetsuExist);
            this.panel5.Controls.Add(this.chkZengetsuExist);
            this.panel5.Location = new System.Drawing.Point(381, 21);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(449, 24);
            this.panel5.TabIndex = 3;
            // 
            // chkContainDel
            // 
            this.chkContainDel.AutoSize = true;
            this.chkContainDel.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkContainDel.Location = new System.Drawing.Point(310, 0);
            this.chkContainDel.Name = "chkContainDel";
            this.chkContainDel.Padding = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.chkContainDel.Size = new System.Drawing.Size(104, 24);
            this.chkContainDel.TabIndex = 4;
            this.chkContainDel.Text = "非表示を含む";
            this.chkContainDel.UseVisualStyleBackColor = true;
            // 
            // chkBrankDetail
            // 
            this.chkBrankDetail.AutoSize = true;
            this.chkBrankDetail.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkBrankDetail.Location = new System.Drawing.Point(204, 0);
            this.chkBrankDetail.Name = "chkBrankDetail";
            this.chkBrankDetail.Padding = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.chkBrankDetail.Size = new System.Drawing.Size(106, 24);
            this.chkBrankDetail.TabIndex = 3;
            this.chkBrankDetail.Text = "詳細がブランク";
            this.chkBrankDetail.UseVisualStyleBackColor = true;
            // 
            // chkKongetsuExist
            // 
            this.chkKongetsuExist.AutoSize = true;
            this.chkKongetsuExist.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkKongetsuExist.Location = new System.Drawing.Point(102, 0);
            this.chkKongetsuExist.Name = "chkKongetsuExist";
            this.chkKongetsuExist.Padding = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.chkKongetsuExist.Size = new System.Drawing.Size(102, 24);
            this.chkKongetsuExist.TabIndex = 1;
            this.chkKongetsuExist.Text = "今月実績あり";
            this.chkKongetsuExist.UseVisualStyleBackColor = true;
            // 
            // chkZengetsuExist
            // 
            this.chkZengetsuExist.AutoSize = true;
            this.chkZengetsuExist.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkZengetsuExist.Location = new System.Drawing.Point(0, 0);
            this.chkZengetsuExist.Name = "chkZengetsuExist";
            this.chkZengetsuExist.Padding = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.chkZengetsuExist.Size = new System.Drawing.Size(102, 24);
            this.chkZengetsuExist.TabIndex = 0;
            this.chkZengetsuExist.Text = "前月実績あり";
            this.chkZengetsuExist.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(277, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 65;
            this.label3.Text = "作業内容";
            // 
            // txtSagyoName
            // 
            this.txtSagyoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSagyoName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSagyoName.Location = new System.Drawing.Point(381, 70);
            this.txtSagyoName.MaxLength = 50;
            this.txtSagyoName.Name = "txtSagyoName";
            this.txtSagyoName.Size = new System.Drawing.Size(300, 24);
            this.txtSagyoName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(277, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 64;
            this.label2.Text = "プロジェクトコード";
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProjectCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProjectCode.Location = new System.Drawing.Point(381, 45);
            this.txtProjectCode.MaxLength = 8;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Size = new System.Drawing.Size(82, 24);
            this.txtProjectCode.TabIndex = 3;
            // 
            // pnlKokyakuRdo
            // 
            this.pnlKokyakuRdo.Controls.Add(this.rdoFutokutei);
            this.pnlKokyakuRdo.Controls.Add(this.rdoSyanai);
            this.pnlKokyakuRdo.Controls.Add(this.rdoIraimoto);
            this.pnlKokyakuRdo.Controls.Add(this.rdoNothing);
            this.pnlKokyakuRdo.Location = new System.Drawing.Point(381, 122);
            this.pnlKokyakuRdo.Name = "pnlKokyakuRdo";
            this.pnlKokyakuRdo.Size = new System.Drawing.Size(300, 24);
            this.pnlKokyakuRdo.TabIndex = 6;
            // 
            // rdoFutokutei
            // 
            this.rdoFutokutei.AutoSize = true;
            this.rdoFutokutei.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoFutokutei.Location = new System.Drawing.Point(189, 0);
            this.rdoFutokutei.Name = "rdoFutokutei";
            this.rdoFutokutei.Size = new System.Drawing.Size(91, 24);
            this.rdoFutokutei.TabIndex = 3;
            this.rdoFutokutei.TabStop = true;
            this.rdoFutokutei.Text = "不特定客先";
            this.rdoFutokutei.UseVisualStyleBackColor = true;
            // 
            // rdoSyanai
            // 
            this.rdoSyanai.AutoSize = true;
            this.rdoSyanai.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSyanai.Location = new System.Drawing.Point(137, 0);
            this.rdoSyanai.Name = "rdoSyanai";
            this.rdoSyanai.Size = new System.Drawing.Size(52, 24);
            this.rdoSyanai.TabIndex = 2;
            this.rdoSyanai.TabStop = true;
            this.rdoSyanai.Text = "社内";
            this.rdoSyanai.UseVisualStyleBackColor = true;
            // 
            // rdoIraimoto
            // 
            this.rdoIraimoto.AutoSize = true;
            this.rdoIraimoto.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoIraimoto.Location = new System.Drawing.Point(72, 0);
            this.rdoIraimoto.Name = "rdoIraimoto";
            this.rdoIraimoto.Size = new System.Drawing.Size(65, 24);
            this.rdoIraimoto.TabIndex = 1;
            this.rdoIraimoto.TabStop = true;
            this.rdoIraimoto.Text = "依頼元";
            this.rdoIraimoto.UseVisualStyleBackColor = true;
            // 
            // rdoNothing
            // 
            this.rdoNothing.AutoSize = true;
            this.rdoNothing.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoNothing.Location = new System.Drawing.Point(0, 0);
            this.rdoNothing.Name = "rdoNothing";
            this.rdoNothing.Size = new System.Drawing.Size(72, 24);
            this.rdoNothing.TabIndex = 0;
            this.rdoNothing.TabStop = true;
            this.rdoNothing.Text = "選択なし";
            this.rdoNothing.UseVisualStyleBackColor = true;
            // 
            // txtKokyakuName
            // 
            this.txtKokyakuName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokyakuName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKokyakuName.Location = new System.Drawing.Point(381, 147);
            this.txtKokyakuName.MaxLength = 100;
            this.txtKokyakuName.Name = "txtKokyakuName";
            this.txtKokyakuName.Size = new System.Drawing.Size(300, 24);
            this.txtKokyakuName.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(277, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 61;
            this.label4.Text = "顧客名";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1202, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(149, 170);
            this.panel2.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(17, 127);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 35);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // cmbGyomuKbn
            // 
            this.cmbGyomuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGyomuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbGyomuKbn.FormattingEnabled = true;
            this.cmbGyomuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbGyomuKbn.Location = new System.Drawing.Point(381, 95);
            this.cmbGyomuKbn.Name = "cmbGyomuKbn";
            this.cmbGyomuKbn.Size = new System.Drawing.Size(82, 25);
            this.cmbGyomuKbn.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(277, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 57;
            this.label1.Text = "業務区分";
            // 
            // cmbTgtMonth
            // 
            this.cmbTgtMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbTgtMonth.FormattingEnabled = true;
            this.cmbTgtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbTgtMonth.Location = new System.Drawing.Point(85, 19);
            this.cmbTgtMonth.Name = "cmbTgtMonth";
            this.cmbTgtMonth.Size = new System.Drawing.Size(120, 25);
            this.cmbTgtMonth.TabIndex = 0;
            // 
            // lblTgtMonth
            // 
            this.lblTgtMonth.AutoSize = true;
            this.lblTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTgtMonth.Location = new System.Drawing.Point(15, 23);
            this.lblTgtMonth.Name = "lblTgtMonth";
            this.lblTgtMonth.Size = new System.Drawing.Size(60, 17);
            this.lblTgtMonth.TabIndex = 55;
            this.lblTgtMonth.Text = "対象年月";
            // 
            // lblEmploeeId
            // 
            this.lblEmploeeId.AutoSize = true;
            this.lblEmploeeId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeId.Location = new System.Drawing.Point(15, 48);
            this.lblEmploeeId.Name = "lblEmploeeId";
            this.lblEmploeeId.Size = new System.Drawing.Size(62, 17);
            this.lblEmploeeId.TabIndex = 52;
            this.lblEmploeeId.Text = "担当者ID";
            // 
            // cmbEmploeeName
            // 
            this.cmbEmploeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmploeeName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbEmploeeName.FormattingEnabled = true;
            this.cmbEmploeeName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbEmploeeName.Location = new System.Drawing.Point(85, 70);
            this.cmbEmploeeName.MaxLength = 35;
            this.cmbEmploeeName.Name = "cmbEmploeeName";
            this.cmbEmploeeName.Size = new System.Drawing.Size(120, 25);
            this.cmbEmploeeName.TabIndex = 2;
            // 
            // lblEmploeeName
            // 
            this.lblEmploeeName.AutoSize = true;
            this.lblEmploeeName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeName.Location = new System.Drawing.Point(15, 73);
            this.lblEmploeeName.Name = "lblEmploeeName";
            this.lblEmploeeName.Size = new System.Drawing.Size(60, 17);
            this.lblEmploeeName.TabIndex = 53;
            this.lblEmploeeName.Text = "担当者名";
            // 
            // txtEmploeeId
            // 
            this.txtEmploeeId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeId.Location = new System.Drawing.Point(85, 45);
            this.txtEmploeeId.MaxLength = 6;
            this.txtEmploeeId.Name = "txtEmploeeId";
            this.txtEmploeeId.Size = new System.Drawing.Size(120, 24);
            this.txtEmploeeId.TabIndex = 1;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 16);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(519, 16);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnRowCopy
            // 
            this.btnRowCopy.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRowCopy.Location = new System.Drawing.Point(141, 16);
            this.btnRowCopy.Name = "btnRowCopy";
            this.btnRowCopy.Size = new System.Drawing.Size(120, 35);
            this.btnRowCopy.TabIndex = 57;
            this.btnRowCopy.Text = "行コピー（C）";
            this.btnRowCopy.UseVisualStyleBackColor = true;
            // 
            // btnAllCheckOn
            // 
            this.btnAllCheckOn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAllCheckOn.Location = new System.Drawing.Point(267, 16);
            this.btnAllCheckOn.Name = "btnAllCheckOn";
            this.btnAllCheckOn.Size = new System.Drawing.Size(120, 35);
            this.btnAllCheckOn.TabIndex = 58;
            this.btnAllCheckOn.Text = "全て非表示（N）";
            this.btnAllCheckOn.UseVisualStyleBackColor = true;
            // 
            // btnAllCheckOff
            // 
            this.btnAllCheckOff.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAllCheckOff.Location = new System.Drawing.Point(393, 16);
            this.btnAllCheckOff.Name = "btnAllCheckOff";
            this.btnAllCheckOff.Size = new System.Drawing.Size(120, 35);
            this.btnAllCheckOff.TabIndex = 59;
            this.btnAllCheckOff.Text = "全て表示（F）";
            this.btnAllCheckOff.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1188, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(196, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(61, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnAllCheckOff);
            this.pnlFootor.Controls.Add(this.btnAllCheckOn);
            this.pnlFootor.Controls.Add(this.btnRowCopy);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // KouteiSeq
            // 
            this.KouteiSeq.DataPropertyName = "KouteiSeq";
            this.KouteiSeq.HeaderText = "工程Seq";
            this.KouteiSeq.Name = "KouteiSeq";
            this.KouteiSeq.Visible = false;
            // 
            // PBushoKbn
            // 
            this.PBushoKbn.DataPropertyName = "PBushoKbn";
            this.PBushoKbn.HeaderText = "プロジェクト部署区分";
            this.PBushoKbn.Name = "PBushoKbn";
            this.PBushoKbn.Visible = false;
            // 
            // DetailSeq
            // 
            this.DetailSeq.DataPropertyName = "DetailSeq";
            this.DetailSeq.HeaderText = "詳細Seq";
            this.DetailSeq.Name = "DetailSeq";
            this.DetailSeq.Visible = false;
            // 
            // Iraimoto
            // 
            this.Iraimoto.DataPropertyName = "Iraimoto";
            this.Iraimoto.HeaderText = "依頼元";
            this.Iraimoto.Name = "Iraimoto";
            this.Iraimoto.Visible = false;
            this.Iraimoto.Width = 80;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            this.KokyakuName.HeaderText = "顧客名";
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.Width = 250;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトコード";
            this.ProjectCD.MaxInputLength = 8;
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.Width = 120;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "ProjectName";
            this.ProjectName.HeaderText = "作業内容";
            this.ProjectName.MaxInputLength = 50;
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.Width = 250;
            // 
            // KouteiName
            // 
            this.KouteiName.DataPropertyName = "KouteiName";
            this.KouteiName.HeaderText = "工程";
            this.KouteiName.Name = "KouteiName";
            this.KouteiName.Width = 120;
            // 
            // Detail
            // 
            this.Detail.DataPropertyName = "Detail";
            this.Detail.HeaderText = "詳細";
            this.Detail.MaxInputLength = 50;
            this.Detail.Name = "Detail";
            this.Detail.Width = 300;
            // 
            // Biko
            // 
            this.Biko.DataPropertyName = "Biko";
            this.Biko.HeaderText = "備考";
            this.Biko.MaxInputLength = 200;
            this.Biko.Name = "Biko";
            this.Biko.Width = 175;
            // 
            // DelFlg
            // 
            this.DelFlg.DataPropertyName = "DelFlg";
            this.DelFlg.HeaderText = "非表示";
            this.DelFlg.Name = "DelFlg";
            this.DelFlg.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DelFlg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DelFlg.Width = 75;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "編集ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // PAF1020_ProjectLooksBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlFootor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 600);
            this.Name = "PAF1020_ProjectLooksBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "作業プロジェクト一覧";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectLooks)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pnlKokyakuRdo.ResumeLayout(false);
            this.pnlKokyakuRdo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvProjectLooks;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.ComboBox cmbGyomuKbn;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cmbTgtMonth;
        public System.Windows.Forms.Label lblTgtMonth;
        public System.Windows.Forms.Label lblEmploeeId;
        public System.Windows.Forms.ComboBox cmbEmploeeName;
        public System.Windows.Forms.Label lblEmploeeName;
        public System.Windows.Forms.TextBox txtEmploeeId;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnRowCopy;
        public System.Windows.Forms.Button btnAllCheckOn;
        public System.Windows.Forms.Button btnAllCheckOff;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Panel pnlKokyakuRdo;
        public System.Windows.Forms.RadioButton rdoFutokutei;
        public System.Windows.Forms.RadioButton rdoSyanai;
        public System.Windows.Forms.RadioButton rdoIraimoto;
        public System.Windows.Forms.RadioButton rdoNothing;
        public System.Windows.Forms.TextBox txtKokyakuName;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSagyoName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtProjectCode;
        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.CheckBox chkKongetsuExist;
        public System.Windows.Forms.CheckBox chkZengetsuExist;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.CheckBox chkContainDel;
        public System.Windows.Forms.CheckBox chkBrankDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn PBushoKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iraimoto;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Biko;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DelFlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

