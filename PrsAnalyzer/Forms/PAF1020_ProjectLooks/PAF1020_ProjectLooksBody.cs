﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF1020_ProjectLooksBody : Form {
        public PAF1020_ProjectLooksBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF1020_ProjectLooksFormLogic(this, loginInfo);
        }
    }
}
