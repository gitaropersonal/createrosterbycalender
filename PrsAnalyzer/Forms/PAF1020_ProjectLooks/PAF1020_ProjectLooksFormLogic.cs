﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF1020_ProjectLooksFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF1020_ProjectLooksBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// Service（社員マスタ）
        /// </summary>
        private PAF3010_MstEmploeeService _MST_EMPLOEE_SERVICE = new PAF3010_MstEmploeeService();
        /// <summary>
        /// Service（プロジェクト一覧）
        /// </summary>
        private PAF1020_ProjectLooksService _TRN_PROJECT_SERVICE = new PAF1020_ProjectLooksService();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static ProjectLooksGridDto _CELL_HEADER_NAME_DTO = new ProjectLooksGridDto();
        private string[] _EditableGridCells = new string[] { 
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Detail)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Biko)
                                                           , nameof(_CELL_HEADER_NAME_DTO.DelFlg)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Status) };

        private string[] _ForbidUpdateGridCells = new string[] {
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.ProjectName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KokyakuName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Iraimoto)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KouteiName)};

        // 工程・詳細にプレフィックスするアンダーバー
        private const string _STR_UNDER_BAR = "_";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF1020_ProjectLooksFormLogic(PAF1020_ProjectLooksBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpd_ClickEvent();
            };
            // すべてチェックボタン押下
            _Body.btnAllCheckOn.Click += (s, e) => {
                GridCheckAllOnOrOff(true);
            };
            // すべて外すボタン押下
            _Body.btnAllCheckOff.Click += (s, e) => {
                GridCheckAllOnOrOff(false);
            };
            // 行コピーボタン押下
            _Body.btnRowCopy.Click += (s, e) => {
                BtnCopy_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // 社員名SelectionChangeCommitted
            _Body.cmbEmploeeName.SelectedValueChanged += (s, e) => {
                SelectionChangeCommited_EmploeeName();
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // テキストボックスValidated
            _Body.txtEmploeeId.Validated += (s, e) => {
                LostFocus_EmploeeId();
            };
            // グリッドCellEnter
            _Body.dgvProjectLooks.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvProjectLooks.RowPostPaint += (s, e) => {
                GridRowUtil<ProjectLooksGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvProjectLooks.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.N:
                        _Body.btnAllCheckOn.Focus();
                        _Body.btnAllCheckOn.PerformClick();
                        break;
                    case Keys.F:
                        _Body.btnAllCheckOff.Focus();
                        _Body.btnAllCheckOff.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.C:
                        _Body.btnRowCopy.Focus();
                        _Body.btnRowCopy.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 社員IDロストフォーカス
        /// </summary>
        private void LostFocus_EmploeeId() {
            if (!ExecuteLostFocus()) {
                return;
            }
            // 背景色初期化
            InitColor();

            // Validate
            if (!ValidateLostFocus()) {

                // テキストクリア
                _Body.txtEmploeeId.Text = string.Empty;
                _Body.cmbEmploeeName.Text = string.Empty;
                return;
            }
            // 検索
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
                IsContaiDeleted = false,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);

            // 検索データ絞り込み
            if (mstData.Any()) {
                _Body.cmbEmploeeName.Text = mstData.First(n => n.TantoID == _Body.txtEmploeeId.Text).TantoName;
            }
        }
        /// <summary>
        /// 社員名SelectionChangeCommitted
        /// </summary>
        private void SelectionChangeCommited_EmploeeName() {
            var con = new LoadMstTantoCon() {
                IsContaiDeleted = false,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);
            if (mstData.Exists(n => n.TantoName == _Body.cmbEmploeeName.Text)) {
                _Body.txtEmploeeId.Text = mstData.First(n => n.TantoName == _Body.cmbEmploeeName.Text).TantoID;
                return;
            }
            // テキストクリア
            _Body.txtEmploeeId.Text = string.Empty;
        }
        /// <summary>
        /// 使用可否切り替え（検索条件欄）
        /// </summary>
        /// <param name="IsSelectBtnClick"></param>
        private void SwitchEnable_GlpInput(bool IsSelectBtnClick) {
            _Body.txtEmploeeId.Enabled = !IsSelectBtnClick;
            _Body.cmbEmploeeName.Enabled = !IsSelectBtnClick;
            _Body.dgvProjectLooks.Enabled = IsSelectBtnClick;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                _Body.btnRowCopy.Enabled = true;
                _Body.btnAllCheckOn.Enabled = true;
                _Body.btnAllCheckOff.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
            _Body.btnRowCopy.Enabled = false;
            _Body.btnAllCheckOn.Enabled = false;
            _Body.btnAllCheckOff.Enabled = false;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvProjectLooks.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case nameof(_CELL_HEADER_NAME_DTO.Biko):
                    _Body.dgvProjectLooks.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvProjectLooks.ImeMode = ImeMode.NoControl;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvProjectLooks.CellValueChanged -= GridCellValueChanged;

            var dto = new ProjectLooksGridDto();
            string tgtCellName = _Body.dgvProjectLooks.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvProjectLooks.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                _Body.dgvProjectLooks.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string taskId = StringUtil.TostringNullForbid(_Body.dgvProjectLooks.Rows[e.RowIndex].Cells[nameof(dto.ProjectCD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvProjectLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(taskId) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvProjectLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvProjectLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvProjectLooks.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        private void EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex) {
            if (tgtCellVal == null){
                return;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {
                case nameof(_CELL_HEADER_NAME_DTO.ProjectCD):
                case nameof(_CELL_HEADER_NAME_DTO.Detail):
                case nameof(_CELL_HEADER_NAME_DTO.Biko):
                default:
                    // 入力値を編集→セット
                    _Body.dgvProjectLooks.Rows[RowIndex].Cells[tgtCellName].Value = inputVal;
                    break;
            }
        }
        /// <summary>
        /// 工程・詳細の編集（ロストフォーカス時）
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        private string EditLostFocusKouteiDetail(string str) {
            if (string.IsNullOrEmpty(str)) {
                return string.Empty;
            }
            if (str[0].ToString() != _STR_UNDER_BAR) {

                // 作業IDの頭文字が不等号でない場合は付加
                str = string.Concat(_STR_UNDER_BAR, str);
            }
            return str;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void BtnSearch_ClickEvent(bool isShowNotExistMessage = true) {

            try {
                // グリッド初期化
                _Body.dgvProjectLooks.Rows.Clear();

                // Validate
                if (!SelValidate()) {
                    return;
                }
                // 検索データ絞り込み
                string strTgtYM = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM);

                var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked
                                                         , _Body.rdoSyanai.Checked
                                                         , _Body.rdoFutokutei.Checked);

                string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked
                                                                       , _Body.rdoSyanai.Checked
                                                                       , _Body.rdoFutokutei.Checked
                                                                       , _Body.txtKokyakuName.Text);

                // 条件作成
                var con = new LoadProjectLooksConDto() {
                    TantoID = _Body.txtEmploeeId.Text,
                    TaishoYM = strTgtYM,
                    IsContainDeleted = _Body.chkContainDel.Checked,
                    IsContainX = true,
                    ProjectCD = _Body.txtProjectCode.Text,
                    ProjectName = _Body.txtSagyoName.Text,
                    GyomuKbn = _Body.cmbGyomuKbn.Text,
                    KokyakuKbn = kokyakuKbn,
                    KokyakuName = kokyakuName,
                    IsExistZengetsuJisseki = _Body.chkZengetsuExist.Checked,
                    IsExistKongetsuJisseki = _Body.chkKongetsuExist.Checked,
                    IsDetailBrankOnly = _Body.chkBrankDetail.Checked,
                    PBushoKbn = (ProjectConst.PBushoKbn)LoginInfo.PBushoKbn,
                };
                var gridData = _TRN_PROJECT_SERVICE.LoadProjectLooks(_DB_CONN_STRING, con);
                if (!gridData.Any() && isShowNotExistMessage) {
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                // グリッドオブジェクトを作成
                _Body.dgvProjectLooks.DataSource = GetGridBindingSource(gridData);

                // グリッドセル背景色をセット
                SetColor_GridCsll();

                _Body.dgvProjectLooks.Show();
                _Body.dgvProjectLooks.Focus();

                // 検索条件欄使用可否切り替え
                SwitchEnable_GlpInput(true);

                // 使用可否切り替え（更新ボタン）
                SwitchEnable_BtnUpdate();
                // フォーカス
                _Body.dgvProjectLooks.Focus();
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 行コピーボタン押下イベント
        /// </summary>
        private void BtnCopy_ClickEvent() {
            if (_Body.dgvProjectLooks.SelectedRows == null || _Body.dgvProjectLooks.SelectedRows.Count == 0) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_SELECTED_ROW_NOTHING);
                return;
            }
            // 追加行作成
            var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(_Body.dgvProjectLooks.SelectedRows[0]);
            rowDto.Biko = string.Empty;
            rowDto.DelFlg = false;
            rowDto.Detail = string.Empty;
            rowDto.DetailSeq = (GetMaxDetailSeq(rowDto.ProjectCD, rowDto.KouteiSeq) + 1).ToString();
            rowDto.Status = Enums.EditStatus.INSERT;
            BindingList<ProjectLooksGridDto> datas = (BindingList<ProjectLooksGridDto>)_Body.dgvProjectLooks.DataSource;
            datas.Add(rowDto);
            _Body.dgvProjectLooks.DataSource = datas;

            // グリッド背景色をセット
            DataGridViewRow r = _Body.dgvProjectLooks.Rows[datas.Count - 1];
            var dto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
            foreach (DataGridViewCell c in r.Cells) {
                // 更新不可能列の編集
                if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name)) {

                    ControlUtil.SetTimeTrackerCellStyle(dto.ProjectCD.Substring(0, 1), c);
                    c.ReadOnly = true;
                }
                if (rowDto.Status == Enums.EditStatus.INSERT) {
                    if (c.OwningColumn.Name == nameof(_CELL_HEADER_NAME_DTO.Detail)) {
                        c.Style.BackColor = Colors._BACK_COLOR_CELL_EDITED;
                    }
                }
            }
        }
        /// <summary>
        /// 最大の詳細seqを取得
        /// </summary>
        /// <param name="ProjectCD"></param>
        /// <param name="kouteiSeq"></param>
        /// <returns></returns>
        private int GetMaxDetailSeq(string ProjectCD, string kouteiSeq) {
            if (_Body.dgvProjectLooks.SelectedRows == null || _Body.dgvProjectLooks.SelectedRows.Count == 0) {
                return 0;
            }
            var list = new List<ProjectLooksGridDto>();
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
                list.Add(rowDto);
            }
            int ret = list.Where(n => n.ProjectCD == ProjectCD && n.KouteiSeq == kouteiSeq).ToList().Max(n => int.Parse(n.DetailSeq)) ;
            string taishoYM = TypeConversionUtil.ToDateTime(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM);
            var maxSeqFromDb = new PAF1020_ProjectLooksService().GetMaxDetailSeq(_DB_CONN_STRING, _Body.txtEmploeeId.Text, ProjectCD, kouteiSeq);
            if (ret < maxSeqFromDb) {
                ret = maxSeqFromDb;
            }
            return ret;
        }
        /// <summary>
        /// グリッドの削除フラグをすべてON・OFF
        /// </summary>
        /// <param name="check"></param>
        private void GridCheckAllOnOrOff(bool check) {
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                r.Cells[nameof(_CELL_HEADER_NAME_DTO.DelFlg)].Value = check;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpd_ClickEvent() {

            var gridRowDtoList = new List<ProjectLooksGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridRowDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            // 条件作成
            var con = new LoadProjectLooksConDto() {
                TantoID = _Body.txtEmploeeId.Text,
                TaishoYM = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM),
                IsContainDeleted = true,
                IsContainX = true,
                PBushoKbn = (ProjectConst.PBushoKbn)LoginInfo.PBushoKbn,
            };
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridRowDtoList, con)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <param name="con"></param>
        private void AddUpdDatasIntoDB(List<ProjectLooksGridDto> gridRowDtoList, LoadProjectLooksConDto con) {

            try {
                // 更新用データ格納リスト
                var addDatas = new List<VPX01ProjectLooksEntity>();
                var updDatas = new List<VPX01ProjectLooksEntity>();
                
                // 検索データ絞り込み
                var oldDatas = _TRN_PROJECT_SERVICE.LoadProjectLooks(_DB_CONN_STRING, con);

                // 登録用データ作成
                addDatas = CreateAddDatas(gridRowDtoList, con.TaishoYM);

                // 登録処理
                addDatas.ForEach(n => new PAF1020_ProjectLooksService().AddProjectLooks(_DB_CONN_STRING, n));

                // 更新用データ作成
                updDatas = CreateUpdDatas(oldDatas, gridRowDtoList, con.TaishoYM);

                // 更新処理
                updDatas.ForEach(n => new PAF1020_ProjectLooksService().UpdProjectLooks(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <param name="strTgtYM"></param>
        /// <returns></returns>
        private List<VPX01ProjectLooksEntity> CreateAddDatas(List<ProjectLooksGridDto> gridDtoList, string strTgtYM) {

            var ret = new List<VPX01ProjectLooksEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.INSERT).ToList()) {

                // データ作成
                var entity = new VPX01ProjectLooksEntity() {
                    TaishoYM = StringUtil.TostringNullForbid(strTgtYM),
                    TantoID = StringUtil.TostringNullForbid(_Body.txtEmploeeId.Text),
                    ProjectCD = StringUtil.TostringNullForbid(rowDto.ProjectCD),
                    ProjectName = StringUtil.TostringNullForbid(rowDto.ProjectName),
                    KouteiSeq = StringUtil.TostringNullForbid(rowDto.KouteiSeq),
                    KouteiName = StringUtil.TostringNullForbid(rowDto.KouteiName),
                    Iraimoto = StringUtil.TostringNullForbid(rowDto.Iraimoto),
                    DetailSeq = StringUtil.TostringNullForbid(rowDto.DetailSeq),
                    PBushoKbn = StringUtil.TostringNullForbid(LoginInfo.PBushoKbn),
                    Detail = StringUtil.TostringNullForbid(rowDto.Detail),
                    KokyakuName = StringUtil.TostringNullForbid(rowDto.KokyakuName),
                    Biko = StringUtil.TostringNullForbid(rowDto.Biko),
                    DelFlg = StringUtil.ConvertFlgToStrFlg(rowDto.DelFlg),
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="gridDtoList"></param>
        /// <param name="strTgtYM"></param>
        /// <returns></returns>
        private List<VPX01ProjectLooksEntity> CreateUpdDatas(List<VPX01ProjectLooksEntity> oldDatas, List<ProjectLooksGridDto> gridDtoList, string strTgtYM) {

            var ret = new List<VPX01ProjectLooksEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.UPDATE).ToList()) {

                // データ作成
                var entity = new VPX01ProjectLooksEntity() {
                    TaishoYM = StringUtil.TostringNullForbid(strTgtYM),
                    TantoID = StringUtil.TostringNullForbid(_Body.txtEmploeeId.Text),
                    ProjectCD = StringUtil.TostringNullForbid(rowDto.ProjectCD),
                    ProjectName = StringUtil.TostringNullForbid(rowDto.ProjectName),
                    KouteiSeq = StringUtil.TostringNullForbid(rowDto.KouteiSeq),
                    KouteiName = StringUtil.TostringNullForbid(rowDto.KouteiName),
                    Iraimoto = StringUtil.TostringNullForbid(rowDto.Iraimoto),
                    DetailSeq = StringUtil.TostringNullForbid(rowDto.DetailSeq),
                    PBushoKbn = StringUtil.TostringNullForbid(LoginInfo.PBushoKbn),
                    Detail = StringUtil.TostringNullForbid(rowDto.Detail),
                    KokyakuName = StringUtil.TostringNullForbid(rowDto.KokyakuName),
                    Biko = StringUtil.TostringNullForbid(rowDto.Biko),
                    DelFlg = StringUtil.ConvertFlgToStrFlg(rowDto.DelFlg),
                };
                VPX01ProjectLooksEntity tgtEntity = null;
                var projectInfo = new MstCommonService().LoadMstProject(_DB_CONN_STRING, entity.ProjectCD).First();
                if (projectInfo.PBushoKbn == 0) {
                    tgtEntity = oldDatas.FirstOrDefault(n => n.TaishoYM  == entity.TaishoYM
                                                          && n.TantoID   == entity.TantoID
                                                          && n.ProjectCD == entity.ProjectCD
                                                          && n.KouteiSeq == entity.KouteiSeq
                                                          && n.DetailSeq == entity.DetailSeq
                                                          );
                } else {
                    tgtEntity = oldDatas.FirstOrDefault(n => n.TaishoYM  == entity.TaishoYM
                                                          && n.TantoID   == entity.TantoID
                                                          && n.ProjectCD == entity.ProjectCD
                                                          && n.KouteiSeq == entity.KouteiSeq
                                                          && n.DetailSeq == entity.DetailSeq
                                                          && n.PBushoKbn == entity.PBushoKbn
                                                          );
                }
                if (tgtEntity != null) {
                    tgtEntity.KouteiName = entity.KouteiName;
                    tgtEntity.Detail = entity.Detail;
                    tgtEntity.Biko = entity.Biko;
                    tgtEntity.DelFlg = entity.DelFlg;
                    tgtEntity.UpdDate = dtNow;
                    ret.Add(tgtEntity);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
                foreach (DataGridViewCell c in r.Cells) {
                    // 更新不可能列の編集
                    if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name))　{

                        ControlUtil.SetTimeTrackerCellStyle(rowDto.ProjectCD.Substring(0, 1), c);
                        c.ReadOnly = true;
                    }
                    // 削除行
                    if (rowDto.DelFlg) {
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        c.Style.ForeColor = Colors._FORE_COLOR_CLICK;
                        if (c.OwningColumn.Name != nameof(_CELL_HEADER_NAME_DTO.DelFlg)) {
                            c.ReadOnly = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// グリッドViewModel取得
        /// </summary>
        /// <param name="tgtEntites"></param>
        /// <returns></returns>
        private BindingList<ProjectLooksGridDto> GetGridBindingSource(List<VPX01ProjectLooksEntity> tgtEntites) {

            // グリッドオブジェクトセット
            var ret = new BindingList<ProjectLooksGridDto>();
            tgtEntites.ForEach(tgtLooks => ret.Add(
                new ProjectLooksGridDto() {
                    KouteiSeq = tgtLooks.KouteiSeq,
                    DetailSeq = tgtLooks.DetailSeq,
                    ProjectCD = tgtLooks.ProjectCD,
                    ProjectName = tgtLooks.ProjectName,
                    KouteiName = tgtLooks.KouteiName,
                    Detail = tgtLooks.Detail,
                    Iraimoto = tgtLooks.Iraimoto,
                    KokyakuName = tgtLooks.KokyakuName,
                    Biko = tgtLooks.Biko,
                    DelFlg = (int.Parse(tgtLooks.DelFlg) == 1),
                    Status = Enums.EditStatus.SHOW,
                }));

            return ret;
        }
        #endregion

        #region Validate
        /// <summary>
        /// ロストフォーカス時Validate
        /// </summary>
        private bool ValidateLostFocus() {

            // 背景色初期化
            InitColor();

            // 社員IDチェック
            if (string.IsNullOrEmpty(_Body.txtEmploeeId.Text)) {
                return false;
            }
            // マスタCSVロード
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
                IsContaiDeleted = false,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);
            if (!mstData.Any()) {
                _Body.txtEmploeeId.BackColor = Color.Red;
                _Body.txtEmploeeId.Focus();
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_MST);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 検索時Validate
        /// </summary>
        /// <returns></returns>
        private bool SelValidate() {

            // 背景色クリア
            InitColor();

            if (string.IsNullOrEmpty(_Body.txtEmploeeId.Text)) {

                // 社員ID
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, _Body.lblEmploeeId.Text);
                DialogUtil.ShowErroMsg(msg);
                _Body.txtEmploeeId.BackColor = Color.Red;
                _Body.txtEmploeeId.Focus();
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<ProjectLooksGridDto> gridRowDtoList) {

            List<ProjectLooksGridDto> allGridDtoList = new List<ProjectLooksGridDto>();
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(allGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);

                // プロジェクトID
                headerTxt = _Body.dgvProjectLooks.Columns[nameof(rowDto.ProjectCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectCD), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<ProjectLooksGridDto> gridDtoList) {

            // 条件作成
            var con = new LoadProjectLooksConDto() {
                TantoID = _Body.txtEmploeeId.Text,
                TaishoYM = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM),
            };
            // 検索データ絞り込み
            var oldDatas = _TRN_PROJECT_SERVICE.LoadProjectLooks(_DB_CONN_STRING, con);

            string msg = string.Empty;
            bool resutlt = true;
            foreach (var gridDto in gridDtoList) {
                if (ValidateDouple_Insert(oldDatas, gridDto) && ValidateDouple_Updatet(gridDtoList, gridDto)) {
                    continue;
                }
                // 重複キー
                var dto = new ProjectLooksGridDto();
                msg = _Body.dgvProjectLooks.Columns[nameof(dto.ProjectCD)].HeaderText;
                msg = string.Concat(msg, ",", _Body.dgvProjectLooks.Columns[nameof(dto.KouteiName)].HeaderText);
                msg = string.Concat(msg, ",", _Body.dgvProjectLooks.Columns[nameof(dto.Detail)].HeaderText);

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvProjectLooks.Rows) {
                    var rowDto = GridRowUtil<ProjectLooksGridDto>.GetRowModel(r);
                    if (rowDto.ProjectCD == gridDto.ProjectCD
                     && rowDto.KouteiSeq == gridDto.KouteiSeq
                     && rowDto.Detail    == gridDto.Detail) {

                        r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                        r.Cells[nameof(rowDto.KouteiName)].Style.BackColor = Color.Red;
                        r.Cells[nameof(rowDto.Detail)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt) {
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE_WORK_DETAIL, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Insert(List<VPX01ProjectLooksEntity> oldDatas, ProjectLooksGridDto keyInfo) {
            string strTgtDt = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM);
            if (keyInfo.Status == Enums.EditStatus.INSERT
                    && oldDatas.Exists(n => n.ProjectCD == keyInfo.ProjectCD
                                         && n.TaishoYM  == strTgtDt
                                         && n.TantoID   == _Body.txtEmploeeId.Text
                                         && n.KouteiSeq == keyInfo.KouteiSeq
                                         && n.Detail    == keyInfo.Detail
                                         )) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Updatet(List<ProjectLooksGridDto> keyInfos, ProjectLooksGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                     && n.KouteiSeq == keyInfo.KouteiSeq
                                     && n.Detail    == keyInfo.Detail
                                     ).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 対象月
            InitSumStartDateCombo();

            // 社員ID
            InitEmploeeNameCombo();

            // 社員名
            _Body.txtEmploeeId.Text = LoginInfo.TantoID;
            _Body.cmbEmploeeName.Text = LoginInfo.TantoName;

            // 検索条件
            InitSearchProjectLooksCondition();

            // グリッド
            _Body.dgvProjectLooks.Rows.Clear();

            // 検索条件欄使用可否切り替え
            SwitchEnable_GlpInput(false);

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();           

            // フォーカス
            _Body.txtEmploeeId.Focus();

            // ログイン情報
            if (LoginInfo.AuthorityKbn == (int)Enums.AuthorityKbn.Normal) {
                _Body.txtEmploeeId.Enabled = false;
                _Body.cmbEmploeeName.Enabled = false;
            }
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 集計月コンボボックス初期化
        /// </summary>
        private void InitSumStartDateCombo() {
            _Body.cmbTgtMonth.Items.Clear();
            _Body.cmbTgtMonth.Text = string.Empty;
            DateTime dtSumEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1);
            DateTime dtSumStart = dtSumEnd.AddMonths(-7);
            for (DateTime dt = dtSumStart; dt <= dtSumEnd; dt = dt.AddMonths(1)) {
                _Body.cmbTgtMonth.Items.Add(dt.ToString(Formats._FORMAT_DATE_YYYYM_NENTSUKI));
            }
            _Body.cmbTgtMonth.Text = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYM_NENTSUKI);
        }
        /// <summary>
        /// 社員名コンボボックス初期化
        /// </summary>
        private void InitEmploeeNameCombo() {

            // 初期化
            _Body.cmbEmploeeName.Items.Clear();
            _Body.cmbEmploeeName.Items.Add(string.Empty);

            // csvデータ取得
            var con = new LoadMstTantoCon() {
                BushoCD = LoginInfo.BushoCD,
                IsContaiDeleted = false,
            };
            var csvDatas = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);
            csvDatas.ForEach(data => _Body.cmbEmploeeName.Items.Add(data.TantoName));
            _Body.cmbEmploeeName.SelectedIndex = 0;
        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {

            // 初期化
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);

            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length == 1) {
                    _Body.cmbGyomuKbn.Items.Add(pair.Key);
                }
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {
            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // プロジェクト名称
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // 依頼元ラジオボタン
            _Body.rdoNothing.Checked = true;

            // 前月実績チェックボックス
            _Body.chkZengetsuExist.Checked = false;

            // 今月実績チェックボックス
            _Body.chkKongetsuExist.Checked = false;

            // 詳細がブランクチェックボックス
            _Body.chkBrankDetail.Checked = false;

            // 削除含むチェックボックス
            _Body.chkContainDel.Checked = false;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;
        }
        #endregion

    }
}
