﻿namespace PrsAnalyzer.Forms {
    partial class PAF1030_PrsAggregateBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF1030_PrsAggregateBody));
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOutputCsv = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabContorlAggregate = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvPrsAggregate1 = new System.Windows.Forms.DataGridView();
            this.KokyakuName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkTimeH1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KousuNinNichi1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kousu1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvPrsAggregate2 = new System.Windows.Forms.DataGridView();
            this.ProjectCD2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD_Disp2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TantoName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkTimeH2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KousuNinNichi2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kousu2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgvPrsAggregate3 = new System.Windows.Forms.DataGridView();
            this.TantoID3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TantoName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkTimeH3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KousuNinNichi3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kousu3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.cmbPBushoKbn = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSagyoName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.pnlKokyakuRdo = new System.Windows.Forms.Panel();
            this.rdoFutokutei = new System.Windows.Forms.RadioButton();
            this.rdoSyanai = new System.Windows.Forms.RadioButton();
            this.rdoIraimoto = new System.Windows.Forms.RadioButton();
            this.rdoNothing = new System.Windows.Forms.RadioButton();
            this.txtKokyakuName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbGyomuKbn = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWorkTimeDay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTaishoYMEnd = new System.Windows.Forms.TextBox();
            this.txtTaishoYMStart = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkContainGyomuX = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHoujunTime = new System.Windows.Forms.TextBox();
            this.lblTgtMonth = new System.Windows.Forms.Label();
            this.pnlFootor.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabContorlAggregate.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate3)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.pnlKokyakuRdo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 14);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(125, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnOutputCsv);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1209, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(175, 65);
            this.panel4.TabIndex = 102;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(32, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(125, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOutputCsv
            // 
            this.btnOutputCsv.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnOutputCsv.Location = new System.Drawing.Point(141, 14);
            this.btnOutputCsv.Name = "btnOutputCsv";
            this.btnOutputCsv.Size = new System.Drawing.Size(125, 35);
            this.btnOutputCsv.TabIndex = 60;
            this.btnOutputCsv.Text = "CSV出力（O）";
            this.btnOutputCsv.UseVisualStyleBackColor = true;
            this.btnOutputCsv.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabContorlAggregate);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel2.Location = new System.Drawing.Point(0, 211);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.panel2.Size = new System.Drawing.Size(1384, 535);
            this.panel2.TabIndex = 20;
            // 
            // tabContorlAggregate
            // 
            this.tabContorlAggregate.Controls.Add(this.tabPage1);
            this.tabContorlAggregate.Controls.Add(this.tabPage2);
            this.tabContorlAggregate.Controls.Add(this.tabPage3);
            this.tabContorlAggregate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContorlAggregate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabContorlAggregate.Location = new System.Drawing.Point(15, 15);
            this.tabContorlAggregate.Name = "tabContorlAggregate";
            this.tabContorlAggregate.SelectedIndex = 0;
            this.tabContorlAggregate.Size = new System.Drawing.Size(1354, 520);
            this.tabContorlAggregate.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvPrsAggregate1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1346, 490);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "１．プロジェクト・工程単位";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvPrsAggregate1
            // 
            this.dgvPrsAggregate1.AllowUserToAddRows = false;
            this.dgvPrsAggregate1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPrsAggregate1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPrsAggregate1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrsAggregate1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KokyakuName1,
            this.ProjectCD1,
            this.ProjectName1,
            this.KouteiName1,
            this.WorkTimeH1,
            this.KousuNinNichi1,
            this.Kousu1});
            this.dgvPrsAggregate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrsAggregate1.Location = new System.Drawing.Point(3, 3);
            this.dgvPrsAggregate1.Name = "dgvPrsAggregate1";
            this.dgvPrsAggregate1.ReadOnly = true;
            this.dgvPrsAggregate1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPrsAggregate1.RowTemplate.Height = 21;
            this.dgvPrsAggregate1.Size = new System.Drawing.Size(1340, 484);
            this.dgvPrsAggregate1.TabIndex = 21;
            // 
            // KokyakuName1
            // 
            this.KokyakuName1.DataPropertyName = "KokyakuName";
            this.KokyakuName1.HeaderText = "顧客名";
            this.KokyakuName1.Name = "KokyakuName1";
            this.KokyakuName1.ReadOnly = true;
            this.KokyakuName1.Width = 200;
            // 
            // ProjectCD1
            // 
            this.ProjectCD1.DataPropertyName = "ProjectCD";
            this.ProjectCD1.HeaderText = "プロジェクトコード";
            this.ProjectCD1.Name = "ProjectCD1";
            this.ProjectCD1.ReadOnly = true;
            this.ProjectCD1.Width = 120;
            // 
            // ProjectName1
            // 
            this.ProjectName1.DataPropertyName = "ProjectName";
            this.ProjectName1.HeaderText = "作業件名";
            this.ProjectName1.Name = "ProjectName1";
            this.ProjectName1.ReadOnly = true;
            this.ProjectName1.Width = 300;
            // 
            // KouteiName1
            // 
            this.KouteiName1.DataPropertyName = "KouteiName";
            this.KouteiName1.HeaderText = "工程";
            this.KouteiName1.Name = "KouteiName1";
            this.KouteiName1.ReadOnly = true;
            this.KouteiName1.Width = 120;
            // 
            // WorkTimeH1
            // 
            this.WorkTimeH1.DataPropertyName = "WorkTimeH";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.WorkTimeH1.DefaultCellStyle = dataGridViewCellStyle2;
            this.WorkTimeH1.HeaderText = "作業時間（H）";
            this.WorkTimeH1.Name = "WorkTimeH1";
            this.WorkTimeH1.ReadOnly = true;
            this.WorkTimeH1.Width = 130;
            // 
            // KousuNinNichi1
            // 
            this.KousuNinNichi1.DataPropertyName = "KousuNinNichi";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.KousuNinNichi1.DefaultCellStyle = dataGridViewCellStyle3;
            this.KousuNinNichi1.HeaderText = "工数(人日)";
            this.KousuNinNichi1.Name = "KousuNinNichi1";
            this.KousuNinNichi1.ReadOnly = true;
            this.KousuNinNichi1.Width = 110;
            // 
            // Kousu1
            // 
            this.Kousu1.DataPropertyName = "Kousu";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kousu1.DefaultCellStyle = dataGridViewCellStyle4;
            this.Kousu1.HeaderText = "工数(人月)";
            this.Kousu1.Name = "Kousu1";
            this.Kousu1.ReadOnly = true;
            this.Kousu1.Width = 110;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvPrsAggregate2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1346, 490);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "２．プロジェクト・工程・担当者単位";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvPrsAggregate2
            // 
            this.dgvPrsAggregate2.AllowUserToAddRows = false;
            this.dgvPrsAggregate2.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPrsAggregate2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPrsAggregate2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrsAggregate2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProjectCD2,
            this.KokyakuName2,
            this.ProjectCD_Disp2,
            this.ProjectName2,
            this.KouteiName2,
            this.TantoName2,
            this.WorkTimeH2,
            this.KousuNinNichi2,
            this.Kousu2});
            this.dgvPrsAggregate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrsAggregate2.Location = new System.Drawing.Point(3, 3);
            this.dgvPrsAggregate2.Name = "dgvPrsAggregate2";
            this.dgvPrsAggregate2.ReadOnly = true;
            this.dgvPrsAggregate2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvPrsAggregate2.RowTemplate.Height = 21;
            this.dgvPrsAggregate2.Size = new System.Drawing.Size(1340, 484);
            this.dgvPrsAggregate2.TabIndex = 22;
            // 
            // ProjectCD2
            // 
            this.ProjectCD2.DataPropertyName = "ProjectCD";
            this.ProjectCD2.HeaderText = "プロジェクトコード（内部）";
            this.ProjectCD2.Name = "ProjectCD2";
            this.ProjectCD2.ReadOnly = true;
            this.ProjectCD2.Visible = false;
            this.ProjectCD2.Width = 200;
            // 
            // KokyakuName2
            // 
            this.KokyakuName2.DataPropertyName = "KokyakuName";
            this.KokyakuName2.HeaderText = "顧客名";
            this.KokyakuName2.Name = "KokyakuName2";
            this.KokyakuName2.ReadOnly = true;
            this.KokyakuName2.Width = 200;
            // 
            // ProjectCD_Disp2
            // 
            this.ProjectCD_Disp2.DataPropertyName = "ProjectCD_Disp";
            this.ProjectCD_Disp2.HeaderText = "プロジェクトコード";
            this.ProjectCD_Disp2.Name = "ProjectCD_Disp2";
            this.ProjectCD_Disp2.ReadOnly = true;
            this.ProjectCD_Disp2.Width = 120;
            // 
            // ProjectName2
            // 
            this.ProjectName2.DataPropertyName = "ProjectName";
            this.ProjectName2.HeaderText = "作業件名";
            this.ProjectName2.Name = "ProjectName2";
            this.ProjectName2.ReadOnly = true;
            this.ProjectName2.Width = 300;
            // 
            // KouteiName2
            // 
            this.KouteiName2.DataPropertyName = "KouteiName";
            this.KouteiName2.HeaderText = "工程";
            this.KouteiName2.Name = "KouteiName2";
            this.KouteiName2.ReadOnly = true;
            this.KouteiName2.Width = 120;
            // 
            // TantoName2
            // 
            this.TantoName2.DataPropertyName = "TantoName";
            this.TantoName2.HeaderText = "担当者名";
            this.TantoName2.Name = "TantoName2";
            this.TantoName2.ReadOnly = true;
            // 
            // WorkTimeH2
            // 
            this.WorkTimeH2.DataPropertyName = "WorkTimeH";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.WorkTimeH2.DefaultCellStyle = dataGridViewCellStyle6;
            this.WorkTimeH2.HeaderText = "作業時間（H）";
            this.WorkTimeH2.Name = "WorkTimeH2";
            this.WorkTimeH2.ReadOnly = true;
            this.WorkTimeH2.Width = 130;
            // 
            // KousuNinNichi2
            // 
            this.KousuNinNichi2.DataPropertyName = "KousuNinNichi";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.KousuNinNichi2.DefaultCellStyle = dataGridViewCellStyle7;
            this.KousuNinNichi2.HeaderText = "工数(人日)";
            this.KousuNinNichi2.Name = "KousuNinNichi2";
            this.KousuNinNichi2.ReadOnly = true;
            this.KousuNinNichi2.Width = 110;
            // 
            // Kousu2
            // 
            this.Kousu2.DataPropertyName = "Kousu";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kousu2.DefaultCellStyle = dataGridViewCellStyle8;
            this.Kousu2.HeaderText = "工数(人月)";
            this.Kousu2.Name = "Kousu2";
            this.Kousu2.ReadOnly = true;
            this.Kousu2.Width = 110;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvPrsAggregate3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1346, 490);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "３．担当者・プロジェクト・工程単位";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgvPrsAggregate3
            // 
            this.dgvPrsAggregate3.AllowUserToAddRows = false;
            this.dgvPrsAggregate3.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPrsAggregate3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvPrsAggregate3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrsAggregate3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TantoID3,
            this.TantoName3,
            this.KokyakuName3,
            this.ProjectCD3,
            this.ProjectName3,
            this.KouteiName3,
            this.WorkTimeH3,
            this.KousuNinNichi3,
            this.Kousu3});
            this.dgvPrsAggregate3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrsAggregate3.Location = new System.Drawing.Point(3, 3);
            this.dgvPrsAggregate3.Name = "dgvPrsAggregate3";
            this.dgvPrsAggregate3.ReadOnly = true;
            this.dgvPrsAggregate3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPrsAggregate3.RowTemplate.Height = 21;
            this.dgvPrsAggregate3.Size = new System.Drawing.Size(1340, 484);
            this.dgvPrsAggregate3.TabIndex = 23;
            // 
            // TantoID3
            // 
            this.TantoID3.DataPropertyName = "TantoID";
            this.TantoID3.HeaderText = "担当者ID（内部）";
            this.TantoID3.Name = "TantoID3";
            this.TantoID3.ReadOnly = true;
            this.TantoID3.Visible = false;
            this.TantoID3.Width = 200;
            // 
            // TantoName3
            // 
            this.TantoName3.DataPropertyName = "TantoName";
            this.TantoName3.HeaderText = "担当者名";
            this.TantoName3.Name = "TantoName3";
            this.TantoName3.ReadOnly = true;
            // 
            // KokyakuName3
            // 
            this.KokyakuName3.DataPropertyName = "KokyakuName";
            this.KokyakuName3.HeaderText = "顧客名";
            this.KokyakuName3.Name = "KokyakuName3";
            this.KokyakuName3.ReadOnly = true;
            this.KokyakuName3.Width = 200;
            // 
            // ProjectCD3
            // 
            this.ProjectCD3.DataPropertyName = "ProjectCD";
            this.ProjectCD3.HeaderText = "プロジェクトコード";
            this.ProjectCD3.Name = "ProjectCD3";
            this.ProjectCD3.ReadOnly = true;
            this.ProjectCD3.Width = 120;
            // 
            // ProjectName3
            // 
            this.ProjectName3.DataPropertyName = "ProjectName";
            this.ProjectName3.HeaderText = "作業件名";
            this.ProjectName3.Name = "ProjectName3";
            this.ProjectName3.ReadOnly = true;
            this.ProjectName3.Width = 300;
            // 
            // KouteiName3
            // 
            this.KouteiName3.DataPropertyName = "KouteiName";
            this.KouteiName3.HeaderText = "工程";
            this.KouteiName3.Name = "KouteiName3";
            this.KouteiName3.ReadOnly = true;
            this.KouteiName3.Width = 120;
            // 
            // WorkTimeH3
            // 
            this.WorkTimeH3.DataPropertyName = "WorkTimeH";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.WorkTimeH3.DefaultCellStyle = dataGridViewCellStyle10;
            this.WorkTimeH3.HeaderText = "作業時間（H）";
            this.WorkTimeH3.Name = "WorkTimeH3";
            this.WorkTimeH3.ReadOnly = true;
            this.WorkTimeH3.Width = 130;
            // 
            // KousuNinNichi3
            // 
            this.KousuNinNichi3.DataPropertyName = "KousuNinNichi";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.KousuNinNichi3.DefaultCellStyle = dataGridViewCellStyle11;
            this.KousuNinNichi3.HeaderText = "工数(人日)";
            this.KousuNinNichi3.Name = "KousuNinNichi3";
            this.KousuNinNichi3.ReadOnly = true;
            this.KousuNinNichi3.Width = 110;
            // 
            // Kousu3
            // 
            this.Kousu3.DataPropertyName = "Kousu";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kousu3.DefaultCellStyle = dataGridViewCellStyle12;
            this.Kousu3.HeaderText = "工数(人月)";
            this.Kousu3.Name = "Kousu3";
            this.Kousu3.ReadOnly = true;
            this.Kousu3.Width = 110;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 211);
            this.panel1.TabIndex = 0;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.cmbPBushoKbn);
            this.grpInput.Controls.Add(this.label8);
            this.grpInput.Controls.Add(this.label4);
            this.grpInput.Controls.Add(this.txtSagyoName);
            this.grpInput.Controls.Add(this.label5);
            this.grpInput.Controls.Add(this.txtProjectCode);
            this.grpInput.Controls.Add(this.pnlKokyakuRdo);
            this.grpInput.Controls.Add(this.txtKokyakuName);
            this.grpInput.Controls.Add(this.label6);
            this.grpInput.Controls.Add(this.cmbGyomuKbn);
            this.grpInput.Controls.Add(this.label7);
            this.grpInput.Controls.Add(this.label3);
            this.grpInput.Controls.Add(this.txtWorkTimeDay);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.txtTaishoYMEnd);
            this.grpInput.Controls.Add(this.txtTaishoYMStart);
            this.grpInput.Controls.Add(this.panel3);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.txtHoujunTime);
            this.grpInput.Controls.Add(this.lblTgtMonth);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 201);
            this.grpInput.TabIndex = 1;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // cmbPBushoKbn
            // 
            this.cmbPBushoKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPBushoKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbPBushoKbn.FormattingEnabled = true;
            this.cmbPBushoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbPBushoKbn.ItemHeight = 17;
            this.cmbPBushoKbn.Location = new System.Drawing.Point(468, 102);
            this.cmbPBushoKbn.Name = "cmbPBushoKbn";
            this.cmbPBushoKbn.Size = new System.Drawing.Size(151, 25);
            this.cmbPBushoKbn.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(337, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 17);
            this.label8.TabIndex = 76;
            this.label8.Text = "プロジェクト部署区分";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(337, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 74;
            this.label4.Text = "作業内容";
            // 
            // txtSagyoName
            // 
            this.txtSagyoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSagyoName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSagyoName.Location = new System.Drawing.Point(468, 51);
            this.txtSagyoName.MaxLength = 50;
            this.txtSagyoName.Name = "txtSagyoName";
            this.txtSagyoName.Size = new System.Drawing.Size(300, 24);
            this.txtSagyoName.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(337, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 73;
            this.label5.Text = "プロジェクトコード";
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProjectCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProjectCode.Location = new System.Drawing.Point(468, 26);
            this.txtProjectCode.MaxLength = 8;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Size = new System.Drawing.Size(82, 24);
            this.txtProjectCode.TabIndex = 3;
            // 
            // pnlKokyakuRdo
            // 
            this.pnlKokyakuRdo.Controls.Add(this.rdoFutokutei);
            this.pnlKokyakuRdo.Controls.Add(this.rdoSyanai);
            this.pnlKokyakuRdo.Controls.Add(this.rdoIraimoto);
            this.pnlKokyakuRdo.Controls.Add(this.rdoNothing);
            this.pnlKokyakuRdo.Location = new System.Drawing.Point(468, 129);
            this.pnlKokyakuRdo.Name = "pnlKokyakuRdo";
            this.pnlKokyakuRdo.Size = new System.Drawing.Size(300, 23);
            this.pnlKokyakuRdo.TabIndex = 7;
            // 
            // rdoFutokutei
            // 
            this.rdoFutokutei.AutoSize = true;
            this.rdoFutokutei.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoFutokutei.Location = new System.Drawing.Point(189, 0);
            this.rdoFutokutei.Name = "rdoFutokutei";
            this.rdoFutokutei.Size = new System.Drawing.Size(91, 23);
            this.rdoFutokutei.TabIndex = 3;
            this.rdoFutokutei.TabStop = true;
            this.rdoFutokutei.Text = "不特定客先";
            this.rdoFutokutei.UseVisualStyleBackColor = true;
            // 
            // rdoSyanai
            // 
            this.rdoSyanai.AutoSize = true;
            this.rdoSyanai.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSyanai.Location = new System.Drawing.Point(137, 0);
            this.rdoSyanai.Name = "rdoSyanai";
            this.rdoSyanai.Size = new System.Drawing.Size(52, 23);
            this.rdoSyanai.TabIndex = 2;
            this.rdoSyanai.TabStop = true;
            this.rdoSyanai.Text = "社内";
            this.rdoSyanai.UseVisualStyleBackColor = true;
            // 
            // rdoIraimoto
            // 
            this.rdoIraimoto.AutoSize = true;
            this.rdoIraimoto.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoIraimoto.Location = new System.Drawing.Point(72, 0);
            this.rdoIraimoto.Name = "rdoIraimoto";
            this.rdoIraimoto.Size = new System.Drawing.Size(65, 23);
            this.rdoIraimoto.TabIndex = 1;
            this.rdoIraimoto.TabStop = true;
            this.rdoIraimoto.Text = "依頼元";
            this.rdoIraimoto.UseVisualStyleBackColor = true;
            // 
            // rdoNothing
            // 
            this.rdoNothing.AutoSize = true;
            this.rdoNothing.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoNothing.Location = new System.Drawing.Point(0, 0);
            this.rdoNothing.Name = "rdoNothing";
            this.rdoNothing.Size = new System.Drawing.Size(72, 23);
            this.rdoNothing.TabIndex = 0;
            this.rdoNothing.TabStop = true;
            this.rdoNothing.Text = "選択なし";
            this.rdoNothing.UseVisualStyleBackColor = true;
            // 
            // txtKokyakuName
            // 
            this.txtKokyakuName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokyakuName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKokyakuName.Location = new System.Drawing.Point(468, 154);
            this.txtKokyakuName.MaxLength = 100;
            this.txtKokyakuName.Name = "txtKokyakuName";
            this.txtKokyakuName.Size = new System.Drawing.Size(300, 24);
            this.txtKokyakuName.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(337, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 72;
            this.label6.Text = "顧客名";
            // 
            // cmbGyomuKbn
            // 
            this.cmbGyomuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGyomuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbGyomuKbn.FormattingEnabled = true;
            this.cmbGyomuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbGyomuKbn.Location = new System.Drawing.Point(468, 76);
            this.cmbGyomuKbn.Name = "cmbGyomuKbn";
            this.cmbGyomuKbn.Size = new System.Drawing.Size(82, 25);
            this.cmbGyomuKbn.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(337, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 17);
            this.label7.TabIndex = 71;
            this.label7.Text = "業務区分";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(17, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 17);
            this.label3.TabIndex = 64;
            this.label3.Text = "1日の就業時間(H)";
            // 
            // txtWorkTimeDay
            // 
            this.txtWorkTimeDay.Location = new System.Drawing.Point(149, 26);
            this.txtWorkTimeDay.Name = "txtWorkTimeDay";
            this.txtWorkTimeDay.ReadOnly = true;
            this.txtWorkTimeDay.Size = new System.Drawing.Size(62, 24);
            this.txtWorkTimeDay.TabIndex = 0;
            this.txtWorkTimeDay.TabStop = false;
            this.txtWorkTimeDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(216, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 62;
            this.label2.Text = "～";
            // 
            // txtTaishoYMEnd
            // 
            this.txtTaishoYMEnd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishoYMEnd.Location = new System.Drawing.Point(243, 76);
            this.txtTaishoYMEnd.MaxLength = 7;
            this.txtTaishoYMEnd.Name = "txtTaishoYMEnd";
            this.txtTaishoYMEnd.Size = new System.Drawing.Size(62, 24);
            this.txtTaishoYMEnd.TabIndex = 2;
            this.txtTaishoYMEnd.Text = "yyyy/MM";
            this.txtTaishoYMEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTaishoYMStart
            // 
            this.txtTaishoYMStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishoYMStart.Location = new System.Drawing.Point(149, 76);
            this.txtTaishoYMStart.MaxLength = 7;
            this.txtTaishoYMStart.Name = "txtTaishoYMStart";
            this.txtTaishoYMStart.Size = new System.Drawing.Size(62, 24);
            this.txtTaishoYMStart.TabIndex = 1;
            this.txtTaishoYMStart.Text = "yyyy/MM";
            this.txtTaishoYMStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chkContainGyomuX);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1161, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(190, 178);
            this.panel3.TabIndex = 10;
            // 
            // chkContainGyomuX
            // 
            this.chkContainGyomuX.AutoSize = true;
            this.chkContainGyomuX.Location = new System.Drawing.Point(33, 104);
            this.chkContainGyomuX.Name = "chkContainGyomuX";
            this.chkContainGyomuX.Size = new System.Drawing.Size(97, 21);
            this.chkContainGyomuX.TabIndex = 10;
            this.chkContainGyomuX.Text = "休暇を含める";
            this.chkContainGyomuX.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(33, 136);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 35);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(17, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 17);
            this.label1.TabIndex = 60;
            this.label1.Text = "1ヵ月の標準時間(H)";
            // 
            // txtHoujunTime
            // 
            this.txtHoujunTime.Location = new System.Drawing.Point(149, 51);
            this.txtHoujunTime.Name = "txtHoujunTime";
            this.txtHoujunTime.ReadOnly = true;
            this.txtHoujunTime.Size = new System.Drawing.Size(62, 24);
            this.txtHoujunTime.TabIndex = 0;
            this.txtHoujunTime.TabStop = false;
            this.txtHoujunTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTgtMonth
            // 
            this.lblTgtMonth.AutoSize = true;
            this.lblTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTgtMonth.Location = new System.Drawing.Point(17, 79);
            this.lblTgtMonth.Name = "lblTgtMonth";
            this.lblTgtMonth.Size = new System.Drawing.Size(60, 17);
            this.lblTgtMonth.TabIndex = 58;
            this.lblTgtMonth.Text = "対象年月";
            // 
            // PAF1030_PrsAggregateBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlFootor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 600);
            this.Name = "PAF1030_PrsAggregateBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRS実績月次集計";
            this.pnlFootor.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabContorlAggregate.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrsAggregate3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.pnlKokyakuRdo.ResumeLayout(false);
            this.pnlKokyakuRdo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnOutputCsv;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.TabControl tabContorlAggregate;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.DataGridView dgvPrsAggregate1;
        public System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.DataGridView dgvPrsAggregate2;
        public System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.DataGridView dgvPrsAggregate3;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.CheckBox chkContainGyomuX;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtHoujunTime;
        public System.Windows.Forms.Label lblTgtMonth;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtTaishoYMEnd;
        public System.Windows.Forms.TextBox txtTaishoYMStart;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtWorkTimeDay;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtSagyoName;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtProjectCode;
        public System.Windows.Forms.Panel pnlKokyakuRdo;
        public System.Windows.Forms.RadioButton rdoFutokutei;
        public System.Windows.Forms.RadioButton rdoSyanai;
        public System.Windows.Forms.RadioButton rdoIraimoto;
        public System.Windows.Forms.RadioButton rdoNothing;
        public System.Windows.Forms.TextBox txtKokyakuName;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cmbGyomuKbn;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cmbPBushoKbn;
        public System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkTimeH1;
        private System.Windows.Forms.DataGridViewTextBoxColumn KousuNinNichi1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kousu1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TantoID3;
        private System.Windows.Forms.DataGridViewTextBoxColumn TantoName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkTimeH3;
        private System.Windows.Forms.DataGridViewTextBoxColumn KousuNinNichi3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kousu3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD2;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD_Disp2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TantoName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkTimeH2;
        private System.Windows.Forms.DataGridViewTextBoxColumn KousuNinNichi2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kousu2;
    }
}

