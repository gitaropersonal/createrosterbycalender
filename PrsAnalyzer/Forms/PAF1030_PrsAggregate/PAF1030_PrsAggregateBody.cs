﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF1030_PrsAggregateBody : Form {
        public PAF1030_PrsAggregateBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF1030_PrsAggregateFormLogic(this, loginInfo);
        }
    }
}
