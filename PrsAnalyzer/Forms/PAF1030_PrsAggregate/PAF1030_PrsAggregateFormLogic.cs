﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF1030_PrsAggregateFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF1030_PrsAggregateBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF1030_PrsAggregateFormLogic(PAF1030_PrsAggregateBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // CSV出力ボタン押下
            _Body.btnOutputCsv.Click += (s, e) => {


            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // 対象年月（開始）ロストフォーカスイベント
            _Body.txtTaishoYMStart.Validated += (s, e) => {
                EditTextYM(_Body.txtTaishoYMStart);
            };
            // 対象年月（終了）ロストフォーカスイベント
            _Body.txtTaishoYMEnd.Validated += (s, e) => {
                EditTextYM(_Body.txtTaishoYMEnd);
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // グリッド行番号描画
            _Body.dgvPrsAggregate1.RowPostPaint += (s, e) => {
                GridRowUtil<PrsAggregate1GridDto>.SetRowNum(s, e);
            };
            _Body.dgvPrsAggregate2.RowPostPaint += (s, e) => {
                GridRowUtil<PrsAggregate2GridDto>.SetRowNum(s, e);
            };
            _Body.dgvPrsAggregate3.RowPostPaint += (s, e) => {
                GridRowUtil<PrsAggregate2GridDto>.SetRowNum(s, e);
            };

            // グリッドセルPainting
            _Body.dgvPrsAggregate1.CellPainting += (s, e) => {
                Grid1CellPainting();
            };
            _Body.dgvPrsAggregate2.CellPainting += (s, e) => {
                Grid2CellPainting();
            };
            _Body.dgvPrsAggregate3.CellPainting += (s, e) => {
                Grid3CellPainting();
            };

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnOutputCsv.Focus();
                        _Body.btnOutputCsv.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 年月テキストボックスロストフォーカスイベント
        /// </summary>
        /// <param name="txt"></param>
        private void EditTextYM(TextBox txt) {
            txt.Text = TypeConversionUtil.ConvertInputToStrYM(txt.Text);
        }
        /// <summary>
        /// グリッド1CellPainting
        /// </summary>
        private void Grid1CellPainting() {
            var dto1 = new PrsAggregate1GridDto();
            foreach (DataGridViewRow r in _Body.dgvPrsAggregate1.Rows) {
                string gyomuKbn = StringUtil.TostringNullForbid(r.Cells[$"{nameof(dto1.ProjectCD)}{1}"].Value);
                if (!string.IsNullOrEmpty(gyomuKbn)) {
                    gyomuKbn = gyomuKbn.Substring(0, 1);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto1.ProjectCD)}{1}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto1.ProjectName)}{1}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto1.KokyakuName)}{1}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto1.KouteiName)}{1}"]);
                    continue;
                }
                r.Cells[$"{nameof(dto1.KouteiName)}{1}"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                r.Cells[$"{nameof(dto1.KouteiName)}{1}"].Style.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, _Body.dgvPrsAggregate1.DefaultCellStyle.Font.Size, FontStyle.Bold);
            }
        }
        /// <summary>
        /// グリッド2CellPainting
        /// </summary>
        private void Grid2CellPainting() {
            var dto2 = new PrsAggregate2GridDto();
            foreach (DataGridViewRow r in _Body.dgvPrsAggregate2.Rows) {
                string gyomuKbn = StringUtil.TostringNullForbid(r.Cells[$"{nameof(dto2.ProjectCD)}{2}"].Value);

                if (!string.IsNullOrEmpty(gyomuKbn)) {
                    gyomuKbn = gyomuKbn.Substring(0, 1);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.ProjectCD_Disp)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.ProjectName)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.KokyakuName)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.KouteiName)}{2}"]);
                }
                string tantoName = StringUtil.TostringNullForbid(r.Cells[$"{nameof(dto2.TantoName)}{2}"].Value);
                if (tantoName == "（小計）" || tantoName == "（合計）") {
                    r.Cells[$"{nameof(dto2.TantoName)}{2}"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    r.Cells[$"{nameof(dto2.TantoName)}{2}"].Style.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, _Body.dgvPrsAggregate2.DefaultCellStyle.Font.Size, FontStyle.Bold);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.TantoName)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.KokyakuName)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.WorkTimeH)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.KousuNinNichi)}{2}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto2.Kousu)}{2}"]);
                }
            }
            _Body.dgvPrsAggregate2.Columns[$"{nameof(dto2.ProjectCD)}{2}"].Visible = false;
        }
        /// <summary>
        /// グリッド3CellPainting
        /// </summary>
        private void Grid3CellPainting() {
            var dto3 = new PrsAggregate3GridDto();
            foreach (DataGridViewRow r in _Body.dgvPrsAggregate3.Rows) {
                string gyomuKbn = StringUtil.TostringNullForbid(r.Cells[$"{nameof(dto3.ProjectCD)}{3}"].Value);

                if (!string.IsNullOrEmpty(gyomuKbn)) {
                    gyomuKbn = gyomuKbn.Substring(0, 1);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto3.ProjectCD)}{3}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto3.ProjectName)}{3}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto3.KokyakuName)}{3}"]);
                    ControlUtil.SetTimeTrackerCellStyle(gyomuKbn, r.Cells[$"{nameof(dto3.KouteiName)}{3}"]);
                }
                string kouteiName = StringUtil.TostringNullForbid(r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Value);
                if (kouteiName != "（合計）") {
                    r.Cells[$"{nameof(dto3.TantoName)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                }
                if (kouteiName == "（小計）") {
                    r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Style.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, _Body.dgvPrsAggregate3.DefaultCellStyle.Font.Size, FontStyle.Bold);
                    r.Cells[$"{nameof(dto3.KokyakuName)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.ProjectCD)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.TantoName)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.ProjectName)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.WorkTimeH)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.KousuNinNichi)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[$"{nameof(dto3.Kousu)}{3}"].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                }
                if (kouteiName == "（合計）") {
                    r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    r.Cells[$"{nameof(dto3.KouteiName)}{3}"].Style.Font = new Font(ControlUtil._DEFAULT_FONT_NAME, _Body.dgvPrsAggregate3.DefaultCellStyle.Font.Size, FontStyle.Bold);
                }
            }
            _Body.dgvPrsAggregate3.Columns[$"{nameof(dto3.TantoID)}{3}"].Visible = false;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        private void BtnSearch_ClickEvent() {

            _Body.dgvPrsAggregate1.Rows.Clear();
            _Body.dgvPrsAggregate2.Rows.Clear();
            _Body.dgvPrsAggregate3.Rows.Clear();

            // Validate（検索時）
            DateTime dtEnd = DateTime.MinValue;
            if (!ValidateSearch(ref dtEnd)) {
                return;
            }
            // パラメータDto1取得
            var pBushoKbn = ProjectConst.PBushoKbn.NONE;
            if (ProjectConst._DIC_PROJECT_KBN.ToList().Exists(n => n.Value == _Body.cmbPBushoKbn.Text)) {
                pBushoKbn = ProjectConst._DIC_PROJECT_KBN.ToList().First(n => n.Value == _Body.cmbPBushoKbn.Text).Key;
            }
            var con1 = new LoadAggregateConDto() {
                KijunTimeMonth = ConfigUtil.LoadDefaultKijunTimeMonth(),
                WorkTimeDay = ConfigUtil.LoadDefaultWorkTimeDay(),
                StrStartDt = $"{_Body.txtTaishoYMStart.Text}/01",
                StrEndDt = dtEnd.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH),
                IsContainX = _Body.chkContainGyomuX.Checked,
                PBushoKbn = pBushoKbn,
            };
            // パラメータDto2取得
            var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked);
            string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked, _Body.txtKokyakuName.Text);
            var con2 = new LoadProjectLooksConDto() {
                IsContainDeleted = true,
                ProjectCD = _Body.txtProjectCode.Text,
                ProjectName = _Body.txtSagyoName.Text,
                GyomuKbn = _Body.cmbGyomuKbn.Text,
                KokyakuKbn = kokyakuKbn,
                KokyakuName = kokyakuName,
            };
            // グリッドデータ
            var bindingList1 = new BindingList<PrsAggregate1GridDto>();
            var bindingList2 = new BindingList<PrsAggregate2GridDto>();
            var bindingList3 = new BindingList<PrsAggregate3GridDto>();

            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => GetGridDatas(con1, con2, ref bindingList1, ref bindingList2, ref bindingList3)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            if (bindingList1.Count == 1 || bindingList2.Count == 1 || bindingList3.Count == 1) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // グリッドデータセット
            _Body.dgvPrsAggregate1.DataSource = bindingList1;
            _Body.dgvPrsAggregate2.DataSource = bindingList2;
            _Body.dgvPrsAggregate3.DataSource = bindingList3;

            // フォーカス
            _Body.tabContorlAggregate.Focus();

        }
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <param name="bindingList1"></param>
        /// <param name="bindingList2"></param>
        /// <param name="bindingList3"></param>
        private void GetGridDatas(LoadAggregateConDto con1, LoadProjectLooksConDto con2, ref BindingList<PrsAggregate1GridDto> bindingList1
            , ref BindingList<PrsAggregate2GridDto> bindingList2, ref BindingList<PrsAggregate3GridDto> bindingList3) {

            var service = new PAF1030_PrsAggregateService();
            try {
                // データリスト１
                var dtoList1 = service.LoadPrsAggregate1(_DB_CONN_STRING, con1, con2);
                foreach (var dto in dtoList1) {
                    bindingList1.Add(dto);
                }
                // データリスト２
                var dtoList2 = service.LoadPrsAggregate2(_DB_CONN_STRING, con1, con2);
                foreach (var dto in dtoList2) {
                    if (bindingList2.ToList().Exists(n => n.ProjectCD_Disp == dto.ProjectCD_Disp && n.KouteiName == dto.KouteiName)) {
                        dto.KokyakuName = string.Empty;
                        dto.ProjectCD_Disp = string.Empty;
                        dto.ProjectName = string.Empty;
                        dto.KouteiName = string.Empty;
                    }
                    bindingList2.Add(dto);
                }
                // データリスト３
                var dtoList3 = service.LoadPrsAggregate3(_DB_CONN_STRING, con1, con2);
                foreach (var dto in dtoList3) {
                    if (bindingList3.ToList().Exists(n => n.TantoID == dto.TantoID)) {
                        dto.TantoName = string.Empty;
                    }
                    if (dto.ProjectCD == ProjectConst._PROJECT_CODE_SUMMARY_ROW) {
                        dto.ProjectCD = string.Empty;
                    }
                    bindingList3.Add(dto);
                }
            } catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate（検索時）
        /// </summary>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        private bool ValidateSearch(ref DateTime dtEnd) {
            if (string.IsNullOrEmpty(_Body.txtTaishoYMStart.Text)) {
                _Body.txtTaishoYMStart.Focus();
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_EMPTY, "対象年月（開始）"));
                return false;
            }
            DateTime dtStart = TypeConversionUtil.ToDateTime(_Body.txtTaishoYMStart.Text);
            dtEnd = dtStart;
            string strDtEnd = _Body.txtTaishoYMEnd.Text;
            if (string.IsNullOrEmpty(strDtEnd) || DateTime.MaxValue.ToString(Formats._FORMAT_DATE_YYYYMM_SLASH) == strDtEnd) {
                dtEnd = DateTime.MaxValue;
            }
            else {
                dtEnd = TypeConversionUtil.ToDateTime(strDtEnd).AddMonths(1).AddDays(-1);
            }
            if (dtEnd < dtStart) {
                _Body.txtTaishoYMStart.Focus();
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_WRONG, "対象年月の大小関係"));
                return false; ;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 対象月
            InitTxtTaishoYM();

            // 1日当たりの終業時間
            _Body.txtWorkTimeDay.Text = ConfigUtil.LoadDefaultWorkTimeDay().ToString(Formats._FORMAT_FLOAT_1);

            // 1ヵ月当たりの基準時間
            _Body.txtHoujunTime.Text = ConfigUtil.LoadDefaultKijunTimeMonth().ToString(Formats._FORMAT_FLOAT_1);

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // プロジェクト検索条件初期化
            InitSearchProjectLooksCondition();

            // 休暇を含むチェック
            _Body.chkContainGyomuX.Checked = false;

            // グリッド
            _Body.dgvPrsAggregate1.Rows.Clear();
            _Body.dgvPrsAggregate2.Rows.Clear();
            _Body.dgvPrsAggregate3.Rows.Clear();

            // タブ
            _Body.tabContorlAggregate.SelectedTab = _Body.tabPage1;

            // 背景色
            InitColor();

            // フォーカス
            _Body.txtTaishoYMStart.Focus();
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 集計月初期化
        /// </summary>
        private void InitTxtTaishoYM() {
            _Body.txtTaishoYMStart.Text = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMM_SLASH);
            _Body.txtTaishoYMEnd.Text = string.Empty;
        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {

            // 初期化
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);

            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length == 1) {
                    _Body.cmbGyomuKbn.Items.Add(pair.Key);
                }
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト部署区分コンボボックス初期化
        /// </summary>
        private void InitPBUshoKbnCombo() {
            _Body.cmbPBushoKbn.Items.Clear();
            _Body.cmbPBushoKbn.Items.Add(string.Empty);
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                _Body.cmbPBushoKbn.Items.Add(pair.Value);
            }
            _Body.cmbPBushoKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {
            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // プロジェクト名称
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // プロジェクト部署区分コンボボックス初期化
            InitPBUshoKbnCombo();

            // 依頼元ラジオボタン
            _Body.rdoNothing.Checked = true;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;
        }
        #endregion

    }
}
