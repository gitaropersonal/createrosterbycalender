﻿namespace PrsAnalyzer.Forms {
    partial class PAF2010_AnkenExcelCaptureBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF2010_AnkenExcelCaptureBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvAnkenExcelLooks = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCapture = new System.Windows.Forms.Button();
            this.cmbTgtMonth = new System.Windows.Forms.ComboBox();
            this.lblTgtMonth = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.FileName = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.Iraimoto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kousu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KenshuJoken = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Biko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlExecute.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnkenExcelLooks)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.panel3);
            this.pnlExecute.Controls.Add(this.panel1);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(1384, 811);
            this.pnlExecute.TabIndex = 25;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvAnkenExcelLooks);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel3.Location = new System.Drawing.Point(0, 119);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 65);
            this.panel3.Size = new System.Drawing.Size(1384, 692);
            this.panel3.TabIndex = 20;
            // 
            // dgvAnkenExcelLooks
            // 
            this.dgvAnkenExcelLooks.AllowUserToAddRows = false;
            this.dgvAnkenExcelLooks.AllowUserToResizeRows = false;
            this.dgvAnkenExcelLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnkenExcelLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Iraimoto,
            this.ProjectCD,
            this.KokyakuName,
            this.ProjectName,
            this.KouteiName,
            this.KouteiSeq,
            this.Kousu,
            this.KenshuJoken,
            this.Biko});
            this.dgvAnkenExcelLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAnkenExcelLooks.Location = new System.Drawing.Point(15, 15);
            this.dgvAnkenExcelLooks.Name = "dgvAnkenExcelLooks";
            this.dgvAnkenExcelLooks.ReadOnly = true;
            this.dgvAnkenExcelLooks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAnkenExcelLooks.RowTemplate.Height = 21;
            this.dgvAnkenExcelLooks.Size = new System.Drawing.Size(1354, 612);
            this.dgvAnkenExcelLooks.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 119);
            this.panel1.TabIndex = 2;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.cmbTgtMonth);
            this.grpInput.Controls.Add(this.lblTgtMonth);
            this.grpInput.Controls.Add(this.btnSearch);
            this.grpInput.Controls.Add(this.txtFileName);
            this.grpInput.Controls.Add(this.FileName);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 109);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCapture);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1192, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 86);
            this.panel2.TabIndex = 10;
            // 
            // btnCapture
            // 
            this.btnCapture.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCapture.Location = new System.Drawing.Point(21, 34);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(120, 35);
            this.btnCapture.TabIndex = 5;
            this.btnCapture.Text = "取込（J）";
            this.btnCapture.UseVisualStyleBackColor = true;
            // 
            // cmbTgtMonth
            // 
            this.cmbTgtMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbTgtMonth.FormattingEnabled = true;
            this.cmbTgtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbTgtMonth.Location = new System.Drawing.Point(85, 28);
            this.cmbTgtMonth.Name = "cmbTgtMonth";
            this.cmbTgtMonth.Size = new System.Drawing.Size(120, 25);
            this.cmbTgtMonth.TabIndex = 0;
            // 
            // lblTgtMonth
            // 
            this.lblTgtMonth.AutoSize = true;
            this.lblTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTgtMonth.Location = new System.Drawing.Point(15, 31);
            this.lblTgtMonth.Name = "lblTgtMonth";
            this.lblTgtMonth.Size = new System.Drawing.Size(60, 17);
            this.lblTgtMonth.TabIndex = 58;
            this.lblTgtMonth.Text = "対象年月";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(791, 53);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 28);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(85, 55);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(703, 24);
            this.txtFileName.TabIndex = 56;
            this.txtFileName.TabStop = false;
            // 
            // FileName
            // 
            this.FileName.AutoSize = true;
            this.FileName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FileName.Location = new System.Drawing.Point(16, 58);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(67, 17);
            this.FileName.TabIndex = 55;
            this.FileName.Text = "取込Excel";
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(14, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1200, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(49, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(140, 15);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // Iraimoto
            // 
            this.Iraimoto.DataPropertyName = "Iraimoto";
            this.Iraimoto.HeaderText = "依頼元";
            this.Iraimoto.Name = "Iraimoto";
            this.Iraimoto.ReadOnly = true;
            this.Iraimoto.Width = 80;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトコード";
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.ReadOnly = true;
            this.ProjectCD.Width = 120;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            this.KokyakuName.HeaderText = "客先名";
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.ReadOnly = true;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "ProjectName";
            this.ProjectName.HeaderText = "作業件名";
            this.ProjectName.MaxInputLength = 50;
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.ReadOnly = true;
            this.ProjectName.Width = 300;
            // 
            // KouteiName
            // 
            this.KouteiName.DataPropertyName = "KouteiName";
            this.KouteiName.HeaderText = "工程";
            this.KouteiName.Name = "KouteiName";
            this.KouteiName.ReadOnly = true;
            this.KouteiName.Width = 120;
            // 
            // KouteiSeq
            // 
            this.KouteiSeq.DataPropertyName = "KouteiSeq";
            this.KouteiSeq.HeaderText = "工程Seq";
            this.KouteiSeq.Name = "KouteiSeq";
            this.KouteiSeq.ReadOnly = true;
            this.KouteiSeq.Visible = false;
            // 
            // Kousu
            // 
            this.Kousu.DataPropertyName = "Kousu";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kousu.DefaultCellStyle = dataGridViewCellStyle1;
            this.Kousu.HeaderText = "工数(人月)";
            this.Kousu.Name = "Kousu";
            this.Kousu.ReadOnly = true;
            this.Kousu.Width = 110;
            // 
            // KenshuJoken
            // 
            this.KenshuJoken.DataPropertyName = "KenshuJoken";
            this.KenshuJoken.HeaderText = "検収条件";
            this.KenshuJoken.Name = "KenshuJoken";
            this.KenshuJoken.ReadOnly = true;
            // 
            // Biko
            // 
            this.Biko.DataPropertyName = "Biko";
            this.Biko.HeaderText = "備考";
            this.Biko.MaxInputLength = 100;
            this.Biko.Name = "Biko";
            this.Biko.ReadOnly = true;
            this.Biko.Width = 300;
            // 
            // PAF03000_AnkenExcelCaptureBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.pnlFootor);
            this.Controls.Add(this.pnlExecute);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 600);
            this.Name = "PAF03000_AnkenExcelCaptureBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "プロジェクト一覧Excel取込";
            this.pnlExecute.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnkenExcelLooks)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvAnkenExcelLooks;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnCapture;
        public System.Windows.Forms.ComboBox cmbTgtMonth;
        public System.Windows.Forms.Label lblTgtMonth;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtFileName;
        public System.Windows.Forms.Label FileName;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iraimoto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kousu;
        private System.Windows.Forms.DataGridViewTextBoxColumn KenshuJoken;
        private System.Windows.Forms.DataGridViewTextBoxColumn Biko;
    }
}

