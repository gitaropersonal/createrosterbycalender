﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF2010_AnkenExcelCaptureBody : Form {
        public PAF2010_AnkenExcelCaptureBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF2010_AnkenExcelCaptureFormLogic(this, loginInfo);
        }
    }
}
