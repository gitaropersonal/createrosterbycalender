﻿namespace PrsAnalyzer.Forms {
    partial class PAF2020_HGyomuExcelCaptureBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF2020_HGyomuExcelCaptureBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvHgyomuExcelLooks = new System.Windows.Forms.DataGridView();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HBushoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SagyoNaiyo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STantoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ETantoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.txtNendo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCapture = new System.Windows.Forms.Button();
            this.lblTgtYear = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.FileName = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.pnlExecute.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHgyomuExcelLooks)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.panel3);
            this.pnlExecute.Controls.Add(this.panel1);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(1384, 811);
            this.pnlExecute.TabIndex = 25;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvHgyomuExcelLooks);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel3.Location = new System.Drawing.Point(0, 119);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 65);
            this.panel3.Size = new System.Drawing.Size(1384, 692);
            this.panel3.TabIndex = 20;
            // 
            // dgvHgyomuExcelLooks
            // 
            this.dgvHgyomuExcelLooks.AllowUserToAddRows = false;
            this.dgvHgyomuExcelLooks.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHgyomuExcelLooks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHgyomuExcelLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHgyomuExcelLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProjectCD,
            this.HBushoName,
            this.KokyakuName,
            this.SagyoNaiyo,
            this.STantoName,
            this.ETantoName});
            this.dgvHgyomuExcelLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHgyomuExcelLooks.Location = new System.Drawing.Point(15, 15);
            this.dgvHgyomuExcelLooks.Name = "dgvHgyomuExcelLooks";
            this.dgvHgyomuExcelLooks.ReadOnly = true;
            this.dgvHgyomuExcelLooks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvHgyomuExcelLooks.RowTemplate.Height = 21;
            this.dgvHgyomuExcelLooks.Size = new System.Drawing.Size(1354, 612);
            this.dgvHgyomuExcelLooks.TabIndex = 20;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトコード";
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.ReadOnly = true;
            this.ProjectCD.Width = 120;
            // 
            // HBushoName
            // 
            this.HBushoName.DataPropertyName = "HBushoName";
            this.HBushoName.HeaderText = "引受元部署";
            this.HBushoName.Name = "HBushoName";
            this.HBushoName.ReadOnly = true;
            this.HBushoName.Width = 120;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            this.KokyakuName.HeaderText = "顧客名";
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.ReadOnly = true;
            this.KokyakuName.Width = 250;
            // 
            // SagyoNaiyo
            // 
            this.SagyoNaiyo.DataPropertyName = "SagyoNaiyo";
            this.SagyoNaiyo.HeaderText = "作業内容";
            this.SagyoNaiyo.Name = "SagyoNaiyo";
            this.SagyoNaiyo.ReadOnly = true;
            this.SagyoNaiyo.Width = 200;
            // 
            // STantoName
            // 
            this.STantoName.DataPropertyName = "STantoName";
            this.STantoName.HeaderText = "システム担当者名";
            this.STantoName.Name = "STantoName";
            this.STantoName.ReadOnly = true;
            this.STantoName.Width = 180;
            // 
            // ETantoName
            // 
            this.ETantoName.DataPropertyName = "ETantoName";
            this.ETantoName.HeaderText = "営業担当名";
            this.ETantoName.Name = "ETantoName";
            this.ETantoName.ReadOnly = true;
            this.ETantoName.Width = 180;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 119);
            this.panel1.TabIndex = 2;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.txtNendo);
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.lblTgtYear);
            this.grpInput.Controls.Add(this.btnSearch);
            this.grpInput.Controls.Add(this.txtFileName);
            this.grpInput.Controls.Add(this.FileName);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 109);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // txtNendo
            // 
            this.txtNendo.Location = new System.Drawing.Point(85, 28);
            this.txtNendo.Name = "txtNendo";
            this.txtNendo.Size = new System.Drawing.Size(49, 24);
            this.txtNendo.TabIndex = 0;
            this.txtNendo.Text = "YYYY";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCapture);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1192, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 86);
            this.panel2.TabIndex = 10;
            // 
            // btnCapture
            // 
            this.btnCapture.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCapture.Location = new System.Drawing.Point(21, 34);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(120, 35);
            this.btnCapture.TabIndex = 5;
            this.btnCapture.Text = "取込（J）";
            this.btnCapture.UseVisualStyleBackColor = true;
            // 
            // lblTgtYear
            // 
            this.lblTgtYear.AutoSize = true;
            this.lblTgtYear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTgtYear.Location = new System.Drawing.Point(15, 31);
            this.lblTgtYear.Name = "lblTgtYear";
            this.lblTgtYear.Size = new System.Drawing.Size(34, 17);
            this.lblTgtYear.TabIndex = 58;
            this.lblTgtYear.Text = "年度";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(791, 53);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 28);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(85, 55);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(703, 24);
            this.txtFileName.TabIndex = 56;
            this.txtFileName.TabStop = false;
            // 
            // FileName
            // 
            this.FileName.AutoSize = true;
            this.FileName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FileName.Location = new System.Drawing.Point(16, 58);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(67, 17);
            this.FileName.TabIndex = 55;
            this.FileName.Text = "取込Excel";
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(14, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1200, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(49, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(140, 15);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // PAF2020_HGyomuExcelCaptureBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.pnlFootor);
            this.Controls.Add(this.pnlExecute);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 600);
            this.Name = "PAF2020_HGyomuExcelCaptureBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "H個別コードExcel取込";
            this.pnlExecute.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHgyomuExcelLooks)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlExecute;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvHgyomuExcelLooks;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn HBushoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SagyoNaiyo;
        private System.Windows.Forms.DataGridViewTextBoxColumn STantoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ETantoName;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        public System.Windows.Forms.TextBox txtNendo;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnCapture;
        public System.Windows.Forms.Label lblTgtYear;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtFileName;
        public System.Windows.Forms.Label FileName;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel pnlFootor;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnUpdate;
    }
}

