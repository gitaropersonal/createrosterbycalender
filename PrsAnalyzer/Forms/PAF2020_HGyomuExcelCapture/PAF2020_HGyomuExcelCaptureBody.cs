﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF2020_HGyomuExcelCaptureBody : Form {
        public PAF2020_HGyomuExcelCaptureBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF2020_HGyomuExcelCaptureFormLogic(this, loginInfo);
        }
    }
}
