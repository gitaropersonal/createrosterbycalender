﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Logic.Excel;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF2020_HGyomuExcelCaptureFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF2020_HGyomuExcelCaptureBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// 登録工程リスト
        /// </summary>
        List<AnkenExcelDto> _ADD_KOUTEILIST = new List<AnkenExcelDto>();
        /// <summary>
        /// クリアボタン押下回数
        /// </summary>
        private static Enums.InitMode _BTN_CLEAR_MODE = Enums.InitMode.All;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF2020_HGyomuExcelCaptureFormLogic(PAF2020_HGyomuExcelCaptureBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            _BTN_CLEAR_MODE = Enums.InitMode.All;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 取込ボタン押下
            _Body.btnCapture.Click += (s, e) => {
                BtnCapture_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpd_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // グリッド行番号描画
            _Body.dgvHgyomuExcelLooks.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeGridDto>.SetRowNum(s, e);
            };

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.J:
                        _Body.btnCapture.Focus();
                        _Body.btnCapture.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// 使用可否切り替え（検索条件欄）
        /// </summary>
        /// <param name="IsSelectBtnClick"></param>
        private void SwitchEnable_GlpInput(bool IsSelectBtnClick) {
            _Body.grpInput.Enabled = !IsSelectBtnClick;
            _Body.dgvHgyomuExcelLooks.Enabled = IsSelectBtnClick;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvHgyomuExcelLooks.Rows) {
                var rowDto = GridRowUtil<HGyomuExcelDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist && _BTN_CLEAR_MODE == Enums.InitMode.EnableCon) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        private void BtnSearch_ClickEvent() {

            var ofd = new OpenFileDialog() {
                InitialDirectory = @"C:\",
                Filter = "Excelファイル(*.xlsx;*.xls)|*.xlsx;*.xlsx",
                Title = "ファイルを選択してください",
                RestoreDirectory = false,
            };

            //ダイアログを表示する
            if (ofd.ShowDialog() == DialogResult.OK) {
                if (string.IsNullOrEmpty(ofd.FileName)) {
                    return;
                }
                _Body.txtFileName.Text = ofd.FileName;
                _Body.dgvHgyomuExcelLooks.Rows.Clear();
            }
        }
        /// <summary>
        /// 取込ボタン押下イベント
        /// </summary>
        private void BtnCapture_ClickEvent() {

            _Body.txtFileName.BackColor = Color.Empty;

            // Validate
            if (string.IsNullOrEmpty(_Body.txtFileName.Text)) {
                _Body.txtFileName.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_SELECTED_FILE);
                return;
            }
            // Excelを開きっぱなしにしていないかチェック
            if (!CommonUtil.ValidateFileOpened(_Body.txtFileName.Text)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_FILE_ALREADY_OPENDE);
                return;
            }
            var service = new HGyomuExcelLogic();
            if (!service.ValidateExistSheet(_Body.txtFileName.Text)) {
                _Body.txtFileName.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_IREGULAR_FILE);
                return;
            }
            if (!service.ValidateIsMatchNendo(_Body.txtFileName.Text, TypeConversionUtil.ToInteger(_Body.txtNendo.Text))) {
                _Body.txtFileName.BackColor = Colors._BACK_COLOR_ERROR;
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_IREGULAR_NENDO);
                return;
            }
            _Body.dgvHgyomuExcelLooks.Rows.Clear();

            var dtoList = new HGyomuExcelLogic().Main(_Body.txtFileName.Text);
            BindingList<HGyomuExcelDto> gridDtoList = new BindingList<HGyomuExcelDto>();
            dtoList.ForEach(n => gridDtoList.Add(n));
            if (!gridDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            _Body.dgvHgyomuExcelLooks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            _Body.dgvHgyomuExcelLooks.DataSource = gridDtoList;

            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => ShowExcelContents()));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            _Body.dgvHgyomuExcelLooks.Show();

            // クリアボタン押下モード切替
            _BTN_CLEAR_MODE = Enums.InitMode.EnableCon;

            // 検索条件欄使用可否切り替え
            SwitchEnable_GlpInput(true);

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // フォーカス
            _Body.dgvHgyomuExcelLooks.Focus();
            
        }
        /// <summary>
        /// Excel内容表示
        /// </summary>
        private void ShowExcelContents() {

            try {
                foreach (DataGridViewRow r in _Body.dgvHgyomuExcelLooks.Rows) {
                    var dto = GridRowUtil<HGyomuExcelDto>.GetRowModel(r);
                    string YgyomuKbn = string.Empty;
                    if (!string.IsNullOrEmpty(dto.ProjectCD) && 0 < dto.ProjectCD.Length) {
                        YgyomuKbn = dto.ProjectCD.Substring(0, 1);
                    }
                    foreach (DataGridViewCell c in r.Cells) {
                        ControlUtil.SetTimeTrackerCellStyle(YgyomuKbn, c);
                    }
                }
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpd_ClickEvent() {

            var excelDatas = new List<AnkenExcelDto>();

            // 更新時Validate
            if (!UpdValidate(excelDatas)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => InsertAnkenExcelEntityProxy(excelDatas, _Body.txtNendo.Text)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);

            _Body.dgvHgyomuExcelLooks.Rows.Clear();

            // 検索条件欄使用可否切り替え
            SwitchEnable_GlpInput(false);

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
            
        }
        /// <summary>
        /// Excelデータ登録（Proxy）
        /// </summary>
        /// <param name="excelDatas"></param>
        /// <param name="strYYYY"></param>
        private void InsertAnkenExcelEntityProxy(List<AnkenExcelDto> excelDatas, string strYYYY) {
            
            try {
                for (int intMonth = 1; intMonth <= TimeConst._MONTH_COUNT_PER_YEAR; intMonth++) {
                    DateTime taishoDate = TypeConversionUtil.StrToDateTime(strYYYY, intMonth);

                    // Excelデータ登録
                    InsertAnkenExcelEntity(excelDatas, taishoDate);
                }
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex.Message);
                throw ex;
            }
        }
        /// <summary>
        /// Excelデータ登録
        /// </summary>
        /// <param name="excelDatas"></param>
        /// <param name="taishoDate"></param>
        private void InsertAnkenExcelEntity(List<AnkenExcelDto> excelDatas, DateTime taishoDate) {

            // 登録済みデータ削除
            var service = new PAF2010_AnkenExcelCaptureService();
            string taishoYM = taishoDate.ToString(Formats._FORMAT_DATE_YYYYMM);
            service.DelAnkenExcel(_DB_CONN_STRING, taishoYM, ProjectConst.PBushoKbn.FIELD_SUPPROT);
            service.DelPX01ProjectDetail(_DB_CONN_STRING, taishoYM, ProjectConst.PBushoKbn.FIELD_SUPPROT);

            // 更新用データ格納リスト
            var addDatas = new List<PX03AnkenExcelEntity>();

            // 登録用データ作成
            addDatas = CreateAddDatas(excelDatas, taishoYM);

            // 登録処理
            addDatas.ForEach(n => service.AddAnekenExcel(_DB_CONN_STRING, n));

            // プロジェクト詳細テーブル登録
            var tantoMasters = new PAF3010_MstEmploeeService().LoadMstTanto(_DB_CONN_STRING, new LoadMstTantoCon());
            foreach (var tanto in tantoMasters) {
                var bushoInfo = new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, tanto.BushoCD);
                if (bushoInfo.PBushoKbn != (int)ProjectConst.PBushoKbn.FIELD_SUPPROT) {
                    continue;
                }
                service.AddPX01ProjectDetail(_DB_CONN_STRING, tanto.TantoID, taishoDate, ProjectConst.PBushoKbn.FIELD_SUPPROT);
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="excelDtoList"></param>
        /// <param name="taishoYM"></param>
        /// <returns></returns>
        private List<PX03AnkenExcelEntity> CreateAddDatas(List<AnkenExcelDto> excelDtoList, string taishoYM) {

            // プロジェクトマスタ取得
            var mstCommonService = new MstCommonService();
            var projectMaster = mstCommonService.LoadMstProject(_DB_CONN_STRING);

            var addProjectMaster = new List<MX01ProjectEntity>();
            foreach (var excelData in excelDtoList) {
                if (!projectMaster.Exists(n => n.ProjectCD == excelData.ProjectCD)) {

                    // 未登録のプロジェクトマスタを登録
                    var addM01Entity = new MX01ProjectEntity() {
                        ProjectCD = StringUtil.TostringNullForbid(excelData.ProjectCD),
                        ProjectName = StringUtil.TostringNullForbid(excelData.ProjectName),
                        KokyakuName = StringUtil.TostringNullForbid(excelData.KokyakuName),
                        PBushoKbn = ProjectUtil.GetPBushoKbn(excelData.ProjectCD),
                        StartYM = taishoYM.Substring(0, 4) + "01",
                        EndYM = taishoYM.Substring(0, 4) + "12",
                        AddDate = DateTime.Now,
                        UpdDate = DateTime.Now,
                    };
                    mstCommonService.AddMstProject(_DB_CONN_STRING, addM01Entity);
                    projectMaster.Add(addM01Entity);
                }
            }
            // 工程マスタ取得
            var kouteiMaster = new PAF2010_AnkenExcelCaptureService().LoadMstKoutei(_DB_CONN_STRING);

            var ret = new List<PX03AnkenExcelEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in excelDtoList) {
                 // データ作成
                var entity = new PX03AnkenExcelEntity() {
                    TaishoYM = taishoYM,
                    ProjectCD = rowDto.ProjectCD,
                    KouteiSeq = GetKouteiSeq(kouteiMaster, rowDto).ToString(),
                    PBushoKbn = LoginInfo.PBushoKbn,
                    Iraimoto = StringUtil.TostringNullForbid(rowDto.Iraimoto),
                    KenshuJoken = GetKenshuJoken(rowDto.KenshuJoken),
                    Kousu = rowDto.Kousu,
                    Biko = StringUtil.TostringNullForbid(rowDto.Biko),
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 登録用工程Seq取得
        /// </summary>
        /// <param name="kouteiMaster"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private int GetKouteiSeq(List<MX02KouteiEntity> kouteiMaster, AnkenExcelDto dto) {
            var tgtKoutei = kouteiMaster.FirstOrDefault(n => n.ProjectCD == dto.ProjectCD && n.KouteiName == dto.KouteiName);
            if (tgtKoutei != null) {

                // 工程がマスタ登録済みの場合
                dto.KouteiSeq = tgtKoutei.KouteiSeq.ToString();
                if (!_ADD_KOUTEILIST.Exists(n => n.ProjectCD == dto.ProjectCD && n.KouteiName == dto.KouteiName)) {
                    _ADD_KOUTEILIST.Add(dto);
                }
                return tgtKoutei.KouteiSeq;
            }
            var tgtRowMaster = _ADD_KOUTEILIST.FirstOrDefault(n => n.ProjectCD == dto.ProjectCD && n.KouteiName== dto.KouteiName);
            if (tgtRowMaster == null) {

                // マスタにもExcelにもない場合
                _ADD_KOUTEILIST.Add(dto);
                int kouteiSeq = new PAF3040_MstKouteiService().GetMaxKouteiSeq(_DB_CONN_STRING, dto.ProjectCD) + 1;
                dto.KouteiSeq = kouteiSeq.ToString();

                // 工程マスタに新規登録
                var M02Entity = new MX02KouteiEntity() {
                    ProjectCD = dto.ProjectCD,
                    KouteiSeq = kouteiSeq,
                    KouteiName = dto.KouteiName,
                    AddDate = DateTime.Now,
                    UpdDate = DateTime.Now,
                };
                new PAF2010_AnkenExcelCaptureService().AddMstKoutei(_DB_CONN_STRING, M02Entity);

                return kouteiSeq;
            }
            // Excelデータの中に含まれる場合
            return TypeConversionUtil.ToInteger(tgtRowMaster.KouteiSeq);

        }
        /// <summary>
        /// 検収条件取得
        /// </summary>
        /// <param name="kenshuJoken"></param>
        /// <returns></returns>
        private string GetKenshuJoken(string kenshuJoken) {
            var tgtKenshuJoken = ProjectConst._DIC_KENSHU_JOKEN.Where(n => n.Value == kenshuJoken).ToList();
            if (tgtKenshuJoken.Any()) {
                return ((int)tgtKenshuJoken.First().Key).ToString();
            }
            return string.Empty;
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="ankenExcelDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<AnkenExcelDto> ankenExcelDtoList) {

            // Excelを開きっぱなしにしていないかチェック
            if (!CommonUtil.ValidateFileOpened(_Body.txtFileName.Text)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_FILE_ALREADY_OPENDE);
                return false;
            }
            foreach (DataGridViewRow r in _Body.dgvHgyomuExcelLooks.Rows) {
                var gridDto = GridRowUtil<HGyomuExcelDto>.GetRowModel(r);

                // グリッドDtoから登録用Dtoに変換
                var rowDto = ConvertHgyomuGridToAnkenExcelDto(gridDto);
                ankenExcelDtoList.Add(rowDto);
            }
            // ファイル存在チェック
            if (!File.Exists(_Body.txtFileName.Text)) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_FILE);
                return false;
            }
            // 更新対象件数チェック
            if (!ankenExcelDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()){
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(ankenExcelDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Dto変換
        /// </summary>
        /// <param name="gridDto"></param>
        /// <returns></returns>
        private AnkenExcelDto ConvertHgyomuGridToAnkenExcelDto(HGyomuExcelDto gridDto) {
            var rowDto = new AnkenExcelDto();
            rowDto.ProjectCD = gridDto.ProjectCD;
            rowDto.ProjectName = gridDto.SagyoNaiyo;
            rowDto.Iraimoto = gridDto.KokyakuName;
            rowDto.KokyakuName = gridDto.KokyakuName;
            rowDto.Kousu = 0.ToString();
            rowDto.KouteiName = "作業";
            rowDto.KouteiSeq = 1.ToString();
            rowDto.KenshuJoken = ProjectConst._DIC_KENSHU_JOKEN.First(n => n.Key == ProjectConst.KenshuJokenKbn.KOTEI).Value;
            rowDto.Biko = string.Empty;
            return rowDto;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            var errColNames = new List<string>();
            string headerTxt = string.Empty;
            foreach (DataGridViewRow r in _Body.dgvHgyomuExcelLooks.Rows) {
                var gridDto = GridRowUtil<HGyomuExcelDto>.GetRowModel(r);

                // グリッドDtoから登録用Dtoに変換
                var rowDto = ConvertHgyomuGridToAnkenExcelDto(gridDto);

                // プロジェクトID
                headerTxt = _Body.dgvHgyomuExcelLooks.Columns[nameof(rowDto.ProjectCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectCD), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="ankenExcelDtoList"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<AnkenExcelDto> ankenExcelDtoList) {

            // 検索データ絞り込み
            string taishoYM = DateTime.Parse(_Body.txtNendo.Text + "/04/01").ToString(Formats._FORMAT_DATE_YYMM);
            var oldDatas = new PAF2010_AnkenExcelCaptureService().LoadAnkenExcel(_DB_CONN_STRING, taishoYM);

            string msg = string.Empty;
            bool result = true;
            foreach (var keyInfo in ankenExcelDtoList) {
                if (ValidateDouple_InMaster_Insert(oldDatas, keyInfo) && ValidateDouple_InGrid_Insert(ankenExcelDtoList, keyInfo)) {
                    continue;
                }
                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvHgyomuExcelLooks.Rows) {
                    var rowDto = GridRowUtil<HGyomuExcelDto>.GetRowModel(r);
                    if (rowDto.ProjectCD == keyInfo.ProjectCD) {
                        r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                    }
                }
                result = false;
            }
            if (!result) {
                var dto = new AnkenExcelDto();
                msg = _Body.dgvHgyomuExcelLooks.Columns[nameof(dto.ProjectCD)].HeaderText;
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return result;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_InMaster_Insert(List<AnkenExcelDto> keyInfos, AnkenExcelDto keyInfo) {
            if (1 < keyInfos.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                     && n.KouteiName == keyInfo.KouteiName
                                     ).ToList().Count) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="gridDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_InGrid_Insert(List<AnkenExcelDto> gridDatas, AnkenExcelDto keyInfo) {
            if (1 < gridDatas.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                      && n.KouteiName == keyInfo.KouteiName
                                      ).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            if (_BTN_CLEAR_MODE == Enums.InitMode.All) {

                // 対象月
                _Body.txtNendo.Text = DateTime.Now.Year.ToString();

                // 選択ファイル名
                _Body.txtFileName.Text = string.Empty;

                // グリッド
                _Body.dgvHgyomuExcelLooks.Rows.Clear();
                _Body.dgvHgyomuExcelLooks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.ColumnHeader;

                // 登録工程リスト
                _ADD_KOUTEILIST.Clear();

            } else {

                _BTN_CLEAR_MODE = Enums.InitMode.All;

            }
            // 検索条件欄使用可否切り替え
            SwitchEnable_GlpInput(false);

            // 背景色
            InitColor();

            // フォーカス
            _Body.btnSearch.Focus();
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        #endregion

    }
}
