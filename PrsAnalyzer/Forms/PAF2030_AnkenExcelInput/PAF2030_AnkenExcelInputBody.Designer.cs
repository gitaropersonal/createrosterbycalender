﻿namespace PrsAnalyzer.Forms {
    partial class PAF2030_AnkenExcelInputBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF2030_AnkenExcelInputBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvAnkenExcelLooks = new System.Windows.Forms.DataGridView();
            this.Iraimoto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.KouteiSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kousu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KenshuJoken = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Biko = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSagyoName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.pnlKokyakuRdo = new System.Windows.Forms.Panel();
            this.rdoFutokutei = new System.Windows.Forms.RadioButton();
            this.rdoSyanai = new System.Windows.Forms.RadioButton();
            this.rdoIraimoto = new System.Windows.Forms.RadioButton();
            this.rdoNothing = new System.Windows.Forms.RadioButton();
            this.txtKokyakuName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbGyomuKbn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbTgtMonth = new System.Windows.Forms.ComboBox();
            this.lblTgtMonth = new System.Windows.Forms.Label();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlExecute.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnkenExcelLooks)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.pnlKokyakuRdo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.panel3);
            this.pnlExecute.Controls.Add(this.panel1);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(1384, 811);
            this.pnlExecute.TabIndex = 25;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvAnkenExcelLooks);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel3.Location = new System.Drawing.Point(0, 173);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 65);
            this.panel3.Size = new System.Drawing.Size(1384, 638);
            this.panel3.TabIndex = 20;
            // 
            // dgvAnkenExcelLooks
            // 
            this.dgvAnkenExcelLooks.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAnkenExcelLooks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAnkenExcelLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnkenExcelLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Iraimoto,
            this.ProjectCD,
            this.KokyakuName,
            this.ProjectName,
            this.KouteiName,
            this.KouteiSeq,
            this.Kousu,
            this.KenshuJoken,
            this.Biko,
            this.Status});
            this.dgvAnkenExcelLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAnkenExcelLooks.Location = new System.Drawing.Point(15, 15);
            this.dgvAnkenExcelLooks.Name = "dgvAnkenExcelLooks";
            this.dgvAnkenExcelLooks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAnkenExcelLooks.RowTemplate.Height = 21;
            this.dgvAnkenExcelLooks.Size = new System.Drawing.Size(1354, 558);
            this.dgvAnkenExcelLooks.TabIndex = 20;
            // 
            // Iraimoto
            // 
            this.Iraimoto.DataPropertyName = "Iraimoto";
            this.Iraimoto.HeaderText = "依頼元";
            this.Iraimoto.Name = "Iraimoto";
            this.Iraimoto.Width = 80;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトコード";
            this.ProjectCD.MaxInputLength = 8;
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.Width = 120;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            this.KokyakuName.DefaultCellStyle = dataGridViewCellStyle2;
            this.KokyakuName.HeaderText = "客先名";
            this.KokyakuName.MaxInputLength = 100;
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.ReadOnly = true;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "ProjectName";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            this.ProjectName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ProjectName.HeaderText = "作業件名";
            this.ProjectName.MaxInputLength = 50;
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.ReadOnly = true;
            this.ProjectName.Width = 300;
            // 
            // KouteiName
            // 
            this.KouteiName.DataPropertyName = "KouteiName";
            this.KouteiName.HeaderText = "工程";
            this.KouteiName.Name = "KouteiName";
            this.KouteiName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.KouteiName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.KouteiName.Width = 120;
            // 
            // KouteiSeq
            // 
            this.KouteiSeq.DataPropertyName = "KouteiSeq";
            this.KouteiSeq.HeaderText = "工程Seq";
            this.KouteiSeq.Name = "KouteiSeq";
            this.KouteiSeq.Visible = false;
            // 
            // Kousu
            // 
            this.Kousu.DataPropertyName = "Kousu";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kousu.DefaultCellStyle = dataGridViewCellStyle4;
            this.Kousu.HeaderText = "工数(人月)";
            this.Kousu.MaxInputLength = 5;
            this.Kousu.Name = "Kousu";
            this.Kousu.Width = 110;
            // 
            // KenshuJoken
            // 
            this.KenshuJoken.DataPropertyName = "KenshuJoken";
            this.KenshuJoken.HeaderText = "検収条件";
            this.KenshuJoken.Name = "KenshuJoken";
            this.KenshuJoken.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.KenshuJoken.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Biko
            // 
            this.Biko.DataPropertyName = "Biko";
            this.Biko.HeaderText = "備考";
            this.Biko.MaxInputLength = 100;
            this.Biko.Name = "Biko";
            this.Biko.Width = 300;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 173);
            this.panel1.TabIndex = 2;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.label3);
            this.grpInput.Controls.Add(this.txtSagyoName);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.txtProjectCode);
            this.grpInput.Controls.Add(this.pnlKokyakuRdo);
            this.grpInput.Controls.Add(this.txtKokyakuName);
            this.grpInput.Controls.Add(this.label4);
            this.grpInput.Controls.Add(this.cmbGyomuKbn);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.cmbTgtMonth);
            this.grpInput.Controls.Add(this.lblTgtMonth);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 163);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(265, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 74;
            this.label3.Text = "作業内容";
            // 
            // txtSagyoName
            // 
            this.txtSagyoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSagyoName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSagyoName.Location = new System.Drawing.Point(412, 45);
            this.txtSagyoName.MaxLength = 50;
            this.txtSagyoName.Name = "txtSagyoName";
            this.txtSagyoName.Size = new System.Drawing.Size(300, 24);
            this.txtSagyoName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(265, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 73;
            this.label2.Text = "プロジェクトコード";
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProjectCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProjectCode.Location = new System.Drawing.Point(412, 20);
            this.txtProjectCode.MaxLength = 8;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Size = new System.Drawing.Size(82, 24);
            this.txtProjectCode.TabIndex = 1;
            // 
            // pnlKokyakuRdo
            // 
            this.pnlKokyakuRdo.Controls.Add(this.rdoFutokutei);
            this.pnlKokyakuRdo.Controls.Add(this.rdoSyanai);
            this.pnlKokyakuRdo.Controls.Add(this.rdoIraimoto);
            this.pnlKokyakuRdo.Controls.Add(this.rdoNothing);
            this.pnlKokyakuRdo.Location = new System.Drawing.Point(412, 97);
            this.pnlKokyakuRdo.Name = "pnlKokyakuRdo";
            this.pnlKokyakuRdo.Size = new System.Drawing.Size(300, 24);
            this.pnlKokyakuRdo.TabIndex = 5;
            // 
            // rdoFutokutei
            // 
            this.rdoFutokutei.AutoSize = true;
            this.rdoFutokutei.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoFutokutei.Location = new System.Drawing.Point(189, 0);
            this.rdoFutokutei.Name = "rdoFutokutei";
            this.rdoFutokutei.Size = new System.Drawing.Size(91, 24);
            this.rdoFutokutei.TabIndex = 3;
            this.rdoFutokutei.TabStop = true;
            this.rdoFutokutei.Text = "不特定客先";
            this.rdoFutokutei.UseVisualStyleBackColor = true;
            // 
            // rdoSyanai
            // 
            this.rdoSyanai.AutoSize = true;
            this.rdoSyanai.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSyanai.Location = new System.Drawing.Point(137, 0);
            this.rdoSyanai.Name = "rdoSyanai";
            this.rdoSyanai.Size = new System.Drawing.Size(52, 24);
            this.rdoSyanai.TabIndex = 2;
            this.rdoSyanai.TabStop = true;
            this.rdoSyanai.Text = "社内";
            this.rdoSyanai.UseVisualStyleBackColor = true;
            // 
            // rdoIraimoto
            // 
            this.rdoIraimoto.AutoSize = true;
            this.rdoIraimoto.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoIraimoto.Location = new System.Drawing.Point(72, 0);
            this.rdoIraimoto.Name = "rdoIraimoto";
            this.rdoIraimoto.Size = new System.Drawing.Size(65, 24);
            this.rdoIraimoto.TabIndex = 1;
            this.rdoIraimoto.TabStop = true;
            this.rdoIraimoto.Text = "依頼元";
            this.rdoIraimoto.UseVisualStyleBackColor = true;
            // 
            // rdoNothing
            // 
            this.rdoNothing.AutoSize = true;
            this.rdoNothing.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoNothing.Location = new System.Drawing.Point(0, 0);
            this.rdoNothing.Name = "rdoNothing";
            this.rdoNothing.Size = new System.Drawing.Size(72, 24);
            this.rdoNothing.TabIndex = 0;
            this.rdoNothing.TabStop = true;
            this.rdoNothing.Text = "選択なし";
            this.rdoNothing.UseVisualStyleBackColor = true;
            // 
            // txtKokyakuName
            // 
            this.txtKokyakuName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokyakuName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKokyakuName.Location = new System.Drawing.Point(412, 122);
            this.txtKokyakuName.MaxLength = 100;
            this.txtKokyakuName.Name = "txtKokyakuName";
            this.txtKokyakuName.Size = new System.Drawing.Size(300, 24);
            this.txtKokyakuName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(265, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 72;
            this.label4.Text = "顧客名";
            // 
            // cmbGyomuKbn
            // 
            this.cmbGyomuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGyomuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbGyomuKbn.FormattingEnabled = true;
            this.cmbGyomuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbGyomuKbn.Location = new System.Drawing.Point(412, 70);
            this.cmbGyomuKbn.Name = "cmbGyomuKbn";
            this.cmbGyomuKbn.Size = new System.Drawing.Size(82, 25);
            this.cmbGyomuKbn.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(265, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 71;
            this.label1.Text = "業務区分";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1192, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 140);
            this.panel2.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(22, 91);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 35);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // cmbTgtMonth
            // 
            this.cmbTgtMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbTgtMonth.FormattingEnabled = true;
            this.cmbTgtMonth.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbTgtMonth.Location = new System.Drawing.Point(85, 20);
            this.cmbTgtMonth.Name = "cmbTgtMonth";
            this.cmbTgtMonth.Size = new System.Drawing.Size(120, 25);
            this.cmbTgtMonth.TabIndex = 0;
            // 
            // lblTgtMonth
            // 
            this.lblTgtMonth.AutoSize = true;
            this.lblTgtMonth.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTgtMonth.Location = new System.Drawing.Point(15, 23);
            this.lblTgtMonth.Name = "lblTgtMonth";
            this.lblTgtMonth.Size = new System.Drawing.Size(60, 17);
            this.lblTgtMonth.TabIndex = 58;
            this.lblTgtMonth.Text = "対象年月";
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel5);
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnUpdate);
            this.panel5.Controls.Add(this.btnClear);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(473, 65);
            this.panel5.TabIndex = 101;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(141, 15);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 62;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 61;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1200, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(49, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // PAF2030_AnkenExcelInputBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.pnlFootor);
            this.Controls.Add(this.pnlExecute);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 600);
            this.Name = "PAF2030_AnkenExcelInputBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "プロジェクト一覧Excel取込";
            this.pnlExecute.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnkenExcelLooks)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.pnlKokyakuRdo.ResumeLayout(false);
            this.pnlKokyakuRdo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlFootor;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvAnkenExcelLooks;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.ComboBox cmbTgtMonth;
        public System.Windows.Forms.Label lblTgtMonth;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iraimoto;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewComboBoxColumn KouteiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kousu;
        private System.Windows.Forms.DataGridViewComboBoxColumn KenshuJoken;
        private System.Windows.Forms.DataGridViewTextBoxColumn Biko;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSagyoName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtProjectCode;
        public System.Windows.Forms.Panel pnlKokyakuRdo;
        public System.Windows.Forms.RadioButton rdoFutokutei;
        public System.Windows.Forms.RadioButton rdoSyanai;
        public System.Windows.Forms.RadioButton rdoIraimoto;
        public System.Windows.Forms.RadioButton rdoNothing;
        public System.Windows.Forms.TextBox txtKokyakuName;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cmbGyomuKbn;
        public System.Windows.Forms.Label label1;
    }
}

