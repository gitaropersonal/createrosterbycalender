﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF2030_AnkenExcelInputBody : Form {
        public PAF2030_AnkenExcelInputBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF2030_AnkenExcelInputFormLogic(this, loginInfo);
        }
    }
}
