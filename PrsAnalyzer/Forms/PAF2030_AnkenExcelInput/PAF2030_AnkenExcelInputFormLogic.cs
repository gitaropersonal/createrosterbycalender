﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF2030_AnkenExcelInputFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF2030_AnkenExcelInputBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// クリアボタン押下回数
        /// </summary>
        private static Enums.InitMode _BTN_CLEAR_MODE = Enums.InitMode.All;
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static InputAnkenExcelGridDto _CELL_HEADER_NAME_DTO = new InputAnkenExcelGridDto();
        private string[] _EditableGridCells = new string[] {
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Iraimoto)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KouteiName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KenshuJoken)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Kousu)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Biko)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Status)
        };

        private string[] _ForbidUpdateGridCells = new string[] {
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.ProjectName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Iraimoto)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KokyakuName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KouteiName)
        };

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF2030_AnkenExcelInputFormLogic(PAF2030_AnkenExcelInputBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            _BTN_CLEAR_MODE = Enums.InitMode.All;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearchClickEvent();
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpdClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // グリッド行番号描画
            _Body.dgvAnkenExcelLooks.RowPostPaint += (s, e) => {
                GridRowUtil<InputAnkenExcelGridDto>.SetRowNum(s, e);
            };
            // グリッドCellEnter
            _Body.dgvAnkenExcelLooks.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッドCellValueChanged
            _Body.dgvAnkenExcelLooks.CellValueChanged += GridCellValueChanged;
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.J:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvAnkenExcelLooks.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            InputAnkenExcelGridDto dto = new InputAnkenExcelGridDto();
            switch (tgtColName) {
                case nameof(dto.Biko):
                case nameof(dto.KouteiName):
                case nameof(dto.KokyakuName):
                case nameof(dto.Iraimoto):
                    _Body.dgvAnkenExcelLooks.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvAnkenExcelLooks.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvAnkenExcelLooks.CellValueChanged -= GridCellValueChanged;

            var dto = new ProjectLooksGridDto();
            string tgtCellName = _Body.dgvAnkenExcelLooks.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                _Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string projectCode = StringUtil.TostringNullForbid(_Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[nameof(dto.ProjectCD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(projectCode) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvAnkenExcelLooks.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvAnkenExcelLooks.CellValueChanged += GridCellValueChanged;
            _Body.dgvAnkenExcelLooks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            // 使用可否切り替え（更新ボタン）
            SwitchEnableBtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="rowIndex"></param>
        private void EditableCellValueChanged(object tgtCellVal, string tgtCellName, int rowIndex) {
            if (tgtCellVal == null) {
                return;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {
                case nameof(_CELL_HEADER_NAME_DTO.ProjectCD): // プロジェクトコード
                    _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[tgtCellName].Value = inputVal;
                    SetGridComboKoutei(inputVal, rowIndex);
                    break;
                case nameof(_CELL_HEADER_NAME_DTO.Kousu): // 工数
                    decimal kousu;
                    if (!decimal.TryParse(inputVal, out kousu)) {
                        _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[tgtCellName].Value = string.Empty;
                        break;
                    }
                    _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[tgtCellName].Value = kousu.ToString(Formats._FORMAT_FLOAT_3);
                    break;
                case nameof(_CELL_HEADER_NAME_DTO.KouteiName):
                    var kouteiNameComboCell = (DataGridViewComboBoxCell)(_Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KouteiName)]);
                    int kouteiSeq = kouteiNameComboCell.Items.IndexOf(inputVal) + 1;
                    _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KouteiSeq)].Value = kouteiSeq;
                    break;
                default:
                    // 入力値を編集→セット
                    _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[tgtCellName].Value = inputVal;
                    break;
            }
        }
        /// <summary>
        /// グリッドセルコンボにアイテムセット（工程）
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="rowIndex"></param>
        private void SetGridComboKoutei(string projectCode, int rowIndex) {
            var projectInfo = new MstCommonService().LoadMstProject(_DB_CONN_STRING, projectCode).FirstOrDefault();
            if (projectInfo != null) {
                _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.ProjectName)].Value = projectInfo.ProjectName;
                _Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KokyakuName)].Value = projectInfo.KokyakuName;

                var kouteiNameComboCell = (DataGridViewComboBoxCell)(_Body.dgvAnkenExcelLooks.Rows[rowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KouteiName)]);
                string originCellText = StringUtil.ToStringForbidNull(kouteiNameComboCell.Value);
                kouteiNameComboCell.Items.Add(string.Empty);
                kouteiNameComboCell.Value = string.Empty;
                kouteiNameComboCell.Items.Clear();
                var kouteiInfos = new PAF2030_AnkenExcelInputService().LoadMstKouteiByProjectCode(_DB_CONN_STRING, projectInfo.ProjectCD);
                foreach (var info in kouteiInfos) {
                    kouteiNameComboCell.Items.Add(info.KouteiName);
                }
                if (kouteiInfos.FirstOrDefault(n => n.KouteiName == originCellText) != null) {
                    kouteiNameComboCell.Value = originCellText;
                }
            }
        }
        /// <summary>
        /// 使用可否切り替え（検索条件欄）
        /// </summary>
        /// <param name="IsSelectBtnClick"></param>
        private void SwitchEnableGlpInput(bool IsSelectBtnClick) {
            _Body.cmbTgtMonth.Enabled = !IsSelectBtnClick;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnableBtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvAnkenExcelLooks.Rows) {
                var rowDto = GridRowUtil<InputAnkenExcelGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.NONE) {
                    isYukoRowExist = true;
                    break;
                }
            }
            // 検索条件欄使用可否切り替え
            SwitchEnableGlpInput(isYukoRowExist);

            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        private void BtnSearchClickEvent() {
            _Body.dgvAnkenExcelLooks.Rows.Clear();

            // パラメータ取得
            string taishoYYYYMM = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM);
            var pBushoKbn = ProjectConst.PBushoKbn.NONE;
            var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked);
            string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked, _Body.txtKokyakuName.Text);

            // 検索
            var dtoList = new PAF2030_AnkenExcelInputService().GetGridDatas(
                  _DB_CONN_STRING
                , taishoYYYYMM
                , LoginInfo
                , _Body.txtProjectCode.Text
                , _Body.txtSagyoName.Text
                , _Body.cmbGyomuKbn.Text
                , pBushoKbn
                , kokyakuKbn
                , kokyakuName
                );
            BindingList<InputAnkenExcelGridDto> gridDtoList = new BindingList<InputAnkenExcelGridDto>();
            dtoList.ForEach(n => gridDtoList.Add(n));
            _Body.dgvAnkenExcelLooks.DataSource = gridDtoList;

            // 検索結果表示
            ShowLooks();

            // クリアボタン押下モード切替
            _BTN_CLEAR_MODE = Enums.InitMode.EnableCon;

            // 検索条件欄使用可否切り替え
            SwitchEnableGlpInput(true);

            // 使用可否切り替え（更新ボタン）
            SwitchEnableBtnUpdate();

            if (!gridDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // フォーカス
            _Body.dgvAnkenExcelLooks.Focus();
        }
        /// <summary>
        /// グリッド表示
        /// </summary>
        private void ShowLooks() {
            try {
                foreach (DataGridViewRow r in _Body.dgvAnkenExcelLooks.Rows) {
                    var dto = GridRowUtil<InputAnkenExcelGridDto>.GetRowModel(r);
                    string YgyomuKbn = string.Empty;
                    bool existProjectCode = (!string.IsNullOrEmpty(dto.ProjectCD) && 0 < dto.ProjectCD.Length);
                    if (existProjectCode) {
                        YgyomuKbn = dto.ProjectCD.Substring(0, 1);
                        foreach (DataGridViewCell c in r.Cells) {
                            if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name)) {
                                ControlUtil.SetTimeTrackerCellStyle(YgyomuKbn, c);
                                c.ReadOnly = true;
                            }
                        }
                    } else {
                        r.ReadOnly = false;
                        r.Cells[nameof(dto.ProjectName)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        r.Cells[nameof(dto.KokyakuName)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        r.Cells[nameof(dto.ProjectName)].ReadOnly = true;
                        r.Cells[nameof(dto.KokyakuName)].ReadOnly = true;
                    }
                }
                _Body.dgvAnkenExcelLooks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpdClickEvent() {

            var excelDatas = new List<InputAnkenExcelGridDto>();

            // 更新時Validate
            if (!UpdValidate(excelDatas)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            DateTime taishoDate = DateTime.Parse(_Body.cmbTgtMonth.Text);
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => InsertAnkenExcelEntityProxy(excelDatas, taishoDate)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);

            _Body.dgvAnkenExcelLooks.Rows.Clear();

            // 検索後に再検索
            BtnSearchClickEvent();
        }
        /// <summary>
        /// Excelデータ登録（Proxy）
        /// </summary>
        /// <param name="excelDatas"></param>
        /// <param name="taishoDate"></param>
        private void InsertAnkenExcelEntityProxy(List<InputAnkenExcelGridDto> excelDatas, DateTime taishoDate) {
            
            try {
                // Excelデータ登録
                AddAnkenExcelEntity(excelDatas, taishoDate);
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// Excelデータ登録
        /// </summary>
        /// <param name="excelDatas"></param>
        /// <param name="taishoDate"></param>
        private void AddAnkenExcelEntity(List<InputAnkenExcelGridDto> excelDatas, DateTime taishoDate) {

            var service = new PAF2030_AnkenExcelInputService();
            string taishoYYYYMM = taishoDate.ToString(Formats._FORMAT_DATE_YYYYMM);

            // 登録用データ格納リスト
            var addDatas = new List<PX03AnkenExcelEntity>();
            var updDatas = new List<PX03AnkenExcelEntity>();

            // 登録用データ作成
            CreateAddDatas(excelDatas, addDatas, updDatas, taishoYYYYMM);

            // 登録処理
            var tantoInfos = service.GetTantoInfos(_DB_CONN_STRING, LoginInfo.PBushoKbn);
            addDatas.ForEach(n => service.AddUpdProjectDetail(_DB_CONN_STRING, n, tantoInfos, taishoDate));

            // 更新処理
            updDatas.ForEach(n => service.UpdAnekenExcel(_DB_CONN_STRING, n));
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="excelDtoList"></param>
        /// <param name="addDatas"></param>
        /// <param name="updDatas"></param>
        /// <param name="taishoYM"></param>
        private void CreateAddDatas(
              List<InputAnkenExcelGridDto> excelDtoList
            , List<PX03AnkenExcelEntity> addDatas
            , List<PX03AnkenExcelEntity> updDatas
            , string taishoYM) {

            // 既存データ取得
            var oldDatas = new PAF2030_AnkenExcelInputService().SelAnekenExcel(_DB_CONN_STRING, taishoYM);

            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in excelDtoList) {

                if(StringUtil.SubstringRight(rowDto.ProjectCD, 3) == "999") {
                    // 発注用コードの場合はレコードを作成しない
                    continue;
                }
                 // データ作成
                var entity = new PX03AnkenExcelEntity() {
                    TaishoYM = taishoYM,
                    ProjectCD = rowDto.ProjectCD,
                    KouteiSeq = StringUtil.TostringNullForbid(rowDto.KouteiSeq),
                    PBushoKbn = LoginInfo.PBushoKbn,
                    Iraimoto = rowDto.Iraimoto,
                    KenshuJoken = GetKenshuJoken(rowDto.KenshuJoken),
                    Kousu = rowDto.Kousu,
                    Biko = rowDto.Biko,
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                if (rowDto.Status == Enums.EditStatus.UPDATE
                 && oldDatas.Exists(n => n.ProjectCD == entity.ProjectCD
                                      && n.KouteiSeq == entity.KouteiSeq
                                    　&& n.PBushoKbn == entity.PBushoKbn)
                 ) {
                    // 更新
                    updDatas.Add(entity);
                    continue;
                }
                if (rowDto.Status == Enums.EditStatus.INSERT) {
                    // 追加
                    addDatas.Add(entity);
                }
            }
        }
        /// <summary>
        /// 検収条件取得
        /// </summary>
        /// <param name="kenshuJoken"></param>
        /// <returns></returns>
        private string GetKenshuJoken(string kenshuJoken) {
            var tgtKenshuJoken = ProjectConst._DIC_KENSHU_JOKEN.Where(n => n.Value == kenshuJoken).ToList();
            if (tgtKenshuJoken.Any()) {
                return ((int)tgtKenshuJoken.First().Key).ToString();
            }
            return string.Empty;
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="InputAnkenExcelGridDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<InputAnkenExcelGridDto> InputAnkenExcelGridDtoList) {

            foreach (DataGridViewRow r in _Body.dgvAnkenExcelLooks.Rows) {
                var rowDto = GridRowUtil<InputAnkenExcelGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    InputAnkenExcelGridDtoList.Add(rowDto);
                }
            }
            // 更新対象件数チェック
            if (!InputAnkenExcelGridDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()){
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(InputAnkenExcelGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            var errColNames = new List<string>();
            string headerTxt = string.Empty;
            foreach (DataGridViewRow r in _Body.dgvAnkenExcelLooks.Rows) {
                var rowDto = GridRowUtil<InputAnkenExcelGridDto>.GetRowModel(r);

                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // プロジェクトCD
                headerTxt = _Body.dgvAnkenExcelLooks.Columns[nameof(rowDto.ProjectCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectCD), ref errColNames, ref result);

                // 工程
                headerTxt = _Body.dgvAnkenExcelLooks.Columns[nameof(rowDto.KouteiName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KouteiName), ref errColNames, ref result);

                // 検収条件
                headerTxt = _Body.dgvAnkenExcelLooks.Columns[nameof(rowDto.KenshuJoken)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KenshuJoken), ref errColNames, ref result);

                // 依頼元
                headerTxt = _Body.dgvAnkenExcelLooks.Columns[nameof(rowDto.Iraimoto)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.Iraimoto), ref errColNames, ref result);

                // 工数
                headerTxt = _Body.dgvAnkenExcelLooks.Columns[nameof(rowDto.Kousu)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.Kousu), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="InputAnkenExcelGridDtoList"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<InputAnkenExcelGridDto> InputAnkenExcelGridDtoList) {

            // 検索データ絞り込み
            string taishoYM = DateTime.Parse(_Body.cmbTgtMonth.Text).ToString(Formats._FORMAT_DATE_YYYYMM);
            var oldDatas = new PAF2010_AnkenExcelCaptureService().LoadAnkenExcel(_DB_CONN_STRING, taishoYM);

            var errColList = new List<string>();
            string msg = string.Empty;
            bool result = true;
            foreach (var keyInfo in InputAnkenExcelGridDtoList) {
                if (ValidateDouple_InMaster_Insert(oldDatas, keyInfo) && ValidateDouple_InGrid_Insert(InputAnkenExcelGridDtoList, keyInfo)) {
                    continue;
                }
                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvAnkenExcelLooks.Rows) {
                    var rowDto = GridRowUtil<InputAnkenExcelGridDto>.GetRowModel(r);
                    if (rowDto.ProjectCD == keyInfo.ProjectCD && rowDto.KouteiName == keyInfo.KouteiName) {
                        r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                        r.Cells[nameof(rowDto.KouteiName)].Style.BackColor = Color.Red;
                    }
                }
                result = false;
            }
            if (!result) {
                var dto = new InputAnkenExcelGridDto();
                msg = _Body.dgvAnkenExcelLooks.Columns[nameof(dto.ProjectCD)].HeaderText;
                msg = string.Concat(msg, ",", _Body.dgvAnkenExcelLooks.Columns[nameof(dto.KouteiName)].HeaderText);
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return result;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_InMaster_Insert(List<AnkenExcelDto> keyInfos, InputAnkenExcelGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                     && n.KouteiName == keyInfo.KouteiName
                                     ).ToList().Count) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="gridDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_InGrid_Insert(List<InputAnkenExcelGridDto> gridDatas, InputAnkenExcelGridDto keyInfo) {
            if (1 < gridDatas.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                      && n.KouteiName == keyInfo.KouteiName
                                      ).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 検索条件初期化
            InitSearchProjectLooksCondition();

            // グリッド初期化
            InitGrid();

            if (_BTN_CLEAR_MODE == Enums.InitMode.All) {

                // 対象月
                InitSumStartDateCombo();

            } else {

                _BTN_CLEAR_MODE = Enums.InitMode.All;

            }
            // 検索条件欄使用可否切り替え
            SwitchEnableGlpInput(false);

            // 背景色
            InitColor();

            // フォーカス
            _Body.cmbTgtMonth.Focus();
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnableBtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 集計月コンボボックス初期化
        /// </summary>
        private void InitSumStartDateCombo() {
            _Body.cmbTgtMonth.Items.Clear();
            _Body.cmbTgtMonth.Text = string.Empty;
            DateTime dtSumEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1);
            DateTime dtSumStart = new DateTime(DateTime.Now.Year, 1, 1);
            for (DateTime dt = dtSumStart; dt <= dtSumEnd; dt = dt.AddMonths(1)) {
                _Body.cmbTgtMonth.Items.Add(dt.ToString(Formats._FORMAT_DATE_YYYYM_NENTSUKI));
            }
            _Body.cmbTgtMonth.Text = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYM_NENTSUKI);
        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);
            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length != 1) {
                    continue;
                }
                if (pair.Key == "X" || pair.Key == "Y") {
                    continue;
                }
                _Body.cmbGyomuKbn.Items.Add(pair.Key);
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {

            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // 作業名
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // 依頼元ラジオボタン
            _Body.rdoNothing.Checked = true;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;
        }
        /// <summary>
        /// グリッド初期化
        /// </summary>
        private void InitGrid() {
            _Body.dgvAnkenExcelLooks.Rows.Clear();
            _Body.dgvAnkenExcelLooks.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DataGridViewComboBoxColumn cmbKenshuJoken = (DataGridViewComboBoxColumn)_Body.dgvAnkenExcelLooks.Columns[nameof(_CELL_HEADER_NAME_DTO.KenshuJoken)];
            cmbKenshuJoken.Items.Clear();
            foreach (var pair in ProjectConst._DIC_KENSHU_JOKEN) {
                cmbKenshuJoken.Items.Add(pair.Value);
            }
            DataGridViewComboBoxColumn cmbKouteiName = (DataGridViewComboBoxColumn)_Body.dgvAnkenExcelLooks.Columns[nameof(_CELL_HEADER_NAME_DTO.KouteiName)];
            cmbKouteiName.Items.Clear();
            var kouteiAll = new PAF2030_AnkenExcelInputService().LoadMstKouteiAll(_DB_CONN_STRING);
            foreach (var koutei in kouteiAll) {
                cmbKouteiName.Items.Add(koutei);
            }
        }
        #endregion

    }
}
