﻿namespace PrsAnalyzer.Forms {
    partial class PAF3010_MstEmploeeBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF3010_MstEmploeeBody));
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvMstEmploee = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKaishaCD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBushoCD = new System.Windows.Forms.TextBox();
            this.cmbYakushokuKbn = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTantoKbn = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbKengenKbn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkContainDel = new System.Windows.Forms.CheckBox();
            this.lblEmploeeId = new System.Windows.Forms.Label();
            this.txtEmploeeId = new System.Windows.Forms.TextBox();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.EmploeeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmploeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorityKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TantoKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.YakushokuKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BushoCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BushoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KaishaCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KaishaName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AuthorityKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TantoKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.YakushokuKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.MailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlExecute.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstEmploee)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.panel2);
            this.pnlExecute.Controls.Add(this.panel1);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(1384, 811);
            this.pnlExecute.TabIndex = 25;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvMstEmploee);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel2.Location = new System.Drawing.Point(0, 127);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 15, 15, 65);
            this.panel2.Size = new System.Drawing.Size(1384, 684);
            this.panel2.TabIndex = 22;
            // 
            // dgvMstEmploee
            // 
            this.dgvMstEmploee.AllowUserToResizeRows = false;
            this.dgvMstEmploee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMstEmploee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EmploeeId,
            this.EmploeeName,
            this.AuthorityKbn,
            this.TantoKbn,
            this.YakushokuKbn,
            this.BushoCD,
            this.BushoName,
            this.KaishaCD,
            this.KaishaName,
            this.AuthorityKbnName,
            this.TantoKbnName,
            this.YakushokuKbnName,
            this.MailAddress,
            this.DelFlg,
            this.Status});
            this.dgvMstEmploee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMstEmploee.Location = new System.Drawing.Point(15, 15);
            this.dgvMstEmploee.Name = "dgvMstEmploee";
            this.dgvMstEmploee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMstEmploee.RowTemplate.Height = 21;
            this.dgvMstEmploee.Size = new System.Drawing.Size(1354, 604);
            this.dgvMstEmploee.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 127);
            this.panel1.TabIndex = 21;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.label5);
            this.grpInput.Controls.Add(this.txtKaishaCD);
            this.grpInput.Controls.Add(this.label4);
            this.grpInput.Controls.Add(this.txtBushoCD);
            this.grpInput.Controls.Add(this.cmbYakushokuKbn);
            this.grpInput.Controls.Add(this.label3);
            this.grpInput.Controls.Add(this.cmbTantoKbn);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.cmbKengenKbn);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.panel3);
            this.grpInput.Controls.Add(this.lblEmploeeId);
            this.grpInput.Controls.Add(this.txtEmploeeId);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 117);
            this.grpInput.TabIndex = 1;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(18, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 17);
            this.label5.TabIndex = 63;
            this.label5.Text = "会社CD";
            // 
            // txtKaishaCD
            // 
            this.txtKaishaCD.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishaCD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKaishaCD.Location = new System.Drawing.Point(88, 77);
            this.txtKaishaCD.MaxLength = 9;
            this.txtKaishaCD.Name = "txtKaishaCD";
            this.txtKaishaCD.Size = new System.Drawing.Size(83, 24);
            this.txtKaishaCD.TabIndex = 2;
            this.txtKaishaCD.Text = "123456789";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(18, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 17);
            this.label4.TabIndex = 61;
            this.label4.Text = "部署CD";
            // 
            // txtBushoCD
            // 
            this.txtBushoCD.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBushoCD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBushoCD.Location = new System.Drawing.Point(88, 50);
            this.txtBushoCD.MaxLength = 6;
            this.txtBushoCD.Name = "txtBushoCD";
            this.txtBushoCD.Size = new System.Drawing.Size(62, 24);
            this.txtBushoCD.TabIndex = 1;
            this.txtBushoCD.Text = "123456";
            // 
            // cmbYakushokuKbn
            // 
            this.cmbYakushokuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYakushokuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbYakushokuKbn.FormattingEnabled = true;
            this.cmbYakushokuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbYakushokuKbn.Location = new System.Drawing.Point(305, 76);
            this.cmbYakushokuKbn.MaxLength = 35;
            this.cmbYakushokuKbn.Name = "cmbYakushokuKbn";
            this.cmbYakushokuKbn.Size = new System.Drawing.Size(90, 25);
            this.cmbYakushokuKbn.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(224, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 59;
            this.label3.Text = "役職区分";
            // 
            // cmbTantoKbn
            // 
            this.cmbTantoKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTantoKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbTantoKbn.FormattingEnabled = true;
            this.cmbTantoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbTantoKbn.Location = new System.Drawing.Point(305, 50);
            this.cmbTantoKbn.MaxLength = 35;
            this.cmbTantoKbn.Name = "cmbTantoKbn";
            this.cmbTantoKbn.Size = new System.Drawing.Size(90, 25);
            this.cmbTantoKbn.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(224, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 57;
            this.label2.Text = "担当者区分";
            // 
            // cmbKengenKbn
            // 
            this.cmbKengenKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKengenKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbKengenKbn.FormattingEnabled = true;
            this.cmbKengenKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbKengenKbn.Location = new System.Drawing.Point(305, 24);
            this.cmbKengenKbn.MaxLength = 35;
            this.cmbKengenKbn.Name = "cmbKengenKbn";
            this.cmbKengenKbn.Size = new System.Drawing.Size(90, 25);
            this.cmbKengenKbn.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(224, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 55;
            this.label1.Text = "権限";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Controls.Add(this.chkContainDel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1168, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(183, 94);
            this.panel3.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(36, 51);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 35);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // chkContainDel
            // 
            this.chkContainDel.AutoSize = true;
            this.chkContainDel.Location = new System.Drawing.Point(36, 24);
            this.chkContainDel.Name = "chkContainDel";
            this.chkContainDel.Size = new System.Drawing.Size(90, 21);
            this.chkContainDel.TabIndex = 11;
            this.chkContainDel.Text = "非表示含む";
            this.chkContainDel.UseVisualStyleBackColor = true;
            // 
            // lblEmploeeId
            // 
            this.lblEmploeeId.AutoSize = true;
            this.lblEmploeeId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeId.Location = new System.Drawing.Point(18, 27);
            this.lblEmploeeId.Name = "lblEmploeeId";
            this.lblEmploeeId.Size = new System.Drawing.Size(62, 17);
            this.lblEmploeeId.TabIndex = 52;
            this.lblEmploeeId.Text = "担当者ID";
            // 
            // txtEmploeeId
            // 
            this.txtEmploeeId.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeId.Location = new System.Drawing.Point(88, 23);
            this.txtEmploeeId.MaxLength = 6;
            this.txtEmploeeId.Name = "txtEmploeeId";
            this.txtEmploeeId.Size = new System.Drawing.Size(62, 24);
            this.txtEmploeeId.TabIndex = 0;
            this.txtEmploeeId.Text = "123456";
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1202, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(182, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(47, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 101;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(141, 16);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 56;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 16);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // EmploeeId
            // 
            this.EmploeeId.DataPropertyName = "EmploeeId";
            this.EmploeeId.HeaderText = "社員ID";
            this.EmploeeId.MaxInputLength = 8;
            this.EmploeeId.Name = "EmploeeId";
            this.EmploeeId.Width = 80;
            // 
            // EmploeeName
            // 
            this.EmploeeName.DataPropertyName = "EmploeeName";
            this.EmploeeName.HeaderText = "社員名";
            this.EmploeeName.MaxInputLength = 20;
            this.EmploeeName.Name = "EmploeeName";
            // 
            // AuthorityKbn
            // 
            this.AuthorityKbn.DataPropertyName = "AuthorityKbn";
            this.AuthorityKbn.HeaderText = "権限（内部データ）";
            this.AuthorityKbn.Name = "AuthorityKbn";
            this.AuthorityKbn.Visible = false;
            // 
            // TantoKbn
            // 
            this.TantoKbn.DataPropertyName = "TantoKbn";
            this.TantoKbn.HeaderText = "担当者区分（内部データ）";
            this.TantoKbn.Name = "TantoKbn";
            this.TantoKbn.Visible = false;
            // 
            // YakushokuKbn
            // 
            this.YakushokuKbn.DataPropertyName = "YakushokuKbn";
            this.YakushokuKbn.HeaderText = "役職区分（内部データ）";
            this.YakushokuKbn.Name = "YakushokuKbn";
            this.YakushokuKbn.Visible = false;
            // 
            // BushoCD
            // 
            this.BushoCD.DataPropertyName = "BushoCD";
            this.BushoCD.HeaderText = "部署コード";
            this.BushoCD.Name = "BushoCD";
            this.BushoCD.Width = 90;
            // 
            // BushoName
            // 
            this.BushoName.DataPropertyName = "BushoName";
            this.BushoName.HeaderText = "部署名";
            this.BushoName.Name = "BushoName";
            this.BushoName.Width = 180;
            // 
            // KaishaCD
            // 
            this.KaishaCD.DataPropertyName = "KaishaCD";
            this.KaishaCD.HeaderText = "会社コード";
            this.KaishaCD.MinimumWidth = 9;
            this.KaishaCD.Name = "KaishaCD";
            // 
            // KaishaName
            // 
            this.KaishaName.DataPropertyName = "KaishaName";
            this.KaishaName.HeaderText = "会社名";
            this.KaishaName.MinimumWidth = 50;
            this.KaishaName.Name = "KaishaName";
            this.KaishaName.Width = 150;
            // 
            // AuthorityKbnName
            // 
            this.AuthorityKbnName.DataPropertyName = "AuthorityKbnName";
            this.AuthorityKbnName.HeaderText = "権限";
            this.AuthorityKbnName.Name = "AuthorityKbnName";
            this.AuthorityKbnName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AuthorityKbnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AuthorityKbnName.Width = 90;
            // 
            // TantoKbnName
            // 
            this.TantoKbnName.DataPropertyName = "TantoKbnName";
            this.TantoKbnName.HeaderText = "担当者区分";
            this.TantoKbnName.Name = "TantoKbnName";
            this.TantoKbnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // YakushokuKbnName
            // 
            this.YakushokuKbnName.DataPropertyName = "YakushokuKbnName";
            this.YakushokuKbnName.HeaderText = "役職区分";
            this.YakushokuKbnName.Name = "YakushokuKbnName";
            this.YakushokuKbnName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.YakushokuKbnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // MailAddress
            // 
            this.MailAddress.DataPropertyName = "MailAddress";
            this.MailAddress.HeaderText = "メールアドレス";
            this.MailAddress.Name = "MailAddress";
            this.MailAddress.Width = 250;
            // 
            // DelFlg
            // 
            this.DelFlg.DataPropertyName = "DelFlg";
            this.DelFlg.HeaderText = "非表示";
            this.DelFlg.Name = "DelFlg";
            this.DelFlg.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DelFlg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DelFlg.Width = 70;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "編集ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // PAF10000_MstEmploeeBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.pnlFootor);
            this.Controls.Add(this.pnlExecute);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "PAF10000_MstEmploeeBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "担当者";
            this.pnlExecute.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstEmploee)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlFootor.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.CheckBox chkContainDel;
        public System.Windows.Forms.Label lblEmploeeId;
        public System.Windows.Forms.TextBox txtEmploeeId;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.DataGridView dgvMstEmploee;
        public System.Windows.Forms.ComboBox cmbYakushokuKbn;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbTantoKbn;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cmbKengenKbn;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtKaishaCD;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtBushoCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmploeeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmploeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AuthorityKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TantoKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn YakushokuKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BushoCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn BushoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KaishaCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn KaishaName;
        private System.Windows.Forms.DataGridViewComboBoxColumn AuthorityKbnName;
        private System.Windows.Forms.DataGridViewComboBoxColumn TantoKbnName;
        private System.Windows.Forms.DataGridViewComboBoxColumn YakushokuKbnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailAddress;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DelFlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

