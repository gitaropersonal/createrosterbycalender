﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF3010_MstEmploeeBody : Form {
        public PAF3010_MstEmploeeBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF3010_MstEmploeeFormLogic(this, loginInfo);
        }
    }
}
