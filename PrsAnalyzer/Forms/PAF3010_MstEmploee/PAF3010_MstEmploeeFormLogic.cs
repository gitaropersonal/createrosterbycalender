﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF3010_MstEmploeeFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF3010_MstEmploeeBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// Service（社員マスタ）
        /// </summary>
        private PAF3010_MstEmploeeService _MST_EMPLOEE_SERVICE = new PAF3010_MstEmploeeService();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static MstEmploeeGridDto _CELL_HEADER_NAME_DTO = new MstEmploeeGridDto();
        private string[] _EditableGridCells = new string[] { nameof(_CELL_HEADER_NAME_DTO.EmploeeId)
                                                           , nameof(_CELL_HEADER_NAME_DTO.EmploeeName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.BushoCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KaishaCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.AuthorityKbnName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.TantoKbnName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.YakushokuKbnName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.MailAddress)
                                                           , nameof(_CELL_HEADER_NAME_DTO.DelFlg) };

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF3010_MstEmploeeFormLogic(PAF3010_MstEmploeeBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                SearchEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                UpdateEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // グリッドCellEnter
            _Body.dgvMstEmploee.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstEmploee.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstEmploee.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstEmploee.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case nameof(_CELL_HEADER_NAME_DTO.EmploeeName):
                    _Body.dgvMstEmploee.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstEmploee.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {

            // イベント一旦削除
            _Body.dgvMstEmploee.CellValueChanged -= GridCellValueChanged;

            var dto = new MstEmploeeGridDto();
            string tgtCellName = _Body.dgvMstEmploee.Columns[e.ColumnIndex].Name;
            DataGridViewRow r = _Body.dgvMstEmploee.Rows[e.RowIndex];
            var tgtCellVal = r.Cells[e.ColumnIndex].Value;
            
            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex, e.ColumnIndex);

                // セル背景色
                _Body.dgvMstEmploee.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string EmploeeId = StringUtil.TostringNullForbid(r.Cells[nameof(dto.EmploeeId)].Value);
                string status = StringUtil.TostringNullForbid(r.Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(EmploeeId) && status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString()) {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                } else {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstEmploee.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        /// <param name="ColumnIndex"></param>
        private void EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex, int ColumnIndex) {

            DataGridViewRow r = _Body.dgvMstEmploee.Rows[RowIndex];
            switch (tgtCellName) {

                case nameof(_CELL_HEADER_NAME_DTO.BushoCD):     // 部署コード
                    string bushoCode = StringUtil.TostringNullForbid(tgtCellVal).PadLeft(6, '0');
                    var tgtBusho = new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, bushoCode);
                    if (string.IsNullOrEmpty(tgtBusho.BushoCD)) {
                        r.Cells[ColumnIndex].Value = string.Empty;
                        r.Cells[nameof(_CELL_HEADER_NAME_DTO.BushoName)].Value = string.Empty;
                        break;
                    }
                    r.Cells[ColumnIndex].Value = bushoCode;
                    r.Cells[nameof(_CELL_HEADER_NAME_DTO.BushoName)].Value = tgtBusho.BushoName;
                    break;

                case nameof(_CELL_HEADER_NAME_DTO.KaishaCD):    // 会社コード
                    string kaishaCode = StringUtil.TostringNullForbid(tgtCellVal);
                    var tgtKaisha = new MstCommonService().LoadMstKaishaSingle(_DB_CONN_STRING, kaishaCode);
                    if (string.IsNullOrEmpty(tgtKaisha.KaishaCD)) {
                        r.Cells[ColumnIndex].Value = string.Empty;
                        r.Cells[nameof(_CELL_HEADER_NAME_DTO.KaishaName)].Value = string.Empty;
                        break;
                    }
                    r.Cells[ColumnIndex].Value = kaishaCode;
                    r.Cells[nameof(_CELL_HEADER_NAME_DTO.KaishaName)].Value = tgtKaisha.KaishaName;
                    break;

                case nameof(_CELL_HEADER_NAME_DTO.AuthorityKbnName): // 権限区分
                    int kengenKbn = (int)Enums._DIC_KENGEN_KBN.FirstOrDefault(n => n.Value == tgtCellVal.ToString()).Key;
                    r.Cells[nameof(_CELL_HEADER_NAME_DTO.AuthorityKbn)].Value = kengenKbn;
                    break;

                case nameof(_CELL_HEADER_NAME_DTO.TantoKbnName):     // 担当区分
                    int tantoKbn = (int)Enums._DIC_TANTO_KBN.FirstOrDefault(n => n.Value == tgtCellVal.ToString()).Key;
                    r.Cells[nameof(_CELL_HEADER_NAME_DTO.TantoKbn)].Value = tantoKbn;
                    break;

                case nameof(_CELL_HEADER_NAME_DTO.YakushokuKbnName): // 役職区分
                    int yakushokuKbn = (int)Enums._DIC_YAKUSHOKU_KBN.FirstOrDefault(n => n.Value == tgtCellVal.ToString()).Key;
                    r.Cells[nameof(_CELL_HEADER_NAME_DTO.YakushokuKbn)].Value = yakushokuKbn;
                    break;
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void SearchEvent(bool isShowNotExistMessage = true) {

            // グリッド初期化
            _Body.dgvMstEmploee.Rows.Clear();

            // 検索データ絞り込み
            var con = new LoadMstTantoCon() {
                TantoID = _Body.txtEmploeeId.Text,
                BushoCD = _Body.txtBushoCD.Text,
                KaishaCD = _Body.txtKaishaCD.Text,
                AuthorityKbn = string.IsNullOrEmpty(_Body.cmbKengenKbn.Text)? 
                    Enums.AuthorityKbn.None : Enums._DIC_KENGEN_KBN.FirstOrDefault(n => n.Value == _Body.cmbKengenKbn.Text).Key,
                TantoKbn = string.IsNullOrEmpty(_Body.cmbTantoKbn.Text) ?
                    Enums.TantoKbn.None : Enums._DIC_TANTO_KBN.FirstOrDefault(n => n.Value == _Body.cmbTantoKbn.Text).Key,
                YakushokuKbn = string.IsNullOrEmpty(_Body.cmbYakushokuKbn.Text) ?
                    Enums.YakushokuKbn.None : Enums._DIC_YAKUSHOKU_KBN.FirstOrDefault(n => n.Value == _Body.cmbYakushokuKbn.Text).Key,
                IsContaiDeleted = _Body.chkContainDel.Checked,
            };
            var mstData = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, con);

            // ソート
            mstData = mstData.OrderBy(n => n.TantoID).ToList();
            if (!mstData.Any() && isShowNotExistMessage) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // グリッドオブジェクトを作成
            _Body.dgvMstEmploee.DataSource = GetGridBindingSource(mstData);

            // グリッドセル背景色をセット
            SetColor_GridCsll();

            _Body.dgvMstEmploee.Show();
            _Body.dgvMstEmploee.Focus();

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // フォーカス
            _Body.dgvMstEmploee.Focus();
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void UpdateEvent() {

            var gridDtoList = new List<MstEmploeeGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            SearchEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// DBにデータ登録
        /// </summary>
        private void AddUpdDatasIntoDB(List<MstEmploeeGridDto> gridDtoList) {

            try {
                // 登録・更新用データ作成
                var addDatas = new List<MX03TantoEntity>();
                var updDatas = new List<MX03TantoEntity>();
                CreateAddUpdDatas(gridDtoList, addDatas, updDatas);

                // DB登録
                addDatas.ForEach(n => _MST_EMPLOEE_SERVICE.AddMstTanto(_DB_CONN_STRING, n));

                // DB更新
                updDatas.ForEach(n => _MST_EMPLOEE_SERVICE.UpdMstTanto(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録・更新用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <param name="addDatas"></param>
        /// <param name="updDatas"></param>
        private void CreateAddUpdDatas(List<MstEmploeeGridDto> gridDtoList, List<MX03TantoEntity> addDatas, List<MX03TantoEntity> updDatas) {

            // 既存データ取得
            var oldDatas = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, new LoadMstTantoCon());

            // 更新用データ作成
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList) {
                if (string.IsNullOrEmpty(rowDto.EmploeeId)) {
                    continue;
                }
                var entity = new MX03TantoEntity() {
                    TantoID = StringUtil.TostringNullForbid(rowDto.EmploeeId),
                    TantoName = StringUtil.TostringNullForbid(rowDto.EmploeeName),
                    BushoCD = StringUtil.TostringNullForbid(rowDto.BushoCD),
                    KaishaCD = StringUtil.TostringNullForbid(rowDto.KaishaCD),
                    AuthorityKbn = rowDto.AuthorityKbn,
                    TantoKbn = rowDto.TantoKbn,
                    YakushokuKbn = rowDto.YakushokuKbn,
                    MailAddress = StringUtil.TostringNullForbid(rowDto.MailAddress),
                    DelFlg = StringUtil.ConvertFlgToStrFlg(rowDto.DelFlg),
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                if (oldDatas.Exists(n => n.TantoID == entity.TantoID) && rowDto.Status == Enums.EditStatus.UPDATE) {
                    // 更新
                    var tgtEntity = oldDatas.FirstOrDefault(n => n.TantoID == entity.TantoID);
                    tgtEntity.TantoName = StringUtil.TostringNullForbid(entity.TantoName);
                    tgtEntity.BushoCD = StringUtil.TostringNullForbid(entity.BushoCD);
                    tgtEntity.KaishaCD = StringUtil.TostringNullForbid(entity.KaishaCD);
                    tgtEntity.AuthorityKbn = entity.AuthorityKbn;
                    tgtEntity.TantoKbn = entity.TantoKbn;
                    tgtEntity.YakushokuKbn = entity.YakushokuKbn;
                    tgtEntity.MailAddress = StringUtil.TostringNullForbid(entity.MailAddress);
                    tgtEntity.DelFlg = entity.DelFlg;
                    tgtEntity.UpdDate = dtNow;
                    updDatas.Add(tgtEntity);
                    continue;
                }
                if (rowDto.Status == Enums.EditStatus.INSERT) {
                    // 追加
                    addDatas.Add(entity);
                }
            }
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool UpdValidate(List<MstEmploeeGridDto> gridRowDtoList) {

            List<MstEmploeeGridDto> allGridDtoList = new List<MstEmploeeGridDto>();
            foreach (DataGridViewRow r in _Body.dgvMstEmploee.Rows) {
                var rowDto = GridRowUtil<MstEmploeeGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // マスタ存在チェック
            if (!ValidateMstExist(gridRowDtoList)) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(allGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            var errColNames = new List<string>();
            string headerTxt = string.Empty;
            foreach (DataGridViewRow r in _Body.dgvMstEmploee.Rows) {
                var rowDto = GridRowUtil<MstEmploeeGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // 社員ID
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.EmploeeId)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.EmploeeId), ref errColNames, ref result);

                // 社員名
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.EmploeeName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.EmploeeName), ref errColNames, ref result);

                // 部署コード
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.BushoCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.BushoCD), ref errColNames, ref result);

                // 会社コード
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.KaishaCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KaishaCD), ref errColNames, ref result);

                // 権限区分
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.AuthorityKbnName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.AuthorityKbnName), ref errColNames, ref result);

                // 担当者区分
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.TantoKbnName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.TantoKbnName), ref errColNames, ref result);

                // 役職区分
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.YakushokuKbnName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.YakushokuKbnName), ref errColNames, ref result);

                // メールアドレス
                headerTxt = _Body.dgvMstEmploee.Columns[nameof(rowDto.MailAddress)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.MailAddress), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MstEmploeeGridDto> gridDtoList) {

            // 既存データ取得
            var oldDatas = _MST_EMPLOEE_SERVICE.LoadMstTanto(_DB_CONN_STRING, new LoadMstTantoCon());

            string msg = string.Empty;
            foreach (var keyInfo in gridDtoList) {

                bool isDouple1 = (keyInfo.Status == Enums.EditStatus.INSERT && oldDatas.Exists(n => n.TantoID == keyInfo.EmploeeId));
                bool isDouple2 = 1 < gridDtoList.Where(n => n.EmploeeId == keyInfo.EmploeeId).ToList().Count;
                if (!isDouple1 && !isDouple2) {
                    continue;
                }
                // 重複キー
                var dto = new MstEmploeeGridDto();
                msg = _Body.dgvMstEmploee.Columns[nameof(dto.EmploeeId)].HeaderText;

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvMstEmploee.Rows) {
                    var rowDto = GridRowUtil<MstEmploeeGridDto>.GetRowModel(r);
                    if (rowDto.EmploeeId == keyInfo.EmploeeId) {
                        r.Cells[nameof(rowDto.EmploeeId)].Style.BackColor = Color.Red;
                    }
                }
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
                return false;
            }
            return true;
        }
        /// <summary>
        /// マスタ存在チェック
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private bool ValidateMstExist(List<MstEmploeeGridDto> gridDtoList) {

            var service = new MstCommonService();
            bool result = true;
            foreach (var rowDto in gridDtoList) {

                // 部署
                string bushoName = service.LoadMstBushoSingle(_DB_CONN_STRING, rowDto.BushoCD).BushoName;
                if (string.IsNullOrEmpty(bushoName)) {
                    int rowIdx = gridDtoList.IndexOf(rowDto);
                    _Body.dgvMstEmploee.Rows[rowIdx].Cells[nameof(rowDto.BushoCD)].Style.BackColor = Color.Red;
                    result = false;
                }
            }
            if (!result) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_MST);
            }
            return result;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvMstEmploee.Rows) {
                var rowDto = GridRowUtil<MstEmploeeGridDto>.GetRowModel(r);
                foreach (DataGridViewCell c in r.Cells) {
                    // 社員ID
                    if (c.OwningColumn.Name == nameof(_CELL_HEADER_NAME_DTO.EmploeeId)
                        && _Body.dgvMstEmploee.Rows.IndexOf(r) < _Body.dgvMstEmploee.Rows.Count - 1) {

                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        c.ReadOnly = true;
                    }
                    // 部署名・会社名
                    if (c.OwningColumn.Name == nameof(_CELL_HEADER_NAME_DTO.BushoName) || c.OwningColumn.Name == nameof(_CELL_HEADER_NAME_DTO.KaishaName)) {

                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        c.ReadOnly = true;
                    }
                    // 削除行
                    if (rowDto.DelFlg) {
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        if (c.OwningColumn.Name != nameof(_CELL_HEADER_NAME_DTO.DelFlg)) {
                            c.ReadOnly = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// グリッドViewModel取得
        /// </summary>
        /// <param name="tgtEmploeeInfos"></param>
        /// <returns></returns>
        private BindingList<MstEmploeeGridDto> GetGridBindingSource(List<MX03TantoEntity> tgtEmploeeInfos) {
            var ret = new BindingList<MstEmploeeGridDto>();
            tgtEmploeeInfos.ForEach(tgtEmp => ret.Add(
                new MstEmploeeGridDto() {
                    EmploeeId = tgtEmp.TantoID,
                    EmploeeName = tgtEmp.TantoName,
                    BushoCD = tgtEmp.BushoCD,
                    BushoName = tgtEmp.BushoName,
                    KaishaCD = tgtEmp.KaishaCD,
                    KaishaName = tgtEmp.KaishaName,
                    TantoKbn = tgtEmp.TantoKbn,
                    TantoKbnName = Enums._DIC_TANTO_KBN.First(n => (int)n.Key == tgtEmp.TantoKbn).Value,
                    YakushokuKbn = tgtEmp.YakushokuKbn,
                    YakushokuKbnName = Enums._DIC_YAKUSHOKU_KBN.First(n => (int)n.Key == tgtEmp.YakushokuKbn).Value,
                    AuthorityKbn = tgtEmp.AuthorityKbn,
                    AuthorityKbnName = Enums._DIC_KENGEN_KBN.First(n => (int)n.Key == tgtEmp.AuthorityKbn).Value,
                    MailAddress = tgtEmp.MailAddress,
                    DelFlg = (int.Parse(tgtEmp.DelFlg) == 1),
                    Status = Enums.EditStatus.SHOW,
                })); ;
            return ret;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvMstEmploee.Rows) {
                var rowDto = GridRowUtil<MstEmploeeGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.EmploeeId)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 社員ID
            _Body.txtEmploeeId.Text = string.Empty;

            // 部署CD
            _Body.txtBushoCD.Text = string.Empty;

            // 会社コード
            _Body.txtKaishaCD.Text = string.Empty;

            // コンボボックス
            InitComboKengenKbn();
            InitComboTantoKbn();
            InitComboYakushokuKbn();

            // 削除含む
            _Body.chkContainDel.Checked = false;

            // グリッド
            _Body.dgvMstEmploee.Rows.Clear();
            DataGridViewComboBoxColumn cmbKengen = (DataGridViewComboBoxColumn)_Body.dgvMstEmploee.Columns[nameof(_CELL_HEADER_NAME_DTO.AuthorityKbnName)];
            cmbKengen.Items.Clear();
            foreach (var pair in Enums._DIC_KENGEN_KBN) {
                cmbKengen.Items.Add(pair.Value);
            }
            DataGridViewComboBoxColumn cmbTanto= (DataGridViewComboBoxColumn)_Body.dgvMstEmploee.Columns[nameof(_CELL_HEADER_NAME_DTO.TantoKbnName)];
            cmbTanto.Items.Clear();
            foreach (var pair in Enums._DIC_TANTO_KBN) {
                cmbTanto.Items.Add(pair.Value);
            }
            DataGridViewComboBoxColumn cmbYakushoku = (DataGridViewComboBoxColumn)_Body.dgvMstEmploee.Columns[nameof(_CELL_HEADER_NAME_DTO.YakushokuKbnName)];
            cmbYakushoku.Items.Clear();
            foreach (var pair in Enums._DIC_YAKUSHOKU_KBN) {
                cmbYakushoku.Items.Add(pair.Value);
            }
            SetColor_GridCsll();

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();

            // フォーカス
            _Body.txtEmploeeId.Focus();
        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {
            
            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);
        }
        /// <summary>
        /// コンボボックス初期化（権限区分）
        /// </summary>
        private void InitComboKengenKbn() {
            _Body.cmbKengenKbn.Items.Clear();
            _Body.cmbKengenKbn.Items.Add(string.Empty);
            foreach (var pair in Enums._DIC_KENGEN_KBN) {
                _Body.cmbKengenKbn.Items.Add(pair.Value);
            }
            _Body.cmbKengenKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// コンボボックス初期化（担当区分）
        /// </summary>
        private void InitComboTantoKbn() {
            _Body.cmbTantoKbn.Items.Clear();
            _Body.cmbTantoKbn.Items.Add(string.Empty);
            foreach (var pair in Enums._DIC_TANTO_KBN) {
                _Body.cmbTantoKbn.Items.Add(pair.Value);
            }
            _Body.cmbTantoKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// コンボボックス初期化（役職区分）
        /// </summary>
        private void InitComboYakushokuKbn() {
            _Body.cmbYakushokuKbn.Items.Clear();
            _Body.cmbYakushokuKbn.Items.Add(string.Empty);
            foreach (var pair in Enums._DIC_YAKUSHOKU_KBN) {
                _Body.cmbYakushokuKbn.Items.Add(pair.Value);
            }
            _Body.cmbYakushokuKbn.SelectedIndex = 0;
        }
        #endregion

    }
}
