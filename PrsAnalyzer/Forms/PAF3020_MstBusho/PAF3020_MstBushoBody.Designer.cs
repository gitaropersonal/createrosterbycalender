﻿namespace PrsAnalyzer.Forms {
    partial class PAF3020_MstBushoBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF3020_MstBushoBody));
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvMstBusho = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.cmbPBushoKbn = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkContainDel = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBushoName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBushoCode = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.PBushoKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BushoCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BushoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PBushoKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DelFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstBusho)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvMstBusho);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel3.Location = new System.Drawing.Point(0, 122);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.panel3.Size = new System.Drawing.Size(684, 224);
            this.panel3.TabIndex = 20;
            // 
            // dgvMstBusho
            // 
            this.dgvMstBusho.AllowUserToResizeRows = false;
            this.dgvMstBusho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMstBusho.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PBushoKbn,
            this.BushoCD,
            this.BushoName,
            this.PBushoKbnName,
            this.DelFlg,
            this.Status});
            this.dgvMstBusho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMstBusho.Location = new System.Drawing.Point(15, 15);
            this.dgvMstBusho.Name = "dgvMstBusho";
            this.dgvMstBusho.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMstBusho.RowTemplate.Height = 21;
            this.dgvMstBusho.Size = new System.Drawing.Size(654, 209);
            this.dgvMstBusho.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(684, 122);
            this.panel1.TabIndex = 0;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.cmbPBushoKbn);
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.label7);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.cmbBushoName);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.txtBushoCode);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(654, 112);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // cmbPBushoKbn
            // 
            this.cmbPBushoKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPBushoKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbPBushoKbn.FormattingEnabled = true;
            this.cmbPBushoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbPBushoKbn.ItemHeight = 17;
            this.cmbPBushoKbn.Location = new System.Drawing.Point(144, 74);
            this.cmbPBushoKbn.Name = "cmbPBushoKbn";
            this.cmbPBushoKbn.Size = new System.Drawing.Size(151, 25);
            this.cmbPBushoKbn.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkContainDel);
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(502, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(149, 89);
            this.panel2.TabIndex = 10;
            // 
            // chkContainDel
            // 
            this.chkContainDel.AutoSize = true;
            this.chkContainDel.Location = new System.Drawing.Point(17, 21);
            this.chkContainDel.Name = "chkContainDel";
            this.chkContainDel.Size = new System.Drawing.Size(90, 21);
            this.chkContainDel.TabIndex = 11;
            this.chkContainDel.Text = "非表示含む";
            this.chkContainDel.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(17, 48);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 35);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(22, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 17);
            this.label7.TabIndex = 84;
            this.label7.Text = "プロジェクト部署区分";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(22, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 80;
            this.label1.Text = "部署名";
            // 
            // cmbBushoName
            // 
            this.cmbBushoName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBushoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbBushoName.FormattingEnabled = true;
            this.cmbBushoName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbBushoName.Location = new System.Drawing.Point(144, 48);
            this.cmbBushoName.Name = "cmbBushoName";
            this.cmbBushoName.Size = new System.Drawing.Size(217, 25);
            this.cmbBushoName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(22, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 82;
            this.label2.Text = "部署コード";
            // 
            // txtBushoCode
            // 
            this.txtBushoCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBushoCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBushoCode.Location = new System.Drawing.Point(144, 23);
            this.txtBushoCode.MaxLength = 6;
            this.txtBushoCode.Name = "txtBushoCode";
            this.txtBushoCode.Size = new System.Drawing.Size(82, 24);
            this.txtBushoCode.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 16);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(141, 16);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(488, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(196, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(61, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 346);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(684, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // PBushoKbn
            // 
            this.PBushoKbn.DataPropertyName = "PBushoKbn";
            this.PBushoKbn.HeaderText = "プロジェクト部署区分(内部)";
            this.PBushoKbn.Name = "PBushoKbn";
            this.PBushoKbn.Visible = false;
            // 
            // BushoCD
            // 
            this.BushoCD.DataPropertyName = "BushoCD";
            this.BushoCD.HeaderText = "部署コード";
            this.BushoCD.MaxInputLength = 6;
            this.BushoCD.Name = "BushoCD";
            // 
            // BushoName
            // 
            this.BushoName.DataPropertyName = "BushoName";
            this.BushoName.HeaderText = "部署名";
            this.BushoName.MaxInputLength = 50;
            this.BushoName.Name = "BushoName";
            this.BushoName.Width = 300;
            // 
            // PBushoKbnName
            // 
            this.PBushoKbnName.DataPropertyName = "PBushoKbnName";
            this.PBushoKbnName.HeaderText = "プロジェクト部署区分";
            this.PBushoKbnName.Name = "PBushoKbnName";
            this.PBushoKbnName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PBushoKbnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PBushoKbnName.Width = 150;
            // 
            // DelFlg
            // 
            this.DelFlg.DataPropertyName = "DelFlg";
            this.DelFlg.HeaderText = "非表示";
            this.DelFlg.Name = "DelFlg";
            this.DelFlg.Width = 60;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "編集ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // PAF15000_MstBushoBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlFootor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "PAF15000_MstBushoBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "部署";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstBusho)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvMstBusho;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.ComboBox cmbPBushoKbn;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cmbBushoName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtBushoCode;
        public System.Windows.Forms.CheckBox chkContainDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn PBushoKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BushoCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn BushoName;
        private System.Windows.Forms.DataGridViewComboBoxColumn PBushoKbnName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DelFlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

