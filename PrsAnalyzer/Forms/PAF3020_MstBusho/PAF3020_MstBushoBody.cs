﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF3020_MstBushoBody : Form {
        public PAF3020_MstBushoBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF3020_MstBushoFormLogic(this, loginInfo);
        }
    }
}
