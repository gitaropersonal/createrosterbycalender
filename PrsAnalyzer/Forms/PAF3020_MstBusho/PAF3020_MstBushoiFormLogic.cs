﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF3020_MstBushoFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF3020_MstBushoBody _Body;
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static MstBushoGridDto _CELL_HEADER_NAME_DTO = new MstBushoGridDto();
        private string[] _EditableGridCells = new string[] { nameof(_CELL_HEADER_NAME_DTO.BushoCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.BushoName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.DelFlg)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Status) };

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF3020_MstBushoFormLogic(PAF3020_MstBushoBody body, LoginInfoDto loginInfo) {
            _Body = body;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpd_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // テキストボックスValidated
            _Body.txtBushoCode.Validated += (s, e) => {
                LostFocus_BushoCd();
            };
            // 部署名SelectionChangeCommitted
            _Body.cmbBushoName.SelectedValueChanged += (s, e) => {
                SelectionChangeCommited_BushoName();
            };
            // グリッドCellEnter
            _Body.dgvMstBusho.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstBusho.RowPostPaint += (s, e) => {
                GridRowUtil<MstBushoGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstBusho.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvMstBusho.Rows) {
                var rowDto = GridRowUtil<MstBushoGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.BushoCD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstBusho.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case nameof(_CELL_HEADER_NAME_DTO.BushoName):
                    _Body.dgvMstBusho.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstBusho.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// 部署コードロストフォーカス
        /// </summary>
        private void LostFocus_BushoCd() {
            if (!ExecuteLostFocus()) {
                return;
            }
            // 背景色初期化
            InitColor();

            // Validate
            if (!ValidateLostFocus()) {

                // テキストクリア
                _Body.txtBushoCode.Text = string.Empty;
                _Body.cmbBushoName.Text = string.Empty;
                return;
            }
            // マスタデータ検索
            var mstData = new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, _Body.txtBushoCode.Text);
            if (mstData != null && !string.IsNullOrEmpty(mstData.BushoCD)) {
                _Body.cmbBushoName.Text = mstData.BushoName;
            }
        }
        /// <summary>
        /// 部署名SelectionChangeCommitted
        /// </summary>
        private void SelectionChangeCommited_BushoName() {
            var mstData = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, new MX06BushoEntity(), false);
            if (mstData.Exists(n => n.BushoName == _Body.cmbBushoName.Text)) {
                _Body.txtBushoCode.Text = mstData.First(n => n.BushoName == _Body.cmbBushoName.Text).BushoCD;
                return;
            }
            // テキストクリア
            _Body.txtBushoCode.Text = string.Empty;
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvMstBusho.CellValueChanged -= GridCellValueChanged;

            var dto = new MstBushoGridDto();
            string tgtCellName = _Body.dgvMstBusho.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvMstBusho.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                _Body.dgvMstBusho.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string bushoCd = StringUtil.TostringNullForbid(_Body.dgvMstBusho.Rows[e.RowIndex].Cells[nameof(dto.BushoCD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvMstBusho.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(bushoCd) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvMstBusho.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvMstBusho.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstBusho.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        private bool EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex) {
            if (tgtCellVal == null){
                return false;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {
                case nameof(_CELL_HEADER_NAME_DTO.BushoCD):
                    string bushoCode = inputVal.PadLeft(6, '0');
                    string bushoName = new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, bushoCode).BushoName;
                    _Body.dgvMstBusho.Rows[RowIndex].Cells[tgtCellName].Value = bushoCode;
                    _Body.dgvMstBusho.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.BushoName)].Value = bushoName;
                    return true;

                default:
                    // 入力値を編集→セット
                    _Body.dgvMstBusho.Rows[RowIndex].Cells[tgtCellName].Value = inputVal;
                    return true;
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void BtnSearch_ClickEvent(bool isShowNotExistMessage = true) {

            try {
                // グリッド初期化
                _Body.dgvMstBusho.Rows.Clear();

                // パラメータDto1取得
                var pBushoKbn = ProjectConst.PBushoKbn.NONE;
                if (ProjectConst._DIC_PROJECT_KBN.ToList().Exists(n => n.Value == _Body.cmbPBushoKbn.Text)) {
                    pBushoKbn = ProjectConst._DIC_PROJECT_KBN.ToList().First(n => n.Value == _Body.cmbPBushoKbn.Text).Key;
                }
                var con1 = new MX06BushoEntity() {
                    BushoCD = _Body.txtBushoCode.Text,
                    PBushoKbn = (int)pBushoKbn,
                };
                var datas = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, con1, _Body.chkContainDel.Checked);

                // グリッドオブジェクトを作成
                var gridData = new BindingList<MstBushoGridDto>();
                foreach (var data in datas) {
                    var dto = new MstBushoGridDto() {
                        BushoCD = data.BushoCD,
                        BushoName = data.BushoName,
                        PBushoKbn = data.PBushoKbn.ToString(),
                        PBushoKbnName = ProjectConst._DIC_PROJECT_KBN.First(n => (int)n.Key == data.PBushoKbn).Value,
                        DelFlg = (int.Parse(data.DelFlg) == 1),
                        Status = Enums.EditStatus.SHOW,
                    };
                    gridData.Add(dto);
                }
                if (!gridData.Any() && isShowNotExistMessage) {
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                _Body.dgvMstBusho.DataSource = gridData;

                // グリッドセル背景色をセット
                SetColor_GridCsll();

                _Body.dgvMstBusho.Show();
                _Body.dgvMstBusho.Focus();

                // 使用可否切り替え（更新ボタン）
                SwitchEnable_BtnUpdate();
                
                // フォーカス
                _Body.dgvMstBusho.Focus();
            }
            catch (Exception ex) {
                LogUtil.ShowLogErr(ex);
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpd_ClickEvent() {

            var gridRowDtoList = new List<MstBushoGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridRowDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            string gyomuKbn = _Body.cmbBushoName.Text;
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridRowDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <param name="strTgtYM"></param>
        private void AddUpdDatasIntoDB(List<MstBushoGridDto> gridRowDtoList) {

            try {
                // 更新用データ格納リスト
                var addDatas = new List<MX06BushoEntity>();
                var updDatas = new List<MX06BushoEntity>();

                // 検索データ絞り込み
                var oldDatas = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, new MX06BushoEntity(), true);

                // 登録用データ作成
                addDatas = CreateAddDatas(gridRowDtoList);

                // 登録処理
                var service = new PAF3020_MstBushoService();
                addDatas.ForEach(n => service.AddMstBusho(_DB_CONN_STRING, n));

                // 更新用データ作成
                updDatas = CreateUpdDatas(oldDatas, gridRowDtoList);

                // 更新処理
                updDatas.ForEach(n => service.UpdMstBusho(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX06BushoEntity> CreateAddDatas(List<MstBushoGridDto> gridDtoList) {

            var ret = new List<MX06BushoEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.INSERT).ToList()) {

                // データ作成
                var entity = new MX06BushoEntity() {
                    BushoCD = StringUtil.TostringNullForbid(rowDto.BushoCD),
                    BushoName = StringUtil.TostringNullForbid(rowDto.BushoName),
                    PBushoKbn = TypeConversionUtil.ToInteger(rowDto.DelFlg),
                    DelFlg = StringUtil.ConvertFlgToStrFlg(rowDto.DelFlg),
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX06BushoEntity> CreateUpdDatas(List<MX06BushoEntity> oldDatas, List<MstBushoGridDto> gridDtoList) {

            var ret = new List<MX06BushoEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.UPDATE).ToList()) {

                // データ作成
                var tgtEntity = oldDatas.Where(n => n.BushoCD == rowDto.BushoCD);                
                if (tgtEntity != null) {
                    var addEntity = new MX06BushoEntity();
                    addEntity.BushoName = StringUtil.TostringNullForbid(rowDto.BushoName);
                    addEntity.PBushoKbn = TypeConversionUtil.ToInteger(rowDto.DelFlg);
                    addEntity.DelFlg = StringUtil.ConvertFlgToStrFlg(rowDto.DelFlg);
                    addEntity.UpdDate = dtNow;
                    ret.Add(addEntity);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvMstBusho.Rows) {
                var rowDto = GridRowUtil<MstBushoGridDto>.GetRowModel(r);

                // 部署コード
                if (!string.IsNullOrEmpty(rowDto.BushoCD)) {
                    r.Cells[nameof(rowDto.BushoCD)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[nameof(rowDto.BushoCD)].ReadOnly = true;
                    continue;
                }
                foreach (DataGridViewCell c in r.Cells) {
                    // 更新不可能列の編集
                    if (!string.IsNullOrEmpty(rowDto.BushoCD)) {

                        r.Cells[nameof(rowDto.BushoCD)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        r.Cells[nameof(rowDto.BushoCD)].ReadOnly = true;
                    }
                    // 削除行
                    if (rowDto.DelFlg) {
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        if (c.OwningColumn.Name != nameof(_CELL_HEADER_NAME_DTO.DelFlg)) {
                            c.ReadOnly = true;
                        }
                    }
                }
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate
        /// </summary>
        private bool ValidateLostFocus() {

            // 背景色初期化
            InitColor();

            if (string.IsNullOrEmpty(_Body.txtBushoCode.Text)) {
                return true;
            }
            // マスタCSVロード
            var mstData = new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, _Body.txtBushoCode.Text);
            if (mstData == null || string.IsNullOrEmpty(mstData.BushoCD)) {
                _Body.txtBushoCode.BackColor = Color.Red;
                _Body.txtBushoCode.Focus();
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_MST);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<MstBushoGridDto> gridRowDtoList) {

            foreach (DataGridViewRow r in _Body.dgvMstBusho.Rows) {
                var rowDto = GridRowUtil<MstBushoGridDto>.GetRowModel(r);
                gridRowDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            gridRowDtoList = gridRowDtoList.Where(n => n.Status == Enums.EditStatus.INSERT || n.Status == Enums.EditStatus.UPDATE).ToList();
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(gridRowDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in _Body.dgvMstBusho.Rows) {
                
                var rowDto = GridRowUtil<MstBushoGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // 部署CD
                headerTxt = _Body.dgvMstBusho.Columns[nameof(rowDto.BushoCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.BushoCD), ref errColNames, ref result);

                // 部署名
                headerTxt = _Body.dgvMstBusho.Columns[nameof(rowDto.BushoName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.BushoName), ref errColNames, ref result);

                // プロジェクト部署区分名
                headerTxt = _Body.dgvMstBusho.Columns[nameof(rowDto.PBushoKbnName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.PBushoKbnName), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MstBushoGridDto> keyInfos) {

            // 検索データ絞り込み
            var oldDatas = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, new MX06BushoEntity(), true);

            string msg = string.Empty;
            bool resutlt = true;
            foreach (var keyInfo in keyInfos) {
                if (ValidateDouple_Insert(oldDatas, keyInfo) && ValidateDouple_Updatet(keyInfos, keyInfo)) {

                    continue;

                }
                // 重複キー
                var dto = new MstBushoGridDto();
                msg = _Body.dgvMstBusho.Columns[nameof(dto.BushoCD)].HeaderText;

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvMstBusho.Rows) {
                    var rowDto = GridRowUtil<MstBushoGridDto>.GetRowModel(r);
                    if (rowDto.BushoCD == keyInfo.BushoCD) {

                        r.Cells[nameof(rowDto.BushoCD)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt) {
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Insert(List<MX06BushoEntity> oldDatas, MstBushoGridDto keyInfo) {
            if (keyInfo.Status == Enums.EditStatus.INSERT
                    && oldDatas.Exists(n => n.BushoCD == keyInfo.BushoCD)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Updatet(List<MstBushoGridDto> keyInfos, MstBushoGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.BushoCD == keyInfo.BushoCD).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 検索条件
            InitSearchProjectLooksCondition();

            // グリッド
            _Body.dgvMstBusho.Rows.Clear();
            DataGridViewComboBoxColumn cmb = (DataGridViewComboBoxColumn)_Body.dgvMstBusho.Columns[nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)];
            cmb.Items.Clear();
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                cmb.Items.Add(pair.Value);
            }
            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();       

            // フォーカス
            _Body.txtBushoCode.Focus();

        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 部署名コンボボックス初期化
        /// </summary>
        private void InitBushoNameCombo() {

            // 初期化
            _Body.cmbBushoName.Items.Clear();
            _Body.cmbBushoName.Items.Add(string.Empty);

            // マスタデータ取得
            var mstDatas = new MstCommonService().LoadMstBusho(_DB_CONN_STRING, new MX06BushoEntity(), true);
            mstDatas.ForEach(data => _Body.cmbBushoName.Items.Add(data.BushoName));
            _Body.cmbBushoName.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト部署区分コンボボックス初期化
        /// </summary>
        private void InitPBUshoKbnCombo() {
            _Body.cmbPBushoKbn.Items.Clear();
            _Body.cmbPBushoKbn.Items.Add(string.Empty);
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                _Body.cmbPBushoKbn.Items.Add(pair.Value);
            }
            _Body.cmbPBushoKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// 検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {
            // 部署コード
            _Body.txtBushoCode.Text = string.Empty;

            // 部署名コンボボックス初期化
            InitBushoNameCombo();

            // プロジェクト部署区分コンボボックス初期化
            InitPBUshoKbnCombo();

            // 削除フラグOFF
            _Body.chkContainDel.Checked = false;
        }
        #endregion

    }
}
