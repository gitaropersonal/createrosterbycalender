﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF3030_MstProjectBody : Form {
        public PAF3030_MstProjectBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF3030_MstProjectFormLogic(this, loginInfo);
        }
    }
}
