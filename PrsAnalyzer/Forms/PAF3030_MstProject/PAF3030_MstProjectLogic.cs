﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF3030_MstProjectFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF3030_MstProjectBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static MstProjectGridDto _CELL_HEADER_NAME_DTO = new MstProjectGridDto();
        private string[] _EditableGridCells = new string[] { nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.ProjectName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KokyakuName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.StartYM_Disp)
                                                           , nameof(_CELL_HEADER_NAME_DTO.EndYM_Disp)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Status) };

        private string[] _ForbidUpdateGridCells = new string[] { nameof(_CELL_HEADER_NAME_DTO.ProjectCD)};

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF3030_MstProjectFormLogic(PAF3030_MstProjectBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpdate_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // グリッドCellEnter
            _Body.dgvMstProject.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstProject.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstProject.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstProject.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            var dto = new MstProjectGridDto();
            switch (tgtColName) {
                case nameof(dto.ProjectName):
                case nameof(dto.KokyakuName):
                    _Body.dgvMstProject.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstProject.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvMstProject.CellValueChanged -= GridCellValueChanged;

            var dto = new MstProjectGridDto();
            string tgtCellName = _Body.dgvMstProject.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvMstProject.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                if (!EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex)) {

                    // イベント回復
                    _Body.dgvMstProject.CellValueChanged += GridCellValueChanged;
                    return;
                }
                // セル背景色
                _Body.dgvMstProject.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string taskId = StringUtil.TostringNullForbid(_Body.dgvMstProject.Rows[e.RowIndex].Cells[nameof(dto.ProjectCD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvMstProject.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(taskId) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvMstProject.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvMstProject.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstProject.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        private bool EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex) {
            if (tgtCellVal == null){
                return false;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {

                case nameof(_CELL_HEADER_NAME_DTO.ProjectCD):
                    string newInputVal = GetProjectCodeGridCellEdited(inputVal);
                    if (string.IsNullOrEmpty(newInputVal)) {
                        _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    }
                    _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = newInputVal;
                    return true;

                case nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName):
                    int pBushoKbn = (int) ProjectConst._DIC_PROJECT_KBN.FirstOrDefault(n => n.Value == tgtCellVal.ToString()).Key;
                    _Body.dgvMstProject.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.PBushoKbn)].Value = pBushoKbn;
                    _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = tgtCellVal.ToString();
                    return true;

                case nameof(_CELL_HEADER_NAME_DTO.StartYM_Disp):
                    string strStartVal = TypeConversionUtil.ConvertInputToStrYM(inputVal);
                    if (string.IsNullOrEmpty(strStartVal)) {
                        _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    }
                    _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = strStartVal;
                    _Body.dgvMstProject.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.StartYM)].Value = strStartVal.Replace("/", string.Empty);
                    return true;

                case nameof(_CELL_HEADER_NAME_DTO.EndYM_Disp):
                    string strEndVal = TypeConversionUtil.ConvertInputToStrYM(inputVal);
                    if (string.IsNullOrEmpty(strEndVal)) {
                        _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    }
                    _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = strEndVal;
                    _Body.dgvMstProject.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.EndYM)].Value = strEndVal.Replace("/", string.Empty);
                    return true;

                default:
                    // 入力値を編集→セット
                    _Body.dgvMstProject.Rows[RowIndex].Cells[tgtCellName].Value = inputVal;
                    return true;
            }
        }
        /// <summary>
        /// プロジェクトコード編集（グリッドセル編集時）
        /// </summary>
        /// <param name="inputVal"></param>
        /// <returns></returns>
        private string GetProjectCodeGridCellEdited(string inputVal) {
            if (ProjectConst._PROJECT_CODE_LENGTH < inputVal.Length) {
                inputVal = inputVal.Substring(0, ProjectConst._PROJECT_CODE_LENGTH);
            }
            string newInputVal = string.Empty;
            foreach (var c in inputVal) {
                if (inputVal.IndexOf(c) == 0) {
                    newInputVal += c;
                    continue;
                }
                if (int.TryParse(c.ToString(), out int i)) {
                    newInputVal += c;
                }
            }
            return newInputVal;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void BtnSearch_ClickEvent(bool isShowNotExistMessage = true) {

            try {
                // グリッド初期化
                _Body.dgvMstProject.Rows.Clear();

                // パラメータDto1取得
                var pBushoKbn = ProjectConst.PBushoKbn.NONE;
                if (ProjectConst._DIC_PROJECT_KBN.ToList().Exists(n => n.Value == _Body.cmbPBushoKbn.Text)) {
                    pBushoKbn = ProjectConst._DIC_PROJECT_KBN.ToList().First(n => n.Value == _Body.cmbPBushoKbn.Text).Key;
                }
                var con1 = new LoadAggregateConDto() {
                    PBushoKbn = pBushoKbn,
                };
                // パラメータDto2取得
                var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked);
                string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked, _Body.txtKokyakuName.Text);
                var con2 = new LoadProjectLooksConDto() {
                    ProjectCD = _Body.txtProjectCode.Text,
                    ProjectName = _Body.txtSagyoName.Text,
                    GyomuKbn = _Body.cmbGyomuKbn.Text,
                    KokyakuKbn = kokyakuKbn,
                    KokyakuName = kokyakuName,
                };
                // 検索
                var datas = new PAF3030_MstProjectService().LoadMstProject(_DB_CONN_STRING, con1,  con2);

                // グリッドオブジェクトを作成
                var gridData = new BindingList<MstProjectGridDto>();
                foreach (var data in datas) {
                    gridData.Add(data);
                }
                if (!gridData.Any() && isShowNotExistMessage) {
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                _Body.dgvMstProject.DataSource = gridData;

                // グリッドセル背景色をセット
                SetColor_GridCsll();

                _Body.dgvMstProject.Show();
                _Body.dgvMstProject.Focus();

                // 使用可否切り替え（更新ボタン）
                SwitchEnable_BtnUpdate();
                
                // フォーカス
                _Body.dgvMstProject.Focus();
            }
            catch (Exception ex) {
                LogUtil.ShowLogErr(ex);
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpdate_ClickEvent() {

            var gridRowDtoList = new List<MstProjectGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridRowDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            string gyomuKbn = _Body.cmbGyomuKbn.Text;
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridRowDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <param name="strTgtYM"></param>
        private void AddUpdDatasIntoDB(List<MstProjectGridDto> gridRowDtoList) {

            try {
                // 更新用データ格納リスト
                var addDatas = new List<MX01ProjectEntity>();
                var updDatas = new List<MX01ProjectEntity>();

                // 検索データ絞り込み
                var service = new MstCommonService();
                var oldDatas = service.LoadMstProject(_DB_CONN_STRING);

                // 登録用データ作成
                addDatas = CreateAddDatas(gridRowDtoList);

                // 登録処理
                addDatas.ForEach(n => service.AddMstProject(_DB_CONN_STRING, n));

                // 更新用データ作成
                updDatas = CreateUpdDatas(oldDatas, gridRowDtoList);

                // 更新処理
                updDatas.ForEach(n => new PAF3030_MstProjectService().UpdMstProject(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX01ProjectEntity> CreateAddDatas(List<MstProjectGridDto> gridDtoList) {

            var ret = new List<MX01ProjectEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.INSERT).ToList()) {

                // データ作成
                var entity = new MX01ProjectEntity() {
                    ProjectCD = rowDto.ProjectCD,
                    ProjectName = rowDto.ProjectName,
                    KokyakuName = rowDto.KokyakuName,
                    PBushoKbn = TypeConversionUtil.ToInteger(rowDto.PBushoKbn),
                    StartYM = string.IsNullOrEmpty(rowDto.StartYM)? ProjectConst._MAX_PROJECT_SPAN.Replace("/", string.Empty) : rowDto.StartYM,
                    EndYM = string.IsNullOrEmpty(rowDto.EndYM) ? ProjectConst._MAX_PROJECT_SPAN.Replace("/", string.Empty) : rowDto.EndYM,
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX01ProjectEntity> CreateUpdDatas(List<MX01ProjectEntity> oldDatas, List<MstProjectGridDto> gridDtoList) {

            var ret = new List<MX01ProjectEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.UPDATE).ToList()) {

                // データ作成
                var tgtEntity = oldDatas.FirstOrDefault(n => n.ProjectCD == rowDto.ProjectCD);
                if (tgtEntity != null) {
                    var updEntity = new MX01ProjectEntity();
                    updEntity.ProjectCD = tgtEntity.ProjectCD;
                    updEntity.ProjectName = rowDto.ProjectName;
                    updEntity.KokyakuName = rowDto.KokyakuName;
                    updEntity.PBushoKbn = TypeConversionUtil.ToInteger(rowDto.PBushoKbn);
                    updEntity.StartYM = rowDto.StartYM;
                    updEntity.EndYM = rowDto.EndYM;
                    updEntity.UpdDate = dtNow;
                    ret.Add(updEntity);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                if (string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    continue;
                }
                foreach (DataGridViewCell c in r.Cells) {
                    // 更新不可能列の編集
                    if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name)) {

                        ControlUtil.SetTimeTrackerCellStyle(rowDto.ProjectCD.Substring(0, 1), c);
                        c.ReadOnly = true;
                        continue;
                    }
                    if (new string[] { "A", "B", "C", "X", "Y" }.Contains(rowDto.ProjectCD.Substring(0, 1))) {
                        c.ReadOnly = true;
                        c.Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    }
                }
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<MstProjectGridDto> gridRowDtoList) {

            List<MstProjectGridDto> allGridDtoList = new List<MstProjectGridDto>();
            foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // グリッドセルプロジェクトコードチェック
            if (!ValidateProjectCD()) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(allGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // プロジェクトID
                headerTxt = _Body.dgvMstProject.Columns[nameof(rowDto.ProjectCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectCD), ref errColNames, ref result);

                // プロジェクト名
                headerTxt = _Body.dgvMstProject.Columns[nameof(rowDto.ProjectName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectName), ref errColNames, ref result);

                // 顧客名
                headerTxt = _Body.dgvMstProject.Columns[nameof(rowDto.KokyakuName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KokyakuName), ref errColNames, ref result);

                // プロジェクト部署区分
                headerTxt = _Body.dgvMstProject.Columns[nameof(rowDto.PBushoKbnName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.PBushoKbnName), ref errColNames, ref result);

                // 開始年月
                headerTxt = _Body.dgvMstProject.Columns[nameof(rowDto.StartYM_Disp)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.StartYM_Disp), ref errColNames, ref result);

            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// グリッドセルプロジェクトコードチェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateProjectCD() {

            foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // プロジェクトCDの桁数
                if (rowDto.ProjectCD.Length != ProjectConst._PROJECT_CODE_LENGTH) {
                    r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_WRONG_PROJECT_LENGTH);
                    return false;
                }
                // プロジェクトCDの先頭1桁
                if (!ProjectConst._DIC_GYOMU_KBN.ToList().Exists(n => n.Key== rowDto.ProjectCD.Substring(0, 1))) {
                    r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_WRONG_PROJECT_LEFT1);
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="allGridDtoList"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<MstProjectGridDto> allGridDtoList) {

            // 登録済みデータ取得
            var oldDatas = new PAF3030_MstProjectService().LoadMstProject(_DB_CONN_STRING, new LoadAggregateConDto(), new LoadProjectLooksConDto());
            string msg = string.Empty;
            bool resutlt = true;
            foreach (var gridDto in allGridDtoList) {
                if (ValidateDouple_Insert(oldDatas, gridDto) && ValidateDouple_Updatet(allGridDtoList, gridDto)) {
                    continue;
                }
                // 重複キー
                var dto = new MstProjectGridDto();
                msg = _Body.dgvMstProject.Columns[nameof(dto.ProjectCD)].HeaderText;

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvMstProject.Rows) {
                    var rowDto = GridRowUtil<MstProjectGridDto>.GetRowModel(r);
                    if (rowDto.ProjectCD == gridDto.ProjectCD) {

                        r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt) {
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Insert(List<MstProjectGridDto> oldDatas, MstProjectGridDto keyInfo) {
            if (keyInfo.Status == Enums.EditStatus.INSERT && oldDatas.Exists(n => n.ProjectCD == keyInfo.ProjectCD)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Updatet(List<MstProjectGridDto> keyInfos, MstProjectGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.ProjectCD == keyInfo.ProjectCD).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 検索条件
            InitSearchProjectLooksCondition();

            // グリッド
            _Body.dgvMstProject.Rows.Clear();
            DataGridViewComboBoxColumn cmb = (DataGridViewComboBoxColumn)_Body.dgvMstProject.Columns[nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)];
            cmb.Items.Clear();
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                cmb.Items.Add(pair.Value);
            }

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();          

            // フォーカス
            _Body.txtProjectCode.Focus();

        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);
            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length == 1) {
                    _Body.cmbGyomuKbn.Items.Add(pair.Key);
                }
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト部署区分コンボボックス初期化
        /// </summary>
        private void InitPBUshoKbnCombo() {
            _Body.cmbPBushoKbn.Items.Clear();
            _Body.cmbPBushoKbn.Items.Add(string.Empty);
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                _Body.cmbPBushoKbn.Items.Add(pair.Value);
            }
            _Body.cmbPBushoKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {
            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // プロジェクト名称
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // プロジェクト部署区分コンボボックス初期化
            InitPBUshoKbnCombo();

            // 依頼元ラジオボタン
            _Body.rdoNothing.Checked = true;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;
        }
        #endregion

    }
}
