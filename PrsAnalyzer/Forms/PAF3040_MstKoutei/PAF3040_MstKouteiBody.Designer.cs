﻿namespace PrsAnalyzer.Forms {
    partial class PAF3040_MstKouteiBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PAF3040_MstKouteiBody));
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvMstKoutei = new System.Windows.Forms.DataGridView();
            this.PBushoKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KouteiSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KokyakuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PBushoKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.KouteiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.cmbPBushoKbn = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlKokyakuRdo = new System.Windows.Forms.Panel();
            this.rdoFutokutei = new System.Windows.Forms.RadioButton();
            this.rdoSyanai = new System.Windows.Forms.RadioButton();
            this.rdoIraimoto = new System.Windows.Forms.RadioButton();
            this.rdoNothing = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSagyoName = new System.Windows.Forms.TextBox();
            this.cmbGyomuKbn = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtProjectCode = new System.Windows.Forms.TextBox();
            this.txtKokyakuName = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRowCopy = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstKoutei)).BeginInit();
            this.panel1.SuspendLayout();
            this.grpInput.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlKokyakuRdo.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvMstKoutei);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 122);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.panel3.Size = new System.Drawing.Size(1384, 624);
            this.panel3.TabIndex = 20;
            // 
            // dgvMstKoutei
            // 
            this.dgvMstKoutei.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMstKoutei.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMstKoutei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMstKoutei.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PBushoKbn,
            this.KouteiSeq,
            this.KokyakuName,
            this.ProjectCD,
            this.ProjectName,
            this.PBushoKbnName,
            this.KouteiName,
            this.Status});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMstKoutei.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMstKoutei.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMstKoutei.Location = new System.Drawing.Point(15, 15);
            this.dgvMstKoutei.Name = "dgvMstKoutei";
            this.dgvMstKoutei.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMstKoutei.RowTemplate.Height = 21;
            this.dgvMstKoutei.Size = new System.Drawing.Size(1354, 609);
            this.dgvMstKoutei.TabIndex = 20;
            // 
            // PBushoKbn
            // 
            this.PBushoKbn.DataPropertyName = "PBushoKbn";
            this.PBushoKbn.HeaderText = "プロジェクト区分（内部）";
            this.PBushoKbn.Name = "PBushoKbn";
            this.PBushoKbn.Visible = false;
            // 
            // KouteiSeq
            // 
            this.KouteiSeq.DataPropertyName = "KouteiSeq";
            this.KouteiSeq.HeaderText = "工程Seq";
            this.KouteiSeq.Name = "KouteiSeq";
            this.KouteiSeq.Visible = false;
            // 
            // KokyakuName
            // 
            this.KokyakuName.DataPropertyName = "KokyakuName";
            this.KokyakuName.HeaderText = "顧客名";
            this.KokyakuName.Name = "KokyakuName";
            this.KokyakuName.Width = 300;
            // 
            // ProjectCD
            // 
            this.ProjectCD.DataPropertyName = "ProjectCD";
            this.ProjectCD.HeaderText = "プロジェクトコード";
            this.ProjectCD.MaxInputLength = 8;
            this.ProjectCD.Name = "ProjectCD";
            this.ProjectCD.Width = 140;
            // 
            // ProjectName
            // 
            this.ProjectName.DataPropertyName = "ProjectName";
            this.ProjectName.HeaderText = "作業内容";
            this.ProjectName.MaxInputLength = 50;
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.Width = 350;
            // 
            // PBushoKbnName
            // 
            this.PBushoKbnName.DataPropertyName = "PBushoKbnName";
            this.PBushoKbnName.HeaderText = "プロジェクト部署区分";
            this.PBushoKbnName.Name = "PBushoKbnName";
            this.PBushoKbnName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PBushoKbnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PBushoKbnName.Width = 150;
            // 
            // KouteiName
            // 
            this.KouteiName.DataPropertyName = "KouteiName";
            this.KouteiName.HeaderText = "工程";
            this.KouteiName.Name = "KouteiName";
            this.KouteiName.Width = 120;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "編集ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpInput);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15, 10, 15, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 122);
            this.panel1.TabIndex = 0;
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.cmbPBushoKbn);
            this.grpInput.Controls.Add(this.panel2);
            this.grpInput.Controls.Add(this.label7);
            this.grpInput.Controls.Add(this.pnlKokyakuRdo);
            this.grpInput.Controls.Add(this.label3);
            this.grpInput.Controls.Add(this.label1);
            this.grpInput.Controls.Add(this.txtSagyoName);
            this.grpInput.Controls.Add(this.cmbGyomuKbn);
            this.grpInput.Controls.Add(this.label2);
            this.grpInput.Controls.Add(this.label4);
            this.grpInput.Controls.Add(this.txtProjectCode);
            this.grpInput.Controls.Add(this.txtKokyakuName);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInput.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpInput.Location = new System.Drawing.Point(15, 10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(1354, 112);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "条件";
            // 
            // cmbPBushoKbn
            // 
            this.cmbPBushoKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPBushoKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbPBushoKbn.FormattingEnabled = true;
            this.cmbPBushoKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbPBushoKbn.ItemHeight = 17;
            this.cmbPBushoKbn.Location = new System.Drawing.Point(603, 20);
            this.cmbPBushoKbn.Name = "cmbPBushoKbn";
            this.cmbPBushoKbn.Size = new System.Drawing.Size(151, 25);
            this.cmbPBushoKbn.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1202, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(149, 89);
            this.panel2.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(17, 48);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 35);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(481, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 17);
            this.label7.TabIndex = 84;
            this.label7.Text = "プロジェクト部署区分";
            // 
            // pnlKokyakuRdo
            // 
            this.pnlKokyakuRdo.Controls.Add(this.rdoFutokutei);
            this.pnlKokyakuRdo.Controls.Add(this.rdoSyanai);
            this.pnlKokyakuRdo.Controls.Add(this.rdoIraimoto);
            this.pnlKokyakuRdo.Controls.Add(this.rdoNothing);
            this.pnlKokyakuRdo.Location = new System.Drawing.Point(603, 48);
            this.pnlKokyakuRdo.Name = "pnlKokyakuRdo";
            this.pnlKokyakuRdo.Size = new System.Drawing.Size(300, 24);
            this.pnlKokyakuRdo.TabIndex = 4;
            // 
            // rdoFutokutei
            // 
            this.rdoFutokutei.AutoSize = true;
            this.rdoFutokutei.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoFutokutei.Location = new System.Drawing.Point(189, 0);
            this.rdoFutokutei.Name = "rdoFutokutei";
            this.rdoFutokutei.Size = new System.Drawing.Size(91, 24);
            this.rdoFutokutei.TabIndex = 3;
            this.rdoFutokutei.TabStop = true;
            this.rdoFutokutei.Text = "不特定客先";
            this.rdoFutokutei.UseVisualStyleBackColor = true;
            // 
            // rdoSyanai
            // 
            this.rdoSyanai.AutoSize = true;
            this.rdoSyanai.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSyanai.Location = new System.Drawing.Point(137, 0);
            this.rdoSyanai.Name = "rdoSyanai";
            this.rdoSyanai.Size = new System.Drawing.Size(52, 24);
            this.rdoSyanai.TabIndex = 2;
            this.rdoSyanai.TabStop = true;
            this.rdoSyanai.Text = "社内";
            this.rdoSyanai.UseVisualStyleBackColor = true;
            // 
            // rdoIraimoto
            // 
            this.rdoIraimoto.AutoSize = true;
            this.rdoIraimoto.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoIraimoto.Location = new System.Drawing.Point(72, 0);
            this.rdoIraimoto.Name = "rdoIraimoto";
            this.rdoIraimoto.Size = new System.Drawing.Size(65, 24);
            this.rdoIraimoto.TabIndex = 1;
            this.rdoIraimoto.TabStop = true;
            this.rdoIraimoto.Text = "依頼元";
            this.rdoIraimoto.UseVisualStyleBackColor = true;
            // 
            // rdoNothing
            // 
            this.rdoNothing.AutoSize = true;
            this.rdoNothing.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoNothing.Location = new System.Drawing.Point(0, 0);
            this.rdoNothing.Name = "rdoNothing";
            this.rdoNothing.Size = new System.Drawing.Size(72, 24);
            this.rdoNothing.TabIndex = 0;
            this.rdoNothing.TabStop = true;
            this.rdoNothing.Text = "選択なし";
            this.rdoNothing.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(22, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 83;
            this.label3.Text = "作業内容";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(22, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 80;
            this.label1.Text = "業務区分";
            // 
            // txtSagyoName
            // 
            this.txtSagyoName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSagyoName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSagyoName.Location = new System.Drawing.Point(144, 48);
            this.txtSagyoName.MaxLength = 50;
            this.txtSagyoName.Name = "txtSagyoName";
            this.txtSagyoName.Size = new System.Drawing.Size(300, 24);
            this.txtSagyoName.TabIndex = 1;
            // 
            // cmbGyomuKbn
            // 
            this.cmbGyomuKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGyomuKbn.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbGyomuKbn.FormattingEnabled = true;
            this.cmbGyomuKbn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cmbGyomuKbn.Location = new System.Drawing.Point(144, 73);
            this.cmbGyomuKbn.Name = "cmbGyomuKbn";
            this.cmbGyomuKbn.Size = new System.Drawing.Size(82, 25);
            this.cmbGyomuKbn.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(22, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 82;
            this.label2.Text = "プロジェクトコード";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(481, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 81;
            this.label4.Text = "顧客名";
            // 
            // txtProjectCode
            // 
            this.txtProjectCode.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtProjectCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProjectCode.Location = new System.Drawing.Point(144, 23);
            this.txtProjectCode.MaxLength = 8;
            this.txtProjectCode.Name = "txtProjectCode";
            this.txtProjectCode.Size = new System.Drawing.Size(82, 24);
            this.txtProjectCode.TabIndex = 0;
            // 
            // txtKokyakuName
            // 
            this.txtKokyakuName.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKokyakuName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKokyakuName.Location = new System.Drawing.Point(603, 73);
            this.txtKokyakuName.MaxLength = 100;
            this.txtKokyakuName.Name = "txtKokyakuName";
            this.txtKokyakuName.Size = new System.Drawing.Size(300, 24);
            this.txtKokyakuName.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 16);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 35);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(267, 16);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 35);
            this.btnUpdate.TabIndex = 60;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnRowCopy
            // 
            this.btnRowCopy.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRowCopy.Location = new System.Drawing.Point(141, 16);
            this.btnRowCopy.Name = "btnRowCopy";
            this.btnRowCopy.Size = new System.Drawing.Size(120, 35);
            this.btnRowCopy.TabIndex = 57;
            this.btnRowCopy.Text = "行コピー（C）";
            this.btnRowCopy.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1188, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(196, 65);
            this.panel4.TabIndex = 100;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(61, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 35);
            this.btnClose.TabIndex = 102;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel4);
            this.pnlFootor.Controls.Add(this.btnRowCopy);
            this.pnlFootor.Controls.Add(this.btnUpdate);
            this.pnlFootor.Controls.Add(this.btnClear);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 746);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(1384, 65);
            this.pnlFootor.TabIndex = 50;
            // 
            // PAF12000_MstKouteiBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlFootor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 600);
            this.Name = "PAF12000_MstKouteiBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "工程";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstKoutei)).EndInit();
            this.panel1.ResumeLayout(false);
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlKokyakuRdo.ResumeLayout(false);
            this.pnlKokyakuRdo.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.pnlFootor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.DataGridView dgvMstKoutei;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnRowCopy;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel pnlFootor;
        private System.Windows.Forms.DataGridViewTextBoxColumn PBushoKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn KokyakuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectName;
        private System.Windows.Forms.DataGridViewComboBoxColumn PBushoKbnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KouteiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        public System.Windows.Forms.ComboBox cmbPBushoKbn;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Panel pnlKokyakuRdo;
        public System.Windows.Forms.RadioButton rdoFutokutei;
        public System.Windows.Forms.RadioButton rdoSyanai;
        public System.Windows.Forms.RadioButton rdoIraimoto;
        public System.Windows.Forms.RadioButton rdoNothing;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtSagyoName;
        public System.Windows.Forms.ComboBox cmbGyomuKbn;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtProjectCode;
        public System.Windows.Forms.TextBox txtKokyakuName;
    }
}

