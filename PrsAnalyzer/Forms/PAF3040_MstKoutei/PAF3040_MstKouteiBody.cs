﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF3040_MstKouteiBody : Form {
        public PAF3040_MstKouteiBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF3040_MstKouteiFormLogic(this, loginInfo);
        }
    }
}
