﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF3040_MstKouteiFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF3040_MstKouteiBody _Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo { get; set; }
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// Service（社員マスタ）
        /// </summary>
        private PAF3010_MstEmploeeService _MST_EMPLOEE_SERVICE = new PAF3010_MstEmploeeService();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static MstKouteiGridDto _CELL_HEADER_NAME_DTO = new MstKouteiGridDto();
        private string[] _EditableGridCells = new string[] { 
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.ProjectName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KouteiName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.Status) };

        private string[] _ForbidUpdateGridCells = new string[] {
                                                             nameof(_CELL_HEADER_NAME_DTO.ProjectCD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.ProjectName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KokyakuName)
                                                           , nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)};

        // 工程・詳細にプレフィックスするアンダーバー
        private const string _STR_UNDER_BAR = "_";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF3040_MstKouteiFormLogic(PAF3040_MstKouteiBody body, LoginInfoDto loginInfo) {
            _Body = body;
            LoginInfo = loginInfo;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpd_ClickEvent();
            };
            // 行コピーボタン押下
            _Body.btnRowCopy.Click += (s, e) => {
                BtnCopy_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // 依頼元選択変更
            _Body.rdoIraimoto.CheckedChanged += (s, e) => {
                _Body.txtKokyakuName.Visible = _Body.rdoIraimoto.Checked;
                _Body.txtKokyakuName.Text = string.Empty;
            };
            // グリッドCellEnter
            _Body.dgvMstKoutei.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstKoutei.RowPostPaint += (s, e) => {
                GridRowUtil<MstKouteiGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstKoutei.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.C:
                        _Body.btnRowCopy.Focus();
                        _Body.btnRowCopy.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                _Body.btnRowCopy.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
            _Body.btnRowCopy.Enabled = false;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstKoutei.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case nameof(_CELL_HEADER_NAME_DTO.KouteiName):
                    _Body.dgvMstKoutei.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstKoutei.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvMstKoutei.CellValueChanged -= GridCellValueChanged;

            var dto = new MstKouteiGridDto();
            string tgtCellName = _Body.dgvMstKoutei.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvMstKoutei.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                _Body.dgvMstKoutei.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string taskId = StringUtil.TostringNullForbid(_Body.dgvMstKoutei.Rows[e.RowIndex].Cells[nameof(dto.ProjectCD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvMstKoutei.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(taskId) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvMstKoutei.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvMstKoutei.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstKoutei.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        private bool EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex) {
            if (tgtCellVal == null){
                return false;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {
                case nameof(_CELL_HEADER_NAME_DTO.ProjectCD):
                    string newInputVal = GetGridCellProjectCode(inputVal);
                    _Body.dgvMstKoutei.Rows[RowIndex].Cells[tgtCellName].Value = newInputVal;
                    if (string.IsNullOrEmpty(GetGridCellProjectCode(newInputVal))) {
                        return false;
                    }
                    // プロジェクトマスタ検索
                    var projectMst = new MstCommonService().LoadMstProject(_DB_CONN_STRING, newInputVal).FirstOrDefault();
                    if (projectMst != null) {
                        var tgtpBushoKbn = ProjectConst._DIC_PROJECT_KBN.First(n => (int)n.Key == projectMst.PBushoKbn);
                        _Body.dgvMstKoutei.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KokyakuName)].Value = projectMst.KokyakuName;
                        _Body.dgvMstKoutei.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.ProjectName)].Value = projectMst.ProjectName;
                        _Body.dgvMstKoutei.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.PBushoKbn)].Value = ((int)tgtpBushoKbn.Key).ToString();
                        _Body.dgvMstKoutei.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)].Value = tgtpBushoKbn.Value;
                        _Body.dgvMstKoutei.Rows[RowIndex].Cells[nameof(_CELL_HEADER_NAME_DTO.KouteiSeq)].Value = (GetMaxKouteiSeq(projectMst.ProjectCD) + 1).ToString();
                    }
                    return true;

                default:
                    // 入力値を編集→セット
                    _Body.dgvMstKoutei.Rows[RowIndex].Cells[tgtCellName].Value = inputVal;
                    return true;
            }
        }
        /// <summary>
        /// グリッドセルの編集値取得（プロジェクトコード）
        /// </summary>
        /// <param name="inputVal"></param>
        /// <returns></returns>
        private string GetGridCellProjectCode(string inputVal) {
            if (ProjectConst._PROJECT_CODE_LENGTH < inputVal.Length) {
                inputVal = inputVal.Substring(0, ProjectConst._PROJECT_CODE_LENGTH);
            }
            string newInputVal = string.Empty;
            foreach (var c in inputVal) {
                if (inputVal.IndexOf(c) == 0) {
                    newInputVal += c;
                    continue;
                }
                if (int.TryParse(c.ToString(), out int i)) {
                    newInputVal += c;
                }
            }
            return newInputVal;
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void BtnSearch_ClickEvent(bool isShowNotExistMessage = true) {

            try {
                // グリッド初期化
                _Body.dgvMstKoutei.Rows.Clear();

                // パラメータDto1取得
                var pBushoKbn = ProjectConst.PBushoKbn.NONE;
                if (ProjectConst._DIC_PROJECT_KBN.ToList().Exists(n => n.Value == _Body.cmbPBushoKbn.Text)) {
                    pBushoKbn = ProjectConst._DIC_PROJECT_KBN.ToList().First(n => n.Value == _Body.cmbPBushoKbn.Text).Key;
                }
                var con1 = new LoadAggregateConDto() {
                    PBushoKbn = pBushoKbn,
                };
                var kokyakuKbn = ProjectUtil.GetKokyakuKbn(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked);
                string kokyakuName = ProjectUtil.GetKokyakuNameForSearch(_Body.rdoIraimoto.Checked, _Body.rdoSyanai.Checked, _Body.rdoFutokutei.Checked, _Body.txtKokyakuName.Text);
                var con2 = new LoadProjectLooksConDto() {
                    ProjectCD = _Body.txtProjectCode.Text,
                    ProjectName = _Body.txtSagyoName.Text,
                    GyomuKbn = _Body.cmbGyomuKbn.Text,
                    KokyakuKbn = kokyakuKbn,
                    KokyakuName = kokyakuName,
                };
                var datas = new PAF3040_MstKouteiService().LoadMstKoutei(_DB_CONN_STRING, con1, con2);

                // グリッドオブジェクトを作成
                var gridData = new BindingList<MstKouteiGridDto>();
                foreach (var data in datas) {
                    gridData.Add(data);
                }
                if (!gridData.Any() && isShowNotExistMessage) {
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                _Body.dgvMstKoutei.DataSource = gridData;

                // グリッドセル背景色をセット
                SetColor_GridCsll();

                _Body.dgvMstKoutei.Show();
                _Body.dgvMstKoutei.Focus();

                // 使用可否切り替え（更新ボタン）
                SwitchEnable_BtnUpdate();
                
                // フォーカス
                _Body.dgvMstKoutei.Focus();
            }
            catch (Exception ex) {
                LogUtil.ShowLogErr(ex);
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 行コピーボタン押下イベント
        /// </summary>
        private void BtnCopy_ClickEvent() {
            if (_Body.dgvMstKoutei.SelectedRows == null || _Body.dgvMstKoutei.SelectedRows.Count == 0) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_SELECTED_ROW_NOTHING);
                return;
            }
            var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(_Body.dgvMstKoutei.SelectedRows[0]);
            if (rowDto.Status != Enums.EditStatus.SHOW) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_SELECTED_ROW_NOTHING);
                return;
            }
            // 追加行作成
            rowDto.KouteiSeq = (GetMaxKouteiSeq(rowDto.ProjectCD) + 1).ToString();
            rowDto.KouteiName = string.Empty;
            rowDto.Status = Enums.EditStatus.INSERT;
            BindingList<MstKouteiGridDto> datas = (BindingList<MstKouteiGridDto>)_Body.dgvMstKoutei.DataSource;
            datas.Add(rowDto);
            _Body.dgvMstKoutei.DataSource = datas;

            // グリッド背景色をセット
            DataGridViewRow r = _Body.dgvMstKoutei.Rows[datas.Count - 1];
            var dto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
            foreach (DataGridViewCell c in r.Cells) {
                // 更新不可能列の編集
                if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name)) {

                    ControlUtil.SetTimeTrackerCellStyle(dto.ProjectCD.Substring(0, 1), c);
                    c.ReadOnly = true;
                }
                if (rowDto.Status == Enums.EditStatus.INSERT) {
                    if (c.OwningColumn.Name == nameof(_CELL_HEADER_NAME_DTO.KouteiName)) {
                        c.Style.BackColor = Colors._BACK_COLOR_CELL_EDITED;
                    }
                }
            }
        }
        /// <summary>
        /// 最大の工程seqを取得
        /// </summary>
        /// <param name="ProjectCD"></param>
        /// <returns></returns>
        private int GetMaxKouteiSeq(string ProjectCD) {
            var list = new List<MstKouteiGridDto>();
            foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                list.Add(rowDto);
            }
            int ret = list.Where(n => n.ProjectCD == ProjectCD).ToList().Max(n => TypeConversionUtil.ToInteger(n.KouteiSeq));
            var maxSeqFromDb = new PAF3040_MstKouteiService().GetMaxKouteiSeq(_DB_CONN_STRING, ProjectCD);
            if (ret < maxSeqFromDb) {
                ret = maxSeqFromDb;
            }
            return ret;
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpd_ClickEvent() {

            var gridRowDtoList = new List<MstKouteiGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridRowDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            string gyomuKbn = _Body.cmbGyomuKbn.Text;
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridRowDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <param name="strTgtYM"></param>
        private void AddUpdDatasIntoDB(List<MstKouteiGridDto> gridRowDtoList) {

            try {
                // 更新用データ格納リスト
                var addDatas = new List<MX02KouteiEntity>();
                var updDatas = new List<MX02KouteiEntity>();

                // 検索データ絞り込み
                var service = new PAF3040_MstKouteiService();
                var oldDatas = service.LoadMstKoutei(_DB_CONN_STRING, new LoadAggregateConDto(),  new LoadProjectLooksConDto());

                // 登録用データ作成
                addDatas = CreateAddDatas(gridRowDtoList);

                // 登録処理
                addDatas.ForEach(n => service.AddMstKouteiProxy(_DB_CONN_STRING, n));

                // 更新用データ作成
                updDatas = CreateUpdDatas(oldDatas, gridRowDtoList);

                // 更新処理
                updDatas.ForEach(n => service.UpdKouteiMst(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX02KouteiEntity> CreateAddDatas(List<MstKouteiGridDto> gridDtoList) {

            var ret = new List<MX02KouteiEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.INSERT).ToList()) {

                // データ作成
                var entity = new MX02KouteiEntity() {
                    ProjectCD = StringUtil.TostringNullForbid(rowDto.ProjectCD),
                    KouteiSeq = GetMaxKouteiSeq(rowDto.ProjectCD) + 1,
                    KouteiName = StringUtil.TostringNullForbid(rowDto.KouteiName),
                    AddDate = dtNow,
                    UpdDate = dtNow,
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX02KouteiEntity> CreateUpdDatas(List<MstKouteiGridDto> oldDatas, List<MstKouteiGridDto> gridDtoList) {

            var ret = new List<MX02KouteiEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.UPDATE).ToList()) {

                // データ作成
                var entity = new MX02KouteiEntity() {
                    ProjectCD = StringUtil.TostringNullForbid(rowDto.ProjectCD),
                    KouteiSeq = TypeConversionUtil.ToInteger(rowDto.KouteiSeq),
                    KouteiName = StringUtil.TostringNullForbid(rowDto.KouteiName),
                };
                var tgtEntity = oldDatas.FirstOrDefault(n => n.ProjectCD == entity.ProjectCD && TypeConversionUtil.ToInteger(n.KouteiSeq) == entity.KouteiSeq);
                if (tgtEntity != null) {
                    var addEntity = new MX02KouteiEntity();
                    addEntity.ProjectCD = StringUtil.TostringNullForbid(tgtEntity.ProjectCD);
                    addEntity.KouteiSeq = TypeConversionUtil.ToInteger(tgtEntity.KouteiSeq);
                    addEntity.KouteiName = StringUtil.TostringNullForbid(entity.KouteiName);
                    addEntity.UpdDate = dtNow;
                    ret.Add(addEntity);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                if (string.IsNullOrEmpty(rowDto.ProjectCD)) {
                    r.Cells[nameof(rowDto.KokyakuName)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[nameof(rowDto.KokyakuName)].ReadOnly = true;
                    r.Cells[nameof(rowDto.ProjectName)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[nameof(rowDto.ProjectName)].ReadOnly = true;
                    r.Cells[nameof(rowDto.PBushoKbnName)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[nameof(rowDto.PBushoKbnName)].ReadOnly = true;
                    continue;
                }
                foreach (DataGridViewCell c in r.Cells) {
                    // 更新不可能列の編集
                    if (_ForbidUpdateGridCells.Contains(c.OwningColumn.Name))　{

                        ControlUtil.SetTimeTrackerCellStyle(rowDto.ProjectCD.Substring(0, 1), c);
                        c.ReadOnly = true;
                    }
                }
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<MstKouteiGridDto> gridRowDtoList) {

            List<MstKouteiGridDto> allGridDtoList = new List<MstKouteiGridDto>();
            foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(allGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                
                var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // プロジェクトCD
                headerTxt = _Body.dgvMstKoutei.Columns[nameof(rowDto.ProjectCD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ProjectCD), ref errColNames, ref result);

                if (string.IsNullOrEmpty(rowDto.ProjectName) || string.IsNullOrEmpty(rowDto.KokyakuName)) {
                    r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                    if (!errColNames.Contains(headerTxt)) {
                        errColNames.Add(headerTxt);
                    }
                    result = false;
                }
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MstKouteiGridDto> allGridDtoList) {

            // 検索データ絞り込み
            var oldDatas = new PAF3040_MstKouteiService().LoadMstKoutei(_DB_CONN_STRING, new LoadAggregateConDto(), new LoadProjectLooksConDto());

            string msg = string.Empty;
            bool resutlt = true;
            foreach (var gridDto in allGridDtoList) {
                if (ValidateDouple_Insert(oldDatas, gridDto) && ValidateDouple_Updatet(allGridDtoList, gridDto)) {
                    continue;
                }
                // 重複キー
                var dto = new MstKouteiGridDto();
                msg = _Body.dgvMstKoutei.Columns[nameof(dto.ProjectCD)].HeaderText;
                msg = string.Concat(msg, ",", _Body.dgvMstKoutei.Columns[nameof(dto.KouteiName)].HeaderText);

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvMstKoutei.Rows) {
                    var rowDto = GridRowUtil<MstKouteiGridDto>.GetRowModel(r);
                    if (rowDto.ProjectCD == gridDto.ProjectCD && rowDto.KouteiSeq == gridDto.KouteiSeq) {
                        r.Cells[nameof(rowDto.ProjectCD)].Style.BackColor = Color.Red;
                        r.Cells[nameof(rowDto.KouteiName)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt) {
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Insert(List<MstKouteiGridDto> oldDatas, MstKouteiGridDto keyInfo) {
            if (keyInfo.Status == Enums.EditStatus.INSERT
                    && oldDatas.Exists(n => n.ProjectCD == keyInfo.ProjectCD
                                         && n.KouteiSeq.ToString() == keyInfo.KouteiSeq
                                         )) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Updatet(List<MstKouteiGridDto> keyInfos, MstKouteiGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.ProjectCD == keyInfo.ProjectCD
                                     && n.KouteiSeq == keyInfo.KouteiSeq
                                     ).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 検索条件
            InitSearchProjectLooksCondition();

            // グリッド
            _Body.dgvMstKoutei.Rows.Clear();
            DataGridViewComboBoxColumn cmb = (DataGridViewComboBoxColumn)_Body.dgvMstKoutei.Columns[nameof(_CELL_HEADER_NAME_DTO.PBushoKbnName)];
            cmb.Items.Clear();
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                cmb.Items.Add(pair.Value);
            }
            SetColor_GridCsll();

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();        

            // フォーカス
            _Body.txtProjectCode.Focus();

        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);

        }
        /// <summary>
        /// 業務区分コンボボックス初期化
        /// </summary>
        private void InitGyomuKbnCombo() {

            // 初期化
            _Body.cmbGyomuKbn.Items.Clear();
            _Body.cmbGyomuKbn.Items.Add(string.Empty);

            foreach (var pair in Colors._DIC_TIME_TRACKER_COLOR) {
                if (pair.Key.Length == 1) {
                    _Body.cmbGyomuKbn.Items.Add(pair.Key);
                }
            }
            _Body.cmbGyomuKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト部署区分コンボボックス初期化
        /// </summary>
        private void InitPBUshoKbnCombo() {
            _Body.cmbPBushoKbn.Items.Clear();
            _Body.cmbPBushoKbn.Items.Add(string.Empty);
            foreach (var pair in ProjectConst._DIC_PROJECT_KBN) {
                _Body.cmbPBushoKbn.Items.Add(pair.Value);
            }
            _Body.cmbPBushoKbn.SelectedIndex = 0;
        }
        /// <summary>
        /// プロジェクト検索条件初期化
        /// </summary>
        private void InitSearchProjectLooksCondition() {
            // プロジェクトコード
            _Body.txtProjectCode.Text = string.Empty;

            // プロジェクト名称
            _Body.txtSagyoName.Text = string.Empty;

            // 業務区分コンボボックス初期化
            InitGyomuKbnCombo();

            // プロジェクト部署区分コンボボックス初期化
            InitPBUshoKbnCombo();

            // 依頼元ラジオボタン
            _Body.rdoNothing.Checked = true;

            // 顧客名
            _Body.rdoNothing.Checked = true;
            _Body.txtKokyakuName.Text = string.Empty;
            _Body.txtKokyakuName.Visible = false;
        }
        #endregion

    }
}
