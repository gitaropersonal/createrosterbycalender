﻿using System.Windows.Forms;
using PrsAnalyzer.Dto;

namespace PrsAnalyzer.Forms {
    public partial class PAF3050_MstKyugyoDateBody : Form {
        public PAF3050_MstKyugyoDateBody(LoginInfoDto loginInfo) {
            InitializeComponent();
            var fl = new PAF3050_MstKyugyoDateFormLogic(this, loginInfo);
        }
    }
}
