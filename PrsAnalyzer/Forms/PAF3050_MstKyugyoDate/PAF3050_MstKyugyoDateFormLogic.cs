﻿using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Forms {
    public class PAF3050_MstKyugyoDateFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private PAF3050_MstKyugyoDateBody _Body;
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private static MstKyugyoDateGridDto _CELL_HEADER_NAME_DTO = new MstKyugyoDateGridDto();
        private string[] _EditableGridCells = new string[] { nameof(_CELL_HEADER_NAME_DTO.KyugyoYMD)
                                                           , nameof(_CELL_HEADER_NAME_DTO.KyugyoName) };

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public PAF3050_MstKyugyoDateFormLogic(PAF3050_MstKyugyoDateBody body, LoginInfoDto loginInfo) {
            _Body = body;
            Init();

            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                BtnSearch_ClickEvent();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                BtnUpd_ClickEvent();
            };
            // 削除ボタン押下
            _Body.btnDelete.Click += (s, e) => {
                BtnDel_ClickEvent();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var drConfirm = DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_CLEAR);
                if (drConfirm == DialogResult.OK) {
                    Init();
                }
            };
            // テキストボックスValidated
            _Body.txtTaishoYear.Validated += (s, e) => {
                //LostFocus_KyugyoYMD();
            };
            // グリッドCellEnter
            _Body.dgvMstyugyoDate.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstyugyoDate.RowPostPaint += (s, e) => {
                GridRowUtil<MstKyugyoDateGridDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstyugyoDate.CellValueChanged += GridCellValueChanged;

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.D:
                        _Body.btnDelete.Focus();
                        _Body.btnDelete.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
        }

        #region イベント
        /// <summary>
        /// ロストフォーカス実行判定
        /// </summary>
        /// <returns></returns>
        private bool ExecuteLostFocus() {
            if (_Body.btnClose.Focused) {
                return false;
            }
            if (_Body.btnClear.Focused) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnable_BtnUpdate() {
            bool isYukoRowExist = false;
            foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.Rows) {
                var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.KyugyoYMD)) {
                    isYukoRowExist = true;
                    break;
                }
            }
            if (isYukoRowExist) {
                // 使用可能
                _Body.btnUpdate.Enabled = true;
                _Body.btnDelete.Enabled = true;
                return;
            }
            // 使用不可
            _Body.btnUpdate.Enabled = false;
            _Body.btnDelete.Enabled = false;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstyugyoDate.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case nameof(_CELL_HEADER_NAME_DTO.KyugyoName):
                    _Body.dgvMstyugyoDate.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstyugyoDate.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex < 0 || e.RowIndex < 0) {
                return;
            }
            // イベント一旦削除
            _Body.dgvMstyugyoDate.CellValueChanged -= GridCellValueChanged;

            var dto = new MstKyugyoDateGridDto();
            string tgtCellName = _Body.dgvMstyugyoDate.Columns[e.ColumnIndex].Name;
            var tgtCellVal = _Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;

            if (_EditableGridCells.Contains(tgtCellName)) {

                // グリッドCellValueChanged（編集可能セル）
                EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                _Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // ステータス更新
                string KyugyoYMD = StringUtil.TostringNullForbid(_Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[nameof(dto.KyugyoYMD)].Value);
                string status = StringUtil.TostringNullForbid(_Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(KyugyoYMD) && (status == Enums.EditStatus.SHOW.ToString() || status == Enums.EditStatus.UPDATE.ToString())) {
                    _Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.UPDATE;
                }
                else {
                    _Body.dgvMstyugyoDate.Rows[e.RowIndex].Cells[nameof(dto.Status)].Value = Enums.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstyugyoDate.CellValueChanged += GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();
        }
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="RowIndex"></param>
        private bool EditableCellValueChanged(object tgtCellVal, string tgtCellName, int RowIndex) {
            if (tgtCellVal == null){
                return false;
            }
            string inputVal = StringUtil.TostringNullForbid(tgtCellVal);
            switch (tgtCellName) {
                case nameof(_CELL_HEADER_NAME_DTO.KyugyoYMD):
                    string strKyugyoYMDVal = TypeConversionUtil.ConvertInputToStrYMD(inputVal);
                    if (string.IsNullOrEmpty(strKyugyoYMDVal)) {
                        _Body.dgvMstyugyoDate.Rows[RowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    }
                    _Body.dgvMstyugyoDate.Rows[RowIndex].Cells[tgtCellName].Value = strKyugyoYMDVal;
                    return true;

                default:
                    // 入力値を編集→セット
                    _Body.dgvMstyugyoDate.Rows[RowIndex].Cells[tgtCellName].Value = inputVal;
                    return true;
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="isShowNotExistMessage"></param>
        private void BtnSearch_ClickEvent(bool isShowNotExistMessage = true) {

            try {
                // グリッド初期化
                _Body.dgvMstyugyoDate.Rows.Clear();

                // パラメータDto1取得
                int? taishoYear = null;
                if (!string.IsNullOrEmpty(_Body.txtTaishoYear.Text)){
                    int year;
                    if (!int.TryParse(_Body.txtTaishoYear.Text, out year)) {
                        _Body.txtTaishoYear.Focus();
                        _Body.txtTaishoYear.BackColor = Colors._BACK_COLOR_ERROR;
                        DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_EMPTY, "対象年"));
                        return;
                    }
                    taishoYear = year;
                }
                var datas = new MstCommonService().LoadMstKyugyoDate(_DB_CONN_STRING, taishoYear);

                // グリッドオブジェクトを作成
                var gridData = new BindingList<MstKyugyoDateGridDto>();
                foreach (var data in datas) {
                    var dto = new MstKyugyoDateGridDto() {
                        KyugyoYMD = data.KyugyoYMD.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH),
                        KyugyoName = data.KyugyoName,
                        Status = Enums.EditStatus.SHOW,
                    };
                    gridData.Add(dto);
                }
                if (!gridData.Any() && isShowNotExistMessage) {
                    DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                _Body.dgvMstyugyoDate.DataSource = gridData;

                // グリッドセル背景色をセット
                SetColor_GridCsll();

                _Body.dgvMstyugyoDate.Show();
                _Body.dgvMstyugyoDate.Focus();

                // 使用可否切り替え（更新ボタン）
                SwitchEnable_BtnUpdate();
                
                // フォーカス
                _Body.dgvMstyugyoDate.Focus();
            }
            catch (Exception ex) {
                LogUtil.ShowLogErr(ex);
                DialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        private void BtnUpd_ClickEvent() {

            var gridRowDtoList = new List<MstKyugyoDateGridDto>();

            // 更新時Validate
            if (!UpdValidate(gridRowDtoList)) {
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => AddUpdDatasIntoDB(gridRowDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_UPDATE);
            
        }
        /// <summary>
        /// 削除ボタン押下イベント
        /// </summary>
        private void BtnDel_ClickEvent() {

            var gridRowDtoList = new List<MstKyugyoDateGridDto>();

            // Validate
            if (_Body.dgvMstyugyoDate.SelectedRows == null || _Body.dgvMstyugyoDate.SelectedRows.Count == 0) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_SELECTED_ROW_NOTHING);
                return;
            }
            foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.SelectedRows) {
                var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);
                if (string.IsNullOrEmpty(rowDto.KyugyoYMD)) {
                    continue;
                }
                if (rowDto.Status == Enums.EditStatus.INSERT) {
                    continue;
                }
                gridRowDtoList.Add(rowDto);
            }
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_SELECTED_ROW_NOTHING);
                return;
            }
            // 確認ダイアログ
            if (DialogUtil.ShowInfoMsgOKCancel(Messages._MSG_ASK_DELETE) != DialogResult.OK) {
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => DelDatasIntoDB(gridRowDtoList)));
                prgBar.ShowDialog();
                if (!prgBar.Success) {
                    return;
                }
            }
            // 再検索
            BtnSearch_ClickEvent(false);

            // 更新完了メッセージ
            DialogUtil.ShowInfolMsgOK(Messages._MSG_FINISH_DELETE);

        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <param name="strTgtYM"></param>
        private void AddUpdDatasIntoDB(List<MstKyugyoDateGridDto> gridRowDtoList) {

            try {
                // 更新用データ格納リスト
                var addDatas = new List<MX04KyugyoDateEntity>();
                var updDatas = new List<MX04KyugyoDateEntity>();

                // 検索データ絞り込み
                var oldDatas = new MstCommonService().LoadMstKyugyoDate(_DB_CONN_STRING, null);

                // 登録用データ作成
                addDatas = CreateAddDatas(gridRowDtoList);

                // 登録処理
                var service = new PAF3050_MstKyugyoDateService();
                addDatas.ForEach(n => service.InsertMstKyugyoDate(_DB_CONN_STRING, n));

                // 更新用データ作成
                updDatas = CreateUpdDatas(oldDatas, gridRowDtoList);

                // 更新処理
                updDatas.ForEach(n => service.UpdMstKyugyoDate(_DB_CONN_STRING, n));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// DBデータ削除
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        private void DelDatasIntoDB(List<MstKyugyoDateGridDto> gridRowDtoList) {

            try {
                // 削除
                foreach (var dto in gridRowDtoList) {
                    gridRowDtoList.ForEach(n => new PAF3050_MstKyugyoDateService().DelMstKyugyoDate(_DB_CONN_STRING, n.KyugyoYMD));
                }
            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX04KyugyoDateEntity> CreateAddDatas(List<MstKyugyoDateGridDto> gridDtoList) {

            var ret = new List<MX04KyugyoDateEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.INSERT).ToList()) {

                // データ作成
                var entity = new MX04KyugyoDateEntity() {
                    KyugyoYMD = TypeConversionUtil.ToDateTime(rowDto.KyugyoYMD),
                    KyugyoName = StringUtil.TostringNullForbid(rowDto.KyugyoName),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX04KyugyoDateEntity> CreateUpdDatas(List<MX04KyugyoDateEntity> oldDatas, List<MstKyugyoDateGridDto> gridDtoList) {

            var ret = new List<MX04KyugyoDateEntity>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => n.Status == Enums.EditStatus.UPDATE).ToList()) {

                // データ作成
                var tgtEntity = oldDatas.Where(n => n.KyugyoYMD.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH) == rowDto.KyugyoYMD);
                if (tgtEntity != null) {
                    var addEntity = new MX04KyugyoDateEntity();
                    addEntity.KyugyoYMD = TypeConversionUtil.ToDateTime(rowDto.KyugyoYMD);
                    addEntity.KyugyoName = StringUtil.TostringNullForbid(rowDto.KyugyoName);
                    ret.Add(addEntity);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色をセット
        /// </summary>
        private void SetColor_GridCsll() {
            foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.Rows) {
                var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);

                // 休業日コード
                if (!string.IsNullOrEmpty(rowDto.KyugyoYMD)) {
                    r.Cells[nameof(rowDto.KyugyoYMD)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                    r.Cells[nameof(rowDto.KyugyoYMD)].ReadOnly = true;
                    continue;
                }
                foreach (DataGridViewCell c in r.Cells) {
                    // 更新不可能列の編集
                    if (!string.IsNullOrEmpty(rowDto.KyugyoYMD)) {

                        r.Cells[nameof(rowDto.KyugyoYMD)].Style.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        r.Cells[nameof(rowDto.KyugyoYMD)].ReadOnly = true;
                    }
                }
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<MstKyugyoDateGridDto> gridRowDtoList) {

            List<MstKyugyoDateGridDto> allGridDtoList = new List<MstKyugyoDateGridDto>();
            foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.Rows) {
                var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.INSERT || rowDto.Status == Enums.EditStatus.UPDATE) {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            // 更新対象件数チェック
            if (!gridRowDtoList.Any()) {
                DialogUtil.ShowErroMsg(Messages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // グリッドセル空文字チェック
            if (!ValidateEmpty()) {
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(allGridDtoList)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty() {

            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.Rows) {
                
                var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.INSERT && rowDto.Status != Enums.EditStatus.UPDATE) {
                    continue;
                }
                // 休業日
                headerTxt = _Body.dgvMstyugyoDate.Columns[nameof(rowDto.KyugyoYMD)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KyugyoYMD), ref errColNames, ref result);

                // 休業日名
                headerTxt = _Body.dgvMstyugyoDate.Columns[nameof(rowDto.KyugyoName)].HeaderText;
                ControlUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.KyugyoName), ref errColNames, ref result);
            }
            if (!result) {
                string msg = string.Format(Messages._MSG_ERROR_EMPTY, string.Join("、", errColNames));
                DialogUtil.ShowErroMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MstKyugyoDateGridDto> keyInfos) {

            // 検索データ絞り込み
            var oldDatas = new MstCommonService().LoadMstKyugyoDate(_DB_CONN_STRING, null);

            string msg = string.Empty;
            bool resutlt = true;
            foreach (var keyInfo in keyInfos) {
                if (ValidateDouple_Insert(oldDatas, keyInfo) && ValidateDouple_Updatet(keyInfos, keyInfo)) {

                    continue;

                }
                // 重複キー
                var dto = new MstKyugyoDateGridDto();
                msg = _Body.dgvMstyugyoDate.Columns[nameof(dto.KyugyoYMD)].HeaderText;

                // 背景色編集
                foreach (DataGridViewRow r in _Body.dgvMstyugyoDate.Rows) {
                    var rowDto = GridRowUtil<MstKyugyoDateGridDto>.GetRowModel(r);
                    if (rowDto.KyugyoYMD == keyInfo.KyugyoYMD) {

                        r.Cells[nameof(rowDto.KyugyoYMD)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt) {
                DialogUtil.ShowErroMsg(string.Format(Messages._MSG_ERROR_DOUPLE, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Insert(List<MX04KyugyoDateEntity> oldDatas, MstKyugyoDateGridDto keyInfo) {
            if (keyInfo.Status == Enums.EditStatus.INSERT
                    && oldDatas.Exists(n => n.KyugyoYMD.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH) == keyInfo.KyugyoYMD)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDouple_Updatet(List<MstKyugyoDateGridDto> keyInfos, MstKyugyoDateGridDto keyInfo) {
            if (1 < keyInfos.Where(n => n.KyugyoYMD == keyInfo.KyugyoYMD).ToList().Count) {
                return false;
            }
            return true;
        }
        #endregion

        #region 初期化
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {

            // 検索条件
            _Body.txtTaishoYear.Text = DateTime.Now.Year.ToString();

            // グリッド
            _Body.dgvMstyugyoDate.Rows.Clear();

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // 背景色
            InitColor();       

            // フォーカス
            _Body.txtTaishoYear.Focus();

        }
        /// <summary>
        /// 背景色初期化
        /// </summary>
        private void InitColor() {

            // 使用可否切り替え（更新ボタン）
            SwitchEnable_BtnUpdate();

            // コントロール背景色の初期化
            ControlUtil.SetContorolsColor(_Body);
        }

        #endregion

    }
}
