﻿using OfficeOpenXml;
using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PrsAnalyzer.Logic.Excel
{
    public class AnkenExcelLogic {

        private int _MAX_ROUW_COUNT = 100;
        private const string _SHEET_NAEM_ANKEN_NO = "案件No";

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="fileName"></param>
        public List<AnkenExcelDto> Main(string fileName) {
            var xlsxFile = File.OpenRead(fileName);
            var dtoList = new List<AnkenExcelDto>();
            try {
                using (var package = new ExcelPackage(new FileInfo(fileName))) {

                    // 案件Excel読み取り
                    LoadAnkenExcel(package, dtoList);
                }
                
            } catch (Exception ex) {

                throw ex;

            } finally {

                xlsxFile.Close();
                
            }
            return dtoList;
        }
        /// <summary>
        /// 案件Excel読み取り
        /// </summary>
        /// <param name="package"></param>
        /// <param name="dtoList"></param>
        private void LoadAnkenExcel(ExcelPackage package, List<AnkenExcelDto> dtoList) {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var sheet = package.Workbook.Worksheets[_SHEET_NAEM_ANKEN_NO];

            for (int rowIdx = 2; rowIdx < _MAX_ROUW_COUNT; rowIdx++) {
                var dto = new AnkenExcelDto() {
                    Iraimoto    = StringUtil.TostringNullForbid(sheet.Cells["A" + rowIdx].Value),
                    ProjectCD   = StringUtil.TostringNullForbid(sheet.Cells["B" + rowIdx].Value),
                    KokyakuName = StringUtil.TostringNullForbid(sheet.Cells["C" + rowIdx].Value),
                    ProjectName = StringUtil.TostringNullForbid(sheet.Cells["D" + rowIdx].Value),
                    KouteiName  = StringUtil.TostringNullForbid(sheet.Cells["E" + rowIdx].Value),
                    Kousu       = TypeConversionUtil.ToFloat(StringUtil.TostringNullForbid(sheet.Cells["F" + rowIdx].Value)).ToString(Formats._FORMAT_FLOAT_3),
                    KenshuJoken = StringUtil.TostringNullForbid(sheet.Cells["G" + rowIdx].Value),
                    Biko = StringUtil.TostringNullForbid(sheet.Cells["H" + rowIdx].Value),
                };
                if (string.IsNullOrEmpty(dto.KouteiName) && string.IsNullOrEmpty(dto.KenshuJoken)) {
                    continue;
                }
                if ( string.IsNullOrEmpty(dto.ProjectCD)) {
                    dto.Iraimoto = dtoList[0].Iraimoto;
                    dto.ProjectCD = dtoList[0].ProjectCD;
                    dto.KokyakuName = dtoList[0].KokyakuName;
                    dto.ProjectName = dtoList[0].ProjectName;
                }
                dtoList.Add(dto);
            }
            // AP保守専用の修正
            var apHosyuList = dtoList.Where(n => n.ProjectCD == ProjectConst._PROJECT_CODE_AP_HOSYU).ToList();
            float E21TotalKousu = apHosyuList.Sum(n => TypeConversionUtil.ToFloat(n.Kousu));
            float E21EachKousu = E21TotalKousu / apHosyuList.Count;
            foreach (var sp in apHosyuList) {
                sp.Kousu = E21EachKousu.ToString(Formats._FORMAT_FLOAT_3);
            }
        }
        /// <summary>
        /// 案件№シートが存在するか？
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ValidateExistSheet(string fileName) {
            var xlsxFile = File.OpenRead(fileName);
            try {
                using (var package = new ExcelPackage(new FileInfo(fileName))) {

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var sheet = package.Workbook.Worksheets[_SHEET_NAEM_ANKEN_NO];
                    if (sheet == null) {
                        return false;
                    }
                }
            }
            catch {

                return false;

            }
            finally {

                xlsxFile.Close();

            }
            return true;
        }
    }
}
