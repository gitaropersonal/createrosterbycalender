﻿using OfficeOpenXml;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.IO;

namespace PrsAnalyzer.Logic.Excel
{
    public class HGyomuExcelLogic {

        private int _MAX_ROUW_COUNT = 500;
        private const string _SHEET_NAEM_HGYOMU_EXCEL = "個別Ｈコード";

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="fileName"></param>
        public List<HGyomuExcelDto> Main(string fileName) {
            var xlsxFile = File.OpenRead(fileName);
            var dtoList = new List<HGyomuExcelDto>();
            try {
                using (var package = new ExcelPackage(new FileInfo(fileName))) {

                    // H業務Excel読み取り
                    LoadHCodeExcel(package, dtoList);
                }
                
            } catch (Exception ex) {

                throw ex;

            } finally {

                xlsxFile.Close();
                
            }
            return dtoList;
        }
        /// <summary>
        /// H業務Excel読み取り
        /// </summary>
        /// <param name="package"></param>
        /// <param name="dtoList"></param>
        private void LoadHCodeExcel(ExcelPackage package, List<HGyomuExcelDto> dtoList) {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var sheet = package.Workbook.Worksheets[_SHEET_NAEM_HGYOMU_EXCEL];

            for (int rowIdx = 3; rowIdx < _MAX_ROUW_COUNT; rowIdx++) {
                var dto = new HGyomuExcelDto() {
                    ProjectCD   = StringUtil.TostringNullForbid(sheet.Cells["A" + rowIdx].Value),
                    HBushoName  = StringUtil.TostringNullForbid(sheet.Cells["B" + rowIdx].Value),
                    KokyakuName = StringUtil.TostringNullForbid(sheet.Cells["C" + rowIdx].Value),
                    SagyoNaiyo  = StringUtil.TostringNullForbid(sheet.Cells["D" + rowIdx].Value),
                    STantoName  = StringUtil.TostringNullForbid(sheet.Cells["E" + rowIdx].Value),
                    ETantoName  = StringUtil.TostringNullForbid(sheet.Cells["F" + rowIdx].Value),
                };
                if (string.IsNullOrEmpty(dto.ProjectCD)) {
                    continue;
                }
                dtoList.Add(dto);
            }
        }
        /// <summary>
        /// H業務シートが存在するか？
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ValidateExistSheet(string fileName) {
            var xlsxFile = File.OpenRead(fileName);
            try {
                using (var package = new ExcelPackage(new FileInfo(fileName))) {

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var sheet = package.Workbook.Worksheets[_SHEET_NAEM_HGYOMU_EXCEL];
                    if (sheet == null) {
                        return false;
                    }
                }
            }
            catch {

                return false;

            }
            finally {

                xlsxFile.Close();

            }
            return true;
        }
        /// <summary>
        /// 年度が一致しているか？
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="YYYY"></param>
        /// <returns></returns>
        public bool ValidateIsMatchNendo(string fileName, int YYYY) {
            var xlsxFile = File.OpenRead(fileName);
            int tgtExcelNendo = 0;
            using (var package = new ExcelPackage(new FileInfo(fileName))) {

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[_SHEET_NAEM_HGYOMU_EXCEL];
                string strNendo = StringUtil.TostringNullForbid(sheet.Cells["A1"].Value);
                if (strNendo.Length < 4) {
                    return false;
                }
                tgtExcelNendo = TypeConversionUtil.ToInteger(strNendo.Substring(0, 4));
            }
            xlsxFile.Close();
            return tgtExcelNendo == YYYY;
        }
    }
}
