﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Const.Kinmuhyo;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PrsAnalyzer.Logic.Kinmuhyo {
    public class AnalysisCalenderCsvLogic {

        /// <summary>
        /// DB接続文字列
        /// </summary>
        private string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();
        /// <summary>
        /// チャージ№の開始マーク
        /// </summary>
        public const string _START_MARK_OF_CHARGE_NO = "＜";
        /// <summary>
        /// チャージ№の終了マーク
        /// </summary>
        public const string _END_MARK_OF_CHARGE_NO = "＞";

        /// <summary>
        /// 休日フラグ取得
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private TimeConst.RestType GetRestType(EditTimeEntryDto dto) {
            if (dto.ProjectCode.Substring(0, 1) != ProjectConst.GyomuKbn.X.ToString()) {
                return TimeConst.RestType.NONE;
            }
            if (dto.WorkEndTime.Hour <= TimeConst._DEFAULT_NOON_REST_START_HH) {
                return TimeConst.RestType.AM;
            }
            if (dto.TotalManHourDay < (decimal)(TimeConst._MINUTE_AN_HOUR * TimeConst._DEFAULT_WORK_TIME_PER_DAY)) {
                return TimeConst.RestType.PM;
            }
            if (dto.KouteiName == TimeConst.DicRestType.First(n => n.Key == TimeConst.RestType.PAID).Value) {
                return TimeConst.RestType.PAID;
            }
            if (dto.KouteiName == TimeConst.DicRestType.First(n => n.Key == TimeConst.RestType.FURIKAE).Value) {
                return TimeConst.RestType.FURIKAE;
            }
            if (dto.KouteiName == TimeConst.DicRestType.First(n => n.Key == TimeConst.RestType.OTHER).Value) {
                return TimeConst.RestType.OTHER;
            }
            return TimeConst.RestType.NONE;
        }
        /// <summary>
        /// CSVデータ解析Dto取得
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="emp"></param>
        /// <returns></returns>
        private EditTimeEntryDto GetCsvDatas(WorkItemEntity entity, MX03TantoEntity emp) {

            // 出退勤時刻
            var dtWorkDate = KinmuhyoUtil.ConvertYYYYMMDDToDt(entity.DtStart);
            var dtWorkStart = KinmuhyoUtil.ConvertYYYYMMDDHHmmToDt(entity.DtStart);
            if (dtWorkStart.Hour < TimeConst._PRS_WORKSTART_TIME_HH) {
                dtWorkDate = dtWorkDate.AddDays(-1);
            }
            var dtWorkEnd = KinmuhyoUtil.ConvertYYYYMMDDHHmmToDt(entity.DtEnd);

            // 作業時間
            var workTime = (decimal)(dtWorkEnd - dtWorkStart).TotalMinutes;

            // dto作成
            var csvData = new EditTimeEntryDto() {
                UserId = emp.TantoID,
                UserName = emp.TantoName,
                ProjectCode = entity.ProjectCode,
                ProjectName = entity.ProjectName,
                KouteiName = entity.KouteiName,
                Detail = entity.Detail,
                WorkDate = dtWorkDate.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH),
                TotalManHourSagyo = workTime,
                TotalManHourDay = workTime,
                ManHourH = (int)workTime / TimeConst._MINUTE_AN_HOUR,
                ManHourM = (int)workTime % TimeConst._MINUTE_AN_HOUR,
                WorkStartTime = dtWorkStart,
                WorkEndTime = dtWorkEnd,
            };
            return csvData;
        }
        /// <summary>
        /// 始業・終業時刻・実績工数・休憩時間を計算
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="dtList"></param>
        /// <param name="ret"></param>
        /// <returns></returns>
        private List<EditTimeEntryDto> CalcTimes(DateTime dt, List<string> dtList, List<EditTimeEntryDto> ret) {

            // 祝日リスト取得
            var holidayList = new MstCommonService().GetHolidays(_DB_CONN_STRING, dt.Year);

            dtList.ForEach(w => {
                var tgtItems = ret.Where(n => n.WorkDate == w).ToList();

                // 対象日
                var TaishoYMD = TypeConversionUtil.ToDateTime(w, Formats._FORMAT_DATE_YYYYMMDD_SLASH);

                // 実績工数
                decimal totalManHourDay = tgtItems.Sum(x => x.TotalManHourDay);

                // 休憩時間
                decimal restTime = (decimal)(tgtItems.Max(e => e.WorkEndTime) - tgtItems.Min(s => s.WorkStartTime)).TotalMinutes - totalManHourDay;

                // 祝日フラグ
                decimal zangyoTime = 0;
                decimal shinyaTime = 0;
                bool isHoliday = holidayList.ContainsKey(TaishoYMD);

                if (TaishoYMD.DayOfWeek != DayOfWeek.Sunday && TaishoYMD.DayOfWeek != DayOfWeek.Saturday && !isHoliday) {

                    // 残業
                    zangyoTime = totalManHourDay - (decimal)(TimeConst._DEFAULT_WORK_TIME_PER_DAY * TimeConst._MINUTE_AN_HOUR);
                    if (zangyoTime < 0) {
                        zangyoTime = 0;
                    }
                    // 深夜残業
                    DateTime zangyoStartTime = TaishoYMD.AddHours(TimeConst._NIGHT_RESTSTART_HH);
                    var shinyaItems = tgtItems.Where(s => zangyoStartTime < s.WorkEndTime).ToList();
                    if (0 < zangyoTime && shinyaItems.Any()) {
                        var shinyaKaishiDate = TaishoYMD.AddHours(TimeConst._NIGHT_RESTSTART_HH);
                        foreach (var shinya in shinyaItems) {
                            if (shinyaKaishiDate < shinya.WorkStartTime) {
                                shinyaTime += (decimal)(shinya.WorkEndTime - shinya.WorkStartTime).TotalMinutes;
                                continue;
                            }
                            shinyaTime += (decimal)(shinya.WorkEndTime - shinyaKaishiDate).TotalMinutes;
                        }
                        zangyoTime -= shinyaTime;
                    }
                }
                tgtItems.ForEach(x => {
                    // 休日フラグ取得
                    x.RestType = GetRestType(x);
                });
                tgtItems.ForEach(x => {
                    // 始業・終業時刻・実績工数・休憩時間を取得
                    x.RestType = tgtItems.Max(y => y.RestType);
                    x.WorkStartTime = tgtItems.Min(y => y.WorkStartTime);
                    x.WorkEndTime = tgtItems.Max(y => y.WorkEndTime);
                    x.TotalManHourDay = totalManHourDay;
                    x.RestTime = restTime;
                    x.ZangyoTime = zangyoTime;
                    x.ShinyaTime = shinyaTime;
                });
            });
            return ret;
        }

        #region PRS用
        /// <summary>
        /// CSVデータ解析（PRS用）
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="workItemList"></param>
        /// <param name="emp"></param>
        /// <param name="isContainX"></param>
        /// <returns></returns>
        public List<EditTimeEntryDto> EditWorkItemsPrs(DateTime dt,  List<WorkItemEntity> workItemList, MX03TantoEntity emp, bool isContainX = true) {
            var holidayList = new MstCommonService().GetHolidays(_DB_CONN_STRING, dt.Year);
            var ret = new List<EditTimeEntryDto>();
            var dtList = new List<string>();
            foreach (var data in workItemList) {

                if (!isContainX && data.ProjectCode.Substring(1, 1) == ProjectConst.GyomuKbn.X.ToString()) {
                    continue;
                }
                // 解析dto作成
                var csvData = GetCsvDatas(data, emp);

                // 初見の作業日なら追加
                if (!dtList.Contains(csvData.WorkDate)) {
                    dtList.Add(csvData.WorkDate);
                }
                // 初見の作業データであれば追加
                if (!ret.Exists(n => n.WorkDate == csvData.WorkDate && n.ProjectCode == csvData.ProjectCode && n.KouteiName == csvData.KouteiName & n.Detail == csvData.Detail)) {
                    ret.Add(csvData);
                    continue;
                }
                // 初見でない場合は再計算
                var tgtData = ReCalcTimesPrs(ret, csvData);
            }

            // 始業・終業時刻・実績工数・休憩時間
            ret = CalcTimes(dt, dtList, ret);
            return ret;
        }
        /// <summary>
        /// 始業・終業時刻・実績工数・休憩時間を再計算（PRS用）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="csvData"></param>
        /// <returns></returns>
        private EditTimeEntryDto ReCalcTimesPrs(List<EditTimeEntryDto> ret, EditTimeEntryDto csvData) {

            var tgtData = ret.First(n => n.WorkDate == csvData.WorkDate
                                      && n.ProjectCode == csvData.ProjectCode
                                      && n.KouteiName == csvData.KouteiName
                                      && n.Detail == csvData.Detail);
            // 作業時間
            tgtData.TotalManHourSagyo += csvData.TotalManHourSagyo;
            tgtData.TotalManHourDay += csvData.TotalManHourDay;
            tgtData.ManHourH = (int)tgtData.TotalManHourDay / TimeConst._MINUTE_AN_HOUR;
            tgtData.ManHourM = (int)tgtData.TotalManHourDay % TimeConst._MINUTE_AN_HOUR;

            // 出勤時刻
            if (csvData.WorkStartTime < tgtData.WorkStartTime) {
                tgtData.WorkStartTime = csvData.WorkStartTime;
            }
            // 退勤時刻
            if (tgtData.WorkEndTime < csvData.WorkEndTime) {
                tgtData.WorkEndTime = csvData.WorkEndTime;
            }
            // 休暇判定
            if (tgtData.ProjectName.Contains(CellsKinmuhyo._WORK_NAME_REST)
            && TimeConst._DEFAULT_WORK_TIME_PER_DAY <= tgtData.ManHourH) {
                tgtData.RestMark = CellsKinmuhyo._MARK_REST;
            }
            return tgtData;
        }
        #endregion

        #region 勤務表用
        /// <summary>
        /// CSVデータ解析（勤務表用）
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="workItemList"></param>
        /// <param name="emp"></param>
        /// <param name="isContainX"></param>
        /// <returns></returns>
        public List<EditTimeEntryDto> EditWorkItemsKinuhyo(DateTime dt, List<WorkItemEntity> workItemList, MX03TantoEntity emp, bool isContainX = true) {
            var holidayList = new MstCommonService().GetHolidays(_DB_CONN_STRING, dt.Year);
            var ret = new List<EditTimeEntryDto>();
            var dtList = new List<string>();
            foreach (var data in workItemList) {

                if (!isContainX && data.ProjectCode.Substring(1, 1) == ProjectConst.GyomuKbn.X.ToString()) {
                    continue;
                }
                // 解析dto作成
                var csvData = GetCsvDatas(data, emp);

                // 初見の作業日なら追加
                if (!dtList.Contains(csvData.WorkDate)) {
                    dtList.Add(csvData.WorkDate);
                }
                // 初見の作業データであれば追加
                if (!ret.Exists(n => n.WorkDate == csvData.WorkDate
                                  && n.ProjectCode == csvData.ProjectCode)){

                    ret.Add(csvData);
                    continue;

                }
                // 初見でない場合は再計算
                var tgtData = ReCalcTimesKinmuhyo(ret, csvData);
            }

            // 始業・終業時刻・実績工数・休憩時間
            ret = CalcTimes(dt, dtList, ret);
            return ret;
        }
        /// <summary>
        /// 始業・終業時刻・実績工数・休憩時間を再計算（勤務表用）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="csvData"></param>
        /// <returns></returns>
        private EditTimeEntryDto ReCalcTimesKinmuhyo(List<EditTimeEntryDto> ret, EditTimeEntryDto csvData) {

            var tgtData = ret.First(n => n.WorkDate == csvData.WorkDate && n.ProjectCode == csvData.ProjectCode);

            // 作業時間
            tgtData.TotalManHourSagyo += csvData.TotalManHourSagyo;
            tgtData.TotalManHourDay += csvData.TotalManHourDay;
            tgtData.ManHourH = (int)tgtData.TotalManHourDay / TimeConst._MINUTE_AN_HOUR;
            tgtData.ManHourM = (int)tgtData.TotalManHourDay % TimeConst._MINUTE_AN_HOUR;

            // 出勤時刻
            if (csvData.WorkStartTime < tgtData.WorkStartTime) {
                tgtData.WorkStartTime = csvData.WorkStartTime;
            }
            // 退勤時刻
            if (tgtData.WorkEndTime < csvData.WorkEndTime) {
                tgtData.WorkEndTime = csvData.WorkEndTime;
            }
            // 休暇判定
            if (tgtData.ProjectName.Contains(CellsKinmuhyo._WORK_NAME_REST)
            && TimeConst._DEFAULT_WORK_TIME_PER_DAY <= tgtData.ManHourH) {
                tgtData.RestMark = CellsKinmuhyo._MARK_REST;
            }
            return tgtData;
        }
        #endregion
    }
}
