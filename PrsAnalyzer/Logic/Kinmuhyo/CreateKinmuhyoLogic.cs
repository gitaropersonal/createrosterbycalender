﻿using OfficeOpenXml;
using PrsAnalyzer.Const;
using PrsAnalyzer.Const.Kinmuhyo;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Logic.Kinmuhyo {
    public class CreateKinmuhyoLogic {
        /// <summary>
        /// デフォルト勤務表パス
        /// </summary>
        private string _DEFAULT_KINMUHYO_PATH = Path.Combine(Environment.CurrentDirectory, "Template");
        /// <summary>
        /// 勤務表作成パス
        /// </summary>
        private string _OUTPUT_KINMUHYO_PATH = ConfigUtil.LoadOutputFolder();
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="tgtEmp"></param>
        /// <param name="tgtDate"></param>
        /// <returns></returns>
        public void Main(MX03TantoEntity tgtEmp, DateTime tgtDate) {

            try {
                LogUtil.ShowLogInfo(string.Format(Messages._MSG_OPERATION_START, tgtEmp.TantoName));

                // 勤務表出力フォルダ作成
                string kinmuhyoPath = Path.Combine(_OUTPUT_KINMUHYO_PATH, tgtDate.ToString(Formats._FORMAT_DATE_YYYYMM_NENTSUKI));
                CommonUtil.CreateFolder(kinmuhyoPath);

                // 勤務表パス
                string kinmuhyoName = KinmuhyoUtil.GetKinmuhyoName(tgtEmp.TantoName, tgtDate);
                string kinmuhyoFullPath = Path.Combine(kinmuhyoPath, kinmuhyoName);

                // 初期化
                CommonUtil.DeleteFile(kinmuhyoFullPath);
                string defaultKinmuhyoPath = Path.Combine(_DEFAULT_KINMUHYO_PATH, CellsKinmuhyo._DEFAULT_KINMUHYO_NAME);
                InitKinmuhyo(defaultKinmuhyoPath, kinmuhyoFullPath);

                // 作業実績取得
                var workItemList = new PAF1010_PrsEntryService().GetSagyoJisseki(_DB_CONN_STRING, tgtEmp.TantoID, tgtDate, tgtDate.AddMonths(1).AddDays(-1));

                // CSVデータ解析
                var csvDatas = new AnalysisCalenderCsvLogic().EditWorkItemsKinuhyo(tgtDate, workItemList, tgtEmp);

                // 勤怠時刻転記
                PostingWorkTimesProxy(csvDatas, kinmuhyoPath, kinmuhyoName, tgtEmp.TantoName, tgtDate);

                // 作業内容欄転記
                PostingWorkArea(kinmuhyoFullPath, kinmuhyoName, csvDatas);

                LogUtil.ShowLogInfo(string.Format(Messages._MSG_OPERATION_END, tgtEmp.TantoName));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
            finally {
                Cursor.Current = Cursor.Current;
            }
        }
        /// <summary>
        /// 勤務表初期化
        /// </summary>
        /// <param name="defaultKinmuhyoPath"></param>
        /// <param name="kinmuhyoFullPath"></param>
        public void InitKinmuhyo(string defaultKinmuhyoPath, string kinmuhyoFullPath) {

            string CopyKinmuhyoPath = $"{kinmuhyoFullPath}{CellsKinmuhyo._MARK_STAR}";
            File.Copy(defaultKinmuhyoPath, CopyKinmuhyoPath);

            var xlsxFile = File.OpenRead(CopyKinmuhyoPath);
            using (var package = new ExcelPackage(new FileInfo(CopyKinmuhyoPath))) {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                // 作業内容/一覧クリア
                var sheet = package.Workbook.Worksheets[CellsKinmuhyo._SHEET_KINMUHYO_NAME];
                ClearWorkArea(sheet);

                // 保存
                package.SaveAs(new FileInfo(kinmuhyoFullPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(CopyKinmuhyoPath);
        }

        #region 勤怠時刻
        /// <summary>
        /// 勤怠時刻転記（Proxy）
        /// </summary>
        /// <param name="csvDatas"></param>
        /// <param name="kinmuhyoPath"></param>
        /// <param name="kinmuhyoName"></param>
        /// <param name="emploeeName"></param>
        /// <param name="minTaishoDate"></param>
        public void PostingWorkTimesProxy(List<EditTimeEntryDto> csvDatas, string kinmuhyoPath, string kinmuhyoName, string emploeeName, DateTime minTaishoDate) {
            var wrokTimeDtoList = new List<WorkTimeDto>();
            csvDatas.ForEach(csvData => {

                // CSV解析データからWorkTimeDto取得
                var dto = KinmuhyoUtil.GetWorkTimeDtoFromCsvData(csvData);
                wrokTimeDtoList.Add(dto);

            });
            // 勤怠時刻転記
            string KinmuhyoPath = Path.Combine(kinmuhyoPath, kinmuhyoName);
            string tempPath = Path.Combine(kinmuhyoPath, CellsKinmuhyo._MARK_STAR + kinmuhyoName);
            if (File.Exists(KinmuhyoPath)) {
                PostingWorkTimes(emploeeName, KinmuhyoPath, tempPath, wrokTimeDtoList, minTaishoDate);
            }
        }
        /// <summary>
        /// 勤怠時刻転記
        /// </summary>
        /// <param name="emploeeName"></param>
        /// <param name="OriginXlsxPath"></param>
        /// <param name="tempPath"></param>
        /// <param name="dtoList"></param>
        /// <param name="minTaishoDate"></param>
        public void PostingWorkTimes(string emploeeName, string OriginXlsxPath, string tempPath, List<WorkTimeDto> dtoList, DateTime minTaishoDate) {
            var xlsxFile = File.OpenRead(OriginXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(OriginXlsxPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[CellsKinmuhyo._SHEET_KINMUHYO_NAME];

                // 名前
                sheet.Cells[CellsKinmuhyo._CELL_NAME_NAME].Value = emploeeName;

                // 対象年月
                sheet.Cells[CellsKinmuhyo._CELL_NAME_YYYY].Value = minTaishoDate.Year;
                sheet.Cells[CellsKinmuhyo._CELL_NAME_MM].Value = minTaishoDate.Month;

                // 祝日
                SetHoliday(sheet, minTaishoDate);

                // 勤怠時刻
                SetKintaiJikoku(sheet, dtoList);

                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(OriginXlsxPath);
            File.Copy(tempPath, OriginXlsxPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 祝日をセット
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="minTaishoDate"></param>
        private void SetHoliday(ExcelWorksheet sheet, DateTime minTaishoDate) {
            var holidayList = new MstCommonService().GetHolidays(_DB_CONN_STRING, minTaishoDate.Year);
            for (int i = 0; i < TimeConst._MAX_DATE_COUNT_AN_MONTH; i++) {
                int rowIdx = CellsKinmuhyo._SHEET_HEADER_ROW_NUM + i + 1;
                var currDate = minTaishoDate.AddDays(i);
                var cellDayOfWeek = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsKinmuhyo._COL_WEEK_DAY}{rowIdx}"].Value);
                if (holidayList.ContainsKey(currDate) && currDate.DayOfWeek != DayOfWeek.Saturday && currDate.DayOfWeek != DayOfWeek.Sunday) {

                    // 土日以外の祝日のみ"祝"を記載する
                    sheet.Cells[$"{CellsKinmuhyo._COL_HOLI_DAY}{rowIdx}"].Value = CellsKinmuhyo._HOLIDAY_MARK;
                    continue;
                }
                sheet.Cells[$"{CellsKinmuhyo._COL_HOLI_DAY}{rowIdx}"].Value = null;
            }
        }
        /// <summary>
        /// 勤怠時刻セット
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dtoList"></param>
        private void SetKintaiJikoku(ExcelWorksheet sheet, List<WorkTimeDto> dtoList) {
            dtoList.ForEach(dto => {
                int cellNum = dto.Day + CellsKinmuhyo._KINMUHYO_HEADER_ROW_COUNT;
                if (!string.IsNullOrEmpty(dto.StartHH)) {
                    sheet.Cells[$"{CellsKinmuhyo._COL_SHUKKIN_HH}{cellNum}"].Value = int.Parse(dto.StartHH);
                    sheet.Cells[$"{CellsKinmuhyo._COL_SHUKKIN_MM}{cellNum}"].Value = int.Parse(dto.StartMM);
                }
                if (!string.IsNullOrEmpty(dto.EndHH)) {
                    sheet.Cells[$"{CellsKinmuhyo._COL_TAIKIN_HH}{cellNum}"].Value = int.Parse(dto.EndHH);
                    sheet.Cells[$"{CellsKinmuhyo._COL_TAIKIN_MM}{cellNum}"].Value = int.Parse(dto.EndMM);
                }
                if (!string.IsNullOrEmpty(dto.RestHH)) {
                    sheet.Cells[$"{CellsKinmuhyo._COL_KYUKEI_HH}{cellNum}"].Value = int.Parse(dto.RestHH);
                    sheet.Cells[$"{CellsKinmuhyo._COL_KYUKEI_MM}{cellNum}"].Value = int.Parse(dto.RestMM);
                }
                if (!string.IsNullOrEmpty(dto.ZangyoHH)) {
                    sheet.Cells[$"{CellsKinmuhyo._COL_ZANGYO_HH}{cellNum}"].Value = int.Parse(dto.ZangyoHH);
                    sheet.Cells[$"{CellsKinmuhyo._COL_ZANGYO_MM}{cellNum}"].Value = int.Parse(dto.ZangyoMM);
                }
                if (!string.IsNullOrEmpty(dto.ShinyaHH)) {
                    sheet.Cells[$"{CellsKinmuhyo._COL_SHINYA_HH}{cellNum}"].Value = int.Parse(dto.ShinyaHH);
                    sheet.Cells[$"{CellsKinmuhyo._COL_SHINYA_MM}{cellNum}"].Value = int.Parse(dto.ShinyaMM);
                }
                switch (dto.RestType) {
                    case TimeConst.RestType.PAID:
                        sheet.Cells[$"{CellsKinmuhyo._COL_WORK_STATUS}{cellNum}"].Value = CellsKinmuhyo._MARK_PAID;
                        break;
                    case TimeConst.RestType.AM:
                        sheet.Cells[$"{CellsKinmuhyo._COL_AM_REST}{ cellNum}"].Value = CellsKinmuhyo._MARK_REST;
                        break;
                    case TimeConst.RestType.PM:
                        sheet.Cells[$"{CellsKinmuhyo._COL_PM_REST}{cellNum}"].Value = CellsKinmuhyo._MARK_REST;
                        break;
                    case TimeConst.RestType.FURIKAE:
                        sheet.Cells[$"{CellsKinmuhyo._COL_WORK_STATUS}{cellNum}"].Value = CellsKinmuhyo._MARK_FURIKAE;
                        break;
                }
            });
        }
        #endregion

        #region 作業内容
        /// <summary>
        /// 作業内容/一覧転記
        /// </summary>
        /// <param name="kinmuhyoFullPath"></param>
        /// <param name="kinmuhyoName"></param>
        /// <param name="csvDatas"></param>
        public void PostingWorkArea(string kinmuhyoFullPath, string kinmuhyoName, List<EditTimeEntryDto> csvDatas) {
            if (csvDatas == null || !csvDatas.Any()) {
                return;
            }
            string tempPath = Path.Combine(Environment.CurrentDirectory, CellsKinmuhyo._MARK_STAR + kinmuhyoName);
            var xlsxFile = File.OpenRead(kinmuhyoFullPath);
            using (var package = new ExcelPackage(new FileInfo(kinmuhyoFullPath))) {

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[CellsKinmuhyo._SHEET_KINMUHYO_NAME];

                // 作業内容
                CreateWorkDetails(sheet, csvDatas);

                // 作業マスタ
                CreateWorkMaster(sheet, csvDatas);

                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(kinmuhyoFullPath);
            File.Copy(tempPath, kinmuhyoFullPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 作業内容欄作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="csvDatas"></param>
        public void CreateWorkDetails(ExcelWorksheet sheet, List<EditTimeEntryDto> csvDatas) {
            DateTime dtOld = DateTime.MinValue;
            int tgtWorkNameColIndex = CellsKinmuhyo._WORK_DETAILS_DEFAULT_COL_INDEX;
            foreach (var data in csvDatas) {
                DateTime dtDate;
                if (!DateTime.TryParse(data.WorkDate, out dtDate)) {
                    continue;
                }
                int tgtRowIndex = dtDate.Day + CellsKinmuhyo._SHEET_HEADER_ROW_NUM;
                if (dtOld == dtDate) {
                    tgtWorkNameColIndex += CellsKinmuhyo._WORK_DETAILS_WORK_COL_INTERVAL;
                } else {
                    tgtWorkNameColIndex = CellsKinmuhyo._WORK_DETAILS_DEFAULT_COL_INDEX;
                    dtOld = dtDate;
                }
                sheet.Cells[tgtRowIndex, tgtWorkNameColIndex    ].Value = $"{data.ProjectCode}_{data.ProjectName}";
                sheet.Cells[tgtRowIndex, tgtWorkNameColIndex + 1].Value = data.ManHourH;
                sheet.Cells[tgtRowIndex, tgtWorkNameColIndex + 2].Value = data.ManHourM;
            }
        }
        /// <summary>
        /// 作業マスタ作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="csvDatas"></param>
        /// <param name="emp"></param>
        public void CreateWorkMaster(ExcelWorksheet sheet, List<EditTimeEntryDto> csvDatas) {

            // チャージ№リスト作成
            List<KeyValuePair<string, string>> ChargeNumList = new List<KeyValuePair<string, string>>();
            csvDatas.ForEach(n => ChargeNumList.Add(new KeyValuePair<string, string>(n.ProjectCode, n.ProjectName)));
            ChargeNumList = ChargeNumList.Distinct().ToList().OrderBy(n => n.Key).ToList();

            // 明細
            int masterCount = 0;
            ChargeNumList.ForEach(master => {
                var tgtCsvData = csvDatas.First(n => n.ProjectCode == master.Key && n.ProjectName == master.Value);
                if (masterCount < CellsKinmuhyo._WORK_MASTER_MAX_ROW_COUNT) {
                    int cellNum = masterCount + CellsKinmuhyo._WORK_MASTER_BODY_START_ROW_INDEX;

                    // プロジェクトコード
                    sheet.Cells[$"{CellsKinmuhyo._COL_MST_CHARGE_NO}{cellNum}"].Value = master.Key;

                    // 作業名
                    sheet.Cells[$"{CellsKinmuhyo._COL_MST_SAGYO_NAME_DISP}{ cellNum}"].Value = tgtCsvData.ProjectName;
                    sheet.Cells[$"{CellsKinmuhyo._COL_MST_SAGYO_NAME}{cellNum}"].Value = $"{tgtCsvData.ProjectCode}_{tgtCsvData.ProjectName}";
                    masterCount++;
                }
            });
        }
        /// <summary>
        /// 作業内容/一覧クリア
        /// </summary>
        /// <param name="sheet"></param>
        public void ClearWorkArea(ExcelWorksheet sheet) {
            // 作業内容欄
            for (int rowIdx = CellsKinmuhyo._SHEET_HEADER_ROW_NUM + 1;
                rowIdx <= CellsKinmuhyo._SHEET_HEADER_ROW_NUM + TimeConst._MAX_DATE_COUNT_AN_MONTH; rowIdx++) {
                for (int workIdx = 0; workIdx < CellsKinmuhyo._WORK_DETAILS_MAX_WORK_NUM; workIdx++) {

                    // 列インデックス
                    int colIdx = CellsKinmuhyo._WORK_DETAILS_DEFAULT_COL_INDEX;
                    colIdx += (workIdx * CellsKinmuhyo._WORK_DETAILS_WORK_COL_INTERVAL);

                    // セル値編集
                    sheet.Cells[rowIdx, colIdx].Value = null;
                    sheet.Cells[rowIdx, colIdx + 1].Value = null;
                    sheet.Cells[rowIdx, colIdx + 2].Value = null;
                }
            }
            // 作業一覧
            int curIdx = 0;
            for (int rowIdx = CellsKinmuhyo._WORK_MASTER_BODY_START_ROW_INDEX;
                curIdx < CellsKinmuhyo._WORK_MASTER_MAX_ROW_COUNT; rowIdx++, curIdx++) {
                sheet.Cells[$"{CellsKinmuhyo._COL_MST_CHARGE_NO}{rowIdx}"].Value = null;
                sheet.Cells[$"{CellsKinmuhyo._COL_MST_SAGYO_NAME_DISP}{rowIdx}"].Value = null;
                sheet.Cells[$"{CellsKinmuhyo._COL_MST_SAGYO_NAME}{rowIdx}"].Value = null;
            }
        }
        #endregion
    }
}
