﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using PrsAnalyzer.Const;
using PrsAnalyzer.Const.Kinmuhyo;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Service;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Logic.Kinmuhyo {
    public class CreatePrsLogic {
        /// <summary>
        /// デフォルトPRSパス
        /// </summary>
        private string _DEFAULT_PRS_PATH = Path.Combine(Environment.CurrentDirectory, "Template");
        /// <summary>
        /// PRS作成パス
        /// </summary>
        private string _OUTPUT_PRS_PATH = ConfigUtil.LoadOutputFolder();
        /// <summary>
        /// DB接続文字列
        /// </summary>
        private readonly string _DB_CONN_STRING = ConfigUtil.LoadConnDbString();

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="tgtEmp"></param>
        /// <param name="tgtDate"></param>
        /// <returns></returns>
        public void Main(MX03TantoEntity tgtEmp, DateTime tgtDate) {

            try {
                LogUtil.ShowLogInfo(string.Format(Messages._MSG_OPERATION_START, tgtEmp.TantoName));

                // PRS出力フォルダ作成
                string prsPath = Path.Combine(_OUTPUT_PRS_PATH, tgtDate.ToString(Formats._FORMAT_DATE_YYYYMM_NENTSUKI));
                CommonUtil.CreateFolder(prsPath);

                // PRSパス
                string prsName = KinmuhyoUtil.GetPrsName(tgtEmp.TantoName, tgtDate);
                string prsFullPath = Path.Combine(prsPath, KinmuhyoUtil.GetPrsName(tgtEmp.TantoName, tgtDate));

                // 初期化
                string defaultKinmuhyoPath = Path.Combine(_DEFAULT_PRS_PATH, CellsPRS._FILE_PRS_NAME);
                InitPRS(defaultKinmuhyoPath, prsFullPath, tgtDate);

                // 作業実績取得
                var workItemList = new PAF1010_PrsEntryService().GetSagyoJisseki(_DB_CONN_STRING, tgtEmp.TantoID, tgtDate, tgtDate.AddMonths(1).AddDays(-1));

                // CSVデータ解析
                var csvDatas = new AnalysisCalenderCsvLogic().EditWorkItemsPrs(tgtDate, workItemList, tgtEmp, false);

                // 勤怠時刻転記
                PostingWorkTimesProxy(csvDatas, prsPath, prsName, tgtEmp.TantoName, tgtDate);

                // 作業内容欄転記
                var pBushoKbn = (ProjectConst.PBushoKbn)(new MstCommonService().LoadMstBushoSingle(_DB_CONN_STRING, tgtEmp.BushoCD).PBushoKbn);
                PostingWorkArea(prsFullPath, prsName, csvDatas, pBushoKbn);

                // 担当者区分に応じてシート編集
                EditSheetByTantoKbn(prsFullPath, prsName, tgtEmp, pBushoKbn);

                LogUtil.ShowLogInfo(string.Format(Messages._MSG_OPERATION_END, tgtEmp.TantoName));

            }
            catch (Exception ex) {
                DialogUtil.ShowErroMsg(ex);
                throw ex;
            }
            finally {
                Cursor.Current = Cursor.Current;
            }
        }
        /// <summary>
        /// PRS初期化
        /// </summary>
        /// <param name="defaultPrsPath"></param>
        /// <param name="prsFullPath"></param>
        /// <param name="dt"></param>
        private void InitPRS(string defaultPrsPath, string prsFullPath, DateTime dt) {

            string CopyKinmuhyoPath = $"{prsFullPath}{CellsKinmuhyo._MARK_STAR}";
            File.Copy(defaultPrsPath, CopyKinmuhyoPath);

            var xlsxFile = File.OpenRead(CopyKinmuhyoPath);
            using (var package = new ExcelPackage(new FileInfo(CopyKinmuhyoPath))) {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                // 祝日シートクリア
                var sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KYUGYODATE];
                var holidayList = new MstCommonService().GetHolidays(_DB_CONN_STRING, dt.Year);
                int colNum = 2;
                foreach (var pair in holidayList) {
                    sheet.Cells[$"A{colNum}"].Value = pair.Key.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
                    sheet.Cells[$"B{colNum}"].Value = pair.Value;
                    colNum++;
                }
                // 勤怠時刻欄クリア
                sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KINMUHYO];
                ClearWorkTimeArea(sheet);

                // 作業内容/一覧クリア
                sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KINMUHYO];
                ClearSheet(sheet);

                // 保存
                package.SaveAs(new FileInfo(prsFullPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(CopyKinmuhyoPath);
        }

        #region 勤怠時刻
        /// <summary>
        /// 勤怠時刻転記（Proxy）
        /// </summary>
        /// <param name="csvDatas"></param>
        /// <param name="kinmuhyoPath"></param>
        /// <param name="kinmuhyoName"></param>
        /// <param name="emploeeName"></param>
        /// <param name="taishoDate"></param>
        private void PostingWorkTimesProxy(List<EditTimeEntryDto> csvDatas, string kinmuhyoPath, string kinmuhyoName, string emploeeName, DateTime taishoDate) {
            var wrokTimeDtoList = new List<WorkTimeDto>();
            csvDatas.ForEach(csvData => {

                // CSV解析データからWorkTimeDto取得
                var dto = KinmuhyoUtil.GetWorkTimeDtoFromCsvData(csvData);

                // 休暇判定に応じて出退勤時刻・休憩時間を編集
                dto = EditWorkRestTimeByRestType(dto);

                wrokTimeDtoList.Add(dto);
            });
            // 勤怠時刻転記
            string KinmuhyoPath = Path.Combine(kinmuhyoPath, kinmuhyoName);
            string tempPath = Path.Combine(kinmuhyoPath, CellsKinmuhyo._MARK_STAR + kinmuhyoName);
            if (File.Exists(KinmuhyoPath)) {
                PostingWorkTimes(emploeeName, KinmuhyoPath, tempPath, wrokTimeDtoList, taishoDate);
            }
        }
        /// <summary>
        /// 休暇判定に応じて出退勤時刻・休憩時間を編集（PRS独自）
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private WorkTimeDto EditWorkRestTimeByRestType(WorkTimeDto dto) {

            int restTime = 0;
            switch (dto.RestType) {
                case TimeConst.RestType.PAID:
                    dto.StartHH = string.Empty;
                    dto.StartMM = string.Empty;
                    dto.EndHH = string.Empty;
                    dto.EndMM = string.Empty;
                    dto.RestHH = string.Empty;
                    dto.RestMM = string.Empty;
                    break;
                case TimeConst.RestType.AM:
                    dto.StartHH = "13";
                    dto.StartMM = "00";
                    restTime = int.Parse(dto.RestHH) * 60 + int.Parse(dto.RestMM) - TimeConst._MINUTE_AN_HOUR;
                    if (restTime == 0) {
                        dto.RestHH = string.Empty;
                        dto.RestMM = string.Empty;
                    }
                    else {
                        dto.RestHH = (restTime / TimeConst._MINUTE_AN_HOUR).ToString();
                        dto.RestMM = (restTime % TimeConst._MINUTE_AN_HOUR).ToString(Formats._FORMAT_ZERO_2);
                    }
                    break;
                case TimeConst.RestType.PM:
                    dto.EndHH = "12";
                    dto.EndMM = "00";
                    restTime = int.Parse(dto.RestHH) * 60 + int.Parse(dto.RestMM) - TimeConst._MINUTE_AN_HOUR;
                    if (restTime == 0) {
                        dto.RestHH = string.Empty;
                        dto.RestMM = string.Empty;
                    }
                    else {
                        dto.RestHH = (restTime / TimeConst._MINUTE_AN_HOUR).ToString();
                        dto.RestMM = (restTime % TimeConst._MINUTE_AN_HOUR).ToString(Formats._FORMAT_ZERO_2);
                    }
                    break;
            }
            return dto;
        }
        /// <summary>
        /// 勤怠時刻転記
        /// </summary>
        /// <param name="emploeeName"></param>
        /// <param name="OriginXlsxPath"></param>
        /// <param name="tempPath"></param>
        /// <param name="dtoList"></param>
        /// <param name="dtWorkStart"></param>
        private void PostingWorkTimes(string emploeeName, string OriginXlsxPath, string tempPath, List<WorkTimeDto> dtoList, DateTime dtWorkStart) {
            var xlsxFile = File.OpenRead(OriginXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(OriginXlsxPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KINMUHYO];

                // 名前
                sheet.Cells[CellsPRS._CELL_NAME].Value = emploeeName;

                // 年
                sheet.Cells[CellsPRS._CELL_YYYY].Value = dtWorkStart.Year;

                // 月
                sheet.Cells[CellsPRS._CELL_MM].Value = dtWorkStart.Month;

                // 編集
                foreach(var dto in dtoList) {
                    int colNum = CellsPRS._COL_KOSU_START + dto.Day - 1;
                    if (!string.IsNullOrEmpty(dto.StartHH)) {
                        // 始業時刻
                        sheet.Cells[CellsPRS._ROW_SYUTTAIKIN_START, colNum].Style.Numberformat.Format = Formats._FORMAT_TIME_HMM;
                        sheet.Cells[CellsPRS._ROW_SYUTTAIKIN_START, colNum].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        string startTime = $"{dto.StartHH}:{int.Parse(dto.StartMM).ToString(Formats._FORMAT_ZERO_2)}";
                        sheet.Cells[CellsPRS._ROW_SYUTTAIKIN_START, colNum].Value = startTime;
                    }
                    if (!string.IsNullOrEmpty(dto.EndHH)) {
                        // 終業時刻
                        int rowNumWorkEnd = CellsPRS._ROW_SYUTTAIKIN_START + 1;
                        sheet.Cells[rowNumWorkEnd, colNum].Style.Numberformat.Format = Formats._FORMAT_TIME_HMM;
                        sheet.Cells[rowNumWorkEnd, colNum].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        string endTime = $"{dto.EndHH}:{int.Parse(dto.EndMM).ToString(Formats._FORMAT_ZERO_2)}";
                        sheet.Cells[CellsPRS._ROW_SYUTTAIKIN_START + 1, colNum].Value = endTime;
                    }
                    // 休憩時間
                    int rowNumRest = CellsPRS._ROW_SYUTTAIKIN_START + 2;
                    sheet.Cells[rowNumRest, colNum].Style.Numberformat.Format = Formats._FORMAT_TIME_HMM;
                    sheet.Cells[rowNumRest, colNum].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    if (!string.IsNullOrEmpty(dto.RestHH)) {
                        string restTime = $"{dto.RestHH}:{int.Parse(dto.RestMM).ToString(Formats._FORMAT_ZERO_2)}";
                        sheet.Cells[CellsPRS._ROW_SYUTTAIKIN_START + 2, colNum].Value = restTime;
                    }
                }
                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(OriginXlsxPath);
            File.Copy(tempPath, OriginXlsxPath);
            CommonUtil.DeleteFile(tempPath);
        }
        #endregion

        #region 作業内容
        /// <summary>
        /// 作業内容/一覧転記
        /// </summary>
        /// <param name="kinmuhyoFullPath"></param>
        /// <param name="kinmuhyoName"></param>
        /// <param name="csvDatas"></param>
        /// <param name="pBushoKbn"></param>
        private void PostingWorkArea(string kinmuhyoFullPath, string kinmuhyoName, List<EditTimeEntryDto> csvDatas, ProjectConst.PBushoKbn pBushoKbn) {
            if (csvDatas == null || !csvDatas.Any()) {
                return;
            }
            string tempPath = Path.Combine(Environment.CurrentDirectory, CellsKinmuhyo._MARK_STAR + kinmuhyoName);
            var xlsxFile = File.OpenRead(kinmuhyoFullPath);
            using (var package = new ExcelPackage(new FileInfo(kinmuhyoFullPath))) {

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KINMUHYO];

                // 作業一覧作成
                int masterCount = PostingWorkLooks(sheet, csvDatas, (int)pBushoKbn);

                // プロジェクトの合計工数
                int maxRowCount = CellsPRS._ROW_MEISAI_START + masterCount;
                sheet.Cells[CellsPRS._CELL_TOTAL].Formula = string.Format(CellsPRS._FORMULA_SUM_TOTAL, maxRowCount);

                // 作業明細作成
                PostingWorkDetails(sheet, csvDatas, masterCount);

                // 重複項目をブランクにする
                if (pBushoKbn != ProjectConst.PBushoKbn.FIELD_SUPPROT){
                    ClearDouple(sheet, maxRowCount);
                }
                // 罫線
                EditLine(sheet, masterCount);

                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(kinmuhyoFullPath);
            File.Copy(tempPath, kinmuhyoFullPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 作業一覧作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="csvDatas"></param>
        /// <param name="pBushoKbn"></param>
        /// <returns></returns>
        private int PostingWorkLooks(ExcelWorksheet sheet, List<EditTimeEntryDto> csvDatas, int pBushoKbn) {

            // プロジェクト一覧検索
            string userId = csvDatas.First().UserId;
            string taishoYM = csvDatas.First().WorkStartTime.ToString(Formats._FORMAT_DATE_YYYYMM);
            var projectLooksMaster = new PAF1020_ProjectLooksService().LoadProjectLooksJoinP02(_DB_CONN_STRING, userId, taishoYM, pBushoKbn);
            if (!projectLooksMaster.Any()) {
                return 0;
            }
            foreach (var master in projectLooksMaster) {

                // 作業一覧作成
                int tgtRowNum = CellsPRS._ROW_MEISAI_START + projectLooksMaster.IndexOf(master);
                sheet.Cells[$"{CellsPRS._COL_PROJECT_CODE}{tgtRowNum}"].Value = master.ProjectCD;
                sheet.Cells[$"{CellsPRS._COL_KOKYAKU_NAME}{tgtRowNum}"].Value = master.KokyakuName;
                sheet.Cells[$"{CellsPRS._COL_PROJECT_NAME}{tgtRowNum}"].Value = master.ProjectName;
                sheet.Cells[$"{CellsPRS._COL_KOUTEI_NAME}{tgtRowNum}"].Value = master.KouteiName;
                sheet.Cells[$"{CellsPRS._COL_DETAIL_NAME}{tgtRowNum}"].Value = master.Detail;
                sheet.Cells[$"{CellsPRS._COL_SUM_KOSU}{tgtRowNum}"].Formula = string.Format(CellsPRS._FORMULA_SUM_KINTAI, tgtRowNum);
                sheet.Cells[$"{CellsPRS._COL_SUM_KOSU}{tgtRowNum}"].Style.Numberformat.Format = Formats._FORMAT_FLOAT_1;

                // セル文字色を編集
                string cellRanegeLooksRow = $"{CellsPRS._COL_PROJECT_CODE}{tgtRowNum}:{CellsPRS._COL_DETAIL_NAME}{tgtRowNum}";
                if (master.ProjectCD.Substring(0, 1) == ProjectConst.GyomuKbn.Y.ToString()){
                    sheet.Cells[cellRanegeLooksRow].Style.Font.Color.SetColor(Colors._BACK_COLOR_ERROR);
                }else {
                    sheet.Cells[cellRanegeLooksRow].Style.Font.Color.SetColor(Colors._FORE_COLOR_CLICK);
                }
                // フォントサイズ
                string range = $"{CellsPRS._COL_PROJECT_CODE}{tgtRowNum}:{CellsPRS._COL_SUM_KOSU}{tgtRowNum}";
                sheet.Cells[range].Style.Font.Size = CellsPRS._FONT_SIZE_KOSU;
            }
            return projectLooksMaster.Count;
        }
        /// <summary>
        /// 作業明細作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="csvDatas"></param>
        /// <param name="masterCount"></param>
        private void PostingWorkDetails(ExcelWorksheet sheet, List<EditTimeEntryDto> csvDatas, int masterCount) {

            int maxRowCount = 0;
            foreach (var data in csvDatas) {
                DateTime dtDate;
                if (!DateTime.TryParse(data.WorkDate, out dtDate)) {
                    continue;
                }
                // ターゲットの列番号取得
                int colNum = CellsPRS._COL_KOSU_START + dtDate.Day - 1;

                // ターゲットの行番号取得（作業名・工程・詳細から判定）
                int rowNum = 0;
                string projectCD = data.ProjectCode;
                string koutei = data.KouteiName;
                string detail = data.Detail;
                maxRowCount = CellsPRS._ROW_MEISAI_START + masterCount;
                for (int i = 0; i < masterCount + 1; i++) {
                    rowNum = CellsPRS._ROW_MEISAI_START + i - 1;
                    if (!JudgeAllowPostingKousu(sheet, data, i)) {
                        continue;
                    }
                    // 「工数」を転記
                    sheet.Cells[rowNum, colNum].Style.Numberformat.Format = Formats._FORMAT_FLOAT_1;
                    sheet.Cells[rowNum, colNum].Style.Font.Color.SetColor(Color.Red);
                    if (0 < data.TotalManHourSagyo) {
                        sheet.Cells[rowNum, colNum].Value = data.TotalManHourSagyo / TimeConst._MINUTE_AN_HOUR;
                    }
                    sheet.Cells[rowNum, colNum].Style.Font.Name = CellsPRS._FONT_NAME_KOSU;
                    sheet.Cells[rowNum, colNum].Style.Font.Size = CellsPRS._FONT_SIZE_KOSU;

                    // その日の合計工数を求めるExcel関数
                    string colAlpha = KinmuhyoUtil.ToAlphabet(colNum);
                    int rowNumFormula = CellsPRS._ROW_MEISAI_START - 1;
                    string cellRangeFormula = $"{colAlpha}{rowNumFormula}";
                    sheet.Cells[cellRangeFormula].Formula = string.Format(CellsPRS._FORMULA_SUM_TOTAL_A_DAY, colAlpha, maxRowCount);
                    sheet.Cells[cellRangeFormula].Style.Font.Color.SetColor(Color.Red);
                    sheet.Cells[cellRangeFormula].Style.Font.Name = CellsPRS._FONT_NAME_KOSU;
                    sheet.Cells[cellRangeFormula].Style.Font.Size = CellsPRS._FONT_SIZE_KOSU;
                }
            }
        }
        /// <summary>
        /// 工数転記を許可する？
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="data"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private bool JudgeAllowPostingKousu(ExcelWorksheet sheet, EditTimeEntryDto data, int i) {
            int rowNum = CellsPRS._ROW_MEISAI_START + i - 1;
            string projectCD = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_PROJECT_CODE}{ rowNum}"].Value);
            string projectName = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_PROJECT_NAME}{rowNum}"].Value);
            string koutei = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_KOUTEI_NAME}{rowNum}"].Value);
            string detail = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_DETAIL_NAME}{rowNum}"].Value);
            if (data.ProjectCode != projectCD) {
                return false;
            }
            if (data.ProjectName != projectName) {
                return false;
            }
            if (data.KouteiName != koutei) {
                return false;
            }
            if (data.Detail != detail) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複項目をブランクにする
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="maxRowCount"></param>
        private void ClearDouple(ExcelWorksheet sheet, int maxRowCount) {
            for (int i = maxRowCount; CellsPRS._ROW_MEISAI_START - 1 < i; i--) {
                string afteterProjectCode = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_PROJECT_CODE}{i + 1}"].Value);
                string currProjectCode    = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_PROJECT_CODE}{i}"].Value);

                if (afteterProjectCode == currProjectCode) {

                    // プロジェクトコードが一致する場合はブランクにする
                    sheet.Cells[$"{CellsPRS._COL_KOKYAKU_NAME}{(i + 1)}"].Value = null;
                    sheet.Cells[$"{CellsPRS._COL_PROJECT_CODE}{(i + 1)}"].Value = null;
                    sheet.Cells[$"{CellsPRS._COL_PROJECT_NAME}{(i + 1)}"].Value = null;
                    

                    string afterKouteiName = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_KOUTEI_NAME}{i + 1}"].Value);
                    string currKouteiName = StringUtil.TostringNullForbid(sheet.Cells[$"{CellsPRS._COL_KOUTEI_NAME}{i}"].Value);
                    if (afterKouteiName == currKouteiName) {

                        // 工程名称まで一致する場合はブランクにする
                        sheet.Cells[$"{CellsPRS._COL_KOUTEI_NAME}{(i + 1)}"].Value = null;
                    }
                }
            }
        }
        /// <summary>
        /// 勤怠時刻欄クリア
        /// </summary>
        /// <param name="sheet"></param>
        public void ClearWorkTimeArea(ExcelWorksheet sheet) {

            for (int rowNum = CellsPRS._ROW_SYUTTAIKIN_START; rowNum < CellsPRS._ROW_SYUTTAIKIN_START + 3; rowNum++) {
                for (int colNum = CellsPRS._COL_NUM_HEADER_COL_END + 1; colNum <= CellsPRS._COL_NUM_AL; colNum++) {
                    // セル値をクリア
                    sheet.Cells[rowNum, colNum].Value = null;
                }
            }
        }
        /// <summary>
        /// 作業内容/一覧クリア
        /// </summary>
        /// <param name="sheet"></param>
        public void ClearSheet(ExcelWorksheet sheet) {

            // 目盛り線
            sheet.View.ShowGridLines = true;

            // 合計行
            int sumRowNum = CellsPRS._ROW_MEISAI_START - 2;
            string cellRangeMeisaiJAN = $"{CellsPRS._COL_PROJECT_CODE}{sumRowNum}:{CellsPRS._COL_MEI__END}{sumRowNum + 1}";
            sheet.Cells[cellRangeMeisaiJAN].Style.Numberformat.Format = Formats._FORMAT_FLOAT_1;
            sheet.Cells[cellRangeMeisaiJAN].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            // 罫線をクリア（明細行）
            int rowNum = CellsPRS._ROW_MEISAI_START + CellsPRS._MAX_ROW_COUNT;
            string cellRangeDetails = $"{CellsPRS._COL_PROJECT_CODE}{CellsPRS._ROW_MEISAI_START}:{CellsPRS._COL_MEI__END}{rowNum}";
            sheet.Cells[cellRangeDetails].Clear();
            sheet.Cells[cellRangeDetails].Style.Border.Right.Style = ExcelBorderStyle.None;
            sheet.Cells[cellRangeDetails].Style.Border.Bottom.Style = ExcelBorderStyle.None;
            sheet.Cells[cellRangeDetails].Style.Font.Color.SetColor(Colors._FORE_COLOR_CLICK);

            // 罫線をクリア（外周）
            string cellRangeArround = $"{CellsPRS._COL_PROJECT_CODE}{CellsPRS._ROW_MEISAI_START}:{CellsPRS._COL_MEI__END}{rowNum}";
            sheet.Cells[cellRangeArround].Style.Border.BorderAround(ExcelBorderStyle.None);
        }
        /// <summary>
        /// 罫線編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="masterCount"></param>
        public void EditLine(ExcelWorksheet sheet, int masterCount) {

            int maxRowCount = CellsPRS._ROW_MEISAI_START + masterCount - 1;

            // 作業一覧セル
            string cellRangeLooksRows = $"{CellsPRS._COL_PROJECT_CODE}{CellsPRS._ROW_MEISAI_START}:{CellsPRS._COL_SUM_KOSU}{maxRowCount}";
            sheet.Cells[cellRangeLooksRows].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            sheet.Cells[cellRangeLooksRows].Style.Border.BorderAround(ExcelBorderStyle.Thin);

            // 作業明細セル
            string cellRangeDetailRows = $"{CellsPRS._COL_KOU_STRT}{CellsPRS._ROW_MEISAI_START}:{CellsPRS._COL_MEI__END}{maxRowCount}";
            sheet.Cells[cellRangeDetailRows].Style.Border.Right.Style = ExcelBorderStyle.Hair;

            // 行のセル下部分は点線
            string cellRangeAllRows = $"{CellsPRS._COL_PROJECT_CODE}{CellsPRS._ROW_MEISAI_START}:{CellsPRS._COL_MEI__END}{maxRowCount}";
            sheet.Cells[cellRangeAllRows].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
            sheet.Cells[cellRangeAllRows].Style.Border.BorderAround(ExcelBorderStyle.Thin);
        }
        #endregion

        #region 担当者区分に応じてシート編集
        /// <summary>
        /// 担当者区分に応じてシート編集
        /// </summary>
        /// <param name="kinmuhyoFullPath"></param>
        /// <param name="kinmuhyoName"></param>
        /// <param name="emp"></param>
        /// <param name="pBushoKbn"></param>
        private void EditSheetByTantoKbn(string kinmuhyoFullPath, string kinmuhyoName, MX03TantoEntity emp, ProjectConst.PBushoKbn pBushoKbn) {
            string tempPath = Path.Combine(Environment.CurrentDirectory, CellsKinmuhyo._MARK_STAR + kinmuhyoName);
            var xlsxFile = File.OpenRead(kinmuhyoFullPath);
            using (var package = new ExcelPackage(new FileInfo(kinmuhyoFullPath))) {

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var tgtKaishaInfoDtoForPrs = new MstCommonService().LoadMstBushoForPrs(_DB_CONN_STRING, emp.BushoCD, emp.TantoID);

                switch (emp.TantoKbn) {
                    case (int)Enums.TantoKbn.Naiko:// 内工
                        if (pBushoKbn != ProjectConst.PBushoKbn.FIELD_SUPPROT) {
                            DelSheet(package, CellsPRS._SHEET_PRS);
                        }
                        DelSheet(package, CellsPRS._SHEET_JISSEKI_HOKOKUSHO);
                        DelSheet(package, CellsPRS._SHEET_KANRIDAICHO);
                        break;
                    case (int)Enums.TantoKbn.Inin: // 委任
                        DelSheet(package, CellsPRS._SHEET_KANRIDAICHO);
                        PostingIninJissekiHokokusho(package, tgtKaishaInfoDtoForPrs);
                        break;
                    case (int)Enums.TantoKbn.Haken: // 派遣
                        DelSheet(package, CellsPRS._SHEET_JISSEKI_HOKOKUSHO);
                        PostingHakenKanriDaicho(package, tgtKaishaInfoDtoForPrs);
                        break;
                }
                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(kinmuhyoFullPath);
            File.Copy(tempPath, kinmuhyoFullPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// シート削除
        /// </summary>
        /// <param name="package"></param>
        /// <param name="sheetName"></param>
        private void DelSheet(ExcelPackage package, string sheetName) {
            var worksheet = package.Workbook.Worksheets.FirstOrDefault(s => s.Name == sheetName);
            if (worksheet != null) {
                package.Workbook.Worksheets.Delete(sheetName);
            }
        }
        /// <summary>
        /// 作業実績報告書（委任用）
        /// </summary>
        /// <param name="package"></param>
        /// <param name="dto"></param>
        private void PostingIninJissekiHokokusho(ExcelPackage package, KaishaInfoForPrsDto dto) {
            var sheet = package.Workbook.Worksheets[CellsPRS._SHEET_JISSEKI_HOKOKUSHO];
            sheet.Cells[CellsPRS._CELL_ININ_KAISHA_CODE].Value = dto.KaishaCD;
            sheet.Cells[CellsPRS._CELL_ININ_KAISHA_NAME].Value = dto.KaishaName;
            sheet.Cells[CellsPRS._CELL_ININ_SEKININ_NAME].Value = dto.SekininName;
            sheet.Cells[CellsPRS._CELL_ININ_TEL].Value = dto.TelNo;
            sheet.Cells[CellsPRS._CELL_ININ_TANTO_BUSHO].Value = dto.BushoName;
            sheet.Cells[CellsPRS._CELL_ININ_SHIKI_MEIREI].Value = dto.ShikiMeireiTantoName;
        }
        /// <summary>
        /// 管理台帳（要員派遣）
        /// </summary>
        /// <param name="package"></param>
        /// <param name="dto"></param>
        private void PostingHakenKanriDaicho(ExcelPackage package, KaishaInfoForPrsDto dto) {
            var sheet = package.Workbook.Worksheets[CellsPRS._SHEET_KANRIDAICHO];
            sheet.Cells[CellsPRS._CELL_HAKEN_KAISHA_CODE].Value = dto.KaishaCD;
            sheet.Cells[CellsPRS._CELL_HAKEN_KAISHA_NAME].Value = dto.KaishaName;
            sheet.Cells[CellsPRS._CELL_HAKEN_BUSHO_YAKUSHOKU].Value = dto.BushoYakushokuName;
            sheet.Cells[CellsPRS._CELL_HAKEN_SEKININ_NAME].Value = dto.SekininName;
            sheet.Cells[CellsPRS._CELL_HAKEN_TEL].Value = dto.TelNo;
            sheet.Cells[CellsPRS._CELL_HAKEN_SHIKI_MEIREI].Value = dto.ShikiMeireiTantoName;
        }
        #endregion
    }
}
