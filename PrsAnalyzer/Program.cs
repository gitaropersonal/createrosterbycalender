﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Util;
using PrsAnalyzer.Forms;
using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace PrsAnalyzer {
    static class Program {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool createdNew;
            using (var mutex = new Mutex(true, "PrsAnalyzer", out createdNew)) {
                try {
                    if (!createdNew) {
                        // 多重起動の場合はエラーメッセージを表示して終了
                        throw new Exception(Messages._MSG_ERROR_DOUPLE_BOOT);
                    }
                    // アイコンをすべての画面に反映
                    typeof(Form).GetField("defaultIcon", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, new Icon(@"PrsAnalyzer.ico"));

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new LoginDialogBody());
                } catch (Exception e) {
                    DialogUtil.ShowErroMsg(e);
                }
            }
        }
    }
}
