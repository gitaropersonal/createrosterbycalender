﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Util;
using System;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class LoginService {
        /// <summary>
        /// ログイン情報取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <returns></returns>
        public LoginInfoDto LoadLoginInfo(
              string dbConnString
            , string tantoID
            , string passWord
            ){
            var ret = new LoginInfoDto();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT TOP(1) ");
            sb.AppendLine("    MX03.TantoID ");
            sb.AppendLine(",   MX03.TantoName ");
            sb.AppendLine(",   MX03.BushoCD ");
            sb.AppendLine(",   MX03.AuthorityKbn ");
            sb.AppendLine(",   MX06.PBushoKbn ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX03Tanto AS MX03 ");
            sb.AppendLine("INNER JOIN ");
            sb.AppendLine("    MX99PassWord AS MX99 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.TantoID = MX99.TantoID ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX06Busho AS MX06 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.BushoCD = MX06.BushoCD ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX03.TantoID  = @tantoId ");
            sb.AppendLine("AND MX99.PassWord = @passWord ");
            sb.AppendLine("AND MX03.DelFlg   = 0 ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@passWord", passWord));
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            ret.TantoID = StringUtil.ToStringForbidNull(dr[nameof(ret.TantoID)]);
                            ret.TantoName = StringUtil.ToStringForbidNull(dr[nameof(ret.TantoName)]);
                            ret.BushoCD = StringUtil.ToStringForbidNull(dr[nameof(ret.BushoCD)]);
                            ret.AuthorityKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(ret.AuthorityKbn)]));
                            ret.PBushoKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(ret.PBushoKbn)]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// ログイン情報登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="passWord"></param>
        public void InsertLoginInfo(
              string dbConnString
            , string tantoID
            , string passWord
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX99PassWord( ");
            sb.AppendLine("    TantoID ");
            sb.AppendLine(",   PassWord ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @tantoId ");
            sb.AppendLine(",   @passWord ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@passWord", passWord));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ログイン情報更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="newPassWord"></param>
        public void UpdLoginInfo(
              string dbConnString
            , string tantoID
            , string newPassWord
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX99PassWord ");
            sb.AppendLine("SET ");
            sb.AppendLine("    PassWord = @passWord ");
            sb.AppendLine(",   UpdDate  = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID  = @tantoId ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@passWord", newPassWord));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
