﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class MstCommonService {
        #region プロジェクト
        /// <summary>
        /// プロジェクトマスタ取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="projectCD"></param>
        /// <param name="projectName"></param>
        /// <param name="gyomuKbn"></param>
        /// <param name="kokyakuKbn"></param>
        /// <param name="kokyakuName"></param>
        /// <returns></returns>
        public List<MX01ProjectEntity> LoadMstProject(
              string dbConnString
            , string projectCD = ""
            , string projectName = ""
            , string gyomuKbn = ""
            , ProjectConst.KokyakumeiKbn kokyakuKbn = ProjectConst.KokyakumeiKbn.NONE
            , string kokyakuName = ""
            ) {

            var ret = new List<MX01ProjectEntity>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(projectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(projectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(gyomuKbn));

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX01.ProjectCD ");
            sb.AppendLine(",   MX01.ProjectName ");
            sb.AppendLine(",   MX01.KokyakuName ");
            sb.AppendLine(",   MX01.PBushoKbn ");
            sb.AppendLine(",   MX01.StartYM ");
            sb.AppendLine(",   MX01.EndYM ");
            sb.AppendLine(",   MX01.AddDate ");
            sb.AppendLine(",   MX01.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX01Project AS MX01 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine($@"{cmOutProjectCD}AND MX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutGyomuKbn }AND SUBSTRING(MX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{cmOutProjectNm}AND MX01.ProjectName LIKE @projectName ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(kokyakuKbn, kokyakuName, "MX01")} ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@projectCD", projectCD));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", gyomuKbn));
                    cmd.Parameters.AddWithValue("@projectName", "%" + projectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + kokyakuName + "%");

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MX01ProjectEntity();
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.PBushoKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]));
                            entity.StartYM = StringUtil.ToStringForbidNull(dr[nameof(entity.StartYM)]);
                            entity.EndYM = StringUtil.ToStringForbidNull(dr[nameof(entity.EndYM)]);
                            entity.AddDate = TypeConversionUtil.ToDateTime(dr[nameof(entity.AddDate)]);
                            entity.UpdDate = TypeConversionUtil.ToDateTime(dr[nameof(entity.UpdDate)]);
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// プロジェクトマスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddMstProject(
              string dbConnString
            , MX01ProjectEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX01Project( ");
            sb.AppendLine("    ProjectCD ");
            sb.AppendLine(",   ProjectName ");
            sb.AppendLine(",   KokyakuName ");
            sb.AppendLine(",   PBushoKbn ");
            sb.AppendLine(",   StartYM ");
            sb.AppendLine(",   EndYM ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @ProjectCD ");
            sb.AppendLine(",   @ProjectName ");
            sb.AppendLine(",   @KokyakuName ");
            sb.AppendLine(",   @PBushoKbn ");
            sb.AppendLine(",   @StartYM ");
            sb.AppendLine(",   @EndYM ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@ProjectName", addEntity.ProjectName));
                    cmd.Parameters.Add(new SqlParameter("@KokyakuName", addEntity.KokyakuName));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", addEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@StartYM", addEntity.StartYM));
                    cmd.Parameters.Add(new SqlParameter("@EndYM", addEntity.EndYM));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region 休業日
        /// <summary>
        /// 祝日取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Dictionary<DateTime, string> GetHolidays(
              string dbConnString
            , int year
            ) {
            Dictionary<DateTime, string> Holidays = new Dictionary<DateTime, string>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    KyugyoYMD ");
            sb.AppendLine(",   KyugyoName ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX04KyugyoDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine($@"    FORMAT(KyugyoYMD, 'yyyy') = '{year}' ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            DateTime key = TypeConversionUtil.ToDateTime(StringUtil.ToStringForbidNull(dr[0]));
                            string val = StringUtil.ToStringForbidNull(dr[1]);
                            Holidays.Add(key, val);
                        }
                    }
                }
            }
            return Holidays;
        }
        /// <summary>
        /// 休業日取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public List<MX04KyugyoDateEntity> LoadMstKyugyoDate(
              string dbConnString
            , int? year
            ) {

            // 条件に応じて一部コメントアウト
            string cmOutTgtYear = StringUtil.GetCmout(year == null);

            // パラメータ
            int tahishoYear = 0;
            if (year != null) {
                tahishoYear = year.Value;
            }
            List<MX04KyugyoDateEntity> ret = new List<MX04KyugyoDateEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    KyugyoYMD ");
            sb.AppendLine(",   KyugyoName ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX04KyugyoDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine($@"{cmOutTgtYear}AND FORMAT(KyugyoYMD, 'yyyy') = @year ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    KyugyoYMD ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@year", tahishoYear));
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MX04KyugyoDateEntity();
                            entity.KyugyoYMD = TypeConversionUtil.ToDateTime(StringUtil.ToStringForbidNull(dr[nameof(entity.KyugyoYMD)]));
                            entity.KyugyoName = StringUtil.ToStringForbidNull(dr[nameof(entity.KyugyoName)]);
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region 部署
        /// <summary>
        /// 部署マスタロード（部署コード指定）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="bushoCD"></param>
        /// <returns></returns>
        public MX06BushoEntity LoadMstBushoSingle(
              string dbConnString
            , string bushoCD
            ) {
            var ret = new MX06BushoEntity();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX06.BushoCD ");
            sb.AppendLine(",   MX06.BushoName ");
            sb.AppendLine(",   MX06.PBushoKbn ");
            sb.AppendLine(",   MX06.DelFlg ");
            sb.AppendLine(",   MX06.AddDate ");
            sb.AppendLine(",   MX06.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX06Busho AS MX06 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX06.BushoCD = @BushoCD ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", bushoCD));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            ret.BushoCD = StringUtil.ToStringForbidNull(dr[nameof(ret.BushoCD)]);
                            ret.BushoName = StringUtil.ToStringForbidNull(dr[nameof(ret.BushoName)]);
                            ret.PBushoKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(ret.PBushoKbn)]));
                            ret.DelFlg = StringUtil.ToStringForbidNull(dr[nameof(ret.DelFlg)]);
                            ret.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(ret.AddDate)]));
                            ret.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(ret.UpdDate)]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 部署マスタロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con"></param>
        /// <param name="isContainDeleted"></param>
        /// <returns></returns>
        public List<MX06BushoEntity> LoadMstBusho(
              string dbConnString
            , MX06BushoEntity con
            , bool isContainDeleted
            ) {
            var ret = new List<MX06BushoEntity>();

            // 条件に応じて一部コメントアウト
            string cmOutBushoCD = StringUtil.GetCmout(string.IsNullOrEmpty(con.BushoCD));
            string cmOutDelFlg = StringUtil.GetCmout(isContainDeleted);
            string cmOutPBushoKbn = StringUtil.GetCmout(con.PBushoKbn == (int)ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX06.BushoCD ");
            sb.AppendLine(",   MX06.BushoName ");
            sb.AppendLine(",   MX06.PBushoKbn ");
            sb.AppendLine(",   MX06.DelFlg ");
            sb.AppendLine(",   MX06.AddDate ");
            sb.AppendLine(",   MX06.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX06Busho AS MX06 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine($@"{cmOutBushoCD}AND MX06.BushoCD = @BushoCD ");
            sb.AppendLine($@"{cmOutPBushoKbn}AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"{cmOutDelFlg}AND MX06.DelFlg = 0 ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", StringUtil.TostringNullForbid(con.BushoCD)));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", StringUtil.TostringNullForbid(con.PBushoKbn)));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MX06BushoEntity();
                            entity.BushoCD = StringUtil.ToStringForbidNull(dr[nameof(entity.BushoCD)]);
                            entity.BushoName = StringUtil.ToStringForbidNull(dr[nameof(entity.BushoName)]);
                            entity.PBushoKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]));
                            entity.DelFlg = StringUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                            entity.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.AddDate)]));
                            entity.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region 会社
        /// <summary>
        /// 会社マスタロード（会社コード指定）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="kaishaCd"></param>
        /// <returns></returns>
        public MX07KaishaEntity LoadMstKaishaSingle(
              string dbConnString
            , string kaishaCd
            ) {
            var ret = new MX07KaishaEntity();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX07.KaishaCD ");
            sb.AppendLine(",   MX07.KaishaName ");
            sb.AppendLine(",   MX07.KaishaNameR ");
            sb.AppendLine(",   MX07.KaishaNameKN ");
            sb.AppendLine(",   MX07.KaishaKBN ");
            sb.AppendLine(",   MX07.TankaKBNJ ");
            sb.AppendLine(",   MX07.TankaKBNU ");
            sb.AppendLine(",   MX07.SekininName ");
            sb.AppendLine(",   MX07.SekininBusho ");
            sb.AppendLine(",   MX07.SekininYaku ");
            sb.AppendLine(",   MX07.TelNo ");
            sb.AppendLine(",   MX07.HakenKyokaNo ");
            sb.AppendLine(",   MX07.BankName ");
            sb.AppendLine(",   MX07.StartYM ");
            sb.AppendLine(",   MX07.EndYM ");
            sb.AppendLine(",   MX07.AddDate ");
            sb.AppendLine(",   MX07.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX07Kaisha AS MX07 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX07.KaishaCD = @KaishaCD ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@KaishaCD", kaishaCd));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            ret.KaishaCD = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaCD)]);
                            ret.KaishaName = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaName)]);
                            ret.KaishaNameR = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaNameR)]);
                            ret.KaishaNameKN = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaNameKN)]);
                            ret.KaishaKBN = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaKBN)]);
                            ret.TankaKBNJ = StringUtil.ToStringForbidNull(dr[nameof(ret.TankaKBNJ)]);
                            ret.TankaKBNU = StringUtil.ToStringForbidNull(dr[nameof(ret.TankaKBNU)]);
                            ret.SekininName = StringUtil.ToStringForbidNull(dr[nameof(ret.SekininName)]);
                            ret.SekininBusho = StringUtil.ToStringForbidNull(dr[nameof(ret.SekininBusho)]);
                            ret.SekininYaku = StringUtil.ToStringForbidNull(dr[nameof(ret.SekininYaku)]);
                            ret.TelNo = StringUtil.ToStringForbidNull(dr[nameof(ret.TelNo)]);
                            ret.HakenKyokaNo = StringUtil.ToStringForbidNull(dr[nameof(ret.HakenKyokaNo)]);
                            ret.BankName = StringUtil.ToStringForbidNull(dr[nameof(ret.BankName)]);
                            ret.StartYM = StringUtil.ToStringForbidNull(dr[nameof(ret.StartYM)]);
                            ret.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(ret.AddDate)]));
                            ret.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(ret.UpdDate)]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// PRS用の会社情報取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="bushoCD"></param>
        /// <param name="tantoID"></param>
        /// <returns></returns>
        public KaishaInfoForPrsDto LoadMstBushoForPrs(
              string dbConnString
            , string bushoCD
            , string tantoID
            ) {
            var ret = new KaishaInfoForPrsDto();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX07.KaishaCD ");
            sb.AppendLine(",   MX07.KaishaName ");
            sb.AppendLine(",   LTRIM(MX07.SekininBusho + ' ' + MX07.SekininYaku) AS BushoYakushokuName ");
            sb.AppendLine(",   MX07.SekininName ");
            sb.AppendLine(",   MX07.TelNo ");
            sb.AppendLine(",   MX06.BushoName ");
            sb.AppendLine(",   MX03_S.TantoName AS ShikiMeireiTantoName ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX07Kaisha AS MX07 ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX03Tanto AS MX03 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.KaishaCD = MX07.KaishaCD ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX06Busho AS MX06 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.BushoCD = MX06.BushoCD ");
            sb.AppendLine("LEFT OUTER JOIN( ");
            sb.AppendLine("SELECT TOP(1) ");
            sb.AppendLine("    X03.* ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX03Tanto AS X03 ");
            sb.AppendLine("INNER JOIN( ");
            sb.AppendLine("    SELECT ");
            sb.AppendLine("        M03.BushoCD ");
            sb.AppendLine("    ,   MAX(M03.YakushokuKbn) AS YakushokuKbn ");
            sb.AppendLine("    FROM MX03Tanto AS M03 ");
            sb.AppendLine("    WHERE ");
            sb.AppendLine("        M03.BushoCD = @BushoCD ");
            sb.AppendLine("    GROUP BY ");
            sb.AppendLine("        M03.BushoCD ");
            sb.AppendLine(") AS MaxYakushokuKbn ");
            sb.AppendLine("ON ");
            sb.AppendLine("    X03.BushoCD = MaxYakushokuKbn.BushoCD ");
            sb.AppendLine("AND X03.YakushokuKbn = MaxYakushokuKbn.YakushokuKbn ");
            sb.AppendLine(")AS MX03_S ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03_S.BushoCD = MX03.BushoCD ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX03.TantoID = @TantoID ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", StringUtil.TostringNullForbid(bushoCD)));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", StringUtil.TostringNullForbid(tantoID)));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            ret.KaishaCD = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaCD)]);
                            ret.KaishaName = StringUtil.ToStringForbidNull(dr[nameof(ret.KaishaName)]);
                            ret.BushoYakushokuName = StringUtil.ToStringForbidNull(dr[nameof(ret.BushoYakushokuName)]);
                            ret.SekininName = StringUtil.ToStringForbidNull(dr[nameof(ret.SekininName)]);
                            ret.TelNo = StringUtil.ToStringForbidNull(dr[nameof(ret.TelNo)]);
                            ret.BushoName = StringUtil.ToStringForbidNull(dr[nameof(ret.BushoName)]);
                            ret.ShikiMeireiTantoName = StringUtil.ToStringForbidNull(dr[nameof(ret.ShikiMeireiTantoName)]);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}
