﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF1010_PrsEntryService {
        /// <summary>
        /// プロジェクト一覧ロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public List<SagyoNaiyoLooksGridDto> LoadProjectLooks(
              string dbConnString
            , LoadProjectLooksConDto con
            ) {
            var ret = new List<SagyoNaiyoLooksGridDto>();

            string strZengetshYYYYMM = new DateTime(int.Parse(con.TaishoYM.Substring(0, 4))
                                                  , int.Parse(con.TaishoYM.Substring(4, 2)), 1).AddMonths(-1).ToString(Formats._FORMAT_DATE_YYYYMM);

            // 対象年月条件取得
            string taishoYMJoken = GetTaishoYMJoken(con.TaishoYM
                                                  , strZengetshYYYYMM
                                                  , con.IsExistZengetsuJisseki
                                                  , con.IsExistKongetsuJisseki);

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con.GyomuKbn));
            string cmOutJisseki = StringUtil.GetCmout(!(con.IsExistKongetsuJisseki || con.IsExistZengetsuJisseki));
            string cmOutDeleted = StringUtil.GetCmout(!con.IsContainDeleted);
            string cmOutPBushoKbn = StringUtil.GetCmout(con.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    LEFT(VPX01.TaishoYM, 4) + '年' + CONVERT(VARCHAR,CONVERT(INT,RIGHT(VPX01.TaishoYM, 2))) + '月' AS TaishoYM_Disp ");
            sb.AppendLine(",   VPX01.KokyakuName ");
            sb.AppendLine(",   VPX01.TaishoYM AS TaishoYM ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine(",   VPX01.ProjectName ");
            sb.AppendLine(",   VPX01.KouteiName ");
            sb.AppendLine(",   VPX01.Detail ");
            sb.AppendLine(",   '＜' + VPX01.ProjectCD + '＞' + '（' + VPX01.KokyakuName + '）' + VPX01.ProjectName + '_' + VPX01.KouteiName + '_' + VPX01.Detail AS SagyoNaiyoFull ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    VPX01ProjectLooks AS VPX01 ");
            sb.AppendLine($@"{cmOutJisseki}INNER JOIN( ");
            sb.AppendLine($@"{cmOutJisseki}    SELECT DISTINCT ");
            sb.AppendLine($@"{cmOutJisseki}        P02.TantoID ");
            sb.AppendLine($@"{cmOutJisseki}    ,   @strYYYYMM  AS TaishoYM ");
            sb.AppendLine($@"{cmOutJisseki}    ,   P02.ProjectCD ");
            sb.AppendLine($@"{cmOutJisseki}    ,   P02.KouteiSeq ");
            sb.AppendLine($@"{cmOutJisseki}    ,   P02.DetailSeq ");
            sb.AppendLine($@"{cmOutJisseki}    FROM ");
            sb.AppendLine($@"{cmOutJisseki}        PX02SagyoMeisai AS P02 ");
            sb.AppendLine($@"{cmOutJisseki}    WHERE ");
            sb.AppendLine($@"{cmOutJisseki}        P02.TantoID = @tantoID ");
            sb.AppendLine($@"{cmOutJisseki}    AND FORMAT(P02.TaishoYMD, 'yyyyMM') IN({taishoYMJoken}) ");
            sb.AppendLine($@"{cmOutJisseki})AS PX02 ");
            sb.AppendLine($@"{cmOutJisseki}ON ");
            sb.AppendLine($@"{cmOutJisseki}    VPX01.TaishoYM  = PX02.TaishoYM ");
            sb.AppendLine($@"{cmOutJisseki}AND VPX01.ProjectCD = PX02.ProjectCD ");
            sb.AppendLine($@"{cmOutJisseki}AND VPX01.KouteiSeq = PX02.KouteiSeq ");
            sb.AppendLine($@"{cmOutJisseki}AND VPX01.DetailSeq = PX02.DetailSeq ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine("AND VPX01.TantoID  = @tantoID ");
            sb.AppendLine("AND VPX01.TaishoYM IN(@strYYYYMM)  ");
            sb.AppendLine("AND VPX01.StartYM <= @strYYYYMM ");
            sb.AppendLine("AND VPX01.EndYM   >= @strYYYYMM ");
            sb.AppendLine($@"{cmOutPBushoKbn}AND VPX01.PBushoKbn IN(0, @pBushoKbn) ");
            sb.AppendLine($@"{cmOutProjectCD}AND VPX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutGyomuKbn }AND SUBSTRING(VPX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{cmOutProjectNm}AND VPX01.ProjectName LIKE @projectName ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(con.KokyakuKbn, con.KokyakuName, "VPX01")} ");
            sb.AppendLine($@"{cmOutDeleted}AND VPX01.DelFlg   = 0 ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    VPX01.TaishoYM ");
            sb.AppendLine(",   VPX01.TantoID ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KokyakuName ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine("; ");


            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@strYYYYMM", con.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@tantoID", con.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", con.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", con.GyomuKbn));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", (int)con.PBushoKbn));
                    cmd.Parameters.AddWithValue("@projectName", "%" + con.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con.KokyakuName + "%");

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new SagyoNaiyoLooksGridDto();
                            dto.TaishoYM_Disp = StringUtil.ToStringForbidNull(dr[nameof(dto.TaishoYM_Disp)]);
                            dto.TaishoYM = StringUtil.ToStringForbidNull(dr[nameof(dto.TaishoYM)]);
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiSeq)]);
                            dto.DetailSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.DetailSeq)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.Detail = StringUtil.ToStringForbidNull(dr[nameof(dto.Detail)]);
                            dto.SagyoNaiyoFull = StringUtil.ToStringForbidNull(dr[nameof(dto.SagyoNaiyoFull)]);
                            dto.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(dto.KokyakuName)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 対象年月条件取得
        /// </summary>
        /// <param name="taishoYM"></param>
        /// <param name="zengetshYM"></param>
        /// <param name="existZengetsuJisseki"></param>
        /// <param name="existKongetsuJisseki"></param>
        /// <returns></returns>
        private string GetTaishoYMJoken(
              string taishoYM
            , string zengetshYM
            , bool existZengetsuJisseki
            , bool existKongetsuJisseki
            ) {
            string taishoYMJokenUnit = "'{0}'";
            List<string> taishoJokenList = new List<string>();
            if (existZengetsuJisseki) {
                taishoJokenList.Add(string.Format(taishoYMJokenUnit, zengetshYM));
            }
            if (existKongetsuJisseki) {
                taishoJokenList.Add(string.Format(taishoYMJokenUnit, taishoYM));
            }
            var array = taishoJokenList.ToArray();
            string ret = string.Empty;
            if (0 < array.Length) {
                ret = string.Join(",", array);
            }
            return ret;
        }
        /// <summary>
        /// 作業明細検索
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="minTaishoYMD"></param>
        /// <param name="maxTaishoYMD"></param>
        /// <returns></returns>
        public List<PX02SagyoMeisaiEntity> SearchSagyoMeisai(
              string dbConnString
            , string tantoID
            , string minTaishoYMD
            , string maxTaishoYMD
            ) {

            var ret = new List<PX02SagyoMeisaiEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    TaishoYMD ");
            sb.AppendLine(",   TantoID ");
            sb.AppendLine(",   StartTime ");
            sb.AppendLine(",   EndTime ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   SagyoTimeH ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX02SagyoMeisai AS PX02 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    (@minTaishoYMD <= PX02.TaishoYMD  AND PX02.TaishoYMD <= @maxTaishoYMD) ");
            sb.AppendLine("AND PX02.TantoID   = @tantoID ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@minTaishoYMD", minTaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@maxTaishoYMD", maxTaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@tantoID", tantoID));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new PX02SagyoMeisaiEntity();
                            dto.TaishoYMD = StringUtil.ToStringForbidNull(dr[nameof(dto.TaishoYMD)]);
                            dto.TantoID = StringUtil.ToStringForbidNull(dr[nameof(dto.TantoID)]);
                            dto.StartTime = TypeConversionUtil.ToDateTime(dr[nameof(dto.StartTime)]);
                            dto.EndTime = TypeConversionUtil.ToDateTime(dr[nameof(dto.EndTime)]);
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiSeq)]);
                            dto.DetailSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.DetailSeq)]);
                            dto.SagyoTimeH = TypeConversionUtil.ToInteger(dr[nameof(dto.SagyoTimeH)]);
                            dto.AddDate = TypeConversionUtil.ToDateTime(dr[nameof(dto.AddDate)]);
                            dto.UpdDate = TypeConversionUtil.ToDateTime(dr[nameof(dto.UpdDate)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 作業明細登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="AddEntity"></param>
        /// <returns></returns>
        public void InsertSagyoMeisai(
              string dbConnString
            , PX02SagyoMeisaiEntity AddEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    PX02SagyoMeisai( ");
            sb.AppendLine("    TaishoYMD ");
            sb.AppendLine(",   TantoID ");
            sb.AppendLine(",   StartTime ");
            sb.AppendLine(",   EndTime ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   SagyoTimeH ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @TaishoYMD ");
            sb.AppendLine(",   @TantoID ");
            sb.AppendLine(",   @StartTime ");
            sb.AppendLine(",   @EndTime ");
            sb.AppendLine(",   @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @DetailSeq ");
            sb.AppendLine(",   @SagyoTimeH ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TaishoYMD", AddEntity.TaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", AddEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@StartTime", AddEntity.StartTime));
                    cmd.Parameters.Add(new SqlParameter("@EndTime", AddEntity.EndTime));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", AddEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", AddEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", AddEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@SagyoTimeH", AddEntity.SagyoTimeH));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", AddEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", AddEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 作業明細が存在するか？
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool JudgeIsExistRec(
              string dbConnString
            , PX02SagyoMeisaiEntity entity
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    COUNT(*) ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX02SagyoMeisai AS PX02 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    PX02.TaishoYMD = @TaishoYMD ");
            sb.AppendLine("AND PX02.TantoID   = @TantoID ");
            sb.AppendLine("AND PX02.StartTime = @StartTime ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TaishoYMD", entity.TaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", entity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@StartTime", entity.StartTime));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            int i = TypeConversionUtil.ToInteger(dr[0]);
                            return 0 < i;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// 作業明細更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdSagyoMeisai(
              string dbConnString
            , PX02SagyoMeisaiEntity updEntity
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    PX02SagyoMeisai ");
            sb.AppendLine("SET ");
            sb.AppendLine("    EndTime    = @EndTime ");
            sb.AppendLine(",   ProjectCD  = @ProjectCD ");
            sb.AppendLine(",   KouteiSeq  = @KouteiSeq ");
            sb.AppendLine(",   DetailSeq  = @DetailSeq ");
            sb.AppendLine(",   SagyoTimeH = @SagyoTimeH ");
            sb.AppendLine(",   UpdDate    = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYMD = @TaishoYMD ");
            sb.AppendLine("AND TantoID   = @TantoID ");
            sb.AppendLine("AND StartTime = @StartTime ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@EndTime", updEntity.EndTime));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", updEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", updEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@SagyoTimeH", updEntity.SagyoTimeH));
                    cmd.Parameters.Add(new SqlParameter("@TaishoYMD", updEntity.TaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", updEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@StartTime", updEntity.StartTime));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 作業明細削除
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="delEntity"></param>
        /// <returns></returns>
        public void DelSagyoMeisai(
              string dbConnString
            , PX02SagyoMeisaiEntity delEntity
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("DELETE FROM ");
            sb.AppendLine("    PX02SagyoMeisai ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYMD = @TaishoYMD ");
            sb.AppendLine("AND TantoID   = @TantoID ");
            sb.AppendLine("AND StartTime = @StartTime ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TaishoYMD", delEntity.TaishoYMD));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", delEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@StartTime", delEntity.StartTime));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 作業実績取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<WorkItemEntity> GetSagyoJisseki(
              string dbConnString
            , string tantoID
            , DateTime startDate
            , DateTime endDate
            ) {
            var ret = new List<WorkItemEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT DISTINCT ");
            sb.AppendLine("    ROW_NUMBER() OVER(ORDER BY PX02.StartTime,EndTime) AS No ");
            sb.AppendLine(",   MX01.ProjectCD  AS ProjectCode");
            sb.AppendLine(",   MX01.ProjectName  ");
            sb.AppendLine(",   MX02.KouteiName");
            sb.AppendLine(",   MX05.Detail");
            sb.AppendLine(",   FORMAT(PX02.StartTime, 'yyyyMMddHHmm') AS DtStart ");
            sb.AppendLine(",   FORMAT(PX02.EndTime, 'yyyyMMddHHmm')   AS DtEnd ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX02SagyoMeisai AS PX02 ");
            sb.AppendLine("INNER JOIN ");
            sb.AppendLine("    MX01Project AS MX01 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX01.ProjectCD = PX02.ProjectCD ");
            sb.AppendLine("INNER JOIN ");
            sb.AppendLine("    MX02Koutei AS MX02 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX02.ProjectCD = PX02.ProjectCD ");
            sb.AppendLine("AND MX02.KouteiSeq = PX02.KouteiSeq ");
            sb.AppendLine("INNER JOIN ");
            sb.AppendLine("    PX01ProjectDetail AS PX01 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    PX01.TaishoYM = FORMAT(PX02.TaishoYMD, 'yyyyMM') ");
            sb.AppendLine("AND PX01.ProjectCD = PX02.ProjectCD ");
            sb.AppendLine("AND PX01.KouteiSeq = PX02.KouteiSeq ");
            sb.AppendLine("AND PX01.DetailSeq = PX02.DetailSeq ");
            sb.AppendLine("INNER JOIN( ");
            sb.AppendLine("    SELECT * FROM MX05Detail AS M05 ");
            sb.AppendLine("    WHERE ");
            sb.AppendLine("        M05.TantoID  = @TantoID ");
            sb.AppendLine(")AS MX05 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX05.TantoID   = PX01.TantoID ");
            sb.AppendLine("AND MX05.ProjectCD = PX01.ProjectCD ");
            sb.AppendLine("AND MX05.KouteiSeq = PX01.KouteiSeq ");
            sb.AppendLine("AND MX05.DetailSeq = PX01.DetailSeq ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    PX02.TantoID = @TantoID ");
            sb.AppendLine("AND @StartYMD <= PX02.TaishoYMD ");
            sb.AppendLine("AND PX02.TaishoYMD <= @EndYMD ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@StartYMD", startDate.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@EndYMD", endDate.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH)));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new WorkItemEntity();
                            dto.No = StringUtil.ToStringForbidNull(dr[nameof(dto.No)]);
                            dto.ProjectCode = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCode)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.Detail = StringUtil.ToStringForbidNull(dr[nameof(dto.Detail)]);
                            dto.DtStart = StringUtil.ToStringForbidNull(dr[nameof(dto.DtStart)]);
                            dto.DtEnd = StringUtil.ToStringForbidNull(dr[nameof(dto.DtEnd)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
    }
}
