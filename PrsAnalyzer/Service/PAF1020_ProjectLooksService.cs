﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF1020_ProjectLooksService {
        /// <summary>
        /// プロジェクト一覧ロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public List<VPX01ProjectLooksEntity> LoadProjectLooks(
              string dbConnString
            , LoadProjectLooksConDto con
            ) {
            var ret = new List<VPX01ProjectLooksEntity>();

            string strZengetshYYYYMM = new DateTime(int.Parse(con.TaishoYM.Substring(0, 4))
                                                  , int.Parse(con.TaishoYM.Substring(4, 2)), 1).AddMonths(-1).ToString(Formats._FORMAT_DATE_YYYYMM);

            // 対象年月条件取得
            string taishoYMJoken = GetTaishoYMJoken(con.TaishoYM
                                                  , strZengetshYYYYMM
                                                  , con.IsExistZengetsuJisseki
                                                  , con.IsExistKongetsuJisseki);

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con.GyomuKbn));
            string cmOutZengetsuJisseki = StringUtil.GetCmout(!(con.IsExistKongetsuJisseki || con.IsExistZengetsuJisseki));
            string cmOutDeleted = StringUtil.GetCmout(con.IsContainDeleted);
            string cmOutDetailBrankOnly = StringUtil.GetCmout(!con.IsDetailBrankOnly);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    VPX01.TaishoYM ");
            sb.AppendLine(",   VPX01.TantoID ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine(",   VPX01.PBushoKbn ");
            sb.AppendLine(",   VPX01.KokyakuName ");
            sb.AppendLine(",   VPX01.ProjectName ");
            sb.AppendLine(",   VPX01.KouteiName ");
            sb.AppendLine(",   VPX01.Iraimoto ");
            sb.AppendLine(",   VPX01.Detail ");
            sb.AppendLine(",   VPX01.PBushoKbn ");
            sb.AppendLine(",   VPX01.Biko ");
            sb.AppendLine(",   VPX01.DelFlg ");
            sb.AppendLine(",   VPX01.AddDate ");
            sb.AppendLine(",   VPX01.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    VPX01ProjectLooks AS VPX01 ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}INNER JOIN( ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    SELECT DISTINCT ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}        P02.TantoID ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    ,   @taishoYM  AS TaishoYM ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    ,   P02.ProjectCD ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    ,   P02.KouteiSeq ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    ,   P02.DetailSeq ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    FROM ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}        PX02SagyoMeisai AS P02 ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    WHERE ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}        P02.TantoID = @tantoId ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    AND FORMAT(P02.TaishoYMD, 'yyyyMM') IN({taishoYMJoken}) ");
            sb.AppendLine($@"{cmOutZengetsuJisseki})AS PX02 ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}ON ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}    VPX01.TaishoYM  = PX02.TaishoYM ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}AND VPX01.ProjectCD = PX02.ProjectCD ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}AND VPX01.KouteiSeq = PX02.KouteiSeq ");
            sb.AppendLine($@"{cmOutZengetsuJisseki}AND VPX01.DetailSeq = PX02.DetailSeq ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    VPX01.TantoID   = @tantoId ");
            sb.AppendLine("AND VPX01.TaishoYM  = @taishoYM  ");
            sb.AppendLine("AND VPX01.PBushoKbn IN(0, @pBushoKbn)  ");
            sb.AppendLine("AND VPX01.StartYM  <= @taishoYM ");
            sb.AppendLine("AND VPX01.EndYM    >= @taishoYM ");
            sb.AppendLine($@"{cmOutProjectCD}AND VPX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutGyomuKbn }AND SUBSTRING(VPX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{cmOutProjectNm}AND VPX01.ProjectName LIKE @projectName ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(con.KokyakuKbn, con.KokyakuName, "VPX01")} ");
            sb.AppendLine($@"{cmOutDetailBrankOnly}AND VPX01.Detail  = '' ");
            sb.AppendLine($@"{cmOutDeleted}AND VPX01.DelFlg  = 0 ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    VPX01.DelFlg ");
            sb.AppendLine(",   VPX01.TaishoYM ");
            sb.AppendLine(",   VPX01.TantoID ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", con.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@tantoId", con.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", con.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", con.GyomuKbn));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con.PBushoKbn));
                    cmd.Parameters.AddWithValue("@projectName", "%" + con.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con.KokyakuName + "%");

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new VPX01ProjectLooksEntity();
                            entity.TaishoYM = StringUtil.ToStringForbidNull(dr[nameof(entity.TaishoYM)]);
                            entity.TantoID = StringUtil.ToStringForbidNull(dr[nameof(entity.TantoID)]);
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiSeq)]);
                            entity.DetailSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.DetailSeq)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]);
                            entity.Iraimoto = StringUtil.ToStringForbidNull(dr[nameof(entity.Iraimoto)]);
                            entity.Detail = StringUtil.ToStringForbidNull(dr[nameof(entity.Detail)]);
                            entity.PBushoKbn = StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]);
                            entity.Biko = StringUtil.ToStringForbidNull(dr[nameof(entity.Biko)]);
                            entity.DelFlg = StringUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                            entity.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            entity.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            if (!con.IsContainX && entity.ProjectCD.Substring(0, 1) == ProjectConst.GyomuKbn.X.ToString()) {
                                continue;
                            }
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 対象年月条件取得
        /// </summary>
        /// <param name="taishoYM"></param>
        /// <param name="zengetsuYM"></param>
        /// <param name="existZengetsuJisseki"></param>
        /// <param name="existKongetsuJisseki"></param>
        /// <returns></returns>
        private string GetTaishoYMJoken(
              string taishoYM
            , string zengetsuYM
            , bool existZengetsuJisseki
            , bool existKongetsuJisseki
            ) {
            string taishoYMJokenUnit = "'{0}'";
            List<string> taishoJokenList = new List<string>();
            if (existZengetsuJisseki) {
                taishoJokenList.Add(string.Format(taishoYMJokenUnit, zengetsuYM));
            }
            if (existKongetsuJisseki) {
                taishoJokenList.Add(string.Format(taishoYMJokenUnit, taishoYM));
            }
            var array = taishoJokenList.ToArray();
            string ret = string.Empty;
            if (0 < array.Length) {
                ret = string.Join(",", array);
            }
            return ret;
        }
        /// <summary>
        /// プロジェクト一覧ロード（作業実績があるレコード限定）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="taishoYM"></param>
        /// <param name="pBushoKbn"></param>
        /// <returns></returns>
        public List<VPX01ProjectLooksEntity> LoadProjectLooksJoinP02(
              string dbConnString
            , string tantoID
            , string taishoYM
            , int pBushoKbn
            ) {
            var ret = new List<VPX01ProjectLooksEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT DISTINCT ");
            sb.AppendLine("    VPX01.TaishoYM ");
            sb.AppendLine(",   VPX01.TantoID ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine(",   VPX01.PBushoKbn ");
            sb.AppendLine(",   VPX01.KokyakuName ");
            sb.AppendLine(",   VPX01.ProjectName ");
            sb.AppendLine(",   VPX01.KouteiName ");
            sb.AppendLine(",   VPX01.Iraimoto ");
            sb.AppendLine(",   VPX01.Detail ");
            sb.AppendLine(",   VPX01.Biko ");
            sb.AppendLine(",   VPX01.DelFlg ");
            sb.AppendLine(",   CURRENT_TIMESTAMP AS AddDate ");
            sb.AppendLine(",   CURRENT_TIMESTAMP AS UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    VPX01ProjectLooks AS VPX01 ");
            sb.AppendLine("INNER JOIN ");
            sb.AppendLine("    PX02SagyoMeisai AS PX02 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    PX02.TantoID   = VPX01.TantoID ");
            sb.AppendLine("AND FORMAT(PX02.TaishoYMD,'yyyyMM')  = VPX01.TaishoYM ");
            sb.AppendLine("AND PX02.ProjectCD = VPX01.ProjectCD ");
            sb.AppendLine("AND PX02.KouteiSeq = VPX01.KouteiSeq ");
            sb.AppendLine("AND PX02.DetailSeq = VPX01.DetailSeq ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    VPX01.TantoID   = @tantoId ");
            sb.AppendLine("AND VPX01.TaishoYM  = @taishoYM  ");
            sb.AppendLine("AND VPX01.PBushoKbn IN(0, @pBushoKbn) ");
            sb.AppendLine("AND VPX01.StartYM  <= @taishoYM ");
            sb.AppendLine("AND VPX01.EndYM    >= @taishoYM ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    VPX01.DelFlg ");
            sb.AppendLine(",   VPX01.TaishoYM ");
            sb.AppendLine(",   VPX01.TantoID ");
            sb.AppendLine(",   VPX01.ProjectCD ");
            sb.AppendLine(",   VPX01.KouteiSeq ");
            sb.AppendLine(",   VPX01.DetailSeq ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", pBushoKbn));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new VPX01ProjectLooksEntity();
                            entity.TaishoYM = StringUtil.ToStringForbidNull(dr[nameof(entity.TaishoYM)]);
                            entity.TantoID = StringUtil.ToStringForbidNull(dr[nameof(entity.TantoID)]);
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiSeq)]);
                            entity.DetailSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.DetailSeq)]);
                            entity.PBushoKbn = StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]);
                            entity.Iraimoto = StringUtil.ToStringForbidNull(dr[nameof(entity.Iraimoto)]);
                            entity.Detail = StringUtil.ToStringForbidNull(dr[nameof(entity.Detail)]);
                            entity.Biko = StringUtil.ToStringForbidNull(dr[nameof(entity.Biko)]);
                            entity.DelFlg = StringUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                            entity.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            entity.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            if (entity.ProjectCD.Substring(0, 1) == ProjectConst.GyomuKbn.X.ToString()) {
                                continue;
                            }
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// プロジェクト一覧登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddProjectLooks(
              string dbConnString
            , VPX01ProjectLooksEntity addEntity
            ) {

            // プロジェクト詳細テーブル登録
            InsertProjectDetail(dbConnString, addEntity);

            // 詳細マスタ登録
            InsetMX05(dbConnString, addEntity);
        }
        /// <summary>
        /// プロジェクト詳細テーブル登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        private void InsertProjectDetail(
              string dbConnString
            , VPX01ProjectLooksEntity addEntity
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    PX01ProjectDetail( ");
            sb.AppendLine("    ProjectCD ");
            sb.AppendLine(",   TaishoYM ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   TantoID ");
            sb.AppendLine(",   Biko ");
            sb.AppendLine(",   DelFlg ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @ProjectCD ");
            sb.AppendLine(",   @TaishoYM ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @DetailSeq ");
            sb.AppendLine(",   @TantoID ");
            sb.AppendLine(",   @Biko ");
            sb.AppendLine(",   @DelFlg ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@TaishoYM", addEntity.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", addEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", addEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@Biko", StringUtil.TostringNullForbid(addEntity.Biko)));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", addEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 詳細マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        private void InsetMX05(
              string dbConnString
            , VPX01ProjectLooksEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX05Detail( ");
            sb.AppendLine("    TantoID ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   Detail ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @TantoID ");
            sb.AppendLine(",   @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @DetailSeq ");
            sb.AppendLine(",   @Detail ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", addEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", addEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@Detail", addEntity.Detail));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// プロジェクト一覧更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="AddEntity"></param>
        /// <returns></returns>
        public void UpdProjectLooks(
              string dbConnString
            , VPX01ProjectLooksEntity updEntity
            ) {

            // プロジェクト詳細テーブル更新
            UpdPX01(dbConnString, updEntity);

            // 詳細マスタ更新
            UpdMX05(dbConnString, updEntity);
        }
        /// <summary>
        /// プロジェクト詳細テーブル更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        private void UpdPX01(
              string dbConnString
            , VPX01ProjectLooksEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    PX01ProjectDetail ");
            sb.AppendLine("SET ");
            sb.AppendLine("    Biko     = @Biko ");
            sb.AppendLine(",   DelFlg   = @DelFlg ");
            sb.AppendLine(",   UpdDate  = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID   = @TantoID ");
            sb.AppendLine("AND TaishoYM  = @TaishoYM ");
            sb.AppendLine("AND ProjectCD = @ProjectCD ");
            sb.AppendLine("AND KouteiSeq = @KouteiSeq ");
            sb.AppendLine("AND DetailSeq = @DetailSeq ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@TaishoYM", updEntity.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", updEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", updEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@TantoID", updEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@Biko", StringUtil.TostringNullForbid(updEntity.Biko)));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", updEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 詳細マスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        private void UpdMX05(
              string dbConnString
            , VPX01ProjectLooksEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX05Detail ");
            sb.AppendLine("SET ");
            sb.AppendLine("    Detail   = @Detail ");
            sb.AppendLine(",   UpdDate  = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID   = @TantoID ");
            sb.AppendLine("AND ProjectCD = @ProjectCD ");
            sb.AppendLine("AND KouteiSeq = @KouteiSeq ");
            sb.AppendLine("AND DetailSeq = @DetailSeq ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", updEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", updEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@DetailSeq", updEntity.DetailSeq));
                    cmd.Parameters.Add(new SqlParameter("@Detail", updEntity.Detail));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 最大の詳細Seq取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="projectCD"></param>
        /// <param name="kouteiSeq"></param>
        /// <returns></returns>
        public int GetMaxDetailSeq(
              string dbConnString
            , string tantoID
            , string projectCD
            , string kouteiSeq
            ) {

            int ret = 0;

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MAX(MX05.DetailSeq) AS MaxDetailSeq ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX05Detail AS MX05 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX05.TantoID   = @TantoID ");
            sb.AppendLine("AND MX05.ProjectCD = @ProjectCD ");
            sb.AppendLine("AND MX05.KouteiSeq = @KouteiSeq ");
            sb.AppendLine("GROUP BY ");
            sb.AppendLine("    MX05.TantoID ");
            sb.AppendLine(",   MX05.ProjectCD ");
            sb.AppendLine(",   MX05.KouteiSeq ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", projectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", kouteiSeq));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            ret = TypeConversionUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return ret;
        }
    }
}
