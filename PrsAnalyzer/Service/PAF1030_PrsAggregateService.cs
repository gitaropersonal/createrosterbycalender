﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Util;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF1030_PrsAggregateService {
        /// <summary>
        /// PRS集計結果1取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <returns></returns>
        public List<PrsAggregate1GridDto> LoadPrsAggregate1(
              string dbConnString
            , LoadAggregateConDto con1
            , LoadProjectLooksConDto con2
            ) {

            var ret = new List<PrsAggregate1GridDto>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con2.GyomuKbn));
            string cmOutX = con1.IsContainX ? "--" : string.Empty;
            string cmOutPBushoKbn = StringUtil.GetCmout(con1.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    CASE WHEN DATAS.ProjectCD = 'Z999999' THEN '' ");
            sb.AppendLine($@"         ELSE DATAS.ProjectCD ");
            sb.AppendLine($@"    END AS ProjectCD ");
            sb.AppendLine($@",   DATAS.ProjectName ");
            sb.AppendLine($@",   DATAS.KokyakuName ");
            sb.AppendLine($@",   DATAS.KouteiName ");
            sb.AppendLine($@",   DATAS.WorkTimeH ");
            sb.AppendLine($@",   DATAS.KousuNinNichi ");
            sb.AppendLine($@",   DATAS.Kousu ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"( ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   1 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"    AND PX02.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName LIKE @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@"    GROUP BY ");
            sb.AppendLine($@"        PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        'Z999999' AS ProjectCD ");
            sb.AppendLine($@"    ,   '' AS ProjectName ");
            sb.AppendLine($@"    ,   '' AS KokyakuName ");
            sb.AppendLine($@"    ,   '999999' AS KouteiSeq ");
            sb.AppendLine($@"    ,   '（合計）' AS KouteiName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   100 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"    AND PX02.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName LIKE @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@") AS DATAS ");
            sb.AppendLine($@"ORDER BY ");
            sb.AppendLine($@"    DATAS.ProjectCD ");
            sb.AppendLine($@",   DATAS.KouteiSeq ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_S", con1.StrStartDt));
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_E", con1.StrEndDt));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con1.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@WorkTimeDay", con1.WorkTimeDay));
                    cmd.Parameters.Add(new SqlParameter("@MonthHour", con1.KijunTimeMonth));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", con2.ProjectCD));
                    cmd.Parameters.AddWithValue("@ProjectName", "%" + con2.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con2.KokyakuName + "%");
                    cmd.Parameters.Add(new SqlParameter("@GyomuKbn", con2.GyomuKbn));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new PrsAggregate1GridDto();
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(dto.KokyakuName)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.WorkTimeH = StringUtil.ToStringForbidNull(dr[nameof(dto.WorkTimeH)]);
                            dto.KousuNinNichi = StringUtil.ToStringForbidNull(dr[nameof(dto.KousuNinNichi)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// PRS集計結果2取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <returns></returns>
        public List<PrsAggregate2GridDto> LoadPrsAggregate2(
              string dbConnString
            , LoadAggregateConDto con1
            , LoadProjectLooksConDto con2
            ) {

            var ret = new List<PrsAggregate2GridDto>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con2.GyomuKbn));
            string cmOutX = con1.IsContainX ? "--" : string.Empty;
            string cmOutPBushoKbn = StringUtil.GetCmout(con1.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    DATAS.ProjectCD ");
            sb.AppendLine($@",   CASE WHEN DATAS.TantoID = 'ZZZZZZ' THEN '' ");
            sb.AppendLine($@"         ELSE DATAS.ProjectCD ");
            sb.AppendLine($@"    END AS ProjectCD_Disp ");
            sb.AppendLine($@",   DATAS.ProjectName ");
            sb.AppendLine($@",   DATAS.KokyakuName ");
            sb.AppendLine($@",   DATAS.KouteiName ");
            sb.AppendLine($@",   CASE WHEN DATAS.TantoID = 'ZZZZZZ' THEN '' ");
            sb.AppendLine($@"         ELSE DATAS.TantoID ");
            sb.AppendLine($@"    END AS TantoID ");
            sb.AppendLine($@",   DATAS.TantoName ");
            sb.AppendLine($@",   DATAS.WorkTimeH ");
            sb.AppendLine($@",   DATAS.KousuNinNichi ");
            sb.AppendLine($@",   DATAS.Kousu ");
            sb.AppendLine($@"FROM( ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    ,   PX02.TantoID ");
            sb.AppendLine($@"    ,   MX03.TantoName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   1 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"    AND PX02.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX03Tanto AS MX03 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.TantoID = MX03.TantoID ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName LIKE @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@"    GROUP BY ");
            sb.AppendLine($@"        PX02.TantoID ");
            sb.AppendLine($@"    ,   MX03.TantoName ");
            sb.AppendLine($@"    ,   PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        PX02.ProjectCD ");
            sb.AppendLine($@"    ,   '' AS ProjectName ");
            sb.AppendLine($@"    ,   '' AS Iraimoto ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   '' AS KouteiName ");
            sb.AppendLine($@"    ,   'ZZZZZZ' AS TantoID ");
            sb.AppendLine($@"    ,   '（小計）' AS TantoName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   10 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName LIKE @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@"    GROUP BY ");
            sb.AppendLine($@"        PX02.ProjectCD ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        'Z9999999' ");
            sb.AppendLine($@"    ,   ''  AS ProjectName ");
            sb.AppendLine($@"    ,   ''  AS KokyakuName ");
            sb.AppendLine($@"    ,   999 AS KouteiSeq ");
            sb.AppendLine($@"    ,   ''  AS KouteiName ");
            sb.AppendLine($@"    ,   'ZZZZZZ' AS TantoID ");
            sb.AppendLine($@"    ,   '（合計）' AS TantoName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   100 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S" );
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName LIKE @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@") AS DATAS ");
            sb.AppendLine($@"ORDER BY ");
            sb.AppendLine($@"    DATAS.ProjectCD ");
            sb.AppendLine($@",   DATAS.KouteiSeq ");
            sb.AppendLine($@",   DATAS.TantoID ");
            sb.AppendLine($@",   DATAS.OrderKBN ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_S", con1.StrStartDt));
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_E", con1.StrEndDt));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con1.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@WorkTimeDay", con1.WorkTimeDay));
                    cmd.Parameters.Add(new SqlParameter("@MonthHour", con1.KijunTimeMonth));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", con2.ProjectCD));
                    cmd.Parameters.AddWithValue("@ProjectName", "%" + con2.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con2.KokyakuName + "%");
                    cmd.Parameters.Add(new SqlParameter("@GyomuKbn", con2.GyomuKbn));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new PrsAggregate2GridDto();
                            dto.ProjectCD_Disp = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD_Disp)]);
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(dto.KokyakuName)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.TantoName = StringUtil.ToStringForbidNull(dr[nameof(dto.TantoName)]);
                            dto.WorkTimeH = StringUtil.ToStringForbidNull(dr[nameof(dto.WorkTimeH)]);
                            dto.KousuNinNichi = StringUtil.ToStringForbidNull(dr[nameof(dto.KousuNinNichi)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// PRS集計結果3取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <returns></returns>
        public List<PrsAggregate3GridDto> LoadPrsAggregate3(
              string dbConnString
            , LoadAggregateConDto con1
            , LoadProjectLooksConDto con2
            ) {

            var ret = new List<PrsAggregate3GridDto>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con2.GyomuKbn));
            string cmOutX = con1.IsContainX ? "--" : string.Empty;
            string cmOutPBushoKbn = StringUtil.GetCmout(con1.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    DATAS.ProjectCD ");
            sb.AppendLine($@",   DATAS.KokyakuName ");
            sb.AppendLine($@",   DATAS.TantoID ");
            sb.AppendLine($@",   CASE WHEN DATAS.KouteiSeq = '999999' THEN '' ");
            sb.AppendLine($@"         ELSE DATAS.TantoName ");
            sb.AppendLine($@"    END AS TantoName ");
            sb.AppendLine($@",   DATAS.ProjectName ");
            sb.AppendLine($@",   DATAS.KouteiName ");
            sb.AppendLine($@",   DATAS.WorkTimeH ");
            sb.AppendLine($@",   DATAS.KousuNinNichi ");
            sb.AppendLine($@",   DATAS.Kousu ");
            sb.AppendLine($@"FROM( ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        PX02.TantoID ");
            sb.AppendLine($@"    ,   MX03.TantoName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   1 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"    AND PX02.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX03Tanto AS MX03 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.TantoID = MX03.TantoID ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName = @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@"    GROUP BY ");
            sb.AppendLine($@"        PX02.TantoID ");
            sb.AppendLine($@"    ,   MX03.TantoName ");
            sb.AppendLine($@"    ,   PX02.ProjectCD ");
            sb.AppendLine($@"    ,   MX01.ProjectName ");
            sb.AppendLine($@"    ,   MX01.KokyakuName ");
            sb.AppendLine($@"    ,   PX02.KouteiSeq ");
            sb.AppendLine($@"    ,   MX02.KouteiName ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        PX02.TantoID ");
            sb.AppendLine($@"    ,   '' AS TantoName ");
            sb.AppendLine($@"    ,   '' AS KokyakuName ");
            sb.AppendLine($@"    ,   'Z9999999' AS ProjectCD ");
            sb.AppendLine($@"    ,   '' AS ProjectName ");
            sb.AppendLine($@"    ,   '999999' AS KouteiSeq ");
            sb.AppendLine($@"    ,   '（小計）' AS KouteiName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   10 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName = @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@"    GROUP BY ");
            sb.AppendLine($@"        PX02.TantoID ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT ");
            sb.AppendLine($@"        'ZZZZZZ' AS TantoID ");
            sb.AppendLine($@"    ,   '' AS TantoName ");
            sb.AppendLine($@"    ,   '' AS KokyakuName ");
            sb.AppendLine($@"    ,   'Z9999999' AS ProjectCD ");
            sb.AppendLine($@"    ,   '' AS ProjectName ");
            sb.AppendLine($@"    ,   '999999' AS KouteiSeq ");
            sb.AppendLine($@"    ,   '（合計）' AS KouteiName ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 1), SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60)) AS WorkTimeH ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 2),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @WorkTimeDay, 2)) AS KousuNinNichi ");
            sb.AppendLine($@"    ,   CONVERT(DECIMAL(7, 3),ROUND(SUM(CONVERT(DECIMAL(7, 1), PX02.SagyoTimeH) / 60) / @MonthHour, 3)) AS Kousu ");
            sb.AppendLine($@"    ,   100 AS OrderKBN ");
            sb.AppendLine($@"    FROM ");
            sb.AppendLine($@"        PX02SagyoMeisai AS PX02 ");
            sb.AppendLine($@"    LEFT OUTER JOIN ");
            sb.AppendLine($@"        MX01Project AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX02.TaishoYMD >= @taishoDate_S ");
            sb.AppendLine($@"    AND PX02.TaishoYMD <= @taishoDate_E ");
            sb.AppendLine($@"    {cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"    {cmOutProjectCD}AND PX02.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"    {cmOutProjectNm}AND MX01.ProjectName = @ProjectName ");
            sb.AppendLine($@"    {ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"    {cmOutGyomuKbn}AND SUBSTRING(PX02.ProjectCD,1,1) = @GyomuKbn ");
            sb.AppendLine($@"    {cmOutX}AND SUBSTRING(PX02.ProjectCD,1,1) <> 'X' ");
            sb.AppendLine($@") AS DATAS ");
            sb.AppendLine($@"ORDER BY ");
            sb.AppendLine($@"    DATAS.TantoID ");
            sb.AppendLine($@",   DATAS.ProjectCD ");
            sb.AppendLine($@",   DATAS.KouteiSeq ");
            sb.AppendLine($@",   DATAS.OrderKBN ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_S", con1.StrStartDt));
                    cmd.Parameters.Add(new SqlParameter("@taishoDate_E", con1.StrEndDt));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con1.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@WorkTimeDay", con1.WorkTimeDay));
                    cmd.Parameters.Add(new SqlParameter("@MonthHour", con1.KijunTimeMonth));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", con2.ProjectCD));
                    cmd.Parameters.AddWithValue("@ProjectName", "%" + con2.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con2.KokyakuName + "%");
                    cmd.Parameters.Add(new SqlParameter("@GyomuKbn", con2.GyomuKbn));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new PrsAggregate3GridDto();
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(dto.KokyakuName)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.TantoName = StringUtil.ToStringForbidNull(dr[nameof(dto.TantoName)]);
                            dto.TantoID = StringUtil.ToStringForbidNull(dr[nameof(dto.TantoID)]);
                            dto.WorkTimeH = StringUtil.ToStringForbidNull(dr[nameof(dto.WorkTimeH)]);
                            dto.KousuNinNichi = StringUtil.ToStringForbidNull(dr[nameof(dto.KousuNinNichi)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
    }
}
