﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF2010_AnkenExcelCaptureService {
        /// <summary>
        /// 案件Excelデータロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="taishoYM"></param>
        /// <returns></returns>
        public List<AnkenExcelDto> LoadAnkenExcel(
              string dbConnString
            , string taishoYM
            ) {
            var ret = new List<AnkenExcelDto>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.Iraimoto ");
            sb.AppendLine(",   PX03.Kousu ");
            sb.AppendLine(",   PX03.KenshuJoken ");
            sb.AppendLine(",   PX03.Biko ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX03AnkenExcel AS PX03 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYM  = @taishoYM ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.Iraimoto ");
            sb.AppendLine(",   PX03.Kousu ");
            sb.AppendLine(",   PX03.KenshuJoken ");
            sb.AppendLine(",   PX03.Biko ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new AnkenExcelDto();
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiSeq)]);
                            dto.Iraimoto = StringUtil.ToStringForbidNull(dr[nameof(dto.Iraimoto)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            dto.KenshuJoken = StringUtil.ToStringForbidNull(dr[nameof(dto.KenshuJoken)]);
                            dto.Biko = StringUtil.ToStringForbidNull(dr[nameof(dto.Biko)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 案件Excelデータ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddAnekenExcel(
              string dbConnString
            , PX03AnkenExcelEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    PX03AnkenExcel( ");
            sb.AppendLine("    TaishoYM ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   PBushoKbn ");
            sb.AppendLine(",   Iraimoto ");
            sb.AppendLine(",   Kousu ");
            sb.AppendLine(",   KenshuJoken ");
            sb.AppendLine(",   Biko ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @TaishoYM ");
            sb.AppendLine(",   @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @PBushoKbn ");
            sb.AppendLine(",   @Iraimoto ");
            sb.AppendLine(",   @Kousu ");
            sb.AppendLine(",   @KenshuJoken ");
            sb.AppendLine(",   @Biko ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TaishoYM", addEntity.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", addEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@Iraimoto", addEntity.Iraimoto));
                    cmd.Parameters.Add(new SqlParameter("@Kousu", addEntity.Kousu));
                    cmd.Parameters.Add(new SqlParameter("@KenshuJoken", addEntity.KenshuJoken));
                    cmd.Parameters.Add(new SqlParameter("@Biko", StringUtil.TostringNullForbid(addEntity.Biko)));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 案件Excelデータ削除
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="taishoYM"></param>
        /// <param name="pBushoKbn"></param>
        public void DelAnkenExcel(
              string dbConnString
            , string taishoYM
            , ProjectConst.PBushoKbn pBushoKbn)
            {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("DELETE FROM ");
            sb.AppendLine("    PX03AnkenExcel ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYM  = @taishoYM ");
            sb.AppendLine("AND PBushoKbn = @PBushoKbn ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", (int)pBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// プロジェクト詳細テーブル登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="taishoDate"></param>
        /// <param name="pBushoKbn"></param>
        public void AddPX01ProjectDetail(
              string dbConnString
            , string tantoID
            , DateTime taishoDate
            , ProjectConst.PBushoKbn pBushoKbn
            ) {

            // 詳細マスタ登録
            AddMstDetail(dbConnString, tantoID, taishoDate, pBushoKbn);

            // プロジェクト詳細テーブル登録
            InsertProjectDetail(dbConnString, tantoID, taishoDate, pBushoKbn);
        }
        /// <summary>
        /// 詳細マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="taishoDate"></param>
        /// <param name="pBushoKbn"></param>
        public void AddMstDetail(
              string dbConnString
            , string tantoID
            , DateTime taishoDate
            , ProjectConst.PBushoKbn pBushoKbn
            ) {

            // SQL文の作成
            string taishoYM = taishoDate.ToString(Formats._FORMAT_DATE_YYYYMM);
            string dtNow = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO MX05Detail ");
            sb.AppendLine($@"/*毎月発生する業務*/ ");
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    @tantoId AS TantoID ");
            sb.AppendLine($@",   MX01.ProjectCD ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@",   1 AS DetailSeq ");
            sb.AppendLine($@",   CASE ");
            sb.AppendLine($@"         {CreateDetailConditionProxy(1)} ");
            sb.AppendLine($@"         ELSE '' ");
            sb.AppendLine($@"    END AS Detail ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"LEFT OUTER JOIN ");
            sb.AppendLine($@"    MX05Detail AS MX05 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX01.ProjectCD = MX05.ProjectCD ");
            sb.AppendLine($@"AND MX02.KouteiSeq = MX05.KouteiSeq ");
            sb.AppendLine($@"AND MX05.TantoID   = @tantoId ");
            sb.AppendLine($@"AND MX05.DetailSeq = 1 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX03.TantoID = @tantoId ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"AND MX01.PBushoKbn = 0 ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX01.PBushoKbn = 0 ");
            sb.AppendLine($@"AND MX05.TantoID IS NULL ");
            sb.AppendLine($@"UNION ALL ");
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    @tantoId AS TantoID ");
            sb.AppendLine($@",   MX01.ProjectCD ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@",   2 AS DetailSeq ");
            sb.AppendLine($@",   CASE ");
            sb.AppendLine($@"         {CreateDetailConditionProxy(2)} ");
            sb.AppendLine($@"         ELSE '' ");
            sb.AppendLine($@"    END AS Detail ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"AND MX02.KouteiSeq = 1 ");
            sb.AppendLine($@"LEFT OUTER JOIN ");
            sb.AppendLine($@"    MX05Detail AS MX05 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX01.ProjectCD = MX05.ProjectCD ");
            sb.AppendLine($@"AND MX02.KouteiSeq = MX05.KouteiSeq ");
            sb.AppendLine($@"AND MX05.TantoID   = @tantoId ");
            sb.AppendLine($@"AND MX05.DetailSeq = 2 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX03.TantoID = @tantoId ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD  = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX01.ProjectCD = '{ProjectConst._PROJECT_CODE_MANAGEMENT}' ");
            sb.AppendLine($@"AND MX05.TantoID IS NULL ");
            sb.AppendLine($@"AND MX01.PBushoKbn = 0 ");
            sb.AppendLine($@"/*MX05に未登録のレコード*/ ");
            sb.AppendLine($@"UNION ALL ");
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    @tantoId AS TantoID ");
            sb.AppendLine($@",   PX03.ProjectCD ");
            sb.AppendLine($@",   PX03.KouteiSeq ");
            sb.AppendLine($@",   1 AS DetailSeq ");
            sb.AppendLine($@",   ISNULL(MX05.Detail, '') ");
            sb.AppendLine($@",   CURRENT_TIMESTAMP ");
            sb.AppendLine($@",   CURRENT_TIMESTAMP ");
            sb.AppendLine($@"FROM( ");
            sb.AppendLine($@"    SELECT * FROM PX03AnkenExcel AS P03 ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        P03.TaishoYM = @taishoYM  ");
            sb.AppendLine($@") AS PX03 ");
            sb.AppendLine($@"INNER JOIN( ");
            sb.AppendLine($@"    SELECT * FROM MX01Project AS M01 ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        M01.StartYM   <= @taishoYM ");
            sb.AppendLine($@"    AND M01.EndYM     >= @taishoYM ");
            sb.AppendLine($@"    AND M01.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"    AND RIGHT(M01.ProjectCD, 3) <> '999' ");
            sb.AppendLine($@")AS MX01 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    PX03.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX02.ProjectCD = PX03.ProjectCD ");
            sb.AppendLine($@"AND MX02.KouteiSeq = PX03.KouteiSeq ");
            sb.AppendLine($@"LEFT OUTER JOIN( ");
            sb.AppendLine($@"    SELECT * FROM MX05Detail AS M05 ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        M05.TantoID   = @tantoId ");
            sb.AppendLine($@") AS MX05 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX05.ProjectCD = PX03.ProjectCD ");
            sb.AppendLine($@"AND MX05.KouteiSeq = PX03.KouteiSeq ");
            sb.AppendLine($@"AND MX05.DetailSeq = 1 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX03.TantoID = @tantoId ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX05.ProjectCD IS NULL ");
            sb.AppendLine($@"AND MX01.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", (int)pBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@dtNow", dtNow));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// プロジェクト詳細テーブル登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoID"></param>
        /// <param name="taishoDate"></param>
        /// <param name="pBushoKbn"></param>
        private void InsertProjectDetail(
              string dbConnString
            , string tantoID
            , DateTime taishoDate
            , ProjectConst.PBushoKbn pBushoKbn
            ) {

            // SQL文の作成
            string taishoYM = taishoDate.ToString(Formats._FORMAT_DATE_YYYYMM);
            string dtNow = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO PX01ProjectDetail ");
            sb.AppendLine($@"--プロジェクト区分=「0:共通」かつ未登録作業以外 ");
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    MX01.ProjectCD ");
            sb.AppendLine($@",   @taishoYM ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@",   MX05.DetailSeq ");
            sb.AppendLine($@",   @tantoId AS TantoID ");
            sb.AppendLine($@",   '' AS Biko ");
            sb.AppendLine($@",   0 AS DelFlg ");
            sb.AppendLine($@",   CURRENT_TIMESTAMP ");
            sb.AppendLine($@",   CURRENT_TIMESTAMP ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX02.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX03.TantoID = @tantoId ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX05Detail AS MX05 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX05.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"AND MX05.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX01.PBushoKbn = 0 ");
            sb.AppendLine($@"AND LEFT(MX01.ProjectCD,1) IN('A','B','C','X','Y') ");
            sb.AppendLine($@"--プロジェクト区分 <>「0:共通」 ");
            sb.AppendLine($@"UNION ALL ");
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    DATAS.* ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@",   @dtNow ");
            sb.AppendLine($@"FROM( ");
            sb.AppendLine($@"    --前月未登録 ");
            sb.AppendLine($@"    SELECT DISTINCT ");
            sb.AppendLine($@"        PX03.ProjectCD ");
            sb.AppendLine($@"    ,   PX03.TaishoYM ");
            sb.AppendLine($@"    ,   PX03.KouteiSeq ");
            sb.AppendLine($@"    ,   ISNULL(MX05_Z.DetailSeq, 1) AS DetailSeq ");
            sb.AppendLine($@"    ,   @tantoId AS TantoID ");
            sb.AppendLine($@"    ,   '' AS Biko ");
            sb.AppendLine($@"    ,   0 AS DelFlg ");
            sb.AppendLine($@"    FROM( ");
            sb.AppendLine($@"        SELECT * FROM PX03AnkenExcel AS P03 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            P03.TaishoYM  = @taishoYM ");
            sb.AppendLine($@"        AND P03.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"    ) AS PX03 ");
            sb.AppendLine($@"    INNER JOIN( ");
            sb.AppendLine($@"        SELECT * FROM MX01Project AS M01 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            M01.StartYM   <= @taishoYM ");
            sb.AppendLine($@"        AND M01.EndYM     >= @taishoYM ");
            sb.AppendLine($@"        AND M01.PBushoKbn IN(@PBushoKbn,0) ");
            sb.AppendLine($@"    )AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX03.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX02.ProjectCD = PX03.ProjectCD ");
            sb.AppendLine($@"    AND MX02.KouteiSeq = PX03.KouteiSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN( ");
            sb.AppendLine($@"        SELECT * FROM MX05Detail AS M05 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            M05.TantoID   = @tantoId ");
            sb.AppendLine($@"    ) AS MX05_Z ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX05_Z.ProjectCD = PX03.ProjectCD ");
            sb.AppendLine($@"    AND MX05_Z.KouteiSeq = PX03.KouteiSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN( ");
            sb.AppendLine($@"         SELECT * FROM PX01ProjectDetail AS P01 ");
            sb.AppendLine($@"         WHERE ");
            sb.AppendLine($@"             P01.TantoID  = @tantoId ");
            sb.AppendLine($@"         AND P01.TaishoYM = '{taishoDate.AddMonths(-1).ToString(Formats._FORMAT_DATE_YYYYMM)}' ");
            sb.AppendLine($@"    ) AS PX01_Z ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX03.ProjectCD    = PX01_Z.ProjectCD ");
            sb.AppendLine($@"    AND PX03.KouteiSeq    = PX01_Z.KouteiSeq ");
            sb.AppendLine($@"    AND MX05_Z.DetailSeq  = PX01_Z.DetailSeq ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX03Tanto AS MX03 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX03.TantoID = @tantoId ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX06Busho AS MX06 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX06.BushoCD  = MX03.BushoCD ");
            sb.AppendLine($@"    AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"    WHERE ");
            sb.AppendLine($@"        PX01_Z.TantoID IS NULL ");
            sb.AppendLine($@"    --前月登録済 ");
            sb.AppendLine($@"    UNION ALL ");
            sb.AppendLine($@"    SELECT DISTINCT ");
            sb.AppendLine($@"        PX01_Z.ProjectCD ");
            sb.AppendLine($@"    ,   @taishoYM AS TaishoYM ");
            sb.AppendLine($@"    ,   PX01_Z.KouteiSeq ");
            sb.AppendLine($@"    ,   ISNULL(MX05_Z.DetailSeq, 1) AS DetailSeq ");
            sb.AppendLine($@"    ,   @tantoId AS TantoID ");
            sb.AppendLine($@"    ,   ISNULL(PX01_Z.Biko, '') AS Biko ");
            sb.AppendLine($@"    ,   CASE WHEN PX03.ProjectCD IS NOT NULL THEN 0  ");
            sb.AppendLine($@"             ELSE 1 ");
            sb.AppendLine($@"        END AS DelFlg ");
            sb.AppendLine($@"    FROM( ");
            sb.AppendLine($@"         SELECT P01.* FROM PX01ProjectDetail AS P01 ");
            sb.AppendLine($@"         INNER JOIN ");
            sb.AppendLine($@"             MX01Project AS M01 ");
            sb.AppendLine($@"         ON ");
            sb.AppendLine($@"             P01.ProjectCD = M01.ProjectCD ");
            sb.AppendLine($@"         WHERE ");
            sb.AppendLine($@"             P01.TantoID  = @tantoId ");
            sb.AppendLine($@"         AND P01.TaishoYM = '{taishoDate.AddMonths(-1).ToString(Formats._FORMAT_DATE_YYYYMM)}' ");
            sb.AppendLine($@"         AND M01.PBushoKbn IN(@PBushoKbn,0) ");
            sb.AppendLine($@"    ) AS PX01_Z ");
            sb.AppendLine($@"    INNER JOIN( ");
            sb.AppendLine($@"        SELECT * FROM MX01Project AS M01 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            M01.StartYM   <= @taishoYM ");
            sb.AppendLine($@"        AND M01.EndYM     >= @taishoYM ");
            sb.AppendLine($@"        AND M01.PBushoKbn IN(@PBushoKbn,0) ");
            sb.AppendLine($@"    )AS MX01 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX01_Z.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX02Koutei AS MX02 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX02.ProjectCD = PX01_Z.ProjectCD ");
            sb.AppendLine($@"    AND MX02.KouteiSeq = PX01_Z.KouteiSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN( ");
            sb.AppendLine($@"        SELECT * FROM MX05Detail AS M05 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            M05.TantoID  = @tantoId ");
            sb.AppendLine($@"    ) AS MX05_Z ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX05_Z.ProjectCD = PX01_Z.ProjectCD ");
            sb.AppendLine($@"    AND MX05_Z.KouteiSeq = PX01_Z.KouteiSeq ");
            sb.AppendLine($@"    AND MX05_Z.DetailSeq = PX01_Z.DetailSeq ");
            sb.AppendLine($@"    LEFT OUTER JOIN( ");
            sb.AppendLine($@"        SELECT * FROM PX03AnkenExcel AS P03 ");
            sb.AppendLine($@"        WHERE ");
            sb.AppendLine($@"            P03.TaishoYM = @taishoYM ");
            sb.AppendLine($@"        AND RIGHT(P03.ProjectCD, 3) <> '999' ");
            sb.AppendLine($@"        AND P03.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"    ) AS PX03 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        PX01_Z.ProjectCD = PX03.ProjectCD ");
            sb.AppendLine($@"    AND PX01_Z.KouteiSeq = PX03.KouteiSeq ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX03Tanto AS MX03 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX03.TantoID = @tantoId ");
            sb.AppendLine($@"    INNER JOIN ");
            sb.AppendLine($@"        MX06Busho AS MX06 ");
            sb.AppendLine($@"    ON ");
            sb.AppendLine($@"        MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"    AND MX06.PBushoKbn IN(@PBushoKbn,0) ");
            sb.AppendLine($@") AS DATAS ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    LEFT(DATAS.ProjectCD,1) NOT IN('A','B','C','X','Y') ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoID));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", (int)pBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@dtNow", dtNow));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 詳細作成時の条件作成（Proxy）
        /// </summary>
        /// <param name="tgtDetailSeq"></param>
        /// <returns></returns>
        private string CreateDetailConditionProxy(int tgtDetailSeq) {
            string ret = string.Empty;
            foreach (var entity in ProjectConst._LST_SPECIAL_DETAIL) {
                if (entity.DetailSeq != tgtDetailSeq) {
                    continue;
                }
                ret += CreateDetailCondition(entity);
                ret += Environment.NewLine;
            }
            return ret;
        }
        /// <summary>
        /// 詳細作成時の条件作成
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private string CreateDetailCondition(MX05Detail entity) {
            return $@"WHEN MX01.ProjectCD = '{entity.ProjectCD}' AND MX02.KouteiSeq = {entity.KouteiSeq} THEN '{entity.Detail}'";
        }
        /// <summary>
        /// プロジェクト詳細テーブル削除
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="taishoYM"></param>
        /// <param name="pBushoKbn"></param>
        public void DelPX01ProjectDetail(
              string dbConnString
            , string taishoYM
            , ProjectConst.PBushoKbn pBushoKbn
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"DELETE PX01 ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    PX01ProjectDetail AS PX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    PX01.TantoID = MX03.TantoID ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    PX01.TaishoYM  = @taishoYM ");
            sb.AppendLine($@"; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", (int)pBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 工程マスタ取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <returns></returns>
        public List<MX02KouteiEntity> LoadMstKoutei(string dbConnString) {

            var ret = new List<MX02KouteiEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX02.ProjectCD ");
            sb.AppendLine(",   MX02.KouteiSeq ");
            sb.AppendLine(",   MX02.KouteiName ");
            sb.AppendLine(",   MX02.AddDate ");
            sb.AppendLine(",   MX02.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX02Koutei AS MX02 ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MX02KouteiEntity();
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.KouteiSeq = TypeConversionUtil.ToInteger(dr[nameof(entity.KouteiSeq)]);
                            entity.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]);
                            entity.AddDate = TypeConversionUtil.ToDateTime(dr[nameof(entity.AddDate)]);
                            entity.UpdDate = TypeConversionUtil.ToDateTime(dr[nameof(entity.UpdDate)]);
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 工程マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddMstKoutei(
              string dbConnString
            , MX02KouteiEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX02Koutei( ");
            sb.AppendLine("    ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   KouteiName ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @KouteiName ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@KouteiName", addEntity.KouteiName));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
