﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF2030_AnkenExcelInputService {
        /// <summary>
        /// プロジェクト詳細テーブル登録・更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <param name="tantoInfos"></param>
        /// <param name="taishoDate"></param>
        public void AddUpdProjectDetail(
              string dbConnString
            , PX03AnkenExcelEntity addEntity
            , List<MX03TantoEntity> tantoInfos
            , DateTime taishoDate
            ) {
            // 案件Excelデータ登録
            AddAnekenExcel(dbConnString, addEntity);

            foreach (var tanto in tantoInfos) {

                if (!ExistMstDetail(dbConnString, tanto.TantoID, addEntity.ProjectCD, addEntity.KouteiSeq)) {
                    // 詳細マスタ登録
                    AddMstDetail(dbConnString, tanto.TantoID, addEntity.ProjectCD, addEntity.KouteiSeq);
                }
                string taishoYM = taishoDate.ToString(Formats._FORMAT_DATE_YYYYMM);
                if (ExistProjectDetail(dbConnString, tanto.TantoID, taishoYM, addEntity.ProjectCD, addEntity.KouteiSeq)) {
                    // プロジェクト詳細更新
                    UpdProjectDetail(dbConnString, addEntity.ProjectCD, taishoYM, addEntity.KouteiSeq, tanto.TantoID);
                }
                else {
                    // プロジェクト詳細登録
                    AddProjectDetail(dbConnString, addEntity.ProjectCD, taishoYM, addEntity.KouteiSeq, tanto.TantoID);
                }
            }
        }

        #region 案件Excel
        /// <summary>
        /// グリッド表示データロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="taishoYM"></param>
        /// <param name="loginInfo"></param>
        /// <param name="projectCode"></param>
        /// <param name="projectName"></param>
        /// <param name="gyomuKbn"></param>
        /// <param name="pBushoKbn"></param>
        /// <param name="kokyakukbn"></param>
        /// <param name="kokyakuName"></param>
        /// <returns></returns>
        public List<InputAnkenExcelGridDto> GetGridDatas(
              string dbConnString
            , string taishoYM
            , LoginInfoDto loginInfo
            , string projectCode = ""
            , string projectName = ""
            , string gyomuKbn = ""
            , ProjectConst.PBushoKbn pBushoKbn = ProjectConst.PBushoKbn.NONE
            , ProjectConst.KokyakumeiKbn kokyakukbn = ProjectConst.KokyakumeiKbn.NONE
            , string kokyakuName = ""
            ) {
            var ret = new List<InputAnkenExcelGridDto>();

            // 検索条件コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(projectCode));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(projectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(gyomuKbn));
            string cmOutPBushoKbn = StringUtil.GetCmout(pBushoKbn == ProjectConst.PBushoKbn.NONE);
            string cmOutPBushoKbnLogin = StringUtil.GetCmout(pBushoKbn != ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   MX01.ProjectName ");
            sb.AppendLine(",   MX01.KokyakuName ");
            sb.AppendLine(",   MX02.KouteiName ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.Iraimoto ");
            sb.AppendLine(",   PX03.Kousu ");
            sb.AppendLine(",   CASE WHEN PX03.KenshuJoken = 1 THEN '実績' ");
            sb.AppendLine("         WHEN PX03.KenshuJoken = 2 THEN '固定' ");
            sb.AppendLine("    END AS KenshuJoken ");
            sb.AppendLine(",   PX03.Biko ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX03AnkenExcel AS PX03 ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX01Project AS MX01 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    PX03.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX02Koutei AS MX02 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    PX03.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine("AND PX03.KouteiSeq = MX02.KouteiSeq ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYM  = @taishoYM ");
            sb.AppendLine($@"{cmOutProjectCD}AND PX03.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutProjectNm}AND MX01.ProjectName LIKE @projectName ");
            sb.AppendLine($@"{cmOutPBushoKbn}AND PX03.PBushoKbn = @pBushoKbn ");
            sb.AppendLine($@"{cmOutPBushoKbnLogin}AND PX03.PBushoKbn IN(0, @pBushoKbnLogin) ");
            sb.AppendLine($@"{cmOutGyomuKbn }AND SUBSTRING(MX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(kokyakukbn, kokyakuName, "MX01")} ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.Iraimoto ");
            sb.AppendLine(",   PX03.Kousu ");
            sb.AppendLine(",   PX03.KenshuJoken ");
            sb.AppendLine(",   PX03.Biko ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", projectCode));
                    cmd.Parameters.AddWithValue("@projectName", "%" + projectName + "%");
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", pBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbnLogin", loginInfo.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", gyomuKbn));
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + kokyakuName + "%");

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new InputAnkenExcelGridDto();
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectName)]);
                            dto.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(dto.KokyakuName)]);
                            dto.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiSeq)]);
                            dto.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiName)]);
                            dto.Iraimoto = StringUtil.ToStringForbidNull(dr[nameof(dto.Iraimoto)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            dto.KenshuJoken = StringUtil.ToStringForbidNull(dr[nameof(dto.KenshuJoken)]);
                            dto.Biko = StringUtil.ToStringForbidNull(dr[nameof(dto.Biko)]);
                            dto.Status = Enums.EditStatus.SHOW;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 案件Excelデータ検索
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="taishoYM"></param>
        /// <returns></returns>
        public List<PX03AnkenExcelEntity> SelAnekenExcel(
              string dbConnString
            , string taishoYM
            ) {
            var ret = new List<PX03AnkenExcelEntity>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.PBushoKbn ");
            sb.AppendLine(",   PX03.Iraimoto ");
            sb.AppendLine(",   PX03.Kousu ");
            sb.AppendLine(",   PX03.KenshuJoken ");
            sb.AppendLine(",   PX03.Biko ");
            sb.AppendLine(",   PX03.AddDate ");
            sb.AppendLine(",   PX03.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX03AnkenExcel AS PX03 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYM  = @taishoYM ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    PX03.TaishoYM ");
            sb.AppendLine(",   PX03.ProjectCD ");
            sb.AppendLine(",   PX03.KouteiSeq ");
            sb.AppendLine(",   PX03.PBushoKbn ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var dto = new PX03AnkenExcelEntity();
                            dto.TaishoYM = StringUtil.ToStringForbidNull(dr[nameof(dto.TaishoYM)]);
                            dto.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(dto.ProjectCD)]);
                            dto.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(dto.KouteiSeq)]);
                            dto.Iraimoto = StringUtil.ToStringForbidNull(dr[nameof(dto.Iraimoto)]);
                            dto.Kousu = StringUtil.ToStringForbidNull(dr[nameof(dto.Kousu)]);
                            dto.KenshuJoken = StringUtil.ToStringForbidNull(dr[nameof(dto.KenshuJoken)]);
                            dto.Biko = StringUtil.ToStringForbidNull(dr[nameof(dto.Biko)]);
                            dto.PBushoKbn = TypeConversionUtil.ToInteger((dr[nameof(dto.PBushoKbn)]));
                            dto.AddDate = TypeConversionUtil.ToDateTime((dr[nameof(dto.AddDate)]));
                            dto.UpdDate = TypeConversionUtil.ToDateTime((dr[nameof(dto.UpdDate)]));
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 案件Excelデータ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddAnekenExcel(
              string dbConnString
            , PX03AnkenExcelEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    PX03AnkenExcel( ");
            sb.AppendLine("    TaishoYM ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   PBushoKbn ");
            sb.AppendLine(",   Iraimoto ");
            sb.AppendLine(",   Kousu ");
            sb.AppendLine(",   KenshuJoken ");
            sb.AppendLine(",   Biko ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @TaishoYM ");
            sb.AppendLine(",   @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @PBushoKbn ");
            sb.AppendLine(",   @Iraimoto ");
            sb.AppendLine(",   @Kousu ");
            sb.AppendLine(",   @KenshuJoken ");
            sb.AppendLine(",   @Biko ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TaishoYM", addEntity.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", addEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@Iraimoto", addEntity.Iraimoto));
                    cmd.Parameters.Add(new SqlParameter("@Kousu", addEntity.Kousu));
                    cmd.Parameters.Add(new SqlParameter("@KenshuJoken", addEntity.KenshuJoken));
                    cmd.Parameters.Add(new SqlParameter("@Biko", StringUtil.TostringNullForbid(addEntity.Biko)));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 案件Excelデータ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdAnekenExcel(
              string dbConnString
            , PX03AnkenExcelEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    PX03AnkenExcel ");
            sb.AppendLine("SET ");
            sb.AppendLine("    Kousu       = @kousu ");
            sb.AppendLine(",   KenshuJoken = @kenshuJoken ");
            sb.AppendLine(",   Biko        = @biko ");
            sb.AppendLine(",   UpdDate     = @updDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TaishoYM    = @taishoYM ");
            sb.AppendLine("AND ProjectCD   = @projectCD ");
            sb.AppendLine("AND KouteiSeq   = @kouteiSeq ");
            sb.AppendLine("AND PBushoKbn   = @pBushoKbn ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", updEntity.TaishoYM));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", updEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", updEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@kousu", updEntity.Kousu));
                    cmd.Parameters.Add(new SqlParameter("@kenshuJoken", updEntity.KenshuJoken));
                    cmd.Parameters.Add(new SqlParameter("@biko", updEntity.Biko));
                    cmd.Parameters.Add(new SqlParameter("@updDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region プロジェクト詳細
        /// <summary>
        /// プロジェクト詳細レコードが存在するか？
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoId"></param>
        /// <param name="taishoYM"></param>
        /// <param name="projectCode"></param>
        /// <param name="kouteiSeq"></param>
        /// <returns></returns>
        private bool ExistProjectDetail(
              string dbConnString
            , string tantoId
            , string taishoYM
            , string projectCode
            , string kouteiSeq
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    COUNT(*) AS Count ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    PX01ProjectDetail AS PX01 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID   = @tantoId ");
            sb.AppendLine("AND TaishoYM  = @taishoYM ");
            sb.AppendLine("AND ProjectCD = @projectCode ");
            sb.AppendLine("AND KouteiSeq = @kouteiSeq ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoId));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@projectCode", projectCode));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", kouteiSeq));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            return 0 < TypeConversionUtil.ToInteger(dr["Count"]); ;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// プロジェクト詳細テーブル登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="projectCode"></param>
        /// <param name="taishoYM"></param>
        /// <param name="kouteiSeq"></param>
        /// <param name="tantoId"></param>
        private void AddProjectDetail(
              string dbConnString
            , string projectCode
            , string taishoYM
            , string kouteiSeq
            , string tantoId
            ) {
            // SQL文の作成
            string dtNow = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO PX01ProjectDetail( ");
            sb.AppendLine("    ProjectCD ");
            sb.AppendLine(",   TaishoYM ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   TantoID ");
            sb.AppendLine(",   Biko ");
            sb.AppendLine(",   DelFlg ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @projectCD ");
            sb.AppendLine(",   @taishoYM ");
            sb.AppendLine(",   @kouteiSeq ");
            sb.AppendLine(",   1 ");
            sb.AppendLine(",   @tantoId ");
            sb.AppendLine(",   '' ");
            sb.AppendLine(",   0 ");
            sb.AppendLine(",   @dtNow ");
            sb.AppendLine(",   @dtNow ");
            sb.AppendLine(") ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@projectCD", projectCode));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", kouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoId));
                    cmd.Parameters.Add(new SqlParameter("@dtNow", dtNow));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// プロジェクト詳細テーブル更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="projectCode"></param>
        /// <param name="taishoYM"></param>
        /// <param name="kouteiSeq"></param>
        /// <param name="tantoId"></param>
        private void UpdProjectDetail(
              string dbConnString
            , string projectCode
            , string taishoYM
            , string kouteiSeq
            , string tantoId
            ) {
            string dtNow = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    PX01ProjectDetail ");
            sb.AppendLine("SET ");
            sb.AppendLine("    DelFlg    = 0 ");
            sb.AppendLine(",   UpdDate   = @dtNow ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID   = @tantoId ");
            sb.AppendLine("AND TaishoYM  = @taishoYM ");
            sb.AppendLine("AND ProjectCD = @projectCode ");
            sb.AppendLine("AND KouteiSeq = @kouteiSeq ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@projectCode", projectCode));
                    cmd.Parameters.Add(new SqlParameter("@taishoYM", taishoYM));
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoId));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", kouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@dtNow", dtNow));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region 担当者マスタ
        /// <summary>
        /// 担当者マスタ情報取得
        /// </summary>
        /// <param name="dbConnStringing"></param>
        /// <param name="pBushoKbn"></param>
        /// <returns></returns>
        public List<MX03TantoEntity> GetTantoInfos(
              string dbConnStringing
            , int pBushoKbn
            ) {
            var ret = new List<MX03TantoEntity>();
            var tantoMasters = new PAF3010_MstEmploeeService().LoadMstTanto(dbConnStringing, new LoadMstTantoCon());
            foreach (var tanto in tantoMasters) {
                var bushoInfo = new MstCommonService().LoadMstBushoSingle(dbConnStringing, tanto.BushoCD);
                if (bushoInfo.PBushoKbn != pBushoKbn) {
                    continue;
                }
                ret.Add(tanto);
            }
            return ret;
        }
        #endregion

        #region 工程マスタ
        /// <summary>
        /// 工程一覧ロード（全件）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <returns></returns>
        public List<string> LoadMstKouteiAll(string dbConnString) {
            var ret = new List<string>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT DISTINCT ");
            sb.AppendLine($@"    MX02.KouteiName ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX01.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    SUBSTRING(MX01.ProjectCD,1,1) NOT IN('X','Y') ");
            sb.AppendLine($@"AND RIGHT(MX01.ProjectCD,3) <> '999' ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MstKouteiGridDto();
                            ret.Add(StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 工程一覧ロード（プロジェクトコード指定）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="projectCode"></param>
        /// <returns></returns>
        public List<MstKouteiGridDto> LoadMstKouteiByProjectCode(
              string dbConnString
            , string projectCode
            ) {
            var ret = new List<MstKouteiGridDto>();

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    MX01.PBushoKbn ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@",   MX01.KokyakuName ");
            sb.AppendLine($@",   MX01.ProjectCD ");
            sb.AppendLine($@",   MX01.ProjectName ");
            sb.AppendLine($@",   MX02.KouteiName ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX01.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    SUBSTRING(MX01.ProjectCD,1,1) NOT IN('X','Y') ");
            sb.AppendLine($@"AND RIGHT(MX01.ProjectCD,3) <> '999' ");
            sb.AppendLine($@"AND MX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"ORDER BY ");
            sb.AppendLine($@"    MX01.ProjectCD ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@projectCD", projectCode));
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MstKouteiGridDto();
                            entity.PBushoKbn = StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]);
                            entity.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiSeq)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.PBushoKbnName = ProjectConst._DIC_PROJECT_KBN.First(n => (int)n.Key == int.Parse(entity.PBushoKbn)).Value;
                            entity.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]);
                            entity.Status = Enums.EditStatus.SHOW;
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region 詳細マスタ
        /// <summary>
        /// 詳細マスタにレコードが存在するか判定
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoId"></param>
        /// <param name="projectCode"></param>
        /// <param name="kouteiSeq"></param>
        /// <returns></returns>
        private bool ExistMstDetail(
              string dbConnString
            , string tantoId
            , string projectCode
            , string kouteiSeq
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    COUNT(*) AS Count ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX05Detail AS MX05 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID   = @tantoId ");
            sb.AppendLine("AND ProjectCD = @projectCode ");
            sb.AppendLine("AND KouteiSeq = @kouteiSeq ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoId));
                    cmd.Parameters.Add(new SqlParameter("@projectCode", projectCode));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", kouteiSeq));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            return 0 < TypeConversionUtil.ToInteger(dr["Count"]);
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// 詳細マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="tantoId"></param>
        /// <param name="projectCode"></param>
        /// <param name="kouteiSeq"></param>
        private void AddMstDetail(
              string dbConnString
            , string tantoId
            , string projectCode
            , string kouteiSeq
            ) {

            string dtNow = DateTime.Now.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO MX05Detail( ");
            sb.AppendLine("    TantoID ");
            sb.AppendLine(",   ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   DetailSeq ");
            sb.AppendLine(",   Detail ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @tantoId ");
            sb.AppendLine(",   @projectCode ");
            sb.AppendLine(",   @kouteiSeq ");
            sb.AppendLine(",   1 ");
            sb.AppendLine(",   '' ");
            sb.AppendLine(",   @dtNow ");
            sb.AppendLine(",   @dtNow ");;
            sb.AppendLine(") ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@tantoId", tantoId));
                    cmd.Parameters.Add(new SqlParameter("@projectCode", projectCode));
                    cmd.Parameters.Add(new SqlParameter("@kouteiSeq", kouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@dtNow", dtNow));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

    }
}
