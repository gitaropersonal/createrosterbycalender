﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF3010_MstEmploeeService {
        /// <summary>
        /// 社員マスタロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public List<MX03TantoEntity> LoadMstTanto(
              string dbConnString
            , LoadMstTantoCon con
            ) {
            var ret = new List<MX03TantoEntity>();

            string cmOutTantoID = StringUtil.GetCmout(string.IsNullOrEmpty(con.TantoID));
            string cmOutBushoCD = StringUtil.GetCmout(string.IsNullOrEmpty(con.BushoCD));
            string cmOutKaishaCD = StringUtil.GetCmout(string.IsNullOrEmpty(con.KaishaCD));
            string cmOutKengenKbn = StringUtil.GetCmout(con.AuthorityKbn == Enums.AuthorityKbn.None);
            string cmOutTantoKbn = StringUtil.GetCmout(con.TantoKbn == Enums.TantoKbn.None);
            string cmOutYakushokuKbn = StringUtil.GetCmout(con.YakushokuKbn == Enums.YakushokuKbn.None);
            string cmOutKontainDeleted = StringUtil.GetCmout(con.IsContaiDeleted);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX03.TantoID ");
            sb.AppendLine(",   MX03.TantoName ");
            sb.AppendLine(",   MX03.BushoCD ");
            sb.AppendLine(",   MX06.BushoName ");
            sb.AppendLine(",   MX03.KaishaCD ");
            sb.AppendLine(",   MX07.KaishaName ");
            sb.AppendLine(",   MX03.AuthorityKbn ");
            sb.AppendLine(",   MX03.TantoKbn ");
            sb.AppendLine(",   MX03.YakushokuKbn ");
            sb.AppendLine(",   MX03.MailAddress ");
            sb.AppendLine(",   MX03.DelFlg ");
            sb.AppendLine(",   MX03.AddDate ");
            sb.AppendLine(",   MX03.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX03Tanto AS MX03 ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX06Busho AS MX06 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.BushoCD = MX06.BushoCD ");
            sb.AppendLine("LEFT OUTER JOIN ");
            sb.AppendLine("    MX07Kaisha AS MX07 ");
            sb.AppendLine("ON ");
            sb.AppendLine("    MX03.KaishaCD = MX07.KaishaCD ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine($@"{cmOutTantoID}AND MX03.TantoID = @TantoID ");
            sb.AppendLine($@"{cmOutBushoCD}AND MX03.BushoCD = @BushoCD ");
            sb.AppendLine($@"{cmOutKaishaCD}AND MX03.KaishaCD = @KaishaCD ");
            sb.AppendLine($@"{cmOutKengenKbn}AND MX03.AuthorityKbn = @AuthorityKbn ");
            sb.AppendLine($@"{cmOutTantoKbn}AND MX03.TantoKbn = @TantoKbn ");
            sb.AppendLine($@"{cmOutYakushokuKbn}AND MX03.YakushokuKbn = @YakushokuKbn ");
            sb.AppendLine($@"{cmOutKontainDeleted}AND MX03.DelFlg = 0 ");
            sb.AppendLine("ORDER BY ");
            sb.AppendLine("    MX03.DelFlg ");
            sb.AppendLine(",   MX03.TantoID ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", StringUtil.TostringNullForbid(con.TantoID)));
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", StringUtil.TostringNullForbid(con.BushoCD)));
                    cmd.Parameters.Add(new SqlParameter("@KaishaCD", StringUtil.TostringNullForbid(con.KaishaCD)));
                    cmd.Parameters.Add(new SqlParameter("@AuthorityKbn", con.AuthorityKbn));
                    cmd.Parameters.Add(new SqlParameter("@TantoKbn", con.TantoKbn));
                    cmd.Parameters.Add(new SqlParameter("@YakushokuKbn", con.YakushokuKbn));

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MX03TantoEntity();
                            entity.TantoID = StringUtil.ToStringForbidNull(dr[nameof(entity.TantoID)]);
                            entity.TantoName = StringUtil.ToStringForbidNull(dr[nameof(entity.TantoName)]);
                            entity.BushoCD = StringUtil.ToStringForbidNull(dr[nameof(entity.BushoCD)]);
                            entity.BushoName = StringUtil.ToStringForbidNull(dr[nameof(entity.BushoName)]);
                            entity.KaishaCD = StringUtil.ToStringForbidNull(dr[nameof(entity.KaishaCD)]);
                            entity.KaishaName = StringUtil.ToStringForbidNull(dr[nameof(entity.KaishaName)]);
                            entity.AuthorityKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(entity.AuthorityKbn)]));
                            entity.TantoKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(entity.TantoKbn)]));
                            entity.YakushokuKbn = TypeConversionUtil.ToInteger(StringUtil.ToStringForbidNull(dr[nameof(entity.YakushokuKbn)]));
                            entity.MailAddress = StringUtil.ToStringForbidNull(dr[nameof(entity.MailAddress)]);
                            entity.DelFlg = StringUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                            entity.AddDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.AddDate)]));
                            entity.UpdDate = DateTime.Parse(StringUtil.ToStringForbidNull(dr[nameof(entity.UpdDate)]));
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 社員マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        public void AddMstTanto(
              string dbConnString
            , MX03TantoEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX03Tanto( ");
            sb.AppendLine("    TantoID ");
            sb.AppendLine(",   TantoName ");
            sb.AppendLine(",   BushoCD ");
            sb.AppendLine(",   KaishaCD ");
            sb.AppendLine(",   AuthorityKbn ");
            sb.AppendLine(",   TantoKbn ");
            sb.AppendLine(",   YakushokuKbn ");
            sb.AppendLine(",   MailAddress ");
            sb.AppendLine(",   DelFlg ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @TantoID ");
            sb.AppendLine(",   @TantoName ");
            sb.AppendLine(",   @BushoCD ");
            sb.AppendLine(",   @KaishaCD ");
            sb.AppendLine(",   @AuthorityKbn ");
            sb.AppendLine(",   @TantoKbn ");
            sb.AppendLine(",   @YakushokuKbn ");
            sb.AppendLine(",   @MailAddress ");
            sb.AppendLine(",   @DelFlg ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", addEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@TantoName", addEntity.TantoName));
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", addEntity.BushoCD));
                    cmd.Parameters.Add(new SqlParameter("@KaishaCD", addEntity.KaishaCD));
                    cmd.Parameters.Add(new SqlParameter("@AuthorityKbn", addEntity.AuthorityKbn));
                    cmd.Parameters.Add(new SqlParameter("@TantoKbn", addEntity.TantoKbn));
                    cmd.Parameters.Add(new SqlParameter("@YakushokuKbn", addEntity.YakushokuKbn));
                    cmd.Parameters.Add(new SqlParameter("@MailAddress", addEntity.MailAddress));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", addEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
            // ログイン情報を登録
            new LoginService().InsertLoginInfo(dbConnString, addEntity.TantoID, "1");

            var service = new PAF2010_AnkenExcelCaptureService();
            for (int intMonth = DateTime.Now.Month; intMonth <= TimeConst._MONTH_COUNT_PER_YEAR; intMonth++) {

                // 対象日
                DateTime taishoDate = new DateTime(DateTime.Now.Year, intMonth, 1);

                // 所属部署の情報取得
                var bushoInfo = new MstCommonService().LoadMstBushoSingle(dbConnString, addEntity.BushoCD);

                // プロジェクト詳細マスタ登録
                service.AddMstDetail(dbConnString, addEntity.TantoID, taishoDate, (ProjectConst.PBushoKbn)bushoInfo.PBushoKbn);

                // プロジェクト詳細テーブル登録
                service.AddPX01ProjectDetail(dbConnString, addEntity.TantoID, taishoDate, (ProjectConst.PBushoKbn)bushoInfo.PBushoKbn);
            }
        }
        /// <summary>
        /// 社員マスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdMstTanto(
              string dbConnString
            , MX03TantoEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX03Tanto ");
            sb.AppendLine("SET ");
            sb.AppendLine("    TantoName    = @TantoName ");
            sb.AppendLine(",   BushoCD      = @BushoCD ");
            sb.AppendLine(",   KaishaCD     = @KaishaCD ");
            sb.AppendLine(",   AuthorityKbn = @AuthorityKbn ");
            sb.AppendLine(",   TantoKbn     = @TantoKbn ");
            sb.AppendLine(",   YakushokuKbn = @YakushokuKbn ");
            sb.AppendLine(",   MailAddress  = @MailAddress ");
            sb.AppendLine(",   DelFlg       = @DelFlg ");
            sb.AppendLine(",   UpdDate      = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    TantoID      = @TantoID ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", updEntity.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@TantoName", updEntity.TantoName));
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", updEntity.BushoCD));
                    cmd.Parameters.Add(new SqlParameter("@KaishaCD", updEntity.KaishaCD));
                    cmd.Parameters.Add(new SqlParameter("@AuthorityKbn", updEntity.AuthorityKbn));
                    cmd.Parameters.Add(new SqlParameter("@TantoKbn", updEntity.TantoKbn));
                    cmd.Parameters.Add(new SqlParameter("@YakushokuKbn", updEntity.YakushokuKbn));
                    cmd.Parameters.Add(new SqlParameter("@MailAddress", updEntity.MailAddress));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", updEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
