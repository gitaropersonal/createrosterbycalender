﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Entity;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF3020_MstBushoService {
        /// <summary>
        /// 部署マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddMstBusho(
              string dbConnString
            , MX06BushoEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX06Busho( ");
            sb.AppendLine("    BushoCD ");
            sb.AppendLine(",   BushoName ");
            sb.AppendLine(",   PBushoKbn ");
            sb.AppendLine(",   DelFlg ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @BushoCD ");
            sb.AppendLine(",   @BushoName ");
            sb.AppendLine(",   @PBushoKbn ");
            sb.AppendLine(",   @DelFlg ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", addEntity.BushoCD));
                    cmd.Parameters.Add(new SqlParameter("@BushoName", addEntity.BushoName));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", addEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", addEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 部署マスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="UpdEntity"></param>
        /// <returns></returns>
        public void UpdMstBusho(
              string dbConnString
            , MX06BushoEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX06Busho ");
            sb.AppendLine("SET ");
            sb.AppendLine("    BushoName = @BushoName ");
            sb.AppendLine(",   PBushoKbn = @PBushoKbn ");
            sb.AppendLine(",   DelFlg    = @DelFlg ");
            sb.AppendLine(",   UpdDate   = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    BushoCD   = @BushoCD ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@BushoCD", updEntity.BushoCD));
                    cmd.Parameters.Add(new SqlParameter("@BushoName", updEntity.BushoName));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", updEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@DelFlg", updEntity.DelFlg));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
