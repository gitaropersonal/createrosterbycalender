﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF3030_MstProjectService {
        /// <summary>
        /// プロジェクトマスタ取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <returns></returns>
        public List<MstProjectGridDto> LoadMstProject(
              string dbConnString
            , LoadAggregateConDto con1
            , LoadProjectLooksConDto con2
            ) {

            var ret = new List<MstProjectGridDto>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con2.GyomuKbn));
            string cmOutPBushoKbn = StringUtil.GetCmout(con1.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MX01.ProjectCD ");
            sb.AppendLine(",   MX01.ProjectName ");
            sb.AppendLine(",   MX01.KokyakuName ");
            sb.AppendLine(",   MX01.PBushoKbn ");
            sb.AppendLine(",   CASE ");
            sb.AppendLine("        WHEN MX01.StartYM IN('999912') THEN '' ");
            sb.AppendLine("        ELSE LEFT(MX01.StartYM + '0000',4) + '/' + RIGHT('00' + MX01.StartYM, 2) ");
            sb.AppendLine("    END AS StartYM_Disp ");
            sb.AppendLine(",   CASE ");
            sb.AppendLine("        WHEN MX01.EndYM IN('999912') THEN '' ");
            sb.AppendLine("        ELSE LEFT(MX01.EndYM + '0000',4) + '/' + RIGHT('00' + MX01.EndYM, 2) ");
            sb.AppendLine("    END AS EndYM_Disp ");
            sb.AppendLine(",   MX01.StartYM ");
            sb.AppendLine(",   MX01.EndYM ");
            sb.AppendLine(",   MX01.AddDate ");
            sb.AppendLine(",   MX01.UpdDate ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX01Project AS MX01 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    0 = 0 ");
            sb.AppendLine($@"{cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"{cmOutProjectCD}AND MX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutGyomuKbn }AND SUBSTRING(MX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{cmOutProjectNm}AND MX01.ProjectName LIKE @projectName ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con1.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", con2.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", con2.GyomuKbn));
                    cmd.Parameters.AddWithValue("@projectName", "%" + con2.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con2.KokyakuName + "%");

                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MstProjectGridDto();
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.PBushoKbn = StringUtil.ToStringForbidNull(StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]));
                            entity.PBushoKbnName = ProjectConst._DIC_PROJECT_KBN.First(n => (int)n.Key == TypeConversionUtil.ToInteger(entity.PBushoKbn)).Value;
                            entity.StartYM_Disp = StringUtil.ToStringForbidNull(dr[nameof(entity.StartYM_Disp)]);
                            entity.EndYM_Disp = StringUtil.ToStringForbidNull(dr[nameof(entity.EndYM_Disp)]);
                            entity.StartYM = StringUtil.ToStringForbidNull(dr[nameof(entity.StartYM)]);
                            entity.EndYM = StringUtil.ToStringForbidNull(dr[nameof(entity.EndYM)]);
                            entity.Status = Enums.EditStatus.SHOW;
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// プロジェクトマスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdMstProject(
              string dbConnString
            , MX01ProjectEntity updEntity
            ) {
            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX01Project ");
            sb.AppendLine("SET ");
            sb.AppendLine("    ProjectName = @ProjectName ");
            sb.AppendLine(",   KokyakuName = @KokyakuName ");
            sb.AppendLine(",   PBushoKbn   = @PBushoKbn ");
            sb.AppendLine(",   StartYM     = @StartYM ");
            sb.AppendLine(",   EndYM       = @EndYM ");
            sb.AppendLine(",   UpdDate     = @UpdDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    ProjectCD   = @ProjectCD ");
            sb.AppendLine("; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@ProjectName", updEntity.ProjectName));
                    cmd.Parameters.Add(new SqlParameter("@KokyakuName", updEntity.KokyakuName));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", updEntity.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@StartYM", updEntity.StartYM));
                    cmd.Parameters.Add(new SqlParameter("@EndYM", updEntity.EndYM));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
