﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using PrsAnalyzer.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PrsAnalyzer.Service {
    public class PAF3040_MstKouteiService {
        /// <summary>
        /// 工程一覧ロード
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="con1"></param>
        /// <param name="con2"></param>
        /// <returns></returns>
        public List<MstKouteiGridDto> LoadMstKoutei(
              string dbConnString
            , LoadAggregateConDto con1
            , LoadProjectLooksConDto con2
            ) {
            var ret = new List<MstKouteiGridDto>();

            // 条件に応じて一部コメントアウト
            string cmOutProjectCD = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectCD));
            string cmOutProjectNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.ProjectName));
            string cmOutGyomuKbn = StringUtil.GetCmout(string.IsNullOrEmpty(con2.GyomuKbn));
            string cmOutKokyakuNm = StringUtil.GetCmout(string.IsNullOrEmpty(con2.KokyakuName));
            string cmOutPBushoKbn = StringUtil.GetCmout(con1.PBushoKbn == ProjectConst.PBushoKbn.NONE);

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    MX01.PBushoKbn ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@",   MX01.KokyakuName ");
            sb.AppendLine($@",   MX01.ProjectCD ");
            sb.AppendLine($@",   MX01.ProjectName ");
            sb.AppendLine($@",   MX02.KouteiName ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX02Koutei AS MX02 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX01.ProjectCD = MX02.ProjectCD ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    SUBSTRING(MX01.ProjectCD,1,1) NOT IN('X','Y') ");
            sb.AppendLine($@"AND RIGHT(MX01.ProjectCD,3) <> '999' ");
            sb.AppendLine($@"{cmOutPBushoKbn}AND MX01.PBushoKbn =@pBushoKbn ");
            sb.AppendLine($@"{cmOutProjectCD}AND MX01.ProjectCD = @projectCD ");
            sb.AppendLine($@"{cmOutProjectNm}AND MX01.ProjectName LIKE(@projectName) ");
            sb.AppendLine($@"{cmOutGyomuKbn}AND SUBSTRING(MX01.ProjectCD,1,1) = @gyomuKbn ");
            sb.AppendLine($@"{ProjectUtil.GetConkyakuNameCondition(con2.KokyakuKbn, con2.KokyakuName, "MX01")} ");
            sb.AppendLine($@"ORDER BY ");
            sb.AppendLine($@"    MX01.ProjectCD ");
            sb.AppendLine($@",   MX02.KouteiSeq ");
            sb.AppendLine($@"; ");

            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@pBushoKbn", con1.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@projectCD", con2.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@gyomuKbn", con2.GyomuKbn));
                    cmd.Parameters.AddWithValue("@projectName", "%" + con2.ProjectName + "%");
                    cmd.Parameters.AddWithValue("@kokyakuName", "%" + con2.KokyakuName + "%");
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            var entity = new MstKouteiGridDto();
                            entity.PBushoKbn = StringUtil.ToStringForbidNull(dr[nameof(entity.PBushoKbn)]);
                            entity.KouteiSeq = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiSeq)]);
                            entity.KokyakuName = StringUtil.ToStringForbidNull(dr[nameof(entity.KokyakuName)]);
                            entity.ProjectCD = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectCD)]);
                            entity.ProjectName = StringUtil.ToStringForbidNull(dr[nameof(entity.ProjectName)]);
                            entity.PBushoKbnName = ProjectConst._DIC_PROJECT_KBN.First(n => (int)n.Key == int.Parse(entity.PBushoKbn)).Value;
                            entity.KouteiName = StringUtil.ToStringForbidNull(dr[nameof(entity.KouteiName)]);
                            entity.Status = Enums.EditStatus.SHOW;
                            ret.Add(entity);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 工程マスタ登録（Proxy）
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void AddMstKouteiProxy(
              string dbConnString
            , MX02KouteiEntity addEntity
            ) {

            // 工程マスタ登録
            InsertMstKoutei(dbConnString, addEntity);

            var service = new PAF3010_MstEmploeeService();
            var mstCommonService = new MstCommonService();
            var emploeeMst = new PAF3010_MstEmploeeService().LoadMstTanto(dbConnString, new LoadMstTantoCon());
            var projectMst = mstCommonService.LoadMstProject(dbConnString).First(n => n.ProjectCD == addEntity.ProjectCD);
            foreach (var emp in emploeeMst) {

                // 部署区分取得
                var busho = mstCommonService.LoadMstBushoSingle(dbConnString, emp.BushoCD);

                // プロジェクトの開始・終了年月を取得
                int intStartYYYY = TypeConversionUtil.ToInteger(projectMst.StartYM.Substring(0, 4));
                int intStartMM = TypeConversionUtil.ToInteger(projectMst.StartYM.Substring(4, 2));
                int intEndYYYY = TypeConversionUtil.ToInteger(projectMst.EndYM.Substring(0, 4));
                int intEndMM = TypeConversionUtil.ToInteger(projectMst.EndYM.Substring(4, 2));
                DateTime startYM = new DateTime(intStartYYYY, intStartMM, 1);
                DateTime endYM = new DateTime(intEndYYYY, intEndMM, TimeConst._MAX_DATE_COUNT_AN_MONTH);
                DateTime NendoEndDate = new DateTime(intStartYYYY, TimeConst._MONTH_COUNT_PER_YEAR, TimeConst._MAX_DATE_COUNT_AN_MONTH);
                if (NendoEndDate < endYM) {
                    endYM = NendoEndDate;
                }

                for (DateTime dt = startYM; dt < endYM; dt = dt.AddMonths(1)) {

                    // プロジェクト詳細テーブル登録
                    InsertProjectDetail(dbConnString, addEntity, emp, busho, dt.ToString(Formats._FORMAT_DATE_YYYYMM));
                }
                // 詳細マスタ登録
                InsertMstDetail(dbConnString, addEntity, emp, busho);
            }
        }
        /// <summary>
        /// 工程マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        private void InsertMstKoutei(
              string dbConnString
            , MX02KouteiEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO ");
            sb.AppendLine("    MX02Koutei( ");
            sb.AppendLine("    ProjectCD ");
            sb.AppendLine(",   KouteiSeq ");
            sb.AppendLine(",   KouteiName ");
            sb.AppendLine(",   AddDate ");
            sb.AppendLine(",   UpdDate ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @ProjectCD ");
            sb.AppendLine(",   @KouteiSeq ");
            sb.AppendLine(",   @KouteiName ");
            sb.AppendLine(",   @AddDate ");
            sb.AppendLine(",   @UpdDate ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@KouteiName", addEntity.KouteiName));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// プロジェクト詳細テーブル登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <param name="tanto"></param>
        /// <param name="busho"></param>
        /// <param name="taishoYM"></param>
        private void InsertProjectDetail(
              string dbConnString
            , MX02KouteiEntity addEntity
            , MX03TantoEntity tanto
            , MX06BushoEntity busho
            , string taishoYM
            ) {

            // 追加した工程マスタの分、PX01レコード作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO PX01ProjectDetail ");
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    MX01.ProjectCD ");
            sb.AppendLine($@",   '{taishoYM}' AS TaishoYM ");
            sb.AppendLine($@",   @KouteiSeq AS KouteiSeq ");
            sb.AppendLine($@",   1 AS DetailSeq ");
            sb.AppendLine($@",   MX03.TantoID ");
            sb.AppendLine($@",   '' AS Biko ");
            sb.AppendLine($@",   CASE ");
            sb.AppendLine($@"        WHEN PX03.ProjectCD IS NULL THEN 1 ");
            sb.AppendLine($@"        ELSE 0 ");
            sb.AppendLine($@"    END AS DelFLG  ");
            sb.AppendLine($@",   @AddDate ");
            sb.AppendLine($@",   @UpdDate ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX03.TantoID = @TantoID ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = MX01.PBushoKbn ");
            sb.AppendLine($@"LEFT OUTER JOIN ");
            sb.AppendLine($@"    PX03AnkenExcel AS PX03 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    PX03.TaishoYM  = '{taishoYM}' ");
            sb.AppendLine($@"AND PX03.ProjectCD = MX01.ProjectCD ");
            sb.AppendLine($@"AND PX03.KouteiSeq = @KouteiSeq ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX01.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", tanto.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", busho.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 詳細マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <param name="tanto"></param>
        /// <param name="busho"></param>
        private void InsertMstDetail(
              string dbConnString
            , MX02KouteiEntity addEntity
            , MX03TantoEntity tanto
            , MX06BushoEntity busho
            ) {

            // 追加した工程マスタの分、PX05レコード作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"INSERT INTO MX05Detail ");
            sb.AppendLine($@"SELECT ");
            sb.AppendLine($@"    MX03.TantoID ");
            sb.AppendLine($@",   @ProjectCD ");
            sb.AppendLine($@",   @KouteiSeq ");
            sb.AppendLine($@",   1 ");
            sb.AppendLine($@",   '' ");
            sb.AppendLine($@",   @AddDate ");
            sb.AppendLine($@",   @UpdDate ");
            sb.AppendLine($@"FROM ");
            sb.AppendLine($@"    MX03Tanto AS MX03 ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX06Busho AS MX06 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.BushoCD   = MX03.BushoCD ");
            sb.AppendLine($@"AND MX06.PBushoKbn = @PBushoKbn ");
            sb.AppendLine($@"INNER JOIN ");
            sb.AppendLine($@"    MX01Project AS MX01 ");
            sb.AppendLine($@"ON ");
            sb.AppendLine($@"    MX06.PBushoKbn = MX01.PBushoKbn ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    MX01.ProjectCD = @ProjectCD ");
            sb.AppendLine($@"AND MX03.TantoID   = @TantoID ");
            sb.AppendLine($@"; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@TantoID", tanto.TantoID));
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", addEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@PBushoKbn", busho.PBushoKbn));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", addEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@AddDate", addEntity.AddDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", addEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 工程マスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdKouteiMst(
              string dbConnString
            , MX02KouteiEntity updEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"UPDATE ");
            sb.AppendLine($@"    MX02Koutei ");
            sb.AppendLine($@"SET ");
            sb.AppendLine($@"    KouteiName = @KouteiName ");
            sb.AppendLine($@",   UpdDate    = @UpdDate ");
            sb.AppendLine($@"WHERE ");
            sb.AppendLine($@"    ProjectCD  = @ProjectCD ");
            sb.AppendLine($@"AND KouteiSeq  = @KouteiSeq ");
            sb.AppendLine($@"; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@ProjectCD", updEntity.ProjectCD));
                    cmd.Parameters.Add(new SqlParameter("@KouteiName", updEntity.KouteiName));
                    cmd.Parameters.Add(new SqlParameter("@KouteiSeq", updEntity.KouteiSeq));
                    cmd.Parameters.Add(new SqlParameter("@UpdDate", updEntity.UpdDate.ToString(Formats._FORMAT_DATE_YYYYMMDDHHMMSSFFF_SLASH)));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 最大の工程Seq取得
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="projectCode"></param>
        /// <returns></returns>
        public int GetMaxKouteiSeq(
              string dbConnString
            , string projectCode
            ) {

            int retSeq = -1;

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ");
            sb.AppendLine("    MAX(MX02.KouteiSeq) AS KouteiSeq ");
            sb.AppendLine("FROM ");
            sb.AppendLine("    MX02Koutei AS MX02 ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    MX02.ProjectCD = @projectCD ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@projectCD", projectCode));
                    using (var dr = cmd.ExecuteReader()) {
                        while (dr.Read()) {
                            retSeq = TypeConversionUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return retSeq;
        }
    }
}
