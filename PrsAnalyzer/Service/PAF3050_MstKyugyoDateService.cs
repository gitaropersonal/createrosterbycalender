﻿using PrsAnalyzer.Entity;
using System.Data.SqlClient;
using System.Text;

namespace PrsAnalyzer.Service {
    class PAF3050_MstKyugyoDateService {
        /// <summary>
        /// 休業日マスタ登録
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        public void InsertMstKyugyoDate(
              string dbConnString
            , MX04KyugyoDateEntity addEntity
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO MX04KyugyoDate( ");
            sb.AppendLine("    KyugyoYMD ");
            sb.AppendLine(",   KyugyoName ");
            sb.AppendLine(")VALUES( ");
            sb.AppendLine("    @KyugyoYMD ");
            sb.AppendLine(",   @KyugyoName ");
            sb.AppendLine("); ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@KyugyoYMD", addEntity.KyugyoYMD));
                    cmd.Parameters.Add(new SqlParameter("@KyugyoName", addEntity.KyugyoName));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 休業日マスタ更新
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        public void UpdMstKyugyoDate(
              string dbConnString
            , MX04KyugyoDateEntity updEntity) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE ");
            sb.AppendLine("    MX04KyugyoDate ");
            sb.AppendLine("SET ");
            sb.AppendLine("    KyugyoName = @KyugyoName ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    KyugyoYMD  = @KyugyoYMD ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@KyugyoYMD", updEntity.KyugyoYMD));
                    cmd.Parameters.Add(new SqlParameter("@KyugyoName", updEntity.KyugyoName));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 休業日マスタ削除
        /// </summary>
        /// <param name="dbConnString"></param>
        /// <param name="kyugyoYMD"></param>
        /// <returns></returns>
        public void DelMstKyugyoDate(
              string dbConnString
            , string kyugyoYMD
            ) {

            // SQL文の作成
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("DELETE FROM ");
            sb.AppendLine("    MX04KyugyoDate ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    KyugyoYMD  = @KyugyoYMD ");
            sb.AppendLine("; ");
            using (var cn = new SqlConnection(dbConnString)) {
                using (var cmd = cn.CreateCommand()) {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new SqlParameter("@KyugyoYMD", kyugyoYMD));
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
