﻿using System.IO;

namespace PrsAnalyzer.Util {
    public static class CommonUtil {
        /// <summary>
        /// 空文字削除
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CutSpace(string str) {
            return str.Replace("　", string.Empty).Replace(" ", string.Empty);
        }
        /// <summary>
        /// 既存ファイル削除
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path) {
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }
        /// <summary>
        /// フォルダ作成
        /// </summary>
        public static void CreateFolder(string path) {
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
            }
        }
        /// <summary>
        /// フォルダ削除
        /// </summary>
        public static void DeleteFolder(string path) {
            if (Directory.Exists(path)) {
                Directory.Delete(path);
            }
        }
        /// <summary>
        /// ファイルを開きっぱなしにしていないかチェック
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static bool ValidateFileOpened(string fullPath) {
            try {
                if (!File.Exists(fullPath)) {
                    return true;
                }
                using (var fs = File.OpenWrite(fullPath)) {
                }
            }
            catch {
                return false;
            }
            return true;
        }
    }
}
