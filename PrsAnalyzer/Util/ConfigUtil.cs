﻿using System;
using System.Configuration;
using System.IO;

namespace PrsAnalyzer.Util {
    public static class ConfigUtil {

        private const string _CONFIG_NAME = "PrsAnalyzer.config";

        /// <summary>
        /// DB接続文字列取得
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public static string LoadConnDbString() {
            var exeFileMap = new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, _CONFIG_NAME) };
            var config = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            string ret = config.ConnectionStrings.ConnectionStrings["DBConnString"].ConnectionString;
            return ret;
        }
        /// <summary>
        /// 勤務表出力先取得
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public static string LoadOutputFolder() {
            var exeFileMap = new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, _CONFIG_NAME) };
            var config = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            string ret = config.AppSettings.Settings["OutputKinmuhyoPath"].Value.ToString();
            return ret;
        }
        /// <summary>
        /// １月当たりの基準時間
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public static float LoadDefaultKijunTimeMonth() {
            var exeFileMap = new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, _CONFIG_NAME) };
            var config = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            var ret = TypeConversionUtil.ToFloat(config.AppSettings.Settings["DefaultKijunTimeMonth"].Value.ToString());
            return ret;
        }
        /// <summary>
        /// １日当たりの終業時間
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public static float LoadDefaultWorkTimeDay() {
            var exeFileMap = new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, _CONFIG_NAME) };
            var config = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            var ret = TypeConversionUtil.ToFloat(config.AppSettings.Settings["DefaultWorkTimeDay"].Value.ToString());
            return ret;
        }
    }
}
