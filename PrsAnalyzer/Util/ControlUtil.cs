﻿using PrsAnalyzer.Const;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrsAnalyzer.Util {
    public static class ControlUtil {
        /// <summary>
        /// フォント
        /// </summary>
        public const string _DEFAULT_FONT_NAME = "Meiryo UI";
        
        /// <summary>
        /// ヘッダー行高さ
        /// </summary>
        public const int _DEFAULT_HEADER_HEIGHT = 21;
        /// <summary>
        /// フォントサイズ
        /// </summary>
        public const float _DEFAULT_FONT_SIZE = 10;
        /// <summary>
        /// コントロール背景色の初期化
        /// </summary>
        /// <param name="_Body"></param>
        public static void SetContorolsColor(Form _Body) {

            // フォーム背景色
            _Body.BackColor = Colors._BACK_COLOR_FROM;
            _Body.StartPosition = FormStartPosition.CenterScreen;

            // 全コントロール取得
            List<Control> controls = GetAllControls<Control>(_Body);
            foreach (var control in controls) {
                if (control.GetType() == typeof(Button)) {
                    // ボタン背景色セット
                    Button button = (Button)control;
                    if (button.Enabled) {
                        button.BackColor = Colors._BACK_COLOR_BTN_OFF_ENTER;
                        button.ForeColor = Color.White;
                    } else {
                        button.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
                        button.ForeColor = Colors._FORE_COLOR_CLICK;
                    }
                    // フォーカス時に背景色を自動編集する処理を追加
                    button.GotFocus -= BtnEvent_GotFocus;
                    button.GotFocus += BtnEvent_GotFocus;

                    // ロストフォーカス時に背景色を自動編集する処理を追加
                    button.Leave -= BtnEvent_Leave;
                    button.Leave += BtnEvent_Leave;

                    // 使用可否変更時に背景色を自動編集する処理を追加
                    button.EnabledChanged -= BtnEvent_Leave;
                    button.EnabledChanged += BtnEvent_Leave;
                    continue;
                }
                if (control.GetType() == typeof(TextBox)) {

                    // テキストボックス背景色セット
                    TextBox txtBox = (TextBox)control;
                    txtBox.BackColor = Color.Empty;
                    txtBox.ForeColor = SystemColors.ControlText;

                    // フォーカスインイベント
                    txtBox.Enter -= TextBoxEvent_Enter;
                    txtBox.Enter += TextBoxEvent_Enter;

                    txtBox.GotFocus -= TextBoxEvent_GotFocus;
                    txtBox.GotFocus += TextBoxEvent_GotFocus;

                    txtBox.Leave -= TextBoxEvent_Leave;
                    txtBox.Leave += TextBoxEvent_Leave;
                    
                    continue;
                }
                if (control.GetType() == typeof(ComboBox)) {
                    // コンボボックス背景色セット
                    ComboBox cmbBox = (ComboBox)control;
                    cmbBox.BackColor = Color.Empty;
                    cmbBox.ForeColor = SystemColors.ControlText;

                    cmbBox.GotFocus -= CmbBoxEvent_GotFocus;
                    cmbBox.GotFocus += CmbBoxEvent_GotFocus;

                    cmbBox.Leave -= CmbBoxEvent_Leave;
                    cmbBox.Leave += CmbBoxEvent_Leave;

                    continue;
                }
                if (control.GetType() == typeof(GroupBox)) {
                    // グループボックス
                    GroupBox grp = (GroupBox)control;
                    grp.BackColor = Colors._BACK_COLOR_GROUP;
                    continue;
                }
            }
        }

        #region ボタンイベント
        /// <summary>
        /// ボタンGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void BtnEvent_GotFocus(object s, EventArgs e) {
            ((Button)s).BackColor = Colors._BACK_COLOR_BTN_ON_ENTER;
            ((Button)s).ForeColor = Colors._FORE_COLOR_CLICK;
        }
        /// <summary>
        /// ボタンLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void BtnEvent_Leave(object s, EventArgs e) {
            Button button = (Button)s;
            if (button.Enabled) {
                button.BackColor = Colors._BACK_COLOR_BTN_OFF_ENTER;
                button.ForeColor = Color.White;
                return;
            }
            button.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
            button.ForeColor = Colors._FORE_COLOR_CLICK;
        }
        #endregion

        #region テキストボックスイベント
        /// <summary>
        /// テキストボックスEnterイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void TextBoxEvent_Enter(object s, EventArgs e) {
            ((TextBox)s).SelectAll();
        }
        /// <summary>
        /// テキストボックスGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void TextBoxEvent_GotFocus(object s, EventArgs e) {
            ((TextBox)s).BackColor = Colors._BACK_COLOR_BTN_ON_ENTER;
            ((TextBox)s).ForeColor = Colors._FORE_COLOR_CLICK;
        }
        /// <summary>
        /// テキストボックスLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void TextBoxEvent_Leave(object s, EventArgs e) {
            TextBox textBox = (TextBox)s;
            if (textBox.Enabled) {
                textBox.BackColor = Color.Empty;
                textBox.ForeColor = Colors._FORE_COLOR_CLICK; ;
                return;
            }
            textBox.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
            textBox.ForeColor = Colors._FORE_COLOR_CLICK;
        }
        #endregion

        #region コンボボックスイベント
        /// <summary>
        /// コンボボックスGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void CmbBoxEvent_GotFocus(object s, EventArgs e) {
            ((ComboBox)s).BackColor = Colors._BACK_COLOR_BTN_ON_ENTER;
            ((ComboBox)s).ForeColor = Colors._FORE_COLOR_CLICK;
        }
        /// <summary>
        /// コンボボックスLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void CmbBoxEvent_Leave(object s, EventArgs e) {
            ComboBox cmbBox = (ComboBox)s;
            if (cmbBox.Enabled) {
                cmbBox.BackColor = Color.Empty;
                cmbBox.ForeColor = Colors._FORE_COLOR_CLICK; ;
                return;
            }
            cmbBox.BackColor = Colors._BACK_COLOR_BTN_READ_ONLY;
            cmbBox.ForeColor = Colors._FORE_COLOR_CLICK;
        }
        #endregion

        /// <summary>
        /// 全コントロール取得
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<T> GetAllControls<T>(Control top) where T : Control {
            List<T> buf = new List<T>();
            foreach (Control ctrl in top.Controls) {
                if (ctrl is T) buf.Add((T)ctrl);
                buf.AddRange(GetAllControls<T>(ctrl));
            }
            return buf;
        }
        /// <summary>
        /// TimeTrackerの背景色にペイント
        /// </summary>
        /// <param name="gyoumuKBN"></param>
        /// <param name="c"></param>
        public static void SetTimeTrackerCellStyle(string gyoumuKBN, DataGridViewCell c) {

            if (Colors._DIC_TIME_TRACKER_COLOR.ContainsKey(gyoumuKBN)) {
                c.Style.BackColor = Colors._DIC_TIME_TRACKER_COLOR.First(n => n.Key == gyoumuKBN).Value;
                if (gyoumuKBN == ProjectConst.GyomuKbn.Y.ToString()) {
                    c.Style.ForeColor = Colors._FORE_COLOR_Y;
                }else{
                    c.Style.ForeColor = Colors._FORE_COLOR_CLICK;
                }
            }
        }
        /// <summary>
        /// グリッドにデフォルトのフォントをセット
        /// </summary>
        /// <param name="dgv"></param>
        public static DataGridView SetDefaultFont(DataGridView dgv) {
            var newFont = new Font(_DEFAULT_FONT_NAME, _DEFAULT_FONT_SIZE, FontStyle.Regular);
            dgv.DefaultCellStyle.Font = newFont;
            dgv.ColumnHeadersDefaultCellStyle.Font = newFont;
            dgv.RowHeadersDefaultCellStyle.Font = newFont;
            dgv.ColumnHeadersHeight = _DEFAULT_HEADER_HEIGHT;
            return dgv;

        }
        /// <summary>
        /// グリッドセルValidate（空文字）
        /// </summary>
        /// <param name="r"></param>
        /// <param name="headerTxt"></param>
        /// <param name="cellName"></param>
        /// <param name="errColNames"></param>
        /// <param name="result"></param>
        public static void ValidateEmpty(DataGridViewRow r, string headerTxt, string cellName, ref List<string> errColNames, ref bool result) {
            if (string.IsNullOrEmpty(StringUtil.TostringNullForbid(r.Cells[cellName].Value))) {
                r.Cells[cellName].Style.BackColor = Color.Red;
                if (!errColNames.Contains(headerTxt)) {
                    errColNames.Add(headerTxt);
                }
                result = false;
            }
        }
        /// <summary>
        /// クリックワンスのバージョン取得
        /// </summary>
        /// <returns></returns>
        public static string GetVersion() {
            if (ApplicationDeployment.IsNetworkDeployed) {
                var version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                return $"{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
            }
            return "1.0.0.XXX";
        }
    }
}
