﻿using PrsAnalyzer.Const;
using System;
using System.Windows.Forms;

namespace PrsAnalyzer.Util {
    public static class DialogUtil {
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="msgs"></param>
        public static void ShowErroMsg(Exception ex) {
            MessageBox.Show(ex.Message
                          , Messages._DIALOG_TITLE_ERROR
                          , MessageBoxButtons.OK
                          , MessageBoxIcon.Error);
            LogUtil.ShowLogErr(ex);
        }
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowErroMsg(string msg) {
            MessageBox.Show(msg
                          , Messages._DIALOG_TITLE_ERROR
                          , MessageBoxButtons.OK
                          , MessageBoxIcon.Error);
        }
        /// <summary>
        /// 警告メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoExc(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , Messages._DIALOG_TITLE_EXCRAMATION
                                                , MessageBoxButtons.OKCancel
                                                , MessageBoxIcon.Exclamation
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoMsgOKCancel(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , Messages._DIALOG_TITLE_CNFIRM
                                                , MessageBoxButtons.OKCancel
                                                , MessageBoxIcon.Information
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfolMsgOK(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , Messages._DIALOG_TITLE_CNFIRM
                                                , MessageBoxButtons.OK
                                                , MessageBoxIcon.Information
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
    }
}
