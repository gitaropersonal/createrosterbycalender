﻿using PrsAnalyzer.Const;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PrsAnalyzer.Util {
    public static class GridRowUtil<T> where T : new() {
        /// <summary>
        /// 行モデル取得
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static T GetRowModel(DataGridViewRow r) {
            // TypeDescriptorを使用してプロパティ一覧を取得する
            var attributes = new Attribute[] { };
            T ret = new T();
            var pdc = TypeDescriptor.GetProperties(ret, attributes);

            // プロパティ一覧をリフレクションから取得
            var type = ret.GetType();
            foreach (var propertyInfo in type.GetProperties()) {
                propertyInfo.SetValue(ret, r.Cells[propertyInfo.Name].Value);
            }
            return ret;
        }
        /// <summary>
        /// 行番号の描画
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="FontSize"></param>
        public static void SetRowNum(object s, DataGridViewRowPostPaintEventArgs e, float FontSize = 10) {

            // フォントサイズセット
            var newFont = new Font(e.InheritedRowStyle.Font.FontFamily, FontSize,  FontStyle.Regular);

            var dgv = (DataGridView)s;
            if (dgv.RowHeadersVisible) {
                //行番号を描画する範囲を決定する
                var rect = new Rectangle(
                    e.RowBounds.Left, e.RowBounds.Top,
                    dgv.RowHeadersWidth, e.RowBounds.Height);
                rect.Inflate(-2, -2);
                //行番号を描画する
                TextRenderer.DrawText(e.Graphics,
                    (e.RowIndex + 1).ToString(),
                    newFont,
                    rect,
                    e.InheritedRowStyle.ForeColor,
                    TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
            }
        }
        /// <summary>
        /// 行番号に時刻を描画
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="StartTime"></param>
        /// <param name="delTimeH"></param>
        /// <param name="FontSize"></param>
        public static void SetRowTime(object s, DataGridViewRowPostPaintEventArgs e, DateTime StartTime, int delTimeH, float FontSize = 10) {

            if (e.RowIndex % 2 == 1) {
                return;
            }
            // フォントサイズセット
            var newFont = new Font(e.InheritedRowStyle.Font.FontFamily, FontSize,  FontStyle.Regular);

            var dgv = (DataGridView)s;
            //行番号を描画する範囲を決定する
            var rect = new Rectangle(
                e.RowBounds.Left
              , e.RowBounds.Top + (int)(e.RowBounds.Height / 6)
              , dgv.RowHeadersWidth
              , e.RowBounds.Height
            );
            rect.Inflate(-2, -2);
            //行番号を描画する
            TextRenderer.DrawText(e.Graphics,
                (StartTime.AddMinutes(delTimeH * (e.RowIndex))).ToString(Formats._FORMAT_TIME_HMM),
                newFont,
                rect,
                e.InheritedRowStyle.ForeColor,
                TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
        }
    }
}
