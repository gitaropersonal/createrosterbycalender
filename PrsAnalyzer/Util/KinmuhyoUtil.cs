﻿using PrsAnalyzer.Const;
using PrsAnalyzer.Const.Kinmuhyo;
using PrsAnalyzer.Dto;
using PrsAnalyzer.Entity;
using System;
using System.IO;

namespace PrsAnalyzer.Util {
    public static class KinmuhyoUtil {
        private static TimeSpan _TS_15 = TimeSpan.FromMinutes(15);
        private static TimeSpan _TS_30 = TimeSpan.FromMinutes(30);

        /// <summary>
        /// 勤務表ファイル名取得
        /// </summary>
        /// <param name="user"></param>
        /// <param name="Dt"></param>
        /// <returns></returns>
        public static string GetKinmuhyoName(string user, DateTime Dt) {
            return string.Format("{0}年{1}月勤務実績表_{2}.xlsx", Dt.Year, Dt.Month.ToString(Formats._FORMAT_PRE_ZERO_2), CommonUtil.CutSpace(user));
        }
        /// <summary>
        /// PRSファイル名取得
        /// </summary>
        /// <param name="user"></param>
        /// <param name="Dt"></param>
        /// <returns></returns>
        public static string GetPrsName(string user, DateTime Dt) {
            return string.Format("PRS（{0}年{1}月）_{2}.xlsx", Dt.Year, Dt.Month.ToString(Formats._FORMAT_PRE_ZERO_2), CommonUtil.CutSpace(user));
        }
        /// <summary>
        /// CSV解析データからWorkTimeDto取得
        /// </summary>
        /// <param name="csvData"></param>
        /// <returns></returns>
        public static WorkTimeDto GetWorkTimeDtoFromCsvData(EditTimeEntryDto csvData) {

            // 対象日
            var tgtDate = csvData.WorkStartTime;
            var tgtDateZero = TypeConversionUtil.ToDateTime(tgtDate, Formats._FORMAT_DATE_YYYYMMDD_SLASH);

            // 開始時刻
            int startTimeValH = (int)((csvData.WorkStartTime - tgtDateZero).TotalMinutes / TimeConst._MINUTE_AN_HOUR);
            int startTimeValM = (int)((csvData.WorkStartTime - tgtDateZero).TotalMinutes % TimeConst._MINUTE_AN_HOUR);

            // 終了時刻
            int endTimeValH = (int)((csvData.WorkEndTime - tgtDateZero).TotalMinutes / TimeConst._MINUTE_AN_HOUR);
            int endTimeValM = (int)((csvData.WorkEndTime - tgtDateZero).TotalMinutes % TimeConst._MINUTE_AN_HOUR);

            // 休憩時間
            int restTimeValH = (int)(csvData.RestTime / TimeConst._MINUTE_AN_HOUR);
            int restTimeValM = (int)(csvData.RestTime % TimeConst._MINUTE_AN_HOUR);

            // 残業時間
            int zangyoTimeValH = (int)(csvData.ZangyoTime / TimeConst._MINUTE_AN_HOUR);
            int zangyoTimeValM = (int)(csvData.ZangyoTime % TimeConst._MINUTE_AN_HOUR);

            // 深夜時間
            int shinyaTimeValH = (int)(csvData.ShinyaTime / TimeConst._MINUTE_AN_HOUR);
            int shinyaTimeValM = (int)(csvData.ShinyaTime % TimeConst._MINUTE_AN_HOUR);

            // Dto作成
            var dto = new WorkTimeDto() {
                Day = tgtDate.Day,
                WeekDay = ConvertWBWeekDay(tgtDate, tgtDate.Day),
                StartHH = startTimeValH.ToString(),
                StartMM = startTimeValM.ToString(),
                EndHH = GetHHorMM(endTimeValH, endTimeValH, endTimeValH),
                EndMM = GetHHorMM(endTimeValH, endTimeValH, endTimeValM),
                RestHH = GetHHorMM(restTimeValH, restTimeValM, restTimeValH),
                RestMM = GetHHorMM(restTimeValH, restTimeValM, restTimeValM),
                ZangyoHH = GetHHorMMByTime(csvData.ZangyoTime, zangyoTimeValH),
                ZangyoMM = GetHHorMMByTime(csvData.ZangyoTime, zangyoTimeValM),
                ShinyaHH = GetHHorMMByTime(csvData.ShinyaTime, shinyaTimeValH),
                ShinyaMM = GetHHorMMByTime(csvData.ShinyaTime, shinyaTimeValM),
                RestMark = csvData.RestMark,
                RestType = csvData.RestType,
            };
            return dto;
        }
        /// <summary>
        /// 時刻または時間を文字列型に変換
        /// </summary>
        /// <param name="HH"></param>
        /// <param name="MM"></param>
        /// <param name="HHorMM"></param>
        /// <returns></returns>
        private static string GetHHorMM(int HH, int MM, int HHorMM) {
            return (HH == 0 && MM == 0) ? string.Empty : HHorMM.ToString();
        }
        /// <summary>
        /// 時刻または時間を文字列型に変換
        /// </summary>
        /// <param name="time"></param>
        /// <param name="HHorMM"></param>
        /// <returns></returns>
        private static string GetHHorMMByTime(decimal time, int HHorMM) {
            return (time == 0) ? string.Empty : HHorMM.ToString();
        }
        /// <summary>
        /// 勤怠時刻丸め（Proxy）
        /// </summary>
        /// <param name="tgtDt"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static WorkTimeDto RoundWorkTimeProxy( DateTime tgtDt, WorkTimeDto dto) {
            // 有休
            var ret = new WorkTimeDto() {
                RestHH = dto.RestHH,
                RestMM = dto.RestMM,
            };
            if (dto.RestMark == CellsKinmuhyo._MARK_REST) {
                ret.RestMark = CellsKinmuhyo._MARK_PAID;
                ret.RestType = TimeConst.RestType.PAID;
                ret.Day = dto.Day;
                ret.WeekDay = dto.WeekDay;
                return ret;
            }
            // 開始時刻
            bool IsNullStart = string.IsNullOrEmpty(dto.StartHH) || string.IsNullOrEmpty(dto.StartMM);
            if (!IsNullStart) {
                var startDt = new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, int.Parse(dto.StartHH), int.Parse(dto.StartMM), 0);
                ret.StartHH = startDt.Hour.ToString();
                ret.StartMM = startDt.Minute.ToString();
            }
            // 終了時刻
            bool IsNullEnd = string.IsNullOrEmpty(dto.EndHH) || string.IsNullOrEmpty(dto.EndMM);
            if (!IsNullEnd) {
                bool IsDaySpan = (TimeConst._HOURS_A_DAY <= int.Parse(dto.EndHH));
                var endDt = GetWorkEndTime(tgtDt, dto, IsDaySpan);
                ret.EndHH = IsDaySpan ? (endDt.Hour + TimeConst._HOURS_A_DAY).ToString() : endDt.Hour.ToString();
                ret.EndMM = endDt.Minute.ToString();
            }
            ret.Day = dto.Day;
            ret.WeekDay = dto.WeekDay;

            // 出勤・退勤のどちらか一つでも未入力の場合は休暇判定しない
            if (IsNullStart || IsNullEnd) {
                return ret;
            }
            return EditRestType(ret);
        }
        /// <summary>
        /// 休日種別取得
        /// </summary>
        /// <param name="ret"></param>
        /// <returns></returns>
        public static WorkTimeDto EditRestType(WorkTimeDto ret) {
            if (ret.RestType != TimeConst.RestType.NONE) {
                return ret;
            }
            decimal workStartTime = (int.Parse(ret.StartHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.StartMM);
            decimal workEndTime = (int.Parse(ret.EndHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.EndMM);
            if (workEndTime < workStartTime) {
                ret.RestType = TimeConst.RestType.NONE;
                return ret;
            }
            return ret;
        }
        /// <summary>
        /// 終業時刻取得
        /// </summary>
        /// <param name="tgtDt"></param>
        /// <param name="dto"></param>
        /// <param name="isDaySpan"></param>
        /// <returns></returns>
        private static DateTime GetWorkEndTime(DateTime tgtDt, WorkTimeDto dto, bool isDaySpan) {
            int intEndHH = int.Parse(dto.EndHH);
            int intEndMM = int.Parse(dto.EndMM);
            if (isDaySpan) {
                intEndHH -= TimeConst._HOURS_A_DAY;
            }
            return new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, intEndHH, intEndMM, 0);
        }
        /// <summary>
        /// WBの日付から曜日に変換
        /// </summary>
        /// <param name="sumDt"></param>
        /// <param name="tgtDay"></param>
        /// <returns></returns>
        public static string ConvertWBWeekDay(DateTime sumDt, int tgtDay)
        {
            var tgtWeekDay = new DateTime(sumDt.Year, sumDt.Month, tgtDay).DayOfWeek;
            switch (tgtWeekDay)
            {
                case DayOfWeek.Sunday:
                    return "日";
                case DayOfWeek.Monday:
                    return "月";
                case DayOfWeek.Tuesday:
                    return "火";
                case DayOfWeek.Wednesday:
                    return "水";
                case DayOfWeek.Thursday:
                    return "木";
                case DayOfWeek.Friday:
                    return "金";
                case DayOfWeek.Saturday:
                    return "土";
            }
            return string.Empty;
        }
        /// <summary>
        /// Excelのカラム名的なアルファベット文字列へ変換
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string ToAlphabet(this int self) {
            if (self <= 0) {
                return string.Empty;
            }
            int n = self % 26;
            n = (n == 0) ? 26 : n;
            string s = ((char)(n + 64)).ToString();
            if (self == n) {
                return s;
            }
            return ((self - n) / 26).ToAlphabet() + s;
        }
        /// <summary>
        /// 勤務表フルパス取得
        /// </summary>
        /// <param name="tgtEmp"></param>
        /// <param name="dtStart"></param>
        /// <param name="outputKinmuhyoPath"></param>
        /// <returns></returns>
        public static string GetFullPathKinmuhyo(string outputKinmuhyoPath, MX03TantoEntity tgtEmp, DateTime dtStart, Enums.CreateKinmuhyoType type) {
            string strTgtDateYYYYMM = dtStart.ToString(Formats._FORMAT_DATE_YYYYMM_NENTSUKI);
            string kinmuhyoPath = Path.Combine(outputKinmuhyoPath, strTgtDateYYYYMM);
            string kinmuhyouName = string.Empty;
            switch (type) {
                case Enums.CreateKinmuhyoType.PRS:
                    kinmuhyouName = GetPrsName(tgtEmp.TantoName, dtStart);
                    break;
                case Enums.CreateKinmuhyoType.KINMUHYO:
                    kinmuhyouName = GetKinmuhyoName(tgtEmp.TantoName, dtStart);
                    break;
            }
            string kinmuhyoFullPath = Path.Combine(kinmuhyoPath, kinmuhyouName);
            return kinmuhyoFullPath;
        }
        /// <summary>
        /// 同じ年月日か判定
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static bool JudgeIsSameYMD(DateTime date1, DateTime date2) {
            return (date1.Year == date2.Year && date1.Month == date2.Month && date1.Day == date2.Day);
        }
        /// <summary>
        /// 文字列（YYYYMMDD）を日付に変換
        /// </summary>
        /// <param name="YYYYMMDD"></param>
        /// <returns></returns>
        public static DateTime ConvertYYYYMMDDToDt(string YYYYMMDD)
        {
            if (YYYYMMDD.Length != 12)
            {
                return new DateTime();
            }
            return new DateTime(int.Parse(YYYYMMDD.Substring(0, 4))
                              , int.Parse(YYYYMMDD.Substring(4, 2))
                              , int.Parse(YYYYMMDD.Substring(6, 2)));
        }
        /// <summary>
        /// 文字列（YYYYMMDDHHmm）を日付に変換
        /// </summary>
        /// <param name="YYYYMMDDHHmm"></param>
        /// <returns></returns>
        public static DateTime ConvertYYYYMMDDHHmmToDt(string YYYYMMDDHHmm)
        {
            if (YYYYMMDDHHmm.Length != 12)
            {
                return new DateTime();
            }
            return new DateTime(int.Parse(YYYYMMDDHHmm.Substring(0, 4))
                              , int.Parse(YYYYMMDDHHmm.Substring(4, 2))
                              , int.Parse(YYYYMMDDHHmm.Substring(6, 2))
                              , int.Parse(YYYYMMDDHHmm.Substring(8, 2))
                              , int.Parse(YYYYMMDDHHmm.Substring(10, 2))
                              , 0);
        }
        /// <summary>
        /// 日付返還（YYYY年MM月→DateTime）
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime ConvertYYYYMMNenTsukiToDate(string str)
        {
            if (str.Length != 8)
            {
                return DateTime.Now;
            }
            int intYYYY = int.Parse(str.Substring(0, 4));
            int intMM = int.Parse(str.Substring(5, 2));
            return new DateTime(intYYYY, intMM, 1);
        }
    }
}
