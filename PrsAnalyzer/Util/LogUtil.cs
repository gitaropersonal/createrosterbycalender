﻿using log4net;
using System;

namespace PrsAnalyzer.Util {
    public static class LogUtil {
        private static readonly ILog LOG = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string _ERR_TITLE                    = "エラー内容           ：{0}";
        private const string _FROMAT_CONTENT_HOST          = "ホスト名             ：{0}";
        private const string _FROMAT_CONTENT_IP            = "IPアドレス           ：{0}";
        private const string _MSG_STRAT_END_LINE           = "===========================================";

        /// <summary>
        /// 開始終了線のタイプ
        /// </summary>
        public enum StartEndLineType {
            None = 0,
            Start = 1,
            End = 2
        }
        /// <summary>
        /// ログにメッセージ表示（Info）
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="lineType"></param>
        public static void ShowLogInfo(string msg, StartEndLineType lineType = StartEndLineType.None) {

            // 開始線
            if (lineType == StartEndLineType.Start) {
                LOG.Info(_MSG_STRAT_END_LINE);
            }
            // ログ出力
            LOG.Info(msg);

            // 終了線
            if (lineType == StartEndLineType.End) {
                LOG.Info(_MSG_STRAT_END_LINE);
            }
        }
        /// <summary>
        /// ログにメッセージ表示（Error）
        /// </summary>
        /// <param name="ex"></param>
        public static void ShowLogErr(Exception ex) {
            string eMessage = $"{Environment.NewLine}{ex.Message}{Environment.NewLine}{ex.StackTrace}";
            string host = $"{Environment.NewLine}{string.Format(_FROMAT_CONTENT_HOST, Environment.MachineName)}";
            string ip = $"{Environment.NewLine}{string.Format(_FROMAT_CONTENT_IP, NetworkUtil.GetIpAddress())}";
            string err = Environment.NewLine + string.Format(_ERR_TITLE, eMessage);
            LOG.Error($"{eMessage}{host}{ip}{err}");
        }
    }
}
