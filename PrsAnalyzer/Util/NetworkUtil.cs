﻿using System;
using System.Net;

namespace PrsAnalyzer.Util {
    public static class NetworkUtil {
        /// <summary> IPアドレスを取得する</summary>
        static public string GetIpAddress() {
            IPAddress[] adrList = Dns.GetHostAddresses(Environment.MachineName);
            foreach (IPAddress address in adrList) {
                // IPv4 を返却する
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                    return address.ToString();
                }
            }
            return null;
        }
    }
}
