﻿using PrsAnalyzer.Const;
using System.Linq;

namespace PrsAnalyzer.Util {
    public static class ProjectUtil {
        /// <summary>
        /// プロジェクトコードからプロジェクト区分を取得
        /// </summary>
        /// <param name="projectCode"></param>
        /// <returns></returns>
        public static int GetPBushoKbn(string projectCode) {
            if (string.IsNullOrEmpty(projectCode) || projectCode.Length < 1) {
                return (int)ProjectConst.PBushoKbn.NONE;
            }
            string gyomuKbn = projectCode.Substring(0, 1);
            if (!ProjectConst._DIC_GYOMU_KBN.ToList().Exists(n => n.Key == projectCode.Substring(0, 1))) {
                return (int)ProjectConst.PBushoKbn.NONE;
            }
            return (int)ProjectConst._DIC_GYOMU_KBN.ToList().First(n => n.Key == gyomuKbn).Value;
        }
        /// <summary>
        /// 顧客名取得の条件分作成
        /// </summary>
        /// <param name="kbn"></param>
        /// <param name="kokyakuName"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static string GetConkyakuNameCondition(ProjectConst.KokyakumeiKbn kbn, string kokyakuName, string tableName) {

            switch (kbn) {
                case ProjectConst.KokyakumeiKbn.NONE:
                    return string.Empty;
                case ProjectConst.KokyakumeiKbn.SYANAI:
                    return $@"AND {tableName}.KokyakuName = '{ProjectConst._KOKYAKU_NAME_SYANAI}'";
                case ProjectConst.KokyakumeiKbn.FUTOKUTEI:
                    return $@"AND {tableName}.KokyakuName = '{ProjectConst._KOKYAKU_NAME_FUTOKUTEI}'";
                case ProjectConst.KokyakumeiKbn.IRAIMOTO:
                    return $@"AND ({tableName}.KokyakuName LIKE @kokyakuName AND {tableName}.KokyakuName NOT IN('{ProjectConst._KOKYAKU_NAME_SYANAI}', '{ProjectConst._KOKYAKU_NAME_FUTOKUTEI}'))";
            }
            return string.Empty;
        }
        /// <summary>
        /// 顧客区分取得
        /// </summary>
        /// <param name="iraimotoChecked"></param>
        /// <param name="shanaiChecked"></param>
        /// <param name="futokuteiChecked"></param>
        /// <returns></returns>
        public static ProjectConst.KokyakumeiKbn GetKokyakuKbn(bool iraimotoChecked, bool shanaiChecked, bool futokuteiChecked){
            ProjectConst.KokyakumeiKbn kokyakuKbn = ProjectConst.KokyakumeiKbn.NONE;
            if (iraimotoChecked) {
                kokyakuKbn = ProjectConst.KokyakumeiKbn.IRAIMOTO;
            }
            if (shanaiChecked) {
                kokyakuKbn = ProjectConst.KokyakumeiKbn.SYANAI;
            }
            if (futokuteiChecked) {
                kokyakuKbn = ProjectConst.KokyakumeiKbn.FUTOKUTEI;
            }
            return kokyakuKbn;
        }
        /// <summary>
        /// 検索用客先名取得
        /// </summary>
        /// <returns></returns>
        public static string GetKokyakuNameForSearch(bool iraimotoChecked, bool shanaiChecked, bool futokuteiChecked, string kokyakumei) {
            if (iraimotoChecked) {
                return kokyakumei;
            }
            if (shanaiChecked) {
                return ProjectConst._KOKYAKU_NAME_SYANAI;
            }
            if (futokuteiChecked) {
                return ProjectConst._KOKYAKU_NAME_FUTOKUTEI;
            }
            return string.Empty;
        }
    }
}
