﻿using PrsAnalyzer.Const;
using System.Text.RegularExpressions;

namespace PrsAnalyzer.Util {
    public static class StringUtil {
        /// <summary>
        /// ToString（null非許容）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStringForbidNull(object obj) {
            if (obj == null) {
                return string.Empty;
            }
            return obj.ToString();
        }
        /// <summary>
        /// Substring（右から）
        /// </summary>
        /// <param name="target"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string SubstringRight(string target, int length) {
            return target.Substring(target.Length - length, length);
        }
        /// <summary>
        /// 文字列に含まれる半角記号を除外
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CutHalfMarkChars(string text) {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++) {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str) {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$")) {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列に含まれる半角記号を除外（除外の例外を引数で設定）
        /// </summary>
        /// <param name="text"></param>
        /// <param name="exStr"></param>
        /// <returns></returns>
        public static string CutHalfMarkCharsEx(string text, string exStr = "") {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++) {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str) {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$") && item != exStr) {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列→数値→文字列変換
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string StrToIntToString(string str) {
            if (!int.TryParse(str, out int intVal)) {
                return string.Empty;
            }
            return intVal.ToString();
        }
        /// <summary>
        /// 文字列型の数値に前ゼロをつける（2桁）
        /// </summary>
        /// <param name="strInt"></param>
        /// <returns></returns>
        public static string StrIntToStrPreZero2(string strInt) {
            if (string.IsNullOrEmpty(strInt)) {
                return decimal.Zero.ToString(Formats._FORMAT_PRE_ZERO_2);
            }
            var ret = int.Parse(strInt).ToString(Formats._FORMAT_PRE_ZERO_2);
            return ret;
        }
        /// <summary>
        /// 文字列から数字のみ抜き出す
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SelNumericFromStr(string str) {
            return Regex.Replace(str, @"[^0-9]", "");
        }
        /// <summary>
        /// Tostring（null非許容）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string TostringNullForbid(object o) {
            if (o == null) {
                return string.Empty;
            }
            return o.ToString();
        }
        /// <summary>
        /// Nullチェック付きSubstring
        /// </summary>
        /// <param name="o"></param>
        /// <param name="startIdx"></param>
        /// <param name="endIdx"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static string SubstringCheckNull(object o, int startIdx, int endIdx, string defaultVal = "") {
            if (o == null) {
                return defaultVal;
            }
            if (string.IsNullOrEmpty(o.ToString())) {
                return defaultVal;
            }
            return o.ToString().Substring(startIdx, endIdx);
        }
        /// <summary>
        /// CSV出力文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveCsvIllegal(object val) {
            if (val == null) {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("\"", string.Empty).Replace(",", string.Empty);
        }
        /// <summary>
        /// CSV出力文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveUnderBar(object val) {
            if (val == null) {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("_", string.Empty);
        }
        /// <summary>
        /// DB登録文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveSingleQuote(object val) {
            if (val == null) {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("'", string.Empty);
        }
        /// <summary>
        /// 文字列型フラグをbool型に変換
        /// </summary>
        /// <param name="blFlg"></param>
        /// <returns></returns>
        public static string ConvertFlgToStrFlg(bool blFlg) {
            return blFlg ? "1" : "0";
        }
        /// <summary>
        /// 条件に応じてコメントアウト取得
        /// </summary>
        /// <param name="Joken"></param>
        /// <returns></returns>
        public static string GetCmout(bool Joken) {
            return Joken ? "--" : string.Empty;
        }
    }
}
