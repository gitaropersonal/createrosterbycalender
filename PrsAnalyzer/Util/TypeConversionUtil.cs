﻿using PrsAnalyzer.Const;
using System;

namespace PrsAnalyzer.Util {
    public static class TypeConversionUtil {
        /// <summary>
        /// Int型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static int ToInteger(object o, int format = 0) {
            if (o == null) {
                return format;
            }
            if (int.TryParse(o.ToString(), out int i)){
                return i;
            }
            return format;
        }
        /// <summary>
        /// Float型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static float ToFloat(object o, float format = 0) {
            if (o == null) {
                return format;
            }
            if (float.TryParse(o.ToString(), out float i)) {
                return i;
            }
            return format;
        }
        /// <summary>
        /// DateTime型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(object o, string format = "yyyy/MM/dd HH:mm:ss") {
            if (o == null) {
                return DateTime.MinValue;
            }
            if (DateTime.TryParse(o.ToString(), out DateTime dt)) {
                return DateTime.Parse(DateTime.Parse(o.ToString()).ToString(format));
            }
            return DateTime.Parse(ProjectConst._MIN_PROJECT_SPAN + "/01");
        }
        /// <summary>
        /// DateTime型変換
        /// </summary>
        /// <param name="strYYYY"></param>
        /// <param name="intMonth"></param>
        /// <returns></returns>
        public static DateTime StrToDateTime(string strYYYY, int intMonth) {
            return DateTime.Parse(ToInteger(strYYYY) + string.Format($@"/{intMonth}/1"));
        }
        /// <summary>
        /// 入力文字列を年月（YYYY/MM）に変換
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertInputToStrYM(string input) {
            if (input.Length < 6) {
                return string.Empty;
            }
            if (input.Contains("/")) {
                input = input.Replace("/", string.Empty);
            }
            if (!int.TryParse(input, out int intText)) {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(input) || input.Length != 6) {
                return string.Empty;
            }
            string strDt = string.Join("/", input.Substring(0, 4), input.Substring(4, 2), "1");
            DateTime retDt;
            if (!DateTime.TryParse(strDt, out retDt)) {
                return string.Empty;
            }
            return retDt.ToString(Formats._FORMAT_DATE_YYYYMM_SLASH);
        }
        /// <summary>
        /// 入力文字列を年月日（YYYY/MM/DD）に変換
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertInputToStrYMD(string input) {
            input = input.Replace("/", string.Empty);
            if (string.IsNullOrEmpty(input) || input.Length != 8) {
                return string.Empty;
            }
            string strDt = $"{input.Substring(0, 4)}/{input.Substring(4, 2)}/{input.Substring(6, 2)}";
            DateTime retDt;
            if (!DateTime.TryParse(strDt, out retDt)) {
                return string.Empty;
            }
            return retDt.ToString(Formats._FORMAT_DATE_YYYYMMDD_SLASH);
        }
    }
}
